(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



Require Import FunctionalExtensionality ProofIrrelevance.

From mathcomp
Require Import ssreflect ssrbool bigop fintype ssralg
               vector ssrint ssrnum matrix.

Require Import lia_tactics
               utils nat_ord_complements int_complements
               sum_complements mx_complements
               signal.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Import GRing.Theory.


Local Open Scope R.



(** Digital filters, Linear Time-Invariant filters,
    decomposition of a MIMO filter into SISO filters,
    impulse response
**)




Section VTinVTout.


Context {RT : comRingType} {VTin VTout : vectType RT}.


Definition filter := @signal RT VTin -> @signal RT VTout.


Lemma filter_ext (h1 h2 : filter) :
  (forall u k, h1 u k = h2 u k) -> h1 = h2.
Proof.
intro; apply functional_extensionality; intro.
by apply/signalP.
Qed.




(** Definition of Linear Time-Invariant filters and a few of their properties **)
Section LTI_filter.


Variable H : filter.


Definition filter_compat_add :=
  forall u1 u2 : signal, H (u1 + u2) = H u1 + H u2.


Definition filter_compat_scale :=
  forall (c : RT) (u : signal), H (c *: u) = c *: H u.


Definition filter_compat_delay := forall (K : int) (u : signal),
  K >= 0 -> H (delay K u) = delay K (H u).


Definition LTI_filter :=
     filter_compat_add
  /\ filter_compat_scale
  /\ filter_compat_delay.







(** Properties of LTI filters **)



Hypothesis HLTI : LTI_filter.



(** These three facts' only purpose is to not need to remember
    the order of the terms in the definition of LTI_filter **)

Fact LTI_compat_add : filter_compat_add.
Proof. by have [Hadd [Hscale Hdelay]] := HLTI. Qed.

Fact LTI_compat_scale : filter_compat_scale.
Proof. by have [Hadd [Hscale Hdelay]] := HLTI. Qed.

Fact LTI_compat_delay : filter_compat_delay.
Proof. by have [Hadd [Hscale Hdelay]] := HLTI. Qed.



(** We often rather work with images of signals. **)

Fact LTI_compat_add_applied (u1 u2 : signal) (k : int) :
  H (u1 + u2) k = H u1 k + H u2 k.
Proof. by rewrite (proj1 HLTI). Qed.

Fact LTI_compat_scale_applied (c : RT) (u : signal) (k : int) :
  H (scales c u) k = c *: H u k.
Proof. by rewrite (proj1 (proj2 HLTI)). Qed.



(** Other properties of LTI filters **)


Lemma LTI_signal0 : H signal0 = signal0.
Proof.
apply/signalP => k /=.
rewrite -[signal0](scale0r signal0).
by rewrite LTI_compat_scale scale0r.
Qed.


Lemma LTI_compat_sum {n : nat} (us : 'I_n -> signal) :
  H (\sum_(i < n) us i) = \sum_(i < n) H (us i).
Proof.
move: n us. apply: nat_ind.
- move => us. by rewrite !big_ord0 LTI_signal0.
- move => n Hind us. rewrite !big_ord_recr /=.
  by rewrite LTI_compat_add // Hind.
Qed.


Definition causal_filter :=
  forall (u : signal) (start : int),
  (forall k, k < start -> u k = 0) ->
    forall k, k < start -> H u k = 0.


Theorem causal_LTI_filter : causal_filter.
Proof.
move => u start u_start k _Hk.
case: (Num.Theory.ltrP start 0) => Hcase.
  apply: signal_causal.
  by intlia.
rewrite -[u](delay_antidelay _ u_start) //.
rewrite LTI_compat_delay //.
rewrite delayE //.
apply: signal_causal.
by intlia.
Qed.


(** about the same as LTI_filter_is_causal, but easier to use **)
Lemma LTI_preserves_initial_zeroes (u : signal) k :
  (forall m : int, 0 <= m <= k -> u m = 0) -> (H u) k = 0.
Proof.
move => u_lek; apply: causal_LTI_filter => [m Hm|]; last first.
  have ltS : k < k + 1 by intlia.
  exact: ltS.
have[Hcase|Hcase]: (m < 0 \/ 0 <= m) by intlia.
- exact: signal_causal.
- apply: u_lek.
  by autoz.
Qed.


Theorem LTI_ext_before (u1 u2 : signal) (k : int) :
  (forall m : int, 0 <= m <= k -> u1 m = u2 m) -> (H u1) k = (H u2) k.
Proof.
move => Heq_before.
apply: subr0_eq.
rewrite -scaleN1r.
rewrite -LTI_compat_scale_applied.
rewrite -LTI_compat_add_applied.
apply: LTI_preserves_initial_zeroes => m Hm /=.
rewrite (Heq_before m Hm) scaleN1r.
exact: subrr.
Qed.



End LTI_filter.


End VTinVTout.






Section SISO_MIMO.


Context {RT : comRingType}.


Definition SISO_filter := @filter RT (regular_vectType RT) (regular_vectType RT).


Definition MIMO_filter (nu ny : nat) :=
  @filter RT (matrix_vectType RT nu 1) (matrix_vectType RT ny 1).


Definition to_SISO (H : MIMO_filter 1 1) : SISO_filter :=
  fun (u : scsignal) => scsignal_of_v (H (vsignal_of_sc u)).



Section Decompose.


Context {nu ny : nat}.


Variable H : MIMO_filter nu ny.
Variable i : 'I_ny.
Variable j : 'I_nu.


Definition to_SISO_yi_uj : SISO_filter :=
  fun (u : scsignal) => ith_scsignal (H (vsignal_of_sc_at_i u j)) i.


Lemma LTI_to_SISO_yi_uj : LTI_filter H -> LTI_filter to_SISO_yi_uj.
Proof.
repeat split.
{ move => u1 u2. apply/signalP => k.
  rewrite /to_SISO_yi_uj /=.
  rewrite vsignal_of_sc_at_i_compat_add LTI_compat_add //.
  by rewrite mxE. }
{ move => c u. apply/signalP => k.
  rewrite /to_SISO_yi_uj /=.
  rewrite vsignal_of_sc_at_i_compat_scale LTI_compat_scale //.
  by rewrite mxE. }
{ move => K u K_ge0. apply/signalP => k.
  rewrite delayE // /to_SISO_yi_uj.
  rewrite vsignal_of_sc_at_i_compat_delay // LTI_compat_delay //.
  by rewrite signalE delayE. }
Qed.


End Decompose.




Section ImpulseResponse.



(** Impulse response of SISO filter **)


Definition impulse_response (H : SISO_filter) : scsignal := H (@dirac RT).


Theorem convolution_impulse_response (H : SISO_filter) :
  LTI_filter H -> forall u : @scsignal RT,
  H u = convols (impulse_response H) u.
Proof.
move => HLTI u; apply/signalP_ge0 => k _Hk.
rewrite convolsC /=.
rewrite (LTI_ext_before _ (@signal_sum_dirac _ u k)) //.
rewrite LTI_compat_sum // sumsE big_mkord.
apply: eq_big_ord => i.
rewrite LTI_compat_scale_applied // scaler_mulr.
f_equal.
by rewrite LTI_compat_delay.
Qed.



(** Impulse responses of parallel and cascade decompositions
    will not be used in this formalization: they are only
    given are sanity checks **)

Remark impulse_response_parallel (H1 H2 : SISO_filter) :
  impulse_response (H1 \+ H2) = impulse_response H1 + impulse_response H2.
Proof.
by rewrite /impulse_response.
Qed.

Remark impulse_response_cascade (H1 H2 : SISO_filter) :
  LTI_filter H2 ->
  impulse_response (Basics.compose H2 H1) =
  convols (impulse_response H2) (impulse_response H1).
Proof.
move=> *.
rewrite /impulse_response /Basics.compose.
exact: convolution_impulse_response.
Qed.




(** Impulse response of MIMO filter **)


Context { nu ny : nat }.


Variable H : MIMO_filter nu ny.


Lemma MIMO_impulse_response_causal : causal (fun k : int =>
  \matrix_(i, j) (impulse_response (to_SISO_yi_uj H i j) k)
    : 'M[RT]_(ny, nu)).
Proof.
move => k _Hk. apply/matrixP => i j. by rewrite !mxE signal_causal.
Qed.


(** The impulse response of a MIMO filter is a matricial "signal", where the signal
    obtained by considering only the element at position (i,j) corresponds to
    the impulse response of the SISO filter representing the effect of input j on output i. **)
Definition MIMO_impulse_response : @msignal RT ny nu :=
  Build_signal MIMO_impulse_response_causal.


Hypothesis HLTI : LTI_filter H.


Variable u : @vsignal RT nu.


Lemma decomp_one_output (k : int) (i : 'I_ny) :
  H u k _[i]ord =
  \sum_(j < nu) to_SISO_yi_uj H i j (ith_scsignal u j) k.
Proof.
rewrite [in LHS](decomp_vsignal u) LTI_compat_sum // sum_msignalE.
apply: eq_big_ord => j. by rewrite ith_scsignal_applied.
Qed.


Theorem convolution_MIMO_impulse_response :
  H u = convol_mx_ve MIMO_impulse_response u.
Proof.
apply/signalP => k; apply/veP => i.
rewrite decomp_one_output.
rewrite [in RHS](decomp_vsignal u) convol_mx_ve_sum sumsE summxE.
apply: eq_big_ord => j.
rewrite convolution_impulse_response; last exact: LTI_to_SISO_yi_uj.
rewrite !signalE big_mkord summxE.
apply: eq_big_ord => m.
rewrite mxE (sum_ord_singlenon0 j).
  by rewrite mxE (@vsignal_of_sc_at_i_applied_index_i _ nu).
move => j' _Hneq.
rewrite (@vsignal_of_sc_at_i_applied_index_neq _ nu) //.
by simplr.
Qed.




End ImpulseResponse.


End SISO_MIMO.

