(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import all_ssreflect ssralg vector ssrint ssrnum matrix.

Require Import lia_tactics
               utils nat_ord_complements int_complements
               sum_complements mx_complements
               signal filter TF_coefficients State_Space.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Import GRing.Theory.


Local Open Scope ring_scope.





Section SIF.


(** We consider a comUnitRingType here, as we will need
    to work with matrix inverse.                        **)
Context {RT : comUnitRingType}.






(** Property Jprop of matrix J of a SIF: lower triangular with
    only 1s on the diagonal. In particular, this makes its
    inverse easy to compute.                                   **)


Section Context_n.


Context {n : nat}.


(** Property followed by the matrix J of a SIF:
    all values on the diagonal are 1,
    all values above the diagonal are 0.        **)
Definition Jprop (A : 'M_n) :=
(forall i : 'I_n, A i i = (1 : RT))
/\ (forall i j : 'I_n, (i < j)%nat -> A i j = 0).


(** If J verifies Jprop, then "solve_J_mult J v"
    returns a vector u such that J u = v.
    u_0 is computed first, then u_1 using u_0, etc. **)
Definition solve_J_mult (J : 'M[RT]_n) (v : 'cV_n) : 'cV_n :=
  \matrix_(i,_) @strongrec RT 0 (fun i f =>
    v _[i] - \sum_(0 <= k < i) J _[i, k] * f k)   i.


Lemma solve_J_mult_strongrec_safe (J : 'M[RT]_n) (v : 'cV_n) :
strongrec_safe (fun m g =>
    v _[m] - \sum_(0 <= k < m) J _[m, k] * g k).
Proof.
move => N g1 g2 Hpast.
f_equal; f_equal.
apply: eq_big_nat => i _Hi.
f_equal.
apply: Hpast.
by auton.
Qed.


(** This lemma is never directly used, but it means that the vector
    defined by solve_J_mult verifies the intended property. **)
Lemma solve_J_mult_correct (J : 'M_n) (y : 'cV_n) :
  Jprop J -> J *m solve_J_mult J y = y.
Proof.
move => [Jii Jij]; apply/veP => i.
rewrite -[in RHS]fmxn_ord /=.
rewrite mxE big_ord_to_nat.
  exact: nat_pos_if_ord.
move=> n_gt0.
rewrite (sum_nat_cat i) //; last by auton.
rewrite [X in _+X](sum_nat_cat i.+1) //.
rewrite big_nat1 [in X in _+X]sum_nat_all0; last first.
  move => j j_bnd.
  rewrite Jij; last by auton.
  exact: mul0r.
rewrite ord_of_nat_of_ord Jii addr0 mul1r.
rewrite mxE strongrec_n; last first.
  exact: solve_J_mult_strongrec_safe.
rewrite addrA [X in X-_]addrC -addrA.
rewrite -sum_nat_sub sum_nat_all0.
  exact: addr0.
move => j j_bnd.
apply: eq_subr0; f_equal.
  rewrite -fmxn_ord nat_of_ord_of_nat //.
  by auton.
rewrite mxE.
f_equal.
by auton.
Qed.


Definition Jinv J : 'M_n :=
  \matrix_(i,j) (solve_J_mult J (col j 1%:M)) i ord0.


Lemma mul_J_Jinv J : Jprop J -> J *m Jinv J = 1%:M.
Proof.
move => HJ. apply: mx_ext_col => j.
rewrite col_mulmx. rewrite -[RHS](@solve_J_mult_correct J) //.
f_equal. apply/veP => i. by rewrite !mxE.
Qed.


Lemma unitmx_J J : Jprop J -> J \in unitmx.
Proof.
move => HJ. exact: proj1 (mulmx1_unit (mul_J_Jinv HJ)).
Qed.


Lemma solve_J_mult_invmx (J : 'M[RT]_n) (v : 'cV_n) :
  Jprop J -> solve_J_mult J v = (invmx J) *m v.
Proof.
move => HJ. rewrite -[LHS]mul1mx -(mulVmx (unitmx_J HJ)) -mulmxA.
f_equal. exact: solve_J_mult_correct.
Qed.


Lemma Jprop_1 : Jprop (1%:M).
Proof.
split.
- move => i. rewrite mxE /nat_of_bool ifT //.
- move => i j _H. rewrite mxE /nat_of_bool ifF //.
  exact: ltn_eqF.
Qed.


End Context_n.



(** Jprop and submatrices / block matrices **)

Lemma Jprop_ulsubmx {n1 n2} (J : 'M[RT]_(n1+n2)) :
  Jprop J -> Jprop (ulsubmx J).
Proof.
move => [Heq1 Heq0]. split => [i | i j Hij].
- by rewrite !mxE Heq1.
- by rewrite !mxE Heq0.
Qed.

Lemma Jprop_drsubmx {n1 n2} (J : 'M[RT]_(n1+n2)) :
  Jprop J -> Jprop (drsubmx J).
Proof.
move => [Heq1 Heq0]. split => [i | i j Hij].
- by rewrite !mxE Heq1.
- rewrite !mxE Heq0 //=.
  by ssrnatlia.
Qed.

Lemma ursubmx_J_eq0 {n1 n2} (J : 'M[RT]_(n1+n2)) :
  Jprop J -> ursubmx J = 0.
Proof.
move => [_ Heq0]. apply/matrixP => i j.
rewrite !mxE Heq0 //=.
by auton.
Qed.

Lemma invmx_J_block {n1 n2} (Jul : 'M[RT]_n1) Jur Jdl (Jdr : 'M_n2) :
  Jprop (block_mx Jul Jur Jdl Jdr) ->
  invmx (block_mx Jul Jur Jdl Jdr) =
  block_mx (invmx Jul)                       0
           (- invmx Jdr *m Jdl *m invmx Jul) (invmx Jdr).
Proof.
move => HJprop. apply: invmx_from_mul.
rewrite mulmx_block !mulmxV. simplmx.
2,3: apply: unitmx_J.
3: rewrite -(block_mxKul Jul Jur Jdl Jdr); exact: Jprop_ulsubmx.
2: rewrite -(block_mxKdr Jul Jur Jdl Jdr); exact: Jprop_drsubmx.
rewrite -[Jur](block_mxKur Jul Jur Jdl Jdr) ursubmx_J_eq0 //. simplmx.
rewrite !mulNmx mulmxN -mulmxA (mulmxA Jdr) mulmxV.
2:{ have := (Jprop_drsubmx HJprop). rewrite block_mxKdr. exact: unitmx_J. }
rewrite mul1mx subrr. apply: mx1_block.
Qed.

Lemma Jprop_block {n1 n2} (Jul : 'M[RT]_n1) Jur Jdl (Jdr : 'M_n2) :
  Jprop Jul -> Jprop Jdr -> Jur = 0 ->
  Jprop (block_mx Jul Jur Jdl Jdr).
Proof.
move => Hul Hdr Hur. split.
- move => i. rewrite (ord_lrshift i).
  case (ltnP i n1) => [lt_i_n1 | le_n1_i].
  + rewrite col_mxEu row_mxEl. exact: (fst Hul).
  + rewrite col_mxEd row_mxEr. exact: (fst Hdr).
- move => i j Hij. rewrite (ord_lrshift i) (ord_lrshift j).
  case (ltnP i n1) => [lt_i_n1 | le_n1_i];
  case (ltnP j n1) => [lt_j_n1 | le_n1_j].
  + rewrite col_mxEu row_mxEl. exact: (snd Hul).
  + by rewrite col_mxEu row_mxEr Hur mxE.
  + contradict Hij. ssrnatlia.
  + rewrite col_mxEd row_mxEr. apply: (snd Hdr). simpl. ssrnatlia.
Qed.









Section SIF_definition.


Context {nu ny : nat}.
Context {nx nt : nat}.

(**
  J t(k+1) <- M x(k) + N u(k)
    x(k+1) <- K t(k+1) + P x(k) + Q u(k)
      y(k) <- L t(k+1) + R x(k) + S u(k)
**)

Record SIF := { SIF_J     : 'M[RT]_nt       ;
                SIF_Jprop : Jprop SIF_J     ;
                SIF_K     : 'M[RT]_(nx, nt) ;
                SIF_L     : 'M[RT]_(ny, nt) ;
                SIF_M     : 'M[RT]_(nt, nx) ;
                SIF_N     : 'M[RT]_(nt, nu) ;
                SIF_P     : 'M[RT]_nx       ;
                SIF_Q     : 'M[RT]_(nx, nu) ;
                SIF_R     : 'M[RT]_(ny, nx) ;
                SIF_S     : 'M[RT]_(ny, nu) ;
              }.



Section Build_SIF_output.

Variable sif : SIF.

Notation J := (SIF_J sif).
Notation Jinv := (invmx J).
Notation K := (SIF_K sif).
Notation L := (SIF_L sif).
Notation M := (SIF_M sif).
Notation N := (SIF_N sif).
Notation P := (SIF_P sif).
Notation Q := (SIF_Q sif).
Notation Rsif := (SIF_R sif). (** R is usually the set of real numbers **)
Notation S := (SIF_S sif).

Variable u : @vsignal RT nu.


(** computes t(k+1) from u(k) and x(k) **)
Notation compute_tS uk xk := (Jinv *m (M *m xk + N *m uk)).


Definition x := signal_peanorec
  (fun k xk => K *m compute_tS (u k) xk + P *m xk + Q *m u k).


Fact y_causal :
  causal (fun k => L *m compute_tS (u k) (x k) + Rsif *m x k + S *m u k).
Proof.
move=> k k_lt0; rewrite !signal_causal //.
by simplmx.
Qed.
Definition y := Build_signal y_causal.




(** additional properties of t, x, and y **)


Lemma x_Sk (k:int) :
  x (k + 1) = K *m (Jinv *m (M *m x k + N *m u k)) + P *m x k + Q *m u k.
Proof.
case: (Num.Theory.ltrP k 0).
- move=> k_lt0; rewrite ![in RHS]signal_causal //.
  simplmx.
  rewrite signalE peanoreczBC //.
  by intlia.
- by move=> k_ge0; rewrite {1}signalE peanoreczIS.
Qed.


Fact t_causal : causal (fun k => compute_tS (u (k - 1)) (x (k - 1))).
Proof.
move=> k k_lt0.
rewrite !signal_causal; [| by intlia .. ].
by simplmx.
Qed.
Definition t := (Build_signal t_causal).


Lemma x_k (k:int) :
  x k = K *m t k + P *m x (k-1) + Q *m u (k-1).
Proof.
rewrite -{1}(subrK 1 k).
exact: x_Sk.
Qed.


Lemma y_k (k:int) :
  y k = L *m (Jinv *m (M *m x k + N *m u k)) + Rsif *m x k + S *m u k.
Proof.
by [].
Qed.


Lemma y_k_foldt (k:int) :
  y k = L *m t (k + 1) + Rsif *m x k + S *m u k.
Proof.
rewrite signalE [in RHS]signalE.
by ringz_simplify (_ - _).
Qed.


End Build_SIF_output.



Definition filter_from_SIF : SIF -> filter := y.









Section StateSpace_from_SIF.


Variable sif : SIF.


Notation J := (SIF_J sif).
Notation Jinv := (invmx J).
Notation K := (SIF_K sif).
Notation L := (SIF_L sif).
Notation M := (SIF_M sif).
Notation N := (SIF_N sif).
Notation P := (SIF_P sif).
Notation Q := (SIF_Q sif).
Notation Rsif := (SIF_R sif).
Notation S := (SIF_S sif).


Definition StateSpace_from_SIF : StateSpace :=
  Build_StateSpace
    (* StSp_A *) (K *m Jinv *m M + P)
    (* StSp_B *) (K *m Jinv *m N + Q)
    (* StSp_C *) (L *m Jinv *m M + Rsif)
    (* StSp_D *) (L *m Jinv *m N + S).

Notation stsp := (StateSpace_from_SIF : StateSpace).


Lemma StateSpace_from_SIF_same_x (u : signal) :
  State_Space.x stsp u = x sif u.
Proof.
apply/signalP.
apply: peanoindz.
  intros; by rewrite /= !peanoreczBC.
intros k _Hk Hind.
rewrite x_Sk State_Space.x_Sk Hind.
rewrite !mulmxDl addrA; f_equal.
rewrite -addrA [X in _+X]addrC addrA; f_equal.
by rewrite !mulmxA mulmxDr !mulmxA.
Qed.


Theorem StateSpace_from_SIF_same_filter :
  filter_from_StSp stsp = filter_from_SIF sif.
Proof.
apply filter_ext => u k.
rewrite signalE [in RHS]signalE StateSpace_from_SIF_same_x.
rewrite !mulmxDl !addrA; f_equal.
rewrite -addrA [X in _+X]addrC addrA; f_equal.
by rewrite !mulmxA mulmxDr !mulmxA.
Qed.


End StateSpace_from_SIF.



Corollary filter_from_SIF_is_LTI (sif : SIF) :
  LTI_filter (filter_from_SIF sif).
Proof.
rewrite -StateSpace_from_SIF_same_filter.
exact: filter_from_StSp_is_LTI.
Qed.



End SIF_definition.






Section SIF_from_StateSpace.

Context {nu ny nx : nat}.

Variable stsp : @StateSpace RT nu ny nx.

Notation A := (StSp_A stsp).
Notation B := (StSp_B stsp).
Notation C := (StSp_C stsp).
Notation D := (StSp_D stsp).


Definition SIF_from_StateSpace : SIF :=
  @Build_SIF nu ny nx 0
    (* SIF_J *)      (1%:M : 'M[RT]_0)
    (* SIF_Jprop *)  Jprop_1
    (* SIF_K *)      0
    (* SIF_L *)      0
    (* SIF_M *)      0
    (* SIF_N *)      0
    (* SIF_P *)      A
    (* SIF_Q *)      B
    (* SIF_R *)      C
    (* SIF_S *)      D.
(** nt, M and N could actually be anything,
    and J anything that verifies Jprop **)


Notation J := (SIF_J SIF_from_StateSpace).
Notation Jinv := (invmx J).
Notation K := (SIF_K SIF_from_StateSpace).
Notation L := (SIF_L SIF_from_StateSpace).
Notation M := (SIF_M SIF_from_StateSpace).
Notation N := (SIF_N SIF_from_StateSpace).
Notation P := (SIF_P SIF_from_StateSpace).
Notation Q := (SIF_Q SIF_from_StateSpace).
Notation Rsif := (SIF_R SIF_from_StateSpace).
Notation S := (SIF_S SIF_from_StateSpace).


Lemma SIF_from_StateSpace_same_x (u : signal) :
  x SIF_from_StateSpace u = State_Space.x stsp u.
Proof.
apply/signalP. apply: peanoindz.
  intros; by rewrite /= !peanoreczBC.
intros k _Hk Hind.
by rewrite x_Sk State_Space.x_Sk Hind mul0mx add0r.
Qed.


Theorem SIF_from_StateSpace_same_filter :
  filter_from_SIF SIF_from_StateSpace = filter_from_StSp stsp.
Proof.
apply filter_ext => u k.
by rewrite signalE [in RHS]signalE
           SIF_from_StateSpace_same_x mul0mx add0r.
Qed.


End SIF_from_StateSpace.







Section SIF_from_DFI.


Variable tfc : @TFCoeffs RT.

Notation n := (TFC_n tfc).
Notation a := (TFC_a tfc).
Notation b := (TFC_b tfc).


(**
From transfer function coefficients, we want to build a SIF that represents the filter
described by Direct Form I, taking the order of computations into account.
This filter is SISO (Single-Input Single-Output), so nu = ny = 1.
We note u an input signal and y the corresponding output.


Let a1, a2, ... , an, b0, b1, ..., bn transfer function coefficients of order n.
The corresponding filter is defined by the equation:

    y(k) = sum_{0<=i<=n} bi u(k-i) - sum_{1<=i<=n} ai y(k-i)

We use the state vector x(k) to store previous values of u and y:

    x(k) = ( u(k-n)         of size nx := 2n
             u(k-n+1)
             ...
             u(k-1)
             y(k-n)
             ...
             y(k-1) )

i.e. x(k)_i     = u(k-n+i)    for 0 <= i < n
     x(k)_{n+i} = y(k-n+i)    for 0 <= i < n

We use the intermediate vector t(k+1) to compute y(k), which is both returned as output
for step k and stored in x(k+1) for the next step:

    t(k+1) = ( y(k) )       of size nt := 1


From
    J t(k+1) = M x(k) + N u(k)
we get
      J_(1,1) ( sum_{0<=i<=n} bi u(k-i) - sum_{1<=i<=n} ai y(k-i) )
    = sum_{0<=i<n} M_(1,i) u(k-n+i) + sum_{0<=i<n} M_(1,n+i) y(k-n+i) + N_(0,0) u(k)
so:

    J = ( 1 )              of size 1 x 1

    M = ( bn ... b1 -an ... -a1 )       of size 1 x 2n

    N = ( b0 )             of size 1 x 1

From
    x(k+1) = K t(k+1) + P x(k) + Q u(k)
we get
    forall i, 1 <= i <= n ->
         u(k-n+i) =   K_(i,0) y(k) + sum_{0<=j<n} P_(i,j) u(k-n+j)
                      + sum_{0<=j<n} P_(i,n+j) y(k-n+j) + Q_(i,0) u(k)
      /\ y(k-n+i) =   K_(n+i,1) y(k) + sum_{0<=j<n} P_(n+i,j) u(k-n+j)
                      + sum_{0<=j<n} P_(n+i,n+j) y(k-n+j) + Q_(n+i,0) u(k)
so:

    K = ( 0                of size 2n x 1
         ...
          0
          1 )  <- 2n-th line (index 2n-1)

    P = ( 0 1 0 ............... 0           of size 2n x 2n
          0 0 1 ............... 0
                    ...
          0 ... 0 0 1 ......... 0
          0 ... 0 0 0 0 0 0 ... 0    <- nth line (index n-1)
          0 ....... 0 0 1 0 ... 0
          0 ......... 0 0 1 ... 0
                    ...
          0 ............... 0 0 1
          0 ............... 0 0 0 )  <- 2n-th line (index 2n-1)

    Q = ( 0                of size 2n x 1
         ...
          0
          1   <- nth line (index n-1)
          0
         ...
          0 )

From
    y(k) = L t(k+1) + R x(k) + S u(k)
we get
    y(k) = L_(1,1) y(k) + sum_{0<=i<n} R_(0,i) u(k-n+i)
                        + sum_{0<=i<n} R_(0,n+i) y(k-n+i) + S_(0,0) u(k)
so:

    L = ( 1 )              of size 1 x 1

    R = ( 0 ... 0 )        of size 1 x 2n

    S = ( 0 )              of size 1 x 1

**)


Definition SIF_from_DFI : SIF :=
  @Build_SIF 1 1
    (* nx *)        (2*n)
    (* nt *)        1
    (* SIF_J *)     1%:M
    (* SIF_Jprop *) (Jprop_1)
    (* SIF_K *)     (\matrix_(i,_) if ((i:nat) == 2*n-1)%nat then 1 else 0)
    (* SIF_L *)     1%:M
    (* SIF_M *)     (\matrix_(_,j) if ((j:nat) < n)%nat then b (n-j)%nat
                                                      else - a (2*n-j)%nat)
    (* SIF_N *)     (b 0)%:M
    (* SIF_P *)     (\matrix_(i,j)
                       if ((i:nat) != (n-1)%nat) && (i+1 == j)%nat
                       then 1 else 0)
    (* SIF_Q *)     (\matrix_(i,_) if (i:nat) == (n-1)%nat then 1 else 0)
    (* SIF_R *)     0
    (* SIF_S *)     0.


Notation sif := SIF_from_DFI.

Notation J := (SIF_J SIF_from_DFI).
Notation Jinv := (invmx J).
Notation K := (SIF_K SIF_from_DFI).
Notation L := (SIF_L SIF_from_DFI).
Notation M := (SIF_M SIF_from_DFI).
Notation N := (SIF_N SIF_from_DFI).
Notation P := (SIF_P SIF_from_DFI).
Notation Q := (SIF_Q SIF_from_DFI).
Notation Rsif := (SIF_R SIF_from_DFI).
Notation Ssif := (SIF_S SIF_from_DFI).


Hypothesis n_gt0 : (n > 0)%nat.


Section Var_u.

Variable u : @scsignal RT.
Notation t := (t sif (vsignal_of_sc u)).
Notation x := (x sif (vsignal_of_sc u)).
Notation y_SIF := (to_SISO (filter_from_SIF sif) u).
Notation y_DFI := (filter_from_TFC tfc u).


Lemma DFI_x_stores_past_u (k : int) (i : nat) :
  (i < n)%nat -> (x k) _[i] = u (k-(n:int)+i).
Proof.
revert k i; apply: strongindz.
  move => k _Hk i _Hi. rewrite !signal_causal //. 2: autoz.
  rewrite fmxn_0 //=; auton.
move => k _Hk Hind i _Hi.

(** x(k) = K t(k) + P x(k-1) + Q u(k-1) **)
replace k with (k-1+1) at 1 by ringz.
rewrite x_Sk.
rewrite fmxnE.
  by auton.
move => H_i_nx.
rewrite 2!mxE.

(** for 1 <= i <= n, (K t(k))_(i,1) = 0 **)
rewrite mxE big_ord_to_nat.
rewrite sum_nat_all0; last first.
  move => j /andP _Hj.
  rewrite mxE ifF; last by auton.
  exact: mul0r.
rewrite add0r.

case Hcase: (i==(n-1)%nat).

- (** case i = n **)

  (** for i = n, (P x(k-1))_(n,1) = 0 **)
  rewrite mxE big_ord_to_nat; first by ssrnatlia.
  move => _Hnx.
  rewrite sum_nat_all0; last first.
    move => j /andP _Hj.
    by rewrite mxE nat_of_Ordinal Hcase /= mul0r.

  (** for i = n, (Q u(k-1))_(n,1) = u(k-1) **)
  rewrite mxE big_ord_to_nat.
  rewrite add0r big_nat1 //= mxE nat_of_Ordinal Hcase mul1r mxE.
  f_equal.
  move: Hcase => /eqP ->.
  by autoz.

- (** case i <> n **)
  have/eqP Hcase_noteq := Hcase.

  (** for 1 <= i < n, (Q u(k-1))_(i,1) = 0 **)
  rewrite [X in _+X]mxE big_ord_to_nat.
  rewrite sum_nat_all0; last first.
    by move=> *; rewrite mxE nat_of_Ordinal Hcase mul0r.

  (** the only nonzero term of (P x(k-1))_(i,1) = sum_{1<=j<=2n} P_(i,j) x(k-1)_(j,1)
      is for j = i+1, and it is equal to u((k-1)-n-1+(i+1)) = u(k-n-1+i) **)
  rewrite mxE big_ord_to_nat; first by ssrnatlia.
  move => _Hnx.
  rewrite (sum_nat_cat (i.+1)) //; [| simpl; intlia ..].
  rewrite sum_nat_all0.
    rewrite (sum_nat_cat (i.+2)); [| ssrnatlia ..].
    rewrite big_nat1.
    rewrite sum_nat_all0.
      2,3: move => j /andP _Hj; rewrite mxE nat_of_Ordinal Hcase /= ifF ?mul0r //.
      2,3: by apply/eqP; auton.
  rewrite add0r 2!addr0 mxE ifT; last first.
    by auton.
  rewrite mul1r -fmxn_ord nat_of_Ordinal nat_of_ord_of_nat; last first.
    by auton.
  rewrite Hind; cycle 1.
  - by intlia.
  - by ssrnatlia.
  f_equal.
  by autoz.
Qed.


Lemma DFI_x_last k : (x k) _[n+(n-1)] = (t k)%:sc.
Proof.
rewrite x_k.
rewrite fmxnE; first by ssrnatlia.
move => ?.
rewrite 2!mxE.

(** (Q u(k-1))_(2n,1) = 0 **)
rewrite [X in _+X]mxE big_ord_to_nat sum_nat_all0; last first.
  move => j j_bnd; rewrite mxE ifF.
    exact: mul0r.
  by auton.
rewrite addr0.

(** (P x(k-1))_(2n,1) = 0 **)
rewrite [X in _+X]mxE big_ord_to_nat; first by auton.
move => nn_gt0.
rewrite sum_nat_all0; last first.
  move => j j_bnd; rewrite mxE ifF.
    exact: mul0r.
  apply: Bool.andb_false_intro2.
  by auton.
rewrite addr0.

rewrite mxE big_ord_to_nat big_nat1 // mxE ifT; last first.
  by auton.

by rewrite mul1r !zmodp.ord1.
Qed.


Lemma DFI_x_stores_past_y (k : int) (i : nat) :
  (i < n)%nat -> (x k) _[(n+i)%nat] = y_DFI (k-(n:int)+i).
Proof.
revert k i. apply: strongindz.
  move => k _Hk i _Hi. rewrite !signal_causal //. 2: autoz.
  by dvlp_fmxn.
move => k k_ge0 IH i i_lt.
replace k with (k-1+1) at 1 by ringz.
case_eq (i==n-1)%nat => /eqP.
- move=> ->.
  rewrite DFI_x_last.
  ringz_simplify (_ + _).
  rewrite signalE.
  simplmx.
  rewrite scalar_of_mx11_fmxn.
  dvlp_fmxn.
  rewrite signal_strongrec_k; last first.
    exact: DFI_fIS_safe_and_causal.
  rewrite -/y_DFI /DFI_fIS.
  rewrite (sum_nat_cat n) //; last ssrnatlia.
  rewrite addrC addrA; f_equal.
  + rewrite big_nat_recl //; f_equal.
    * rewrite scaler_mulr.
      f_equal; f_equal.
      by autoz.
    * rewrite [in RHS]sum_nat_rev.
      apply: eq_big_nat => j _Hj.
      dvlp_fmxn.
      rewrite ifT // DFI_x_stores_past_u // scaler_mulr.
      f_equal; f_equal.
        by auton.
      by autoz.
  + rewrite -sum_nat_opp (sum_nat_shiftDown (n-1)%nat); last by ssrnatlia.
    rewrite [in RHS]sum_nat_rev.
    apply: eq_sum_nat_eqbounds; [by auton ..|].
    move => j j_bnd.
    dvlp_fmxn.
    rewrite ifF; last by auton.
    rewrite scaler_mulr -mulNr; f_equal.
    * f_equal; f_equal.
      by auton.
    * replace (n-1+j)%nat with (n+(j-1))%nat by auton.
      rewrite IH; [| by autoz | by auton].
      by f_equal; autoz.
- move=> i_neq; rewrite x_Sk.
  dvlp_fmxn.
  rewrite ifF; last by auton. (** for i < n-1, K_[n+i,_] = 0 **)
  rewrite ifF; last by auton. (** for i < n-1, Q_[n+i,_] = 0 **)
  rewrite 2!mul0r add0r addr0.
  rewrite (sum_nat_singlenon0 (n+i+1)%nat) //; cycle 1.
  + by auton.
  + move => j j_bnd j_neq; dvlp_fmxn.
    rewrite ifF.
      exact: mul0r.
    apply: Bool.andb_false_intro2.
    by auton.
  dvlp_fmxn.
  rewrite ifT; last by auton.
  rewrite mul1r.
  replace (n+i+1)%nat with (n+(i+1))%nat by ssrnatlia.
  rewrite IH; [| by autoz | by auton].
  by f_equal; autoz.
Qed.


End Var_u.


Theorem SIF_from_DFI_same_filter :
  to_SISO (filter_from_SIF SIF_from_DFI) = filter_from_TFC tfc.
Proof.
apply: filter_ext => u k.
rewrite signalE y_k_foldt.
simplmx.
rewrite -DFI_x_last.
rewrite DFI_x_stores_past_y; last by ssrnatlia.
f_equal.
by autoz.
Qed.


End SIF_from_DFI.





Section SIF_from_DFII.


Variable tfc : @TFCoeffs RT.

Notation n := (TFC_n tfc).
Notation a := (TFC_a tfc).
Notation b := (TFC_b tfc).

Hypothesis n_gt0 : (n > 0)%nat.


(**
From transfer function coefficients, we want to build a SIF that represents the filter
described by Direct Form II, taking the order of computations into account.
This filter is SISO (Single-Input Single-Output), so nu = ny = 1.
We note u an input signal and y the corresponding output.


Let a1, a2, ... , an, b0, b1, ..., bn transfer function coefficients of order n.
The corresponding filter is defined by the equations:

    e(k) = u(k) - sum_{1<=i<=n} ai e(k-i)
    y(k) = sum_{0<=i<=n} bi e(k-i)

where e is an intermediary signal.

We use the state vector x(k) to store previous values of e:

    x(k) = ( e(k-n)         of size nx := n
             e(k-n+1)
             ...
             e(k-1) )

i.e. x(k)_i     = e(k-n+i)    for 0 <= i < n

We use the intermediate vector t(k+1) to compute e(k), which is both used in the 
computation of current output y(k) and stored in x(k+1) for the next step:

    t(k+1) = ( e(k) )       of size nt := 1


From
    J t(k+1) = M x(k) + N u(k)
we get
    J_(0,0) ( u(k) - sum_{1<=i<=n} ai e(k-i) )
             =   sum_{0<=i<n} M_(0,i) e(k-n+i) + N_(0,0) u(k)
so:

    J = ( 1 )              of size 1 x 1 (note that J verifies indeed Jprop)

    M = ( -an ... -a1 )    of size 1 x n

    N = ( 1 )              of size 1 x 1

From
    x(k+1) = K t(k+1) + P x(k) + Q u(k)
we get
    forall i, 0 <= i < n ->
         e(k+1-n+i) =   K_(i,0) e(k) + ( sum_{0<=j<n} P_(i,j) e(k-n+j) ) + Q_(i,0) u(k)
so:

    K = ( 0                of size n x 1
         ...
          0
          1 )  <- nth line (index n-1)

    P = ( 0 1 0 ... 0             of size n x n
          0 0 1 ... 0
              ...
          0 ... 0 1 0
          0 ... 0 0 1
          0 ... 0 0 0 )  <- nth line (index n-1)

    Q = ( 0                of size n x 1
          0
         ...
          0 )

From
    y(k) = L t(k+1) + R x(k) + S u(k)
we get
    sum_{0<=i<=n} bi e(k-i) =   L_(0,0) e(k) + ( sum_{0<=i<n} R_(0,i) e(k-n+i) )
                              + S_(0,0) u(k)
so:

    L = ( b0 )             of size 1 x 1

    R = ( bn ... b1 )      of size 1 x n

    S = ( 0 )              of size 1 x 1

**)


Definition SIF_from_DFII : SIF :=
  @Build_SIF 1 1
    (* nx *)         n
    (* nt *)         1
    (* SIF_J *)      1%:M
    (* SIF_Jprop *)  Jprop_1
    (* SIF_K *)      (\matrix_(i,_) if (i:nat) == (n-1)%nat then 1 else 0)
    (* SIF_L *)      (b 0)%:M
    (* SIF_M *)      (\matrix_(_,j) - a (n-j)%nat)
    (* SIF_N *)      1%:M
    (* SIF_P *)      (\matrix_(i,j) if (i:int)+1 == j then 1 else 0)
    (* SIF_Q *)      0
    (* SIF_R *)      (\matrix_(_,j) b (n-j)%nat)
    (* SIF_S *)      0.


Notation sif := SIF_from_DFII.


Section Var_u.


(** DFII input, output, and intermediary signals **)
Variable u : @scsignal RT.
Notation y_DFII := (filter_by_DFII tfc u).
Notation e := (DFII_u2e tfc u).


(** SIF input (the same as for DFII but vectorial),
    output (converted back to a scalar signal to compare it with y_DFII),
    and state signals **)
Notation u_ve := (vsignal_of_sc u).
Notation y_SIF := (to_SISO (filter_from_SIF sif) u).
Notation x := (x sif u_ve).
Notation t := (t sif u_ve).


(* Lemma x_last k : (x k) _[n-1] = (compute_tS sif (u_ve (k-1)) (x (k-1))) _[0]. *)
Lemma x_last k : (x k) _[n-1] = t k _[0].
Proof. (** x(k) = K t(k) + P x(k-1) + Q u(k-1) **)
replace k with (k-1+1) at 1 by ringz; rewrite x_Sk.
(** Q = 0 **) simpl (SIF_Q _); rewrite mul0mx addr0.
dvlp_fmxn. rewrite ifT // mul1r.
(** the last line of P is null *) rewrite [in X in _+X]sum_nat_all0.
2:{ move => i _Hi. dvlp_fmxn.
    rewrite ifF. exact: mul0r. autonz. }
by rewrite addr0.
Qed.


Lemma x_notlast (k : int) (i : nat) :
  (i < n-1)%nat -> (x k) _[i] = (x (k-1)) _[i+1].
Proof. (** x(k) = K t(k) + P x(k-1) + Q u(k-1) **)
move => _Hi.
replace k with (k-1+1) at 1 by ringz.
rewrite x_Sk.
simplmx. (** Q = 0, J = 1, N = 1 **)
dvlp_fmxn.
rewrite ifF; last first.
  by auton.
simplmx.
rewrite (sum_nat_singlenon0 (i+1)); cycle 1.
- by auton.
- move => j _Hj _Hneq; dvlp_fmxn.
  rewrite ifF.
    by simplmx.
  by autoz.
dvlp_fmxn.
rewrite ifT //.
by simplmx.
Qed.


Lemma x_stores_n_previous_e (k : int) (i : nat) :
  (i < n)%nat -> (x k) _[i] = e (k-(n:int)+i).
Proof.
revert k i; apply: strongindz => k _Hk.
  intros; rewrite !signal_causal //.
    by dvlp_fmxn.
  by autoz.
move => Hind i _Hi.
case Hcase: (i == (n-1)%nat); last first.
  (** case i < n-1 **)
  rewrite x_notlast; autonz.
  rewrite Hind; autonz.
  f_equal.
  by autoz.
(** case i = n-1 **)
move/eqP: Hcase => ->; rewrite x_last.
rewrite signalE mul_scalar_mx scale1r.
simplmx. (** J = 1, N = 1 **)
dvlp_fmxn.
rewrite e_val.
rewrite addrC; f_equal.
  by f_equal; autoz.
rewrite -sum_nat_opp [in RHS]sum_nat_rev
        [in RHS](sum_nat_shiftDown 1) //.
apply: eq_sum_nat_eqbounds => //.
  by auton.
move => j _Hj; rewrite -mulNr.
f_equal.
- dvlp_fmxn.
  f_equal; f_equal.
  by auton.
- rewrite Hind //; autonz.
  f_equal.
  by autoz.
Qed.


Lemma SIF_from_DFII_same_filter_aux : y_SIF = y_DFII.
Proof.
apply/signalP => k.
rewrite signalE y_k_foldt.
simplmx.
rewrite scalar_of_mx11_fmxn fmxn_add // fmxn_scale //.
rewrite -x_last x_stores_n_previous_e; last by ssrnatlia.
rewrite [in RHS]signalE.
rewrite big_nat_recl //.
f_equal.
  rewrite scaler_mulr.
  do 2 f_equal.
  by autoz.
dvlp_fmxn.
rewrite [in RHS]sum_nat_rev.
apply: eq_big_nat => i i_bnd.
rewrite scaler_mulr; f_equal.
- dvlp_fmxn.
  f_equal.
  by auton.
- rewrite x_stores_n_previous_e //.
  f_equal.
  by autoz.
Qed.


End Var_u.


Theorem SIF_from_DFII_same_filter :
  to_SISO (filter_from_SIF SIF_from_DFII) = filter_by_DFII tfc.
Proof.
apply: filter_ext => u k.
by rewrite SIF_from_DFII_same_filter_aux.
Qed.


End SIF_from_DFII.







Section SIF_from_DFIIt.


Variable tfc : @TFCoeffs RT.

Notation n := (TFC_n tfc).
Notation a := (TFC_a tfc).
Notation b := (TFC_b tfc).

Hypothesis n_gt0 : (n > 0)%nat.


(**
From transfer function coefficients, we want to build a SIF that represents
the filter described by Direct Form II transposed, taking the order of
computations into account.
This filter is SISO (Single-Input Single-Output), so nu = ny = 1.
We note u an input signal and y the corresponding output.

Let a1, a2, ... , an, b0, b1, ..., bn transfer function coefficients of order n.
The corresponding filter is defined by the following equations, using
intermediary signals x1, x2, ... , xn:

  x1(k+1) = b1 u(k) - a1 y(k) + x2(k)
  ...
  x_(n-1)(k+1) = b(n-1) u(k) - a(n-1) y(k) + xn(k)
  xn(k+1) = bn u(k) - an y(k)
  y(k) = b0 u(k) + x1(k)

The intermediary signals x1, ... , xn become the components of the signal x
of state vectors. Note that as our vector indices start at 0, this means
that x(k)[i-1] = xi(k) for 1 <= i <= n.
As y(k) is needed for the computation of x(k+1), 
we compute it first as the single component of t(k+1).

  t(k+1)[0] = b0 u(k) + x(k)[0]
  x(k+1)[i] = b(i+1) u(k) - a(i+1) t(k+1)[0] + x(k)[i+1]
                 for 0 <= i < n (with the convention x(k)[n]=0)
  y(k) = t(k+1)[0]

From
  J t(k+1) = M x(k) + N u(k)
we get
  J_(0,0) ( x(k)[0] + b0 u(k) )
           =   sum_{0<=i<n} M_(0,i) x(k)[i] + N_(0,0) u(k)
so:

  J = ( 1 )              of size 1 x 1 (note that J verifies indeed Jprop)

  M = ( 1 0 ... 0 )    of size 1 x n

  N = ( b0 )              of size 1 x 1

From
  x(k+1) = K t(k+1) + P x(k) + Q u(k)
we get
  forall i, 0 <= i < n ->
    - a(i+1) t(k+1)[0] + x(k)[i+1] + b(i+1) u(k)
         = K_(i,0) t(k+1)[0] + ( sum_{0<=j<n} P_(i,j) x(k)[j] ) + Q_(i,0) u(k)
so:

  K = ( -a1                of size n x 1
        ...
        -an )

  P = ( 0 1 0 ... 0        of size n x n
        0 0 1 ... 0
            ...
        0 ... 0 0 1
        0 ... 0 0 0 )

  Q = (  b1                of size n x 1
        ...
         bn )

From
    y(k) = L t(k+1) + R x(k) + S u(k)
we get
    t(k+1)[0] = L_(0,0) t(k+1)[0] + ( sum_{0<=i<n} R_(0,i) x(k+1)[i] )
                              + S_(0,0) u(k)
so:

    L = ( 1 )              of size 1 x 1

    R = ( 0 ... 0 )        of size 1 x n

    S = ( 0 )              of size 1 x 1

**)


Definition SIF_from_DFIIt : SIF :=
  @Build_SIF 1 1
    (* nx *)         n
    (* nt *)         1
    (* SIF_J *)      1%:M
    (* SIF_Jprop *)  Jprop_1
    (* SIF_K *)      (\matrix_(i,_) - a (i+1)%nat)
    (* SIF_L *)      1%:M
    (* SIF_M *)      (\matrix_(_,j) if (j:nat) == O then 1 else 0)
    (* SIF_N *)      (b 0)%:M
    (* SIF_P *)      (\matrix_(i,j) if (i:int)+1 == j then 1 else 0)
    (* SIF_Q *)      (\matrix_(i,_) b (i+1)%nat)
    (* SIF_R *)      0
    (* SIF_S *)      0.



Section Var_u.


(** DFI input and output **)
Variable u : @scsignal RT.
Notation y_DFI := (filter_from_TFC tfc u).


(** SIF input (the same as for DFII but vectorial),
    output (converted back to a scalar signal to compare it with y_DFII),
    and state signals **)
Notation u_ve := (vsignal_of_sc u).
Notation y_SIF := (to_SISO (filter_from_SIF SIF_from_DFIIt) u).
Notation x := (x SIF_from_DFIIt u_ve).
Notation t := (t SIF_from_DFIIt u_ve).


(**
  y_DFI(k) = sum_{0<=i<=n} bi u(k-i) - sum_{1<=i<=n} ai y_DFI(k-i)

  t(k+1)[0] = b0 u(k) + x(k)[0]
  x(k+1)[i] = b(i+1) u(k) - a(i+1) t(k+1)[0] + x(k)[i+1]
  y_SIF(k) = t(k+1)[0] 
**)


(** t(k+1) = ( y_SIF k ) **)
Lemma DFIIt_t_k k : t k = (y_SIF (k - 1))%:M1.
Proof.
rewrite mx11K y_k_foldt.
simplmx.
by f_equal; ringz.
Qed.


(**
  x(k)[n-1] = bn u(k-1) - an y(k-1)
  x(k)[n-2] = b{n-1} u(k-1) + bn u(k-2) - a{n-1} y(k-1) - an y(k-2)
  ...
  x(k)[0] = sum_{1<=i<=n} ( bi u(k-i) - ai y(k-i) )
**)
Lemma DFIIt_x_prop k i : (i < n)%nat ->
  x k _[n-1-i] = \sum_(j < i+1 )
                   ( b (n-i+j)%nat * u (k-1-(j:int))
                     - a (n-i+j)%nat * y_SIF (k-1-(j:int))).
Proof.
move: k; elim: i.
  move => k H. rewrite zmodp.big_ord1.
  rewrite x_k.
  rewrite DFIIt_t_k.
  dvlp_fmxn.
  rewrite sum_nat_all0; last first.
    move => i _Hi.
    dvlp_fmxn.
    rewrite ifF.
      by simplmx.
    by autoz.
  simplmx.
  rewrite addrC mulNr.
  by f_equal; f_equal; f_equal; [ | f_equal ]; ssrnatlia.
move => i IH k i_lt.
rewrite x_k.
rewrite DFIIt_t_k.
dvlp_fmxn.
rewrite (sum_nat_singlenon0 (n-1-i)%nat); cycle 1.
- by auton.
- move=> j j_bnd j_neq.
  dvlp_fmxn.
  rewrite ifF.
    by simplmx.
  by autoz.
dvlp_fmxn.
rewrite ifT; last first.
  by autoz.
simplmx.
rewrite IH; last by ssrnatlia.
rewrite [in RHS]addn1 [in RHS] big_ord_recl.
rewrite addrC addrA mulNr.
f_equal.
  ringz_simplify (_ - _ - _).
  by f_equal; last f_equal; f_equal; f_equal; auton.
rewrite addn1.
apply: eq_big_ord => j.
rewrite lift0.
f_equal; last f_equal; f_equal; f_equal.
  1,3: by auton.
all: by autoz.
Qed.


Lemma SIF_from_DFIIt_same_filter_aux : y_SIF = y_DFI.
Proof.
apply/signalP.
apply: strongindz.
  by move=> *; rewrite !signal_causal.
move => k Hk Hind.
rewrite filter_from_TFC_verifies_DFI /DFI_fIS.
rewrite 2!signalE.
simplmx.
rewrite scalar_of_mx11_fmxn.
dvlp_fmxn.
rewrite (sum_nat_singlenon0 0) //; last first.
  intros; dvlp_fmxn.
  rewrite ifF.
    by simplmx.
  by auton.
dvlp_fmxn.
simpl (if _ then _ else _).
simplmx.
rewrite big_ltn //.
rewrite addrC -addrA.
f_equal.
  by simplmx.
rewrite -sum_nat_sub.
rewrite -[X in _ _[X]](subnn (n-1)%nat).
rewrite DFIIt_x_prop; last by ssrnatlia.
rewrite subnK // big_ord_to_nat.
rewrite (big_nat_shiftUp 1) !addn1.
apply: eq_big_nat => i Hi.
rewrite Hind; last first.
  by autoz.
rewrite !scaler_mulr.
f_equal; last f_equal; f_equal; f_equal.
all: autonz.
Qed.


End Var_u.


Theorem SIF_from_DFIIt_same_filter :
  to_SISO (filter_from_SIF SIF_from_DFIIt) = filter_from_TFC tfc.
Proof.
apply: filter_ext => u k.
by rewrite SIF_from_DFIIt_same_filter_aux.
Qed.


End SIF_from_DFIIt.







(** Parallel decomposition, ie. sum of two filters: H = H1 + H2 **)
Section Parallel.


(**
Filter H1 is described by sif1 = (J1, K1, etc.), H2 by sif2 = (J2, K2, etc.).
We want to build a sif that describes H = H1 + H2.

         H1
      --------> y1
    u            + --------> y
      --------> y2
         H2



t(k+1) := ( t1(k+1) )     of size nt := nt1 + nt2 + 2 ny
          ( t2(k+1) )
          ( y1(k)   )
          ( y2(k)   )

x(k) := ( x1(k) )         of size nx := nx1 + nx2
        ( x2(k) )


J1 t1(k+1) = M1 x1(k) + N1 u(k)
J2 t2(k+1) = M2 x2(k) + N2 v(k)
   y1(k)   = L1 t1(k+1) + R1 x1(k) + S1 u(k)
   y2(k)   = L2 t2(k+1) + R2 x2(k) + S2 u(k)

(  J1   0   0   0   )          ( M1  0 )        ( N1 )
(   0  J2   0   0   )          (  0 M2 )        ( N2 )
( -L1   0   I   0   ) t(k+1) = ( R1  0 ) x(k) + ( S1 ) u(k)
(   0 -L2   0   I   )          (  0 R2 )        ( S2 )
          J                        M               N

   x1(k+1) = K1 t1(k+1) + P1 x1(k) + Q1 u(k)
   x2(k+1) = K2 t2(k+1) + P2 x2(k) + Q2 u(k)

         ( K1  0 0 0 )          ( P1  0 )        ( Q1 )
x(k+1) = (  0 K2 0 0 ) t(k+1) + (  0 P2 ) x(k) + ( Q2 ) u(k)
                K                   P               Q

      y(k) = y1(k) + y2(k) = t_3(k+1) + t_4(k+1)

y(k) = ( 0 0 I I ) t(k+1) + ( 0 0 ) x(k) + ( 0 ) u(k)
            L                  R             S

**)

Context {nu ny : nat}.
Context {nx1 nt1 : nat}.
Context {nx2 nt2 : nat}.

Variable sif1 : @SIF nu ny nx1 nt1.
Variable sif2 : @SIF nu ny nx2 nt2.

Notation J1 := (SIF_J sif1).
Notation K1 := (SIF_K sif1).
Notation L1 := (SIF_L sif1).
Notation M1 := (SIF_M sif1).
Notation N1 := (SIF_N sif1).
Notation P1 := (SIF_P sif1).
Notation Q1 := (SIF_Q sif1).
Notation R1 := (SIF_R sif1).
Notation S1 := (SIF_S sif1).

Notation J2 := (SIF_J sif2).
Notation K2 := (SIF_K sif2).
Notation L2 := (SIF_L sif2).
Notation M2 := (SIF_M sif2).
Notation N2 := (SIF_N sif2).
Notation P2 := (SIF_P sif2).
Notation Q2 := (SIF_Q sif2).
Notation R2 := (SIF_R sif2).
Notation S2 := (SIF_S sif2).

Notation I := 1%:M.


Definition Jparallel :=
block_mx (block_mx (block_mx  J1     0
                               0    J2)     0
                            (-L1 ▐   0)  I) 0
                            (  0 ▐ -L2 ▐ 0) I.


Lemma Jprop_parallel : Jprop Jparallel.
Proof.
have * := Jprop_1.
repeat apply: Jprop_block => //.
- exact: SIF_Jprop sif1.
- exact: SIF_Jprop sif2.
Qed.


Definition SIF_parallel : @SIF nu ny _ _ :=
  Build_SIF
    (* SIF_Jprop *)  Jprop_parallel
    (* SIF_K *)      ((K1 ▐ 0 ▐ 0 ▐ 0) ▬ (0 ▐ K2 ▐ 0 ▐ 0))
    (* SIF_L *)      (0 ▐ 0 ▐ I ▐ I)
    (* SIF_M *)      ((M1 ▐ 0) ▬ ( 0 ▐ M2 ) ▬ ( R1 ▐ 0 ) ▬ ( 0 ▐ R2 ))
    (* SIF_N *)      (N1 ▬ N2 ▬ S1 ▬ S2)
    (* SIF_P *)      ((P1 ▐ 0) ▬ ( 0 ▐ P2 ))
    (* SIF_Q *)      (Q1 ▬ Q2)
    (* SIF_R *)      0
    (* SIF_S *)      0.



Section SIF_parallel_x_k.


Variable u : @vsignal RT nu.
Notation x1 := (x sif1 u).
Notation x2 := (x sif2 u).
Notation y1 := (y sif1 u).
Notation y2 := (y sif2 u).

Notation sif := SIF_parallel.


Lemma SIF_parallel_x_k k : x sif u k = col_mx (x1 k) (x2 k).
Proof.
have * := SIF_Jprop sif.
have * := Jprop_block.
have * := (SIF_Jprop sif1).
have * := (SIF_Jprop sif2).
have * := Jprop_1.
move: k; apply: (peanoindz_basis (-1)).
  move=> *; rewrite !signal_causal //.
  by symmetry; exact: col_mx0.
move => k _ IH.
rewrite -[LHS]vsubmxK.
f_equal.
- rewrite !x_Sk.
  rewrite mul_col_mx [SIF_P sif *m _]mul_col_mx [SIF_Q sif *m _]mul_col_mx.
  rewrite !add_col_mx col_mxKu.
  f_equal.
  rewrite IH mul_row_col.
  simplmx.
  f_equal.
  rewrite !mul_col_mx !mul_row_col !add_col_mx; simplmx.
  repeat (rewrite invmx_J_block ?mul_block_col ?mul_row_col; simplmx).
    by [].
  1-3: by auto.
- rewrite !x_Sk.
  rewrite mul_col_mx [SIF_P sif *m _]mul_col_mx [SIF_Q sif *m _]mul_col_mx.
  rewrite !add_col_mx col_mxKd.
  f_equal.
  rewrite IH mul_row_col.
  simplmx.
  f_equal.
  rewrite !mul_col_mx !mul_row_col !add_col_mx; simplmx.
  repeat (rewrite invmx_J_block ?mul_block_col ?mul_row_col; simplmx).
    by [].
  1-3: by auto.
Qed.


End SIF_parallel_x_k.



Theorem SIF_parallel_preserves_filter :
  filter_from_SIF SIF_parallel =
  (filter_from_SIF sif1) \+ (filter_from_SIF sif2).
Proof.
apply: filter_ext => u k.
rewrite [RHS]signalE.
rewrite y_k SIF_parallel_x_k.
have * := Jprop_block.
have * := (SIF_Jprop sif1).
have * := (SIF_Jprop sif2).
have * := Jprop_1.
rewrite !invmx_J_block.
  2-4: by auto.
simplmx_block.
by rewrite -!mulmxA !(addrA _ _ (_ *m u k)).
Qed.


End Parallel.








(** Cascade of two filters, ie. composition: H = H2 o H1 **)
Section Cascade.


(**
Filter H1 is described by sif1, H2 by sif2.
We want to build sif that describes H = H2 o H1.

         H1          H2
    u --------> v --------> y

J1 t1(k+1) = M1 x1(k) + N1 u(k)
   x1(k+1) = K1 t1(k+1) + P1 x1(k) + Q1 u(k)
    v(k)   = L1 t1(k+1) + R1 x1(k) + S1 u(k)
J2 t2(k+1) = M2 x2(k) + N2 v(k)
   x2(k+1) = K2 t2(k+1) + P2 x2(k) + Q2 v(k)
    y(k)   = L2 t2(k+1) + R2 x2(k) + S2 v(k)


          ( t1(k+1) )
t(k+1) := ( v(k)    )     of size nt1 + nv(*=ny1=nu2*) + nt2
          ( t2(k+1) )

x(k) := ( x1(k) )         of size nx1 + nx2
        ( x2(k) )


J t(k+1) = M x(k) + N u(k)
  x(k+1) = K t(k+1) + P x(k) + Q u(k)
  y(k)   = L t(k+1) + R x(k) + S u(k)


     ( Jtl  0   0  )          ( Mtl Mtr )          ( Nt )
J := ( Jcl Jcc  0  )     M := ( Mcl Mcr )     N := ( Nc )
     ( Jbl Jbc Jbr )          ( Mbl Mbr )          ( Nb )

Jtl t1(k+1) = Mtl x1(k) + Mtr x2(k) + Nt u(k)
 J1 t1(k+1) = M1 x1(k) + N1 u(k)
-> Jtl = J1, Mtl = M1, Mtr = 0, Nt = N1

Jcc v(k) = -Jcl t1(k+1) + Mcl x1(k) + Mcr x2(k) + Nc u(k)
   v(k)  = L1 t1(k+1) + R1 x1(k) + S1 u(k)
-> Jcc = I, Jcl = -L1, Mcl = R1, Mcr = 0, Nc = S1

Jbr t2(k+1) = -Jbl t1(k+1) - Jbc v(k) + Mbl x1(k) + Mbr x2(k) + Nb u(k)
 J2 t2(k+1) = M2 x2(k) + N2 v(k)
-> Jbr = J2, Jbl = 0, Jbc = -N2, Mbl = 0, Mbr = M2, Nb = 0

         (  J1  0   0 )         ( M1  0 )         ( N1 )
->   J = ( -L1  I   0 )     M = ( R1  0 )     N = ( S1 )
         (  0  -N2 J2 )         (  0 M2 )         (  0 )


K := ( Ktl Ktc Ktr )     P := ( Ptl Ptr )     Q := ( Qt )
     ( Kbl Kbc Kbr )          ( Pbl Pbr )          ( Qr )

x1(k+1) = Ktl t1(k+1) + Ktc v(k) + Ktr t2(k+1) + Ptl x1(k) + Ptr x2(k) + Qt u(k)
        = K1 t1(k+1) + P1 x1(k) + Q1 u(k)
-> Ktl = K1, Ktc = 0, Ktr = 0, Ptl = P1, Ptr = 0, Qt = Q1

x2(k+1) = Kbl t1(k+1) + Kbc v(k) + Kbr t2(k+1) + Pbl x1(k) + Pbr x2(k) + Qb u(k)
        = K2 t2(k+1) + P2 x2(k) + Q2 v(k)
-> Kbl = 0, Kbc = Q2, Kbr = K2, Pbl = 0, Pbr = P2, Qb = 0

->   K = ( K1  0  0 )     P = ( P1  0 )     Q = ( Q1 )
         (  0 Q2 K2 )         (  0 P2 )         (  0 )


L := ( Ll Lc Lr )     R := ( Rl Rr )

y(k) = Ll t1(k+1) + Lc v(k) + Lr t2(k+1) + Rl x1(k) + Rr x2(k) + S u(k)
     = L2 t2(k+1) + R2 x2(k) + S2 v(k)
-> Ll = 0, Lc = S2, Lr = L2, Rl = 0, Rr = R2, S = 0

->   L = ( 0 S2 L2 )     R = ( 0 R2 )     S = 0

**)

Context {nu ny : nat}.
Context {ny1 nx1 nt1 : nat}.
Context {    nx2 nt2 : nat}.

Variable sif1 : @SIF nu ny1 nx1 nt1.
Variable sif2 : @SIF ny1 ny nx2 nt2.

Notation J1 := (SIF_J sif1).
Notation K1 := (SIF_K sif1).
Notation L1 := (SIF_L sif1).
Notation M1 := (SIF_M sif1).
Notation N1 := (SIF_N sif1).
Notation P1 := (SIF_P sif1).
Notation Q1 := (SIF_Q sif1).
Notation R1 := (SIF_R sif1).
Notation S1 := (SIF_S sif1).

Notation J2 := (SIF_J sif2).
Notation K2 := (SIF_K sif2).
Notation L2 := (SIF_L sif2).
Notation M2 := (SIF_M sif2).
Notation N2 := (SIF_N sif2).
Notation P2 := (SIF_P sif2).
Notation Q2 := (SIF_Q sif2).
Notation R2 := (SIF_R sif2).
Notation S2 := (SIF_S sif2).


Definition Jcascade := block_mx (block_mx    J1    0     (*0*)
                                           (-L1) 1%:M )    0
                                 (row_mx     0   (-N2))   J2  .


Lemma Jprop_cascade : Jprop Jcascade.
Proof.
apply: Jprop_block => //.
apply: Jprop_block => //.
exact: SIF_Jprop sif1.
exact: Jprop_1.
exact: SIF_Jprop sif2.
Qed.


Definition SIF_cascade : @SIF nu ny _ _ :=
  Build_SIF
    (* SIF_Jprop *)  Jprop_cascade
    (* SIF_K *)      (block_mx (row_mx K1  0)  0
                           (row_mx  0 Q2) K2 )
    (* SIF_L *)      (row_mx (row_mx 0 S2) L2)
    (* SIF_M *)      (col_mx (col_mx (row_mx M1  0)
                                 (row_mx R1  0) )
                                 (row_mx  0 M2)  )
    (* SIF_N *)      (col_mx (col_mx N1
                                 S1)
                                  0 )
    (* SIF_P *)      (block_mx P1  0
                            0 P2)
    (* SIF_Q *)      (col_mx Q1
                          0 )
    (* SIF_R *)      (row_mx 0 R2)
    (* SIF_S *)      0.


Notation sif := SIF_cascade.



Section SIF_cascade_x_k.


Variable u : @vsignal RT nu.
Notation x1 := (x sif1 u).
Notation v := (y sif1 u).
Notation x2 := (x sif2 v).


Lemma SIF_cascade_x_k k : x sif u k = col_mx (x1 k) (x2 k).
Proof.
have H_Jprop := SIF_Jprop sif.
move: k. apply: strongindz => k _Hk.
  rewrite !signal_causal //. symmetry; exact: col_mx0.
move => Hind. rewrite -[x sif u k]vsubmxK. f_equal.
- replace k with (k-1+1) by intlia. rewrite !x_Sk.
  rewrite mul_col_mx [SIF_P sif *m _]mul_col_mx [SIF_Q sif *m _]mul_col_mx.
  rewrite !add_col_mx col_mxKu. f_equal.
  rewrite Hind; last intlia.
  rewrite mul_row_col. simplmx. f_equal.
  rewrite !mul_col_mx !mul_row_col !add_col_mx; simplmx.
  rewrite invmx_J_block //.
  rewrite mul_block_col mul_row_col. simplmx.
  rewrite invmx_J_block.
  2:{ apply: Jprop_block => //. exact: SIF_Jprop sif1. exact: Jprop_1. }
  rewrite mul_block_col mul_row_col. by simplmx.
- replace k with (k-1+1) by intlia. rewrite !x_Sk.
  rewrite mul_col_mx [SIF_P sif *m _]mul_col_mx [SIF_Q sif *m _]mul_col_mx.
  rewrite !add_col_mx col_mxKd. simplmx.
  rewrite Hind; last intlia.
  rewrite mul_row_col. simplmx.
  rewrite -[in RHS]addrA [P2*m_+_]addrC [in RHS]addrA. f_equal.
  rewrite !mul_col_mx !mul_row_col !add_col_mx; simplmx.
  rewrite invmx_J_block //.
  rewrite mul_block_col mul_row_col. simplmx.
  rewrite invmx_J_block.
  2:{ apply: Jprop_block => //. exact: SIF_Jprop sif1. exact: Jprop_1. }
  rewrite mul_block_col mul_row_col. simplmx.
  rewrite addrC; f_equal; f_equal.
  2: by rewrite mulmxN !mulNmx opprK mul1mx y_k addrA mulmxA.
  rewrite mulmxDr addrC; f_equal.
  rewrite !mulNmx -!mulmxA -mulmxN; f_equal.
  rewrite mul_block_col mul_row_col; simplmx; rewrite !mulNmx 2!opprK.
  by rewrite y_k addrA mulmxA.
Qed.


End SIF_cascade_x_k.



Theorem SIF_cascade_preserves_filter :
  filter_from_SIF sif =
  Basics.compose (filter_from_SIF sif2) (filter_from_SIF sif1).
Proof.
have H_Jprop := SIF_Jprop sif.
apply: filter_ext => u k. rewrite /Basics.compose.
rewrite y_k SIF_cascade_x_k.
rewrite !mul_col_mx !mul_row_col !add_col_mx; simplmx.
rewrite y_k -addrA (addrC (R2 *m _)) addrA. f_equal.
rewrite invmx_J_block // mul_block_col mul_row_col; simplmx.
rewrite invmx_J_block //.
2: apply: Jprop_block => //;
     [ exact: SIF_Jprop sif1 | exact: Jprop_1 ].
rewrite mul_block_col mul_row_col. simplmx.
rewrite addrC; f_equal; f_equal.
2: by rewrite mulmxN !mulNmx opprK mul1mx y_k addrA mulmxA.
rewrite mulmxDr addrC; f_equal.
rewrite !mulNmx -!mulmxA -mulmxN; f_equal.
rewrite mul_block_col mul_row_col; simplmx; rewrite !mulNmx 2!opprK.
by rewrite y_k addrA mulmxA.
Qed.


End Cascade.







(** used in Transpose_SISO_SIF and Change_SIF **)
Create HintDb unitmx.
Hint Extern 0 (is_true (perm_mx _ \in unitmx)) => apply unitmx_perm : unitmx.
Hint Extern 3 (is_true (_ \in unitmx)) => apply unitmx_J : unitmx.
Hint Resolve SIF_Jprop : unitmx.
Hint Extern 0 (is_true (_^T \in unitmx)) => rewrite unitmx_tr : unitmx.
Hint Extern 0 (is_true (_ *m _ \in unitmx)) =>
  (rewrite unitmx_mul; apply/andP) : unitmx.





Section Transpose_SISO_SIF.


Context {nx nt : nat}.


Variable sif : @SIF 1 1 nx nt.


Notation J := (SIF_J sif).
Notation K := (SIF_K sif).
Notation L := (SIF_L sif).
Notation M := (SIF_M sif).
Notation N := (SIF_N sif).
Notation P := (SIF_P sif).
Notation Q := (SIF_Q sif).
Notation Rsif := (SIF_R sif).
Notation S := (SIF_S sif).


Definition s_rev {n} := perm.perm (@rev_ord_inj n).


Definition Jsiftr :=
row_perm s_rev (col_perm s_rev (J^T)).


Lemma Jprop_Jsiftr : Jprop Jsiftr.
Proof.
have [Heq1 Heq0] := SIF_Jprop sif.
split => [i | i j Hij].
- by rewrite !mxE.
- rewrite !mxE. apply: Heq0.
  rewrite !perm.permE !nat_of_Ordinal.
  by auton.
Qed.


Definition SIF_transpose := Build_SIF
(* SIF_Jprop *)  Jprop_Jsiftr
(* SIF_K *)      (col_perm s_rev M^T)
(* SIF_L *)      (col_perm s_rev N^T)
(* SIF_M *)      (row_perm s_rev K^T)
(* SIF_N *)      (row_perm s_rev L^T)
(* SIF_P *)      P^T
(* SIF_Q *)      Rsif^T
(* SIF_R *)      Q^T
(* SIF_S *)      S^T.


Theorem SIF_transpose_same_filter :
  filter_from_SIF SIF_transpose = filter_from_SIF sif.
Proof.
rewrite -!StateSpace_from_SIF_same_filter.
rewrite -[in RHS]StSp_transpose_same_filter.
f_equal.
rewrite /StateSpace_from_SIF /StSp_transpose /= !trmxD.
rewrite /Jsiftr !row_permE !col_permE !invmx_mul; auto with unitmx.
rewrite !mulmxA -(mulmxA M^T) -(mulmxA N^T) mulmxV ?mulmx1;
  last auto with unitmx.
rewrite -(mulmxA (M^T *m _)) -(mulmxA (N^T *m _)) mulVmx ?mulmx1;
  last auto with unitmx.
rewrite -trmx_inv -!trmx_mul !mulmxA //.
Qed.


End Transpose_SISO_SIF.






Section Change_SIF.


Context { nu ny nx nt : nat }.


Variable sif : @SIF nu ny nx nt.


Notation J := (SIF_J sif).
Notation K := (SIF_K sif).
Notation L := (SIF_L sif).
Notation M := (SIF_M sif).
Notation N := (SIF_N sif).
Notation P := (SIF_P sif).
Notation Q := (SIF_Q sif).
Notation Rsif := (SIF_R sif).
Notation S := (SIF_S sif).


(**
    (-J M N)       (Y    )   (W    )
Z = ( K P Q)  -->  (  U' ) Z (  U  )   where U' = U^(-1)
    ( L R S)       (    I)   (    I)

(Y    ) (-J M N) (W    )   ( -YJW  YMU  YN )
(  U' ) ( K P Q) (  U  ) = ( U'KW U'PU U'Q )
(    I) ( L R S) (    I)   (   LW   RU   S )
**)


Variables (Y  W : 'M[RT]_nt) (U : 'M[RT]_nx).


Hypothesis unitmx_U : U \in unitmx.


Definition Jsifchg := Y *m J *m W.
Hypothesis Jprop_Jsifchg : Jprop Jsifchg.


Definition SIF_change_base := Build_SIF
    (* SIF_Jprop *)  Jprop_Jsifchg
    (* SIF_K *)      (invmx U *m K *m W)
    (* SIF_L *)      (L *m W)
    (* SIF_M *)      (Y *m M *m U)
    (* SIF_N *)      (Y *m N)
    (* SIF_P *)      (invmx U *m P *m U)
    (* SIF_Q *)      (invmx U *m Q)
    (* SIF_R *)      (Rsif *m U)
    (* SIF_S *)      S.


Fact unitmx_Y : Y \in unitmx.
Proof.
apply: unitmx_from_unitmx_mulXmx.
apply: unitmx_J.
instantiate (1 := _ *m _).
rewrite mulmxA.
exact: Jprop_Jsifchg.
Qed.


Fact unitmx_W : W \in unitmx.
Proof.
apply: unitmx_from_unitmx_mulmxX.
apply: unitmx_J.
exact: Jprop_Jsifchg.
Qed.


Theorem change_base_SIF_same_filter :
  filter_from_SIF SIF_change_base = filter_from_SIF sif.
Proof.
rewrite -!StateSpace_from_SIF_same_filter.
rewrite /StateSpace_from_SIF /=.
have unitmx_Y := unitmx_Y.
have unitmx_W := unitmx_W.
rewrite !invmx_mul //; auto with unitmx.
rewrite -!(mulmxA _ W) !(mulmxA _ (invmx W))
  mulmxV // !mul1mx.
rewrite !(mulmxA _ _ (invmx Y)) -!(mulmxA _ (invmx Y))
  -!(mulmxA Y) !(mulmxA _ Y) mulVmx // !mul1mx.
rewrite !mulmxA -!mulmxDl -!(mulmxA (invmx U)) -!mulmxDr.
set (stsp := Build_StateSpace (_ *m M + _) _ _ _).
rewrite -/(StSp_change_base stsp U).
exact: StSp_change_base_same_filter.
Qed.


End Change_SIF.




End SIF.

