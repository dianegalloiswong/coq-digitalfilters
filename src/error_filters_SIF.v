(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import ssreflect ssrbool bigop fintype
               ssralg vector ssrint ssrnum matrix.

Require Import lia_tactics
               utils int_complements mx_complements
               signal filter SIF.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Import GRing.Theory.


Local Open Scope ring_scope.








(** The main proofs of this file rely on the following simplification
    tactics. These would not be needed anymore if there were a tactic
    similar to "ring" but for matrices of any size.
    (cf thesis manuscript section 6.2.4)
**)

Ltac strike_opposites X :=
rewrite ?addrA ?(addrC _ (-X)) ?addrA (addrC _ X) ?addrA subrr add0r.

Ltac strike_both_sides X :=
rewrite ?addrA 2?(addrC _ X) -?(addrA X); f_equal.

Ltac sopp_step X :=
match goal with
  | [ |- _ + X = _ ] => rewrite (addrC _ X)
  | [ |- _ = _ + X ] => rewrite (addrC _ X)
  | [ |- context [ (X + _) + _ ] ] => rewrite -(addrA X)
  | [ |- context [ (_ + X) + _ ] ] => rewrite (addrC _ X) -(addrA X)
  | [ |- context [ _ + (X + _) ] ] => rewrite (addrC X) (addrA _ _ X)
  | [ |- context [ _ + (_ + X) ] ] => rewrite (addrA _ _ X)
end.

Ltac sopp0 X :=
repeat sopp_step (-X);
repeat sopp_step X;
rewrite ?(addrA X (-X)) subrr ?add0r.

Tactic Notation "pull" constr(Z) "in" constr(Y) :=
ereplace Y; last (repeat sopp_step Z; reflexivity).

Tactic Notation "simpl_opp" uconstr(t) :=
let X := fresh "X" in
set X := t;
match goal with
  | [ |- context [ ?A + ?B ] ] =>
(*     idtac "trying" A "and" B; *)
    match A with
      | X =>
        match B with
          | - X => rewrite subrr ?add0r ?addr0; idtac "directly X - X"
          | context [ @GRing.opp ?T X ] =>
(*             idtac "X + context [ -X ]"; *)
            pull (@GRing.opp T X) in B;
            rewrite (addrA X (@GRing.opp T X)) subrr add0r
        end
      | context [ X ] =>
        match B with
          | - X =>
(*             idtac "context [ X ] + -X "; *)
            pull X in A;
            rewrite (addrC X) -(addrA _ X (-X)) subrr addr0
          | context [ - X ] =>
(*             idtac "context [ X ] + context [ -X ]"; *)
            pull X in A;
            pull (-X) in B;
            rewrite (addrC X) -(addrA _ X) (addrA X (-X)) subrr add0r
        end
      | context [ - X ] =>
        match B with
          | context [ X ] =>
(*             idtac "found -X in <<<" A ">>> and X in <<<" B ">>>"; *)
            idtac "unhandled branch in simpl_opp"; fail
        end
    end
  | _ => idtac "notfound simpl_opp"; fail
end;
clear X.

Ltac simpl_opp_all :=
rewrite ?(mulNmx, mulmxN);
repeat
match goal with |- context [ - ?t ] =>
  let X := fresh "X" in
  set X := t;
  match goal with
    | |- context [ X + _ ] =>
       simpl_opp X
    | |- context [ _ + X ] =>
       simpl_opp X
  end
end.





Section ErrorFiltersSIF.


Context {RT : comUnitRingType} {nu ny nx nt : nat}.


Variable sif : @SIF RT nu ny nx nt.


Notation J := (SIF_J sif).
Notation Jinv := (invmx J).
Notation K := (SIF_K sif).
Notation L := (SIF_L sif).
Notation M := (SIF_M sif).
Notation N := (SIF_N sif).
Notation P := (SIF_P sif).
Notation Q := (SIF_Q sif).
Notation Rsif := (SIF_R sif).
Notation S := (SIF_S sif).


(** H is the ideal filter corresponding to sif,
    where there is no error related to FP computations.
    We note y the image of u by this filter, and t and x the
    intermediate vectorial signals used to construct y.
 **)
Definition H : filter := filter_from_SIF sif.


Variable u : @vsignal RT nu.



(** Consider a finite precision arithmetic. The model filter usually
    cannot be computed exactly. We note t', x', and y' the computed
    versions of t, x, and y. The coefficients of matrices J, K, L, M, N,
    P, Q, R, and S of the SIF may not be representable in the chosen
    arithmetic: we note K', L', M', etc. the corresponding matrices with
    rounded coefficients (J' is a special case, chosen such that I-J'
    contains the rounded coefficients of I-J). Then, noting +. and *. the
    finite precision operations, we have:
      t'(k+1) = (I - J') *. t'(k+1) +. M' *. x'(k) +. N' *. u(k)
      x'(k+1) = K' *. t'(k+1) +. P' *. x'(k) +. Q' *. u(k)
      y'(k)   = L' *. t'(k+1) +. R' *. x'(k) +. S' *. u(k)
    We use signals eps_t, eps_x, and eps_y to represent computation errors:
      J' t'(k+1) = M' x'(k) + N' u(k) + eps_t(k)
      x'(k+1) = K' t'(k+1) + P' x'(k) + Q' u(k) + eps_x(n)
      y'(k)   = L' t'(k+1) + R' x'(k) + S' u(k) + eps_y(n)
 **)


Variable eps_t : @vsignal RT nt.
Variable eps_x : @vsignal RT nx.
Variable eps_y : @vsignal RT ny.
Definition eps := (col_signal (col_signal eps_t eps_x) eps_y).


Variable J' : 'M[RT]_nt.
Hypothesis Jprop_J' : Jprop J'.
Variable K' : 'M[RT]_(nx, nt).
Variable L' : 'M[RT]_(ny, nt).
Variable M' : 'M[RT]_(nt, nx).
Variable N' : 'M[RT]_(nt, nu).
Variable P' : 'M[RT]_nx.
Variable Q' : 'M[RT]_(nx, nu).
Variable R' : 'M[RT]_(ny, nx).
Variable S' : 'M[RT]_(ny, nu).




(** computes t'(k+1) from k and x'(k) **)
Definition compute_t'S k x'k :=
invmx J' *m (M' *m x'k + N' *m u k + eps_t k).


Definition x'_fIS k valk :=
K' *m compute_t'S k valk + P' *m valk + Q' *m u k + eps_x k.
Definition x' := signal_peanorec x'_fIS.


Fact y'_causal : causal (fun k =>
  L' *m compute_t'S k (x' k) + R' *m x' k + S' *m u k + eps_y k).
Proof.
move => k _Hk.
rewrite /compute_t'S !signal_causal //.
by simplmx.
Qed.
Definition y' := Build_signal y'_causal.



Notation blockDop A A' := (block_mx      A    0
                                      (A'-A)  A' ).


Lemma Jprop_Dop : Jprop (blockDop J J').
Proof.
apply: Jprop_block => //.
exact: SIF_Jprop.
Qed.


Definition sif_Dc := Build_SIF
(* SIF_Jprop *)  Jprop_Dop
(* SIF_K *)      (blockDop K K')
(* SIF_L *)      (row_mx (L'-L) L')
(* SIF_M *)      (blockDop M M')
(* SIF_N *)      (col_mx N (N'-N))
(* SIF_P *)      (blockDop P P')
(* SIF_Q *)      (col_mx Q (Q'-Q))
(* SIF_R *)      (row_mx (R'-Rsif) R')
(* SIF_S *)      (S'-S).


Definition H_Dc := filter_from_SIF sif_Dc.


Definition sif_Dop := Build_SIF
(* SIF_Jprop *)  Jprop_J'
(* SIF_K *)      K'
(* SIF_L *)      L'
(* SIF_M *)      M'
(* SIF_N *)      (row_mx (row_mx 1%:M 0) 0)
(* SIF_P *)      P'
(* SIF_Q *)      (row_mx (row_mx 0 1%:M) 0)
(* SIF_R *)      R'
(* SIF_S *)      (row_mx (row_mx 0 0) 1%:M).


Definition H_Dop : filter := filter_from_SIF sif_Dop.


Lemma usub_x_Dc (k : int) :
  usubmx (SIF.x sif_Dc u k) = SIF.x sif u k.
Proof.
move: k; apply: peanoindz => k _Hk.
  rewrite /= !peanoreczBC //.
  exact: usubmx0.
move => IH.
rewrite !x_Sk !usubmxD !mul_col_mx !col_mxKu.
f_equal; f_equal.
  rewrite invmx_J_block; last first.
    exact: Jprop_Dop.
  rewrite add_col_mx mul_block_col mul_row_col.
  simplmx.
all: rewrite -(vsubmxK (x sif_Dc _ _)) !mul_row_col;
     simplmx;
     by rewrite IH.
Qed.


Lemma x'_prop :
  x' = x sif u + SIF.x sif_Dop eps + dsubsignal (SIF.x sif_Dc u).
Proof.
apply/signalP; apply: peanoindz => k _Hk.
  rewrite /= !peanoreczBC //; simplmx.
  by rewrite dsubmx0.
move => IH.
rewrite signal_peanorecIS //; fold x'.
rewrite 2![in RHS]signalE.
rewrite [in (dsubsignal _ (k+1))]signalE.
rewrite (x_Sk sif_Dc u).
rewrite !dsubmxD !mul_col_mx !col_mxKd.
rewrite invmx_J_block. 2: exact: Jprop_Dop.
rewrite add_col_mx mul_block_col mul_row_col. simplmx.
rewrite -(vsubmxK (x sif_Dc _ _)) !mul_row_col; simplmx.
rewrite usub_x_Dc.
rewrite !(mulmxDl, mulmxDr).
rewrite !(mulNmx, mulmxN).
rewrite !opprK.
rewrite mulVmx; last exact: unitmx_J.
rewrite -(mulmxA _ J) mulmxV; last (apply: unitmx_J; exact: SIF_Jprop).
rewrite mul1mx mulmx1.
simpl_opp_all.
rewrite (x_Sk sif u) !mulmxDr.
simpl_opp_all.
rewrite /x'_fIS IH 2!signalE.
strike_both_sides (Q' *m u k).
rewrite !mulmxDr.
strike_both_sides (P' *m x sif u k).
strike_both_sides (P' *m dsubmx ((x sif_Dc u) k)).
rewrite /compute_t'S !mulmxDr.
strike_both_sides (K' *m (invmx J' *m (M' *m (x sif u) k))).
strike_both_sides (K' *m (invmx J' *m (N' *m u k))).
strike_both_sides (K' *m (invmx J' *m (M' *m dsubmx ((x sif_Dc u) k)))).
rewrite (x_Sk sif_Dop eps); simpl (_ sif_Dop).
strike_both_sides (P' *m x sif_Dop eps k).
rewrite -(vsubmxK (eps k)) -(vsubmxK (usubmx _)) !mul_row_col; simplmx.
rewrite !col_mxKu col_mxKd.
by rewrite !mulmxDr.
Qed.


Theorem error_filters : y' - H u = H_Dop eps + H_Dc u.
Proof.
rewrite -(addrK (H u) (H_Dop eps + H_Dc u)); f_equal.
rewrite [RHS]addrC (addrC (H_Dop _)) addrA.
apply/signalP => k.
rewrite signalE 2![in RHS]signalE !y_k mulmxDl.
simpl (_ sif_Dop).
strike_both_sides (S' *m u k).
rewrite mulNmx.
simpl_opp_all.
rewrite -(vsubmxK (eps k)) !mul_row_col row_mx0.
rewrite -(vsubmxK (usubmx _)) mul_row_col.
simplmx.
rewrite !col_mxKu col_mxKd.
strike_both_sides (eps_y k).
rewrite -(vsubmxK (x sif_Dc _ _)) usub_x_Dc.
rewrite mul_row_col mulmxDl mulNmx.
simpl_opp_all.
rewrite x'_prop 2!signalE.
rewrite !(mulmxDr R').
strike_both_sides (R' *m x sif u k).
strike_both_sides (R' *m x sif_Dop eps k).
strike_both_sides (R' *m dsubmx (x sif_Dc u k)).
rewrite /compute_t'S ![in LHS]mulmxDr.
rewrite (mulmxDr (invmx J')) (mulmxDr L').
strike_both_sides (L' *m (invmx J' *m eps_t k)).
rewrite [X in X+_+_]addrC -![in LHS]addrA addrC.
f_equal.
rewrite invmx_J_block; last first.
  apply: Jprop_block => //.
  exact: SIF_Jprop.
rewrite !mul_block_col.
simplmx.
rewrite (mul_col_mx _ _ (u k)) add_col_mx mul_block_col mul_row_col.
simplmx.
rewrite mulmxDl mulNmx.
simpl_opp (L *m (Jinv *m (M *m (x sif u) k + N *m u k))).
rewrite -!mulmxDr.
f_equal.
rewrite [-_*m_]mulmxDr !mulmxDl.
repeat rewrite ?mulNmx ?mulmxN.
rewrite opprK mulVmx.
  rewrite -(mulmxA _ J) mulmxV.
    2,3: by apply: unitmx_J => //; exact: SIF_Jprop.
simplmx.
rewrite !addrA subrr add0r -!mulmxDr.
f_equal.
simpl_opp_all.
by rewrite mulmxDr.
Qed.


End ErrorFiltersSIF.

