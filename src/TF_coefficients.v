(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import all_ssreflect ssralg vector ssrint.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Import GRing.Theory.

Require Import lia_tactics utils
               int_complements sum_complements signal filter.

Local Open Scope R.







Section TF_coefficients.



Context {RT : comRingType} {VT : vectType RT}.






(** Coefficients of the Transfer Function of a filter:
      (n, a, b) such that
        H(z) = sum_{0<=i<=n} b_i z^{-i} / (1 + sum_{1<=i<=n} a_i z^{-i}).
    n is the order of the filter.
    Note: a and b are defined as total functions because these are easier
    to handle, but in practice we will only use a_i for 1 <= i <= n and
    b_i for 0 <= i <= n.
**)
Record TFCoeffs := { TFC_n : nat ; TFC_a : nat -> RT ; TFC_b : nat -> RT }.



Variable tfc : TFCoeffs.

Notation n := (TFC_n tfc).
Notation a := (TFC_a tfc).
Notation b := (TFC_b tfc).



(** Construction of a filter from TF Coefficients (n, a, b) by
    Direct Form I:
      y(k) = sum_{0<=i<=n} b_i u(k-i) - sum_{1<=i<=n} a_i y(k-i).
**)
Section DFI.



Definition DFI_fIS (u : @signal RT VT) : int -> (int -> VT) -> VT :=
  fun (k : int) (y_before_k : int -> VT) =>
    \sum_(0 <= i < n.+1) b i *: u (k - (i : int)) -
    \sum_(1 <= i < n.+1) a i *: y_before_k (k - (i : int)).


Lemma DFI_fIS_safe_and_causal u :
  signal_strongrec_safe_and_causal (DFI_fIS u).
Proof.
split.
  move => m g1 g2 _H H_g1_g2; rewrite /DFI_fIS.
  f_equal; f_equal.
  apply: eq_big_nat => i /andP [Hi _].
  f_equal.
  apply: H_g1_g2.
  by autoz.
move => m g _Hm Hg; rewrite /DFI_fIS.
 rewrite !sum_nat_all0 ?subr0 // => i Hi.
- rewrite Hg; last by autoz.
  by simplr.
- rewrite signal_causal; last by autoz.
  by simplr.
Qed.



(** filter (function signal -> signal) built from
    TF coefficients using DFI, which is the canonical
    way to build a filter from TF coefficients        **)
Definition filter_from_TFC : filter :=
  fun (u : signal) => signal_strongrec (DFI_fIS u).




Lemma filter_from_TFC_verifies_DFI (u : signal) (k : int) :
  filter_from_TFC u k = DFI_fIS u k (filter_from_TFC u).
Proof.
apply signal_strongrec_k.
apply DFI_fIS_safe_and_causal.
Qed.




Section filter_from_TFC_is_LTI.
(** not necessary now that we have shown how to build a State-Space
    from TFCoeffs so that the filters obtained directly from TFC
    and from the corresponding state space are equal, and we have
    proved that filters obtained from state spaces are LTI.
    But we keep this as it is a good way to detect problems
    if we change the definitions. **)



Lemma filter_from_TFC_compat_add : filter_compat_add filter_from_TFC.
Proof.
move => u1 u2.
apply/signalP=> k.
move: k; apply strongindz => k Hk.
  by rewrite !signal_causal.
move => IH.
rewrite [in RHS]signalE !filter_from_TFC_verifies_DFI /DFI_fIS.
erewrite eq_big_nat.
  erewrite sum_nat_add.
  erewrite (@eq_big_nat _ _ _ 1).
    erewrite (sum_nat_add 1).
    * all: cycle 1.
    move => i i_bnd.
    rewrite IH; last autoz.
    rewrite scalerDr.
    by f_equal; reflexivity.
  move => i i_bnd /=.
  rewrite scalerDr.
  by f_equal; reflexivity.
rewrite opprD !addrA.
f_equal.
rewrite -!addrA.
f_equal.
exact: addrC.
Qed.


Lemma filter_from_TFC_compat_scale : filter_compat_scale filter_from_TFC.
Proof.
move => c u.
apply/signalP=> k.
move: k; apply strongindz => k Hk.
  by rewrite !signal_causal.
move => Hind.
rewrite [in RHS]signalE !filter_from_TFC_verifies_DFI /DFI_fIS.
rewrite scalerBr !scaler_sumr.
f_equal; last f_equal; apply: eq_big_nat => i /andP Hi.
  rewrite /scales /=.
  by rewrite scalerC.
rewrite Hind; last by autoz.
by rewrite scalerC.
Qed.


Lemma filter_from_TFC_compat_delay : filter_compat_delay filter_from_TFC.
Proof.
move => K u K_ge_0.
apply/signalP=> k.
move: k; apply strongindz => k Hk.
  by rewrite !signal_causal.
move => Hind. rewrite delayE // !filter_from_TFC_verifies_DFI /DFI_fIS.
f_equal; last f_equal; apply eq_big_nat => i /andP Hi.
  2: rewrite Hind; last autoz.
  all: rewrite delayE //.
  all: f_equal; f_equal.
  all: ringz.
Qed.


Theorem LTI_filter_from_TFC : LTI_filter filter_from_TFC.
Proof.
repeat split;
[   apply filter_from_TFC_compat_add
  | apply filter_from_TFC_compat_scale
  | apply filter_from_TFC_compat_delay ].
Qed.



End filter_from_TFC_is_LTI.




End DFI.














(** Construction of a filter from TF Coefficients (n, a, b) by
    Direct Form II:
      e(n) = u(n) - sum_{1<=i<=n} a_i e(n-i)
      y(n) = sum_{0<=i<=n} b_i e(n-i).
    Equivalence with DFI.
**)
Section DFII.



Definition DFII_u2e_fIS (u : signal) :=
  fun (k : int) (e_before_k : int -> VT) =>
    u k - \sum_(1 <= i < n.+1) a i *: e_before_k (k-(i:int)).


Definition DFII_u2e u := signal_strongrec (DFII_u2e_fIS u).


Lemma DFII_u2e_fIS_safe_and_causal u : 
  signal_strongrec_safe_and_causal (DFII_u2e_fIS u).
Proof.
rewrite /DFII_u2e_fIS.
split.
- move => k g1 g2 _Hk Hg.
  f_equal. f_equal.
  apply: eq_big_nat => i /andP Hi.
  rewrite Hg //.
  by autoz.
- move => m g _Hm Hg.
  rewrite signal_causal // sum_nat_all0.
    by simplr.
  move => i /andP Hi.
  rewrite Hg; last by autoz.
  by simplr.
Qed.



Fact DFII_e2y_causal (e : @signal RT VT) :
  causal (fun k => \sum_(0 <= i < n.+1) (TFC_b tfc i) *: e (k-(i:int))).
Proof.
move => k _Hk. apply: sum_nat_all0 => i /andP Hi.
rewrite signal_causal; last by autoz.
  by simplr.
Qed.


Definition DFII_e2y (e : signal) := Build_signal (DFII_e2y_causal e).


Definition filter_by_DFII : filter :=
  fun (u : signal) => DFII_e2y (DFII_u2e u).





Variable u : @signal RT VT.
Notation e := (DFII_u2e u).


(** useful to rewrite e without getting
    too lengthy an expression in the sum **)
Lemma e_val k :
  e k = u k - \sum_(1 <= i < n.+1) a i *: e (k-(i:int)).
Proof.
apply: signal_strongrec_k.
exact: DFII_u2e_fIS_safe_and_causal.
Qed.


End DFII.






Section DFI_DFII.


Lemma DFII_implies_DFI (u e y : signal) :
  (forall k, e k = DFII_u2e_fIS u k e) ->
  (forall k, y k = DFII_e2y e k) ->
  (forall k, y k = DFI_fIS u k y).
Proof.
move => He HDFII k. rewrite HDFII.
etransitivity.
  apply: eq_big_nat => i _.
  rewrite He /DFII_u2e_fIS scalerBr scaler_sumr.
  reflexivity.
rewrite /DFI_fIS sum_nat_sub.
f_equal. f_equal.
rewrite exchange_big_nat.
apply: eq_big_nat => i _Hi.
rewrite HDFII /DFII_e2y signalE scaler_sumr.
apply: eq_big_nat => j _.
rewrite scalerC. f_equal; f_equal; f_equal. ringz.
Qed.


Lemma DFI_implies_DFII (u e y : signal) :
  (forall k, y k = DFI_fIS u k y) -> 
  (forall k, e k = DFII_u2e_fIS u k e) ->
  (forall k, y k = DFII_e2y e k).
Proof.
intros HDFI He.
apply strongindz.
  intros; by rewrite !signal_causal.
intros; rewrite HDFI.
rewrite (@DFII_implies_DFI u e (DFII_e2y e)) //.
apply DFI_fIS_safe_and_causal; assumption.
Qed.


Theorem DFI_DFII_same_filter :
  filter_from_TFC = filter_by_DFII.
Proof.
apply filter_ext => u k.
symmetry. apply signal_strongrec_x_k.
  exact: DFI_fIS_safe_and_causal.
intro m. rewrite /filter_by_DFII. set (e := DFII_u2e _).
apply DFII_implies_DFI with (e := e) => // n.
apply signal_strongrec_k.
apply DFII_u2e_fIS_safe_and_causal.
Qed.


End DFI_DFII.



End TF_coefficients.

