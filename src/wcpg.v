(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import all_ssreflect ssralg vector ssrint ssrnum matrix.

Require Import Reals Lra.

From Coquelicot
Require Import Lim_seq Coquelicot.

Require Import Rstruct lia_tactics
               utils nat_ord_complements int_complements
               sum_complements mx_complements
               signal filter State_Space
               R_complements Rbar_complements Lim_seq_complements.



Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Import GRing.Theory.


Local Open Scope ring_scope.
Local Open Scope R_scope.



Notation R := real_field.


Notation boundedby x M := (forall k, Rabs (x k) <= M)%R.
Notation boundedby1 x := (boundedby x 1).




Section SISO_wcpg.


Implicit Types k K : int.
Implicit Types u : @scsignal R.


Variable H : @SISO_filter R.


Hypothesis LTI : LTI_filter H.


Notation h := (impulse_response H).


Definition sum_abs_IR (n : nat) :=
  \sum_(0 <= i < n.+1) Rabs (impulse_response H i).



Definition wcpg := Lim_seq sum_abs_IR.



Lemma sum_abs_IR_increasing (n : nat) :
  (sum_abs_IR n <= sum_abs_IR (S n))%R.
Proof.
rewrite /sum_abs_IR (big_nat_recr n.+1) // -[X in (X <= _)%R]Rplus_0_r.
apply: Rplus_le_compat_l.
exact: Rabs_pos.
Qed.


Lemma ex_lim_sum_abs_IR :
  ex_lim_seq sum_abs_IR.
Proof.
apply: ex_lim_seq_incr. exact: sum_abs_IR_increasing.
Qed.


Lemma wcpg_is_lim_seq : is_lim_seq sum_abs_IR wcpg.
Proof.
apply: Lim_seq_correct. exact: ex_lim_sum_abs_IR.
Qed.

Lemma IR0_ge0 : (0 <= sum_abs_IR 0)%R.
Proof.
rewrite /sum_abs_IR big_nat1 /=.
exact: Rabs_pos.
Qed.


Lemma wcpg_ge0 : Rbar_le 0%R wcpg.
Proof.
eapply Rbar_le_trans; last first.
  apply seq_increasing_le_Lim.
  exact: sum_abs_IR_increasing.
exact: IR0_ge0.
Qed.






(** Special case: wcpg is null. Then, output y is also null. **)
Section wcpg0.

Hypothesis wcpg0 : wcpg = 0%R.

Lemma wcpg0_sum_abs_IR_0 (n : nat) : sum_abs_IR n = 0%R.
Proof.
apply: Rle_antisym.
  apply: Rbar_le_Rle.
  rewrite -wcpg0.
  apply: seq_increasing_le_Lim.
  exact: sum_abs_IR_increasing.
apply: Rle_trans; last first.
  exact: sum_nat_Rabs.
exact: Rabs_pos.
Qed.

Lemma wcpg0_IR0 : wcpg = R0 -> impulse_response H = signal0.
Proof.
intro. apply/signalP_ge0 => k _Hk /=.
apply: Rabs0_r0.
rewrite -(Posz_nat_of_int k) //.
generalize (nat_of_int k).
apply: (well_founded_induction lt_wf).
move=> n IH.
rewrite -(wcpg0_sum_abs_IR_0 n).
rewrite /sum_abs_IR big_nat_recr //.
rewrite sum_nat_all0 /=.
  by simplr.
move=> *; apply: IH.
by apply/leP.
Qed.

Lemma wcpg0_y0 : forall u, H u = signal0.
Proof.
intro. apply/signalP => k.
rewrite convolution_impulse_response //=.
apply: sum_nat_all0 => i _Hi.
rewrite wcpg0_IR0 //. by simplmx.
Qed.

End wcpg0.



Theorem wcpg_theorem_realM (u : scsignal) (M : R) :
  (forall k : int, Rabs (u k) <= M)%R ->
  (forall k : int, Rbar_le (Rabs (H u k)) (Rbar_mult wcpg M)).
Proof.
move => HuB k.
have HM : (0 <= M)%R.
  apply: Rle_trans. 2: exact: (HuB 0%RT). exact: Rabs_pos.
case: (Num.Theory.ltrP k 0) => _Hcase.
- rewrite signal_causal // Rabs_R0. apply: Rbar_mult_both_ge_0.
  exact: wcpg_ge0. exact: HM.
- rewrite convolution_impulse_response // signalE.
  eapply Rbar_le_trans.
  2:{ rewrite -Lim_seq_scal_r. apply seq_increasing_le_Lim => n.
      apply Rmult_le_compat_r. exact: HM. exact: sum_abs_IR_increasing. }
  rewrite /Rbar_le. change Rmult with (@GRing.mul (GRing.Field.ringType real_field)).
  rewrite GRing.mulr_suml.
  eapply Rle_trans.
    apply sum_nat_Rabs.
  rewrite -S_nat_of_int // addn1.
  apply: sum_nat_Rle => i _. rewrite Rabs_mult.
  apply Rmult_le_compat_l. exact: Rabs_pos. exact: HuB.
Qed.


Theorem wcpg_theorem (u : scsignal) (M : Rbar) :
  (forall k : int, Rbar_le (Rabs (u k)) M) ->
  (forall k : int, Rbar_le (Rabs (H u k)) (Rbar_mult wcpg M)).
Proof.
case: M.
- move => r HuB k. apply: wcpg_theorem_realM => k'. exact: HuB.
- move => _ k. case: (Rbar_eq_dec wcpg R0) => Hcase.
  + rewrite wcpg0_y0 // Hcase Rbar_mult_0_l Rabs_R0. apply: Rle_refl.
  + rewrite Rbar_mult_pos_pinfty //. apply: Rbar_le_neq_lt.
    exact: wcpg_ge0. auto.
- move => m_infty_ge.
  by contradict (m_infty_ge 0%RT).
Qed.




Section Optimality.


Lemma Rabs_sign_le1 (r : R) : (Rabs (sign r) <= 1)%R.
Proof.
case: (Rlt_le_dec r 0) => r_rel0;
      last case: (Rle_lt_or_eq_dec 0 r r_rel0) => r_rel0'.
- rewrite sign_eq_m1 // Rabs_m1; lra.
- rewrite sign_eq_1 // Rabs_R1; lra.
- rewrite -r_rel0' sign_0 Rabs_R0; lra.
Qed.


Definition sign_IR_rev0N (N : nat) :=
  @signal_of_fun _ (regular_vectType R)
    (fun k : int => sign (impulse_response H ((N:int) - k)%RT)).


Lemma Rabs_sign_IR_rev0N_le1 (N : nat) (k : int) :
  (Rabs (sign_IR_rev0N N k) <= 1)%R.
Proof.
simpl. case: ifP; intro.
- intros; rewrite Rabs_R0.
  by lra.
- exact: Rabs_sign_le1.
Qed.


Lemma mul_r_signr (r : R) : (r * sign r)%R = Rabs r.
Proof.
case: (Rlt_le_dec r 0) => r_rel0;
    last case: (Rle_lt_or_eq_dec 0 r r_rel0) => r_rel0'.
- rewrite sign_eq_m1 // Rabs_left //. ring.
- rewrite sign_eq_1 // Rabs_right. ring. lra.
- rewrite -r_rel0' sign_0 Rabs_R0. ring.
Qed.


Lemma H_sign_IR_rev0nindexN (N : nat) :
  (H (sign_IR_rev0N N)) N =
  \sum_(0 <= i < N.+1) Rabs ((impulse_response H) i).
Proof.
rewrite convolution_impulse_response //.
rewrite signalE.
apply: eq_big_nat_eqbounds => //.
  by auton.
move=> i _Hi; rewrite /= ifF; last first.
  by autoz.
ringz_simplify (_ - _)%RT.
exact: mul_r_signr.
Qed.


Theorem wcpg_optimal (M : R) :
  Rbar_lt M wcpg ->
  exists (u : scsignal) N,
  (forall k : int, Rabs (u k) <= 1)%R /\ (H u N > M)%R.
Proof.
move=> /(lim_seq_exceed_once wcpg_is_lim_seq) [N atN].
exists (sign_IR_rev0N N), N.
split.
  exact: Rabs_sign_IR_rev0N_le1.
rewrite H_sign_IR_rev0nindexN.
apply: atN.
Qed.


End Optimality.






Section Reachability.



Section Signal_copy_both.

Context  {RT : comRingType} {VT : vectType RT}
         (K : int) (x1 x2 : @signal RT VT).

Fact signal_copy_both_causal :
  causal (fun k => if (k <= K)%int then x1 k else x2 k).
Proof.
move=> k k_lt0.
by case: ifP => intros; rewrite signal_causal.
Qed.
Definition signal_copy_both :=
  Build_signal signal_copy_both_causal.

Lemma signal_copy_both_Prop (P : VT -> Prop) :
  (forall k, P (x1 k)) -> (forall k, P (x2 k)) ->
  forall k, P (signal_copy_both k).
Proof.
move=> *; rewrite signalE.
by case: ifP.
Qed.

Lemma signal_copy_both_left k :
  (k <= K)%int -> signal_copy_both k = x1 k.
Proof.
intro; rewrite signalE; case: ifP => //.
by move=> /negP.
Qed.

Lemma signal_copy_both_right k :
  (K < k)%int -> signal_copy_both k = x2 k.
Proof.
intro; rewrite signalE; case: ifP => //.
by intlia.
Qed.

End Signal_copy_both.




Lemma convol_sign_IR_shifted N1 N2 :
  (N1 <= N2)%nat ->
  (\sum_(N1 <= i < N2.+1)
     sign (impulse_response H ((N2:int) - (i:int))) *
     impulse_response H ((N2:int) - (i:int)))%RT = sum_abs_IR (N2 - N1).
Proof.
move=> N_le.
erewrite eq_big_nat; last first.
intros.
rewrite mulrC.
replace (@GRing.mul (GRing.ComRing.ringType real_comringType)) with Rmult by by [].
by rewrite mul_r_signr.
rewrite (big_nat_shiftDown N1) //.
rewrite subnn.
rewrite big_nat_rev /=.
apply: eq_big_nat_eqbounds => //.
  by ssrnatlia.
intros; repeat f_equal.
by autoz.
Qed.


Fact sum_abs_IR_le_wcpg n :
  is_finite wcpg -> sum_abs_IR n <= wcpg.
Proof.
move=> isf.
apply: is_lim_seq_incr_compare.
  rewrite isf.
  exact: wcpg_is_lim_seq.
exact: sum_abs_IR_increasing.
Qed.


Lemma partial_convol_boundedby1_IR u (N1 N2 : nat) :
  is_finite wcpg -> boundedby1 u -> (N1 < N2)%nat ->
  Rabs (\sum_(0 <= i < N1.+1) u i * h ((N2:int) - (i:int))) <=
    wcpg - sum_abs_IR (N2 - N1).-1.
Proof.
move=> *.
apply: Rle_trans.
  exact: sum_nat_Rabs.
apply: Rle_trans.
  apply: sum_nat_Rle => i i_bnd.
  rewrite Rabs_mult.
  apply: Rmult_le_compat_r => //.
  exact: Rabs_pos.
rewrite sum_nat_rev.
rewrite (sum_nat_shiftUp (N2 - N1)).
rewrite add0n.
replace (_ + _)%nat with N2.+1 by ssrnatlia.
erewrite eq_big_nat; last first.
  move=> i i_bnd.
  rewrite Rmult_1_l.
  replace (_ - _)%int with (i : int) by autoz.
  by [].
rewrite Rle_subRL Rplus_comm.
rewrite /sum_abs_IR.
rewrite prednK; last by ssrnatlia.
replace Rplus with (@GRing.add R) by by [].
rewrite -sum_nat_cat //; last first.
  by ssrnatlia.
exact: sum_abs_IR_le_wcpg.
Qed.


Fact sum_abs_IR_ge0 n : 0 <= sum_abs_IR n.
Proof.
apply: Rle_trans.
  exact: IR0_ge0.
exact: (increasing_nseq_le sum_abs_IR_increasing).
Qed.



Record exceed_witness := {
  wit_u : scsignal ;
  wit_N : nat ;
  wit_M : R ;
  wit_bnd : boundedby1 wit_u ;
  wit_exceed : wit_M < Rabs (H wit_u wit_N)
}.

Lemma extend_exceed_witness (wit0 : exceed_witness) (M : R) :
  Rbar_lt M wcpg ->
  { wit : exceed_witness |
      wit_M wit = M /\ (wit_N wit0 < wit_N wit)%nat /\
      (forall k, (k <= wit_N wit0)%int -> wit_u wit k = wit_u wit0 k) }.
Proof.
move=> M_lt.
pose M' := match wcpg with
             | Finite wcpgR => (M + wcpgR) / 2
             | _ => 2 * M
           end.
have: Rbar_lt M' wcpg.
  subst M'; move: M_lt => /=.
  case: wcpg => //.
  by intro; lra.
move=> /(lim_seq_exceed_once_sig wcpg_is_lim_seq) [N' exceed_M'].
pose N0 := wit_N wit0.
pose N := (N0 + N' + 1)%nat.
pose u0 := wit_u wit0.
pose u :=
  signal_copy_both N0 u0
    (if Rbar_eq_dec wcpg p_infty &&
        Rgt_ge_dec (Rabs (\sum_(0 <= i < N0.+1)
                            u0 i * h ((N:int) - (i:int)))%int)
                   M
     then signal0 else sign_IR_rev0N N).
have u_bnd : boundedby1 u.
  apply: (@signal_copy_both_Prop _ (regular_vectType R) _ _ _
            (fun r => Rabs r <= 1)).
    exact: wit_bnd.
  case: ifP => _.
    move=> /= _.
    by rewrite Rabs_R0; lra.
  exact: Rabs_sign_IR_rev0N_le1.
have exceed_M : M < Rabs (H u N).
  rewrite convolution_impulse_response //.
  rewrite convolsC signalE.
  rewrite (@big_cat_nat _ _ _ N0.+1) //=; last first.
    by subst N; ssrnatlia.
  case: ifP.
    move=> /andP [].
    case: Rbar_eq_dec => // wcpg_infty _.
    case: Rgt_ge_dec => // gtM _.
    rewrite [X in (_ + X)%RT]sum_nat_all0; last first.
      move=> *; rewrite ifF.
        exact: mul0r.
      by autoz.
    simplr.
    erewrite eq_big_nat.
      exact: gtM.
    by move=> *; rewrite ifT //.
  move=> case_hyp.
  erewrite eq_big_nat; last first.
    by move=> *; rewrite ifT //.
  erewrite (@eq_big_nat _ _ _ N0.+1); last first.
    move=> *; rewrite ifF //.
    by autoz.
  apply: Rlt_gt.
  rewrite addrC.
  apply: Rlt_le_trans; last first.
  rewrite -[X in (_ + X)%RT]opprK.
  apply: Rabs_triang_inv.
  rewrite Rabs_Ropp.
  rewrite addn1.
  rewrite convol_sign_IR_shifted; last first.
    by subst N; ssrnatlia.
  rewrite Rlt_subRL.
  apply: Rle_lt_trans; last first.
    subst N.
    replace (_ - _)%nat with N' by ssrnatlia.
    rewrite Rabs_right.
      exact: exceed_M'.
    apply: Rle_ge.
    exact: sum_abs_IR_ge0.
  rewrite Rplus_comm -Rle_subRL.
  move: case_hyp M_lt exceed_M'.
  subst M'.
  case_eq wcpg => //=.
  - move=> wcpgR wcpg_eq _ M_lt exceed_M'.
    apply: Rle_trans.
      apply: partial_convol_boundedby1_IR => //.
      + by rewrite wcpg_eq.
      + exact: wit_bnd.
      + by subst N; ssrnatlia.
    replace _.-1 with N' by (subst N; ssrnatlia).
    rewrite wcpg_eq /=.
    by lra.
  - move=> _ /negP /negP /nandP.
    case=> //.
      by case: Rbar_eq_dec.
    case: Rgt_ge_dec => // M_ge _ _ _.
    rewrite double /Rminus Rplus_assoc Rplus_opp_r Rplus_0_r.
    exact: Rge_le.
exists (Build_exceed_witness u_bnd exceed_M) => /=.
repeat split => //.
  by subst N; ssrnatlia.
exact: signal_copy_both_left.
Qed.




(** sequence of limit r : R with all elements < r **)
Section Approach_under.

Variable r : R.

Notation q := (/ 2).

Definition approach_under := fun n => r - q ^ n.

Lemma approach_under_lim : is_lim_seq approach_under r.
Proof.
rewrite -(Rminus_0_r r).
apply: is_lim_seq_minus'.
  exact: is_lim_seq_const.
apply: is_lim_seq_geom.
by rewrite Rabs_right; lra.
Qed.

Lemma approach_under_lt_lim n : approach_under n < r.
Proof.
rewrite /approach_under.
have := (pow_lt q n).
by lra.
Qed.

End Approach_under.


Definition Mseq : nat -> R :=
  if wcpg is p_infty then INR else approach_under wcpg.


Lemma lim_Mseq : is_lim_seq Mseq wcpg.
Proof.
rewrite /Mseq.
have:= wcpg_ge0; case: wcpg => // [wcpgR|] _.
- exact: approach_under_lim.
- exact: is_lim_seq_INR.
Qed.


Lemma Mseq_lt_wcpg n : Rbar_lt (Mseq n) wcpg.
Proof.
rewrite /Mseq.
have:= wcpg_ge0; case: wcpg => // wcpgR _.
exact: approach_under_lt_lim.
Qed.


Lemma boundedby1_signal0 : boundedby1 (signal0 : scsignal).
Proof.
move=> /= _.
by rewrite Rabs_R0; lra.
Qed.


Fact wit0_exceed : -1 < Rabs (H signal0 O).
Proof.
apply: Rlt_le_trans; last first.
  exact: Rabs_pos.
by lra.
Qed.
Definition wit0 :=
  Build_exceed_witness boundedby1_signal0 wit0_exceed.


Definition exceed_witness_seq : nat -> exceed_witness :=
  nat_rect _ wit0 (fun n witn =>
      sval (extend_exceed_witness witn (Mseq_lt_wcpg n.+1))).


Fact u_reachability_causal :
  causal (fun k => wit_u (exceed_witness_seq k) k).
Proof.
by case.
Qed.
Definition u_reachability := Build_signal u_reachability_causal.

Notation u := u_reachability.


Lemma exceed_witness_seq_props n :
  let witn := exceed_witness_seq n in
  let witSn := exceed_witness_seq n.+1 in
  wit_M witSn = Mseq n.+1 /\
  (wit_N witn < wit_N witSn)%nat /\
  (forall k : int, (k <= wit_N witn)%int -> wit_u witSn k = wit_u witn k).
Proof.
by have := proj2_sig
  (extend_exceed_witness (exceed_witness_seq n) (Mseq_lt_wcpg n.+1)).
Qed.


Lemma N_seq_increasing n :
  (wit_N (exceed_witness_seq n) < wit_N (exceed_witness_seq n.+1))%nat.
Proof.
have := exceed_witness_seq_props n.
by simplP.
Qed.


Lemma N_seq_le n1 n2 :
  (n1 <= n2)%nat ->
  (wit_N (exceed_witness_seq n1) <= wit_N (exceed_witness_seq n2))%nat.
Proof.
move: n2; apply: (ind_basis n1) => n2.
  by move=> *; replace n2 with n1 by ssrnatlia.
move=> n_le IH _.
have {} IH := IH n_le.
have := N_seq_increasing n2.
by ssrnatlia.
Qed.


Lemma exceed_witness_seq_eq n1 n2 k :
  (k <= wit_N (exceed_witness_seq n1))%int ->
  (k <= wit_N (exceed_witness_seq n2))%int ->
  wit_u (exceed_witness_seq n1) k = wit_u (exceed_witness_seq n2) k.
Proof.
wlog: n1 n2 / (n1 <= n2)%nat.
  case_bool (n1 <= n2)%nat.
    by auto.
  move=> n_le main.
  symmetry.
  apply: main => //.
  by ssrnatlia.
move: n2; apply: (ind_basis n1) => n2.
  move=> *; repeat f_equal.
  by ssrnatlia.
move=> n_le IH _ *.
have N_le := N_seq_le n_le; rewrite -lez_nat in N_le.
rewrite IH //; last by intlia.
symmetry.
have := exceed_witness_seq_props n2.
move=> [_] [_] -> //.
by intlia.
Qed.


Lemma wit_N_ge n : (n <= wit_N (exceed_witness_seq n))%nat.
Proof.
elim: n => //.
move=> n IH.
have := N_seq_increasing n.
by ssrnatlia.
Qed.


Lemma u_reachability_extend n k :
  (0 <= k <= wit_N (exceed_witness_seq n))%int ->
  u k = wit_u (exceed_witness_seq n) k.
Proof.
move=> /andP [] *.
apply: exceed_witness_seq_eq => //.
rewrite -(Posz_nat_of_int k) //.
rewrite lez_nat.
exact: wit_N_ge.
Qed.


Lemma u_reachability_exceed (M : R) :
  Rbar_lt M wcpg -> exists k, M < Rabs (H u k).
Proof.
move=> /(lim_seq_exceed lim_Mseq) [n exceed_M].
exists (wit_N (exceed_witness_seq n.+1)).
erewrite LTI_ext_before; last first.
- exact: u_reachability_extend.
- by assumption.
apply: Rlt_trans.
  by eauto.
have := (exceed_witness_seq_props n).
move=> [<- _].
exact: wit_exceed.
Qed.


Theorem wcpg_reachable :
  { u : scsignal |
    (forall k, Rabs (u k) <= 1) /\
    (forall M : R, Rbar_lt M wcpg -> exists k, M < Rabs (H u k)) }.
Proof.
exists u.
split.
- move=> k /=.
  exact: wit_bnd.
- exact: u_reachability_exceed.
Qed.





(** Consequence of reachability: BIBO filters have a finite WCPG. **)


Definition bounded_scsignal (x : @scsignal R) := 
  exists M : R, forall k, (Rabs (x k) <= M)%R.


Definition BIBO_SISO_filter :=
  forall u : scsignal, bounded_scsignal u -> bounded_scsignal (H u).


Theorem BIBO_finite_wcpg :
  BIBO_SISO_filter -> is_finite wcpg.
Proof.
move=> H_BIBO.
case_eq wcpg => //; last first.
  move=> wcpg_minfty.
  have := wcpg_ge0.
  by rewrite wcpg_minfty.
move=> wcpg_infty.
exfalso.
have [u [u_bnd Hu_exceeds]] := wcpg_reachable.
have := (H_BIBO u).
apply: ask_proof.
  by exists 1.
move=> [M bnd_M].
have := (Hu_exceeds M).
apply: ask_proof.
  by rewrite wcpg_infty.
move=> [K].
have := (bnd_M K).
by lra.
Qed.


End Reachability.




End SISO_wcpg.

Arguments wcpg_optimal : clear implicits.










Section MIMO_wcpg.


Context { nin nout : nat }.

Variable H : @MIMO_filter R nin nout.

Hypothesis HLTI : LTI_filter H.



Definition MIMO_wcpg : 'M[Rbar]_(nout, nin) :=
  \matrix_(i,j) wcpg (to_SISO_yi_uj H i j).



Theorem MIMO_wcpg_theorem (u : @vsignal R nin) (ubar : 'cV[Rbar]_nin) :
  (forall (k : int) (j : 'I_nin),
   Rbar_le (Rabs ((u k) _[j]ord))  (ubar _[j]ord)) ->
  (forall (k : int) (i : 'I_nout),
   Rbar_le (Rabs ((H u k) _[i]ord)) ((mulmxRbar MIMO_wcpg ubar) _[i]ord)).
Proof.
move => _H k i. rewrite decomp_one_output //.
apply: Rbar_le_trans.
- instantiate (1 := Finite _). apply: sum_ord_Rabs.
- rewrite sumR_bigRbar mxE. apply: bigRbar_le => j.
  rewrite mxE. apply: wcpg_theorem.
  + exact: LTI_to_SISO_yi_uj.
  + move => k'. by rewrite ith_scsignal_applied.
Qed.


Lemma MIMO_wcpg_impulse_response :
  MIMO_wcpg = Lim_seq_mx (fun n =>
    \sum_(0 <= m < n.+1) absmx (MIMO_impulse_response H m)).
Proof.
apply/matrixP=> i j.
rewrite !mxE.
apply: Lim_seq_ext => n.
rewrite summxE.
apply: eq_big_nat => *.
by rewrite !mxE.
Qed.


Theorem MIMO_wcpg_optimal (i : 'I_nout) (j : 'I_nin) :
  forall M : R, Rbar_lt M (MIMO_wcpg i j) ->
  exists (u : vsignal nin) N,
  (forall k j', Rabs (u k _[j']ord) <= 1) /\ H u N _[i]ord > M.
Proof.
move => M HM. rewrite mxE in HM.
destruct (wcpg_optimal (to_SISO_yi_uj H i j) (LTI_to_SISO_yi_uj i j HLTI) M)
  as [uj [N [Huj H_gtM]]]. assumption.
exists (vsignal_of_sc_at_i uj j), N.
split => //.
move=> k j'.
case_eq (j' == j) => /eqP.
  by move=> ->; rewrite vsignal_of_sc_at_i_applied_index_i.
move=> *; rewrite vsignal_of_sc_at_i_applied_index_neq // Rabs_R0 //.
by lra.
Qed.


End MIMO_wcpg.







Section From_State_Space.


Context { nin nout nx : nat }.

Variable stsp : @StateSpace R nin nout nx.

Notation A := (StSp_A stsp).
Notation B := (StSp_B stsp).
Notation C := (StSp_C stsp).
Notation D := (StSp_D stsp).


Lemma wcpg_from_StSp :
  MIMO_wcpg (filter_from_StSp stsp) =
  addmxRbar (to_mxRbar (absmx D))
             (Lim_seq_mx (fun n => 
                \sum_(0 <= m < n.+1) absmx (C *m expmx A m *m B))).
Proof.
rewrite MIMO_wcpg_impulse_response.
rewrite -Lim_seq_mx_addconst; last first.
  move=> i j.
  apply: ex_lim_seq_ext.
    move=> n /=.
    rewrite summxE.
    reflexivity.
  apply: ex_lim_seq_sum_S_ge0 => n /=.
  rewrite mxE.
  exact: Rabs_pos.
apply/matrixP=> i j.
rewrite !mxE -Lim_seq_incr_1.
apply: Lim_seq_ext => m.
rewrite summxE big_ltn //.
rewrite StateSpace_impulse_response !mxE summxE.
f_equal.
rewrite [RHS](big_nat_shiftUp 1).
apply: eq_big_nat_eqbounds => //; first ssrnatlia.
move=> k.
rewrite StateSpace_impulse_response.
by case: k.
Qed.


End From_State_Space.

