(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import all_ssreflect ssralg matrix.

Require Import Reals.

From Coquelicot
Require Import Coquelicot.

Require Import Rstruct.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Import GRing.Theory.
Import RIneq.


Local Open Scope R.
Local Open Scope ring_scope.




Lemma Rbar_le_neq_lt (a b : Rbar) :
  Rbar_le a b -> a <> b -> Rbar_lt a b.
Proof.
intros; by case: (Rbar_le_lt_or_eq_dec a b).
Qed.



Lemma Rbar_plus_Rplus (r s : R) : Rbar_plus r s = Rplus r s.
Proof.
by [].
Qed.


Lemma Rbar_le_Rle (r s : Rdefinitions.R) : Rbar_le r s -> Rle r s.
Proof.
by [].
Qed.


Lemma Rbar_mult_pos_pinfty (x : Rbar) :
  Rbar_lt R0 x -> Rbar_mult x p_infty = p_infty.
Proof.
case: x => //= r Hr.
case: Rle_dec => // Hcase; last first.
  by contradict Hcase; exact: Rlt_le.
case: Rle_lt_or_eq_dec => //.
intro Heq0; contradict Hr.
rewrite -Heq0.
exact: Rlt_irrefl.
Qed.


Lemma Rbar_mult_both_ge_0 x y :
  Rbar_le R0 x -> Rbar_le R0 y -> Rbar_le R0 (Rbar_mult x y).
Proof.
intros.
unfold Rbar_mult. unfold Rbar_mult'.
case x, y; try easy.
now apply Rmult_le_pos.
all: case (Rle_dec 0 r);
       try (intro Hcase; now contradict Hcase);
     intro Hr; case (Rle_lt_or_eq_dec 0 r Hr);
     [ easy | unfold Rbar_le; intro; apply Rle_refl ].
Qed.






Definition to_mxRbar {h w : nat} (M : 'M[R]_(h, w)) : 'M[Rbar]_(h, w) :=
  \matrix_(i, j) (M i j : Rbar).


Definition addmxRbar {h w : nat} (A B : 'M[Rbar]_(h, w))
               : 'M[Rbar]_(h, w) :=
  \matrix_(i, j) Rbar_plus (A i j) (B i j).


Definition mulmxRbar {h n w : nat}
         (A : 'M[Rbar]_(h, n)) (B : 'M[Rbar]_(n, w))
     : 'M[Rbar]_(h, w) :=
  \matrix_(i, j) \big[Rbar_plus/(R0:Rbar)]_(k < n)
                   Rbar_mult (A i k) (B k j).





Lemma sumR_bigRbar n (F : 'I_n -> R) :
  ((\sum_(i < n) F i) : Rbar) = \big[Rbar_plus/(R0:Rbar)]_(i < n) (F i : Rbar).
Proof.
move: n F; apply: nat_ind.
  by intros; rewrite !big_ord0.
intros; rewrite !big_ord_recl -Rbar_plus_Rplus.
by f_equal.
Qed.


Lemma bigRbar_le n (F1 F2 : 'I_n -> Rbar) :
  (forall i : 'I_n, Rbar_le (F1 i) (F2 i)) ->
  Rbar_le (\big[Rbar_plus/(R0:Rbar)]_(i < n) (F1 i))
          (\big[Rbar_plus/(R0:Rbar)]_(i < n) (F2 i)).
Proof.
move: n F1 F2; apply: nat_ind.
  intros; rewrite !big_ord0.
  exact: Rbar_le_refl.
intros; rewrite !big_ord_recl.
by apply: Rbar_plus_le_compat; auto.
Qed.




Lemma ex_Rbar_plus_Finite (x : R) (y : Rbar) :
  ex_Rbar_plus x y.
Proof.
compute.
by case: y.
Qed.

