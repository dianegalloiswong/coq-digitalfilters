(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import all_ssreflect.

Require Import lia_tactics utils nat_ord_complements.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.




Lemma rconsnil {T} (x :T) : rcons [::] x = [:: x].
Proof.
by [].
Qed.


Lemma notnil T (s : seq T) : size s <> O -> s <> [::].
Proof.
by case: s.
Qed.


Lemma size_gt0 T (s : seq T) :
  s <> [::] -> (0 < size s)%nat.
Proof.
by case: s.
Qed.


Lemma nnil_sizegt0 (T : Type) (s : seq T) :
  (0 < size s)%nat -> s <> [::].
Proof.
move=> size_gt0 s_eq.
move: size_gt0.
by rewrite s_eq.
Qed.


Lemma rconsI T x0 (s : seq T) :
  s <> [::] ->
  s = rcons (take (size s).-1 s) (nth x0 s (size s).-1).
Proof.
move=> /size_gt0 size_gt0.
rewrite -take_nth; last by ssrnatlia.
by rewrite prednK // take_size.
Qed.


Lemma size1_head T (x0 : T) s : size s = 1%nat -> s = [:: head x0 s].
Proof.
case: s => //.
by move=> ?; case.
Qed.


Lemma nth_cons T (x0 : T) x s i :
  nth x0 (x :: s) i.+1 = nth x0 s i.
Proof.
by [].
Qed.


Lemma cons_nth0_behead T (x0 : T) s :
  s <> [::] -> nth x0 s 0 :: behead s = s.
Proof.
by case: s.
Qed.


Lemma sorted_cons T x0 (leT : rel T) h (t : seq T) :
  leT h (nth x0 t 0) /\ sorted leT t -> sorted leT (h :: t).
Proof.
case: t => //.
by intros; rewrite -rwandP.
Qed.


Lemma filter_seq1 {T} P (x :T) :
  [seq y <- [:: x] | P y] = if P x then [:: x] else [::].
Proof.
by [].
Qed.


Lemma filter_id_all {T} P (s : seq T) :
  all P s -> [seq x <- s | P x] = s.
Proof.
exact: all_filterP.
Qed.


Definition lastn T n s : seq T := drop (size s - n) s.


Lemma lastn1s T (x0:T) s : s <> [::] -> lastn 1 s = [:: last x0 s].
Proof.
move: x0. case: s => // x s x0 _. move: x0 x.
elim: s => // x1 s IH x0 x2.
rewrite last_cons -IH.
by rewrite /lastn /= subn1.
Qed.


Lemma lastS T (x0:T) n s :
  (n < size s)%nat ->
  lastn n.+1 s = nth x0 s (size s - n.+1) :: lastn n s.
Proof.
move=> n_lt.
rewrite /lastn [LHS](drop_nth x0); last ssrnatlia.
f_equal; f_equal.
by ssrnatlia.
Qed.


Lemma lastn_size T (s : seq T) : lastn (size s) s = s.
Proof.
by rewrite /lastn subnn drop0.
Qed.


Lemma all_mapP (T1 T2 : eqType) (f : T1 -> T2)
               (a : ssrbool.pred T2) (s : seq T1) :
  reflect {in s, forall x : T1, a (f x)} (all a (map f s)).
Proof.
rewrite all_map.
by apply/allP.
Qed.


Lemma take1 T x0 (s : seq T) :
  (0 < size s)%nat -> take 1 s = [:: nth x0 s 0].
Proof.
move=> size_gt0.
erewrite take_nth => //.
by rewrite take0 rconsnil.
Qed.


Lemma find_nhas (T : Type) (a : ssrbool.pred T) (s : seq T) :
  ~~ (has a s) -> find a s = size s.
Proof.
rewrite has_find => /negP.
have := (find_size a s).
by ssrnatlia.
Qed.









(** seq2 **)


Lemma seq_ind2' {S T} (P : seq S -> seq T -> Type) :
  (forall t, P [::] t) ->
  (forall x s, P (x :: s) [::]) ->
  (forall x y s t, P s t -> P (x :: s) (y :: t)) ->
  forall s t, P s t.
Proof.
move => H1 H2 H3. elim.
  assumption.
move => x s H4. case.
  trivial.
auto.
Qed.


Lemma nth_zip_ltsizes T1 T2 (inh : T1 * T2) s1 s2 i :
  (i < size s1)%nat -> (i < size s2)%nat ->
  nth inh (zip s1 s2) i = (nth inh.1 s1 i, nth inh.2 s2 i).
Proof.
move=> *.
rewrite nth_zip_cond ifT //.
by rewrite size_zip leq_min; autoP.
Qed.


(** all2 **)

Lemma all2_cons S T (r : S -> T -> bool) hs (s : seq S) ht t :
  all2 r (hs :: s) (ht :: t) = r hs ht && all2 r s t.
Proof.
by [].
Qed.

Lemma all2_consP S T (r : S -> T -> bool) hs (s : seq S) ht t :
  all2 r (hs :: s) (ht :: t) <-> r hs ht /\ all2 r s t.
Proof.
by split; rewrite /= -!rwandP.
Qed.

Lemma all2_same_size S T (r : S -> T -> bool) s t :
  all2 r s t -> seq.size s = seq.size t.
Proof.
move: s t. apply: seq_ind2'.
- case => //.
- by [].
- move => x y s t Hind /andP [Hxy Hst].
  simpl. rewrite Hind //.
Qed.

Lemma nth_all2 {S T} rel (s : seq S) (t : seq T) x0 y0 :
  all2 rel s t ->
  forall i, (i < size s)%nat -> rel (nth x0 s i) (nth y0 t i).
Proof.
move: s t.
apply: seq_ind2' => //=.
move=> x y s t IH /andP [relxy all2st].
case => // i Si_lt.
rewrite !nth_cons.
exact: IH.
Qed.

Lemma all2_nth {S T} (rel : S -> T -> bool) (s : seq S) (t : seq T) x0 y0 :
  size s = size t ->
  ( forall i, (i < size s)%nat -> rel (nth x0 s i) (nth y0 t i) ) ->
  all2 rel s t.
Proof.
move: s t.
apply: seq_ind2' => //=.
  by case.
move=> x y s t IH eq_size rel_nth.
apply/andP; split.
  have := (rel_nth O).
  rewrite !nth0 /=.
  by auto.
apply: IH.
  ssrnatlia.
move=> i i_lt.
exact: (rel_nth i.+1).
Qed.


(** map2 **)

Definition map2 {T1 T2 T} (f : T1 -> T2 -> T) s1 s2 :=
  map (fun x => let '(x1, x2) := x in f x1 x2) (zip s1 s2).

Lemma nth_map2  T1 (inh1 : T1) T2 (inh2 : T2) T (inhT : T) f s1 s2 i:
  (i < size s1)%nat -> (i < size s2)%nat ->
  nth inhT (map2 f s1 s2) i = f (nth inh1 s1 i) (nth inh2 s2 i).
Proof.
move=> *.
rewrite (nth_map (inh1, inh2)); last first.
  by rewrite size_zip leq_min; autoP.
by rewrite nth_zip_ltsizes //.
Qed.

Lemma size_map2 T1 T2 T (f : T1 -> T2 -> T) s1 s2 :
  size (map2 f s1 s2) = minn (size s1) (size s2).
Proof.
by rewrite size_map size_zip.
Qed.


(** map3 **)

Definition map3 (T1 T2 T3 T : Type)
                (f : T1 -> T2 -> T3 -> T) s1 s2 s3 : seq T :=
  map2 Basics.apply (map2 f s1 s2) s3.

Lemma nth_map3  T1 (inh1 : T1) T2 (inh2 : T2) T3 (inh3 : T3) T (inhT : T)
                f s1 s2 s3 i :
  (i < size s1)%nat -> (i < size s2)%nat -> (i < size s3)%nat ->
  nth inhT (map3 f s1 s2 s3) i =
  f (nth inh1 s1 i) (nth inh2 s2 i) (nth inh3 s3 i).
Proof.
move=> *.
rewrite (nth_map2 (fun _ => inhT) inh3) => //; last first.
  by rewrite size_map2 leq_min; autoP.
by erewrite nth_map2.
Qed.

Lemma size_map3 T1 T2 T3 T (f : T1 -> T2 -> T3 -> T) s1 s2 s3 :
  size (map3 f s1 s2 s3) =
  minn (minn (size s1) (size s2)) (size s3).
Proof.
by rewrite size_map size_zip size_map size_zip.
Qed.






(** about mask **)


Lemma mask_take_sizes_m T m (s : seq T) :
  mask m s = mask (take (size s) m) s.
Proof.
move: m s.
apply: seq_ind2' => //=.
by case => // x m s <-.
Qed.


Lemma mask_take_sizem_s T m (s : seq T) :
  mask m s = mask m (take (size m) s).
Proof.
move: m s.
apply: seq_ind2' => //=.
by case => // x m s <-.
Qed.


Lemma size_mask_le (T : Type) (m : bitseq) (s : seq T) :
  (size m <= size s)%nat -> size (mask m s) = count id m.
Proof.
move=> size_le.
rewrite mask_take_sizem_s size_mask // size_take.
case: ifP => // /negP not_size_lt.
by ssrnatlia.
Qed.


Lemma size_mask_take T m (s : seq T) :
  size (mask m s) = count ssrfun.id (take (size s) m).
Proof.
case_bool (size s < size m)%nat => Hcase.
- by rewrite mask_take_sizes_m size_mask // size_take ifT //.
- rewrite size_mask_le ?take_oversize //.
  all: by ssrnatlia.
Qed.


Lemma same_size_mask S T m (s : seq S) (t : seq T) :
  size s = size t -> size (mask m s) = size (mask m t).
Proof.
by rewrite !size_mask_take => ->.
Qed.


Lemma all2_mask (S T : Type) (r : S -> T -> bool)
                (s : seq S) (t : seq T) (m : bitseq) :
  all2 r s t -> all2 r (mask m s) (mask m t).
Proof.
move=> H. have := H. move: H m => /all2_same_size.
move: s t; apply: seq_ind2.
  by intros; rewrite !mask0.
move=> hs ht s' t' eq_size IH m /all2_consP [rh all2'].
case: m => // b m'.
rewrite !mask_cons.
case: b => //=.
  apply/andP; split=> //.
all: exact: IH.
Qed.


(** bitseqTF m n == the bitseq of size n beginning with min(m,n) "true" items
                    then filled with "false" items **)
Definition bitseqTF m n : bitseq :=
  if (m <= n)%nat then ncons m true (nseq (n - m) false)
  else nseq n true.


Lemma bitseqTF_0 n : bitseqTF 0 n = nseq n false.
Proof.
rewrite /bitseqTF /=.
f_equal.
by auto with arith.
Qed.


Lemma bitseqTF_S m' n :
  bitseqTF m' n.+1 = match m' with
    | O => nseq n.+1 false
    | p.+1 => true :: bitseqTF p n
  end.
Proof.
case: m'.
  by rewrite bitseqTF_0.
move=> m'.
rewrite /bitseqTF.
case: ifP.
- move=> Hm' /=.
  by rewrite ifT.
- move=> Hm' /=.
  by rewrite ifF //.
Qed.


Lemma take_mask (T : Type) (n : nat) (s : seq T) :
  take n s = mask (bitseqTF n (size s)) s.
Proof.
move: n.
elim: s.
  move=> n /=.
  by rewrite mask0.
move=> h t IH n.
rewrite take_cons /= bitseqTF_S.
case: n.
  by rewrite mask_false.
move=> n.
by rewrite mask_cons IH.
Qed.


(** bitseqFT m n == the bitseq of size n beginning with min(m,n) "false" items
                    then filled with "true" items **)
Definition bitseqFT m n : bitseq := map negb (bitseqTF m n).


Lemma bitseqFT_S m' n :
  bitseqFT m' n.+1 = match m' with
    | O => nseq n.+1 true
    | p.+1 => false :: bitseqFT p n
  end.
Proof.
rewrite /bitseqFT bitseqTF_S.
case: m' => //.
by rewrite map_nseq.
Qed.


Lemma drop_mask (T : Type) (n : nat) (s : seq T) :
  drop n s = mask (bitseqFT n (size s)) s.
Proof.
move: n.
elim: s.
  move=> n /=.
  by rewrite mask0.
move=> h t IH n.
rewrite drop_cons /= bitseqFT_S.
case: n.
  by rewrite mask_true.
move=> n.
by rewrite mask_cons IH.
Qed.


Lemma all2_take (S T : Type) (r : S -> T -> bool)
                (n : nat) (s : seq S) (t : seq T) :
  all2 r s t -> all2 r (take n s) (take n t).
Proof.
move=> all2rst.
rewrite !take_mask.
rewrite (all2_same_size all2rst).
exact: all2_mask.
Qed.


Lemma all2_drop (S T : Type) (r : S -> T -> bool)
                (n : nat) (s : seq S) (t : seq T) :
  all2 r s t -> all2 r (drop n s) (drop n t).
Proof.
move=> all2rst.
rewrite !drop_mask.
rewrite (all2_same_size all2rst).
exact: all2_mask.
Qed.


Lemma sorted_take (T : Type) (leT : rel T) :
  transitive leT -> forall (n : nat) (s : seq T),
  sorted leT s -> sorted leT (take n s).
Proof.
move=> *.
rewrite take_mask.
exact: sorted_mask.
Qed.


Lemma sorted_drop (T : Type) (leT : rel T) :
  transitive leT -> forall (n : nat) (s : seq T),
  sorted leT s -> sorted leT (drop n s).
Proof.
move=> *.
rewrite drop_mask.
exact: sorted_mask.
Qed.








(** about big **)


Lemma big_seq1_pred (R : Type) (idx : R) (op : Monoid.law idx) 
                    (I : Type) (i : I) P (F : I -> R) :
  \big[op/idx]_(i0 <- [:: i] | P i0) F i0 =
  if P i then F i else idx.
Proof.
rewrite -big_filter filter_seq1. case: P.
- exact: big_seq1.
- exact: big_nil.
Qed.


Lemma big_rcons_pred (R : Type) (idx : R) (op : Monoid.law idx) 
     (I : Type) (s : seq I) (i : I) (P : ssrbool.pred I) 
     (F : I -> R) :
 let x := \big[op/idx]_(j <- s | P j) F j in
       \big[op/idx]_(j <- (rcons s i) | P j) F j =
       (if P i then op x (F i) else x).
Proof.
move=> x. rewrite -cats1 big_cat big_seq1_pred.
case: (P i) => //.
exact: Monoid.mulm1.
Qed.


Lemma big_rcons (R : Type) (idx : R) (op : Monoid.law idx)
                (I : Type) (s : seq I) (i : I) (F : I -> R) :
  \big[op/idx]_(j <- rcons s i) F j =
  op (\big[op/idx]_(j <- s) F j) (F i).
Proof.
exact: big_rcons_pred.
Qed.


Lemma head_01_sum (R : Type) (idx : R) (op : Monoid.law idx) (xs : seq R) :
  (size xs <= 1)%nat ->
  head idx xs = \big[op/idx]_(x <- xs) x.
Proof.
case_eq (size xs == O) => /eqP.
  by move=> *; rewrite [xs]size0nil // big_nil.
move=> *; rewrite [xs in RHS](size1_head idx); last by auton.
by rewrite big_seq1.
Qed.



Infix " ` " := (nth (ssralg.GRing.zero _)) (at level 20).

