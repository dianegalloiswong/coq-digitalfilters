(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import all_ssreflect ssralg.

From Flocq
Require Import Core.

Require Import Lia.

Require Import lia_tactics utils nat_ord_complements seq_complements.



Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Local Open Scope Z.





(** misc **)


Lemma ZsubzK : rev_right_loop Z.opp Zplus.
Proof.
move => x y. exact: Z.sub_simpl_r.
Qed.
Arguments ZsubzK (x y)%Z : rename.


Lemma Zadd_reg_r (n m k : Z) :
  (n + k = m + k -> n = m)%Z.
Proof.
intro. rewrite -(Z.add_simpl_r n k) -(Z.add_simpl_r m k).
exact: Zplus_eq_compat.
Qed.


Hint Rewrite Zplus_0_r Z.add_opp_diag_r : ZDb.





(** Z and nat **)


Coercion Z.of_nat : nat >-> Z.


Definition Zle0n := Zle_0_nat.
Definition INZ_ge0 := Zle_0_nat.


Lemma INZ_gt0 (n : nat) : (0 < n)%Z <-> (0 < n)%nat.
Proof.
by case: n.
Qed.


Lemma Z2N_gt0 z : (0 < Z.to_nat z)%nat <-> (0 < z)%Z.
Proof.
split; case: z => // p gt0.
apply/leP.
exact: Pos2Nat.is_pos.
Qed.





(** Z.eqb, Z.leb, Z.ltb **)


Definition ZeqP : forall x y : Z, reflect (x = y) (x =? y)
  := Z.eqb_spec.


Lemma ZleP m n : reflect (Z.le m n) (Z.leb m n).
Proof.
apply: Bool.iff_reflect.
exact: Zle_is_le_bool.
Qed.

Lemma Zleb_trans : transitive Z.leb.
Proof.
move=> x y z /ZleP Hyz /ZleP Hxz.
apply/ZleP.
by auto with zarith.
Qed.

Lemma Zleb_refl : reflexive Z.leb.
Proof.
move=> x.
apply/ZleP.
by auto with zarith.
Qed.


Lemma ZltP m n : reflect (Z.lt m n) (Z.ltb m n).
Proof.
apply: Bool.iff_reflect.
exact: Zlt_is_lt_bool.
Qed.

Lemma Zltb_trans : transitive Z.ltb.
Proof.
move=> x y z /ZltP Hyz /ZltP Hxz.
apply/ZltP.
auto with zarith.
Qed.




(** Z as eqType, choiceType, countType, zmodType **)


Canonical Structure Z_eqMixin := EqMixin ZeqP.
Canonical Structure Z_eqType := Eval hnf in EqType Z Z_eqMixin.


Definition pickleZ z :=
  if Z.leb 1 z then (Z.to_nat z).*2.-1 else (Z.to_nat (- z)).*2.

Definition unpickleZ n :=
  Some (if odd n then Z.of_nat n.+1./2 else - Z.of_nat n./2).

Lemma pcancelZ : pcancel pickleZ unpickleZ.
Proof.
move=> z.
rewrite /pickleZ.
case: ifP => /ZleP case_z; rewrite /unpickleZ.
- case: ifP; last first.
    rewrite odd_pred.
      by rewrite odd_double.
    rewrite double_gt0 Z2N_gt0.
    by lia.
  rewrite prednK; last first.
    rewrite double_gt0 Z2N_gt0.
    by lia.
  rewrite half_double Z2Nat.id //.
  by lia.
- case: ifP.
    by rewrite odd_double.
  rewrite half_double Z2Nat.id; last lia.
  by rewrite Z.opp_involutive.
Qed.



Definition Z_choiceMixin := PcanChoiceMixin pcancelZ.

Canonical Z_choiceType := Eval hnf in ChoiceType Z Z_choiceMixin.



Definition Z_countMixin := PcanCountMixin pcancelZ.

Canonical Z_countType := Eval hnf in CountType Z Z_countMixin.


Definition Z_zmodMixin :=
  ZmodMixin Zplus_assoc Zplus_comm Zplus_0_l Z.add_opp_diag_l.

Canonical Z_zmodType := Eval hnf in ZmodType Z Z_zmodMixin.







(** sorted seq Z **)


Lemma sorted_Zleb_cons2P h0 h1 (s : seq Z) :
  sorted Z.leb (h0 :: h1 :: s) <-> (h0 <= h1 /\ sorted Z.leb (h1 :: s)).
Proof.
by rewrite -rwandP -(rwP (ZleP _ _)).
Qed.


Lemma sorted_Zltb_cons2P h0 h1 (s : seq Z) :
  sorted Z.ltb (h0 :: h1 :: s) <-> (h0 < h1 /\ sorted Z.ltb (h1 :: s)).
Proof.
by rewrite -rwandP -(rwP (ZltP _ _)).
Qed.


Lemma sorted_Zltle (s : seq Z) :
  sorted Z.ltb s -> sorted Z.leb s.
Proof.
elim: s => // h.
case => // a l IH.
rewrite sorted_Zleb_cons2P sorted_Zltb_cons2P.
move => [*].
split.
  by lia.
by auto.
Qed.


Lemma Zle_nth_sorted z0 s i j :
  sorted Z.leb s -> (i <= j < size s)%nat ->
  nth z0 s i <= nth z0 s j.
Proof.
move=> Hsrt /andP Hij.
apply/ZleP.
apply: sorted_leq_nth => //.
- exact Zleb_trans.
- exact Zleb_refl.
1,2: rewrite /in_mem /mem /=.
all: by ssrnatlia.
Qed.


Lemma Zlt_sorted s i j :
  sorted Z.ltb s -> (i < j < size s)%nat -> s ` i < s ` j.
Proof.
move=> Hsrt /andP Hij.
apply/ZltP.
apply: sorted_ltn_nth => //.
  exact Zltb_trans.
1,2: rewrite /in_mem /mem /=.
all: by ssrnatlia.
Qed.


Lemma Zle_nth_sorted_lastle z0 s i j :
  sorted Z.leb s -> last z0 s <= z0 ->
  (i <= j)%nat -> nth z0 s i <= nth z0 s j.
Proof.
move=> Hsrt last_le Hij.
case_bool (j < size s)%nat.
  move=>*; apply: Zle_nth_sorted => //.
  by apply/andP.
move=> *; rewrite (@nth_default _ _ _ j); last ssrnatlia.
case_bool (i < size s)%nat; last first.
  move=> *; rewrite nth_default; last ssrnatlia.
  by lia.
move=> *.
apply: Z.le_trans; last exact: last_le.
rewrite -nth_last.
apply: Zle_nth_sorted => //.
by auton.
Qed.




(** about Z.modulo **)


Lemma Zplus_mod_idem (a b : Z) : (a+b) mod b = a mod b.
Proof.
rewrite -Zplus_mod_idemp_r Z_mod_same_full Zplus_0_r //.
Qed.


Lemma Zminus_mod_idem (a b : Z) : (a-b) mod b = a mod b.
Proof.
rewrite -Zplus_mod_idemp_r.
replace (-b mod b) with 0; try rewrite Zplus_0_r //.
apply sym_eq, Z_mod_zero_opp_full.
apply Z_mod_same_full.
Qed.


Lemma eqm_diff_0 N a b :
  ((a - b) mod N = 0 <-> eqm N a b)%Z.
Proof.
split.
- intro. rewrite -[b]Zplus_0_l -(Z.sub_simpl_r a b).
  exact: Zplus_eqm.
- intro. rewrite -(Zmod_0_l N) -(Zminus_diag b).
  exact: Zminus_eqm.
Qed.

