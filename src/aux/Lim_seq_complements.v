(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import all_ssreflect ssralg vector ssrint ssrnum matrix.

Require Import Reals Lra.

From Coquelicot
Require Import Coquelicot Lim_seq Markov.

Require Import Rstruct lia_tactics
               utils nat_ord_complements Rbar_complements.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



Local Open Scope ring_scope.
Local Open Scope R.



Lemma increasing_nseq_le (u : nat -> R) :
  (forall n : nat, u n <= u (S n)) ->
  (forall n1 n2, (n1 <= n2)%nat -> u n1 <= u n2).
Proof.
move=> u_increasing.
move=> n1; apply: (ind_basis n1) => n2.
  move=> *; replace n2 with n1 by ssrnatlia.
  exact: Rle_refl.
move=> n_rel IH _.
by apply: Rle_trans; last first; eauto.
Qed.


Lemma Lim_seq_ge_lwb (u : nat -> R) (lwb : R) :
  (forall n, Rbar_le lwb (u n)) ->
  Rbar_le lwb (Lim_seq u).
Proof.
intro Hlwb.
rewrite <- Lim_seq_const.
apply Lim_seq_le_loc.
exists O.
intros; apply Hlwb.
Qed.


Lemma seq_increasing_le_Lim (u : nat -> R) :
  (forall n : nat, u n <= u (S n)) ->
  forall n : nat, Rbar_le (u n) (Lim_seq u).
Proof.
move=> u_increasing.
have:= (ex_lim_seq_incr _ u_increasing).
move=> /Lim_seq_correct.
case: (Lim_seq u) => //.
  move=> r ? n.
  exact: is_lim_seq_incr_compare.
move=> islimseq; exfalso.
have := (@Lim_seq_ge_lwb u (u O)).
apply: ask_proof.
  by move=> n; exact: increasing_nseq_le.
rewrite (is_lim_seq_unique _ _ islimseq).
by [].
Qed.
(** only similar lemma in Coquelicot only handles finite limit:
      is_lim_seq_incr_compare :
        forall (u : nat -> R) (l : R),
        is_lim_seq u l -> (forall n : nat, u n <= u (S n)) -> 
        forall n : nat, u n <= l
**)








Lemma finite_lim_seq_exceed (u : nat -> R) (l : R) (M : R) :
  is_lim_seq u l -> M < l ->
  exists N, forall n, (N <= n)%nat -> M < u n.
Proof.
move=> /is_lim_seq_Reals cv M_lt.
destruct (cv (l - M)) as [N aboveN].
  by lra.
exists N.
move=> n n_ge.
have := (aboveN n _).
apply: ask_proof.
  by ssrnatlia.
rewrite /R_dist.
have := (Rabs_maj2 (u n - l)).
by lra.
Qed.


Lemma infinite_lim_seq_exceed (u : nat -> R) (M : R) :
  is_lim_seq u p_infty ->
  exists N, forall n, (N <= n)%nat -> M < u n.
Proof.
move=> /is_lim_seq_p_infty_Reals cv.
destruct (cv M) as [N aboveN].
exists N.
move=> n n_ge.
apply: aboveN.
by ssrnatlia.
Qed.


Lemma lim_seq_exceed (u : nat -> R) (l : Rbar) (M : R) :
  is_lim_seq u l -> Rbar_lt M l ->
  exists N, forall n, (N <= n)%nat -> M < u n.
Proof.
case: l => //=.
- by move=> l; exact: finite_lim_seq_exceed.
- by move=> *; exact: infinite_lim_seq_exceed.
Qed.


Lemma lim_seq_exceed_once (u : nat -> R) (l : Rbar) (M : R) :
  is_lim_seq u l -> Rbar_lt M l ->
  exists N, M < u N.
Proof.
move=> l_lim M_lt.
destruct (lim_seq_exceed l_lim M_lt) as [N aboveN].
by exists N; auto.
Qed.





(** unused **)
Lemma ex_lim_seq_sum_ge0 (u : nat -> R) :
  (forall n, (0 <= u n)%R) ->
  ex_lim_seq (fun n => \sum_(0 <= k < n) u k)%R.
Proof.
move=> u_ge0.
apply: ex_lim_seq_incr=> n.
rewrite big_nat_recr // -[X in (X <= _)%R]Rplus_0_r.
exact: Rplus_le_compat_l.
Qed.

Lemma ex_lim_seq_sum_S_ge0 (u : nat -> R) :
  (forall n, (0 <= u n)%R) ->
  ex_lim_seq (fun n => \sum_(0 <= k < n.+1) u k)%R.
Proof.
move=> u_ge0.
apply: ex_lim_seq_incr=> n.
rewrite (big_nat_recr _.+1) // -[X in (X <= _)%R]Rplus_0_r.
exact: Rplus_le_compat_l.
Qed.




Lemma Lim_seq_addconst (a : R) (u : nat -> R) :
  ex_lim_seq u ->
  Lim_seq (fun n : nat => a + u n) = Rbar_plus a (Lim_seq u).
Proof.
move=> ex_lim_seq_u.
rewrite -Lim_seq_const.
rewrite -Lim_seq_plus => //.
  exact: ex_lim_seq_const.
rewrite Lim_seq_const.
exact: ex_Rbar_plus_Finite.
Qed.






(** (pointwise) limit of a matrix sequence **)

Definition Lim_seq_mx {h w : nat}
      (Ms : nat -> 'M[R]_(h, w)) : 'M[Rbar]_(h, w) :=
  \matrix_(i, j) Lim_seq (fun n => Ms n i j).

Lemma Lim_seq_mx_const {h w : nat} (A : 'M[R]_(h, w)) :
  Lim_seq_mx (fun _ : nat => A) = to_mxRbar A.
Proof.
apply/matrixP => i j.
rewrite !mxE.
exact: Lim_seq_const.
Qed.

Definition ex_lim_seqmx {h w : nat} (Ms : nat -> 'M[R]_(h,w)) :=
  forall i j, ex_lim_seq (fun n : nat => (Ms n) i j).

Lemma Lim_seq_mx_addconst {h w : nat}
         (A : 'M[R]_(h,w)) (Bs : nat -> 'M[R]_(h,w)) :
  ex_lim_seqmx Bs ->
  Lim_seq_mx (fun n => A + Bs n)%RT = addmxRbar (to_mxRbar A) (Lim_seq_mx Bs).
Proof.
move=> ex_lim_seqmx_Bs.
apply/matrixP=> i j.
rewrite !mxE.
rewrite -Lim_seq_addconst.
- apply: Lim_seq_ext => m.
  by rewrite mxE.
- apply: ex_lim_seqmx_Bs.
Qed.







(** corollaries of Limited Principle of Omniscience,
    which is derived by Coquelicot from the axioms
    defining the reals in the standard library       **)


Lemma LPO_cor : forall P : nat -> Prop,
  (forall n : nat, P n \/ ~ P n) ->
  (exists n : nat, P n) -> {n : nat | P n}.
Proof.
intros P Hdec Hex.
pose proof (LPO P) as HLPO; case HLPO; try easy.
intro HnotP.
exfalso.
destruct Hex as [n Pn].
now pose proof (HnotP n).
Qed.


Lemma lim_seq_exceed_once_sig (u : nat -> R) (l : Rbar) (M : R) :
  is_lim_seq u l -> Rbar_lt M l ->
  { n | M < u n}.
Proof.
intros.
apply: LPO_cor.
  by move=> n; case: (Rlt_dec M (u n)); auto.
by apply: lim_seq_exceed_once; eauto.
Qed.

