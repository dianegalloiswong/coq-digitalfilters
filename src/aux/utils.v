(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



(** Miscellaneous auxiliary lemmas and tactics **)

From mathcomp
Require Import all_ssreflect ssralg vector ssrnum.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.






(** To avoid confusion with delimiter %R for reals,
    we will use %RT for ring_scope. We may
    also use %int to highlight that operands
    have type int (for which many operations
    are taken from ring_scope).
**)
Delimit Scope ring_scope with int.
Delimit Scope ring_scope with RT.





Ltac case_bool b := case/orP: (orbN b) => [ | /negP ].


Lemma ask_proof P Q R : P -> (Q -> R) -> (P -> Q) -> R.
Proof.
by auto.
Qed.


Fact apply_l A B : A -> (A -> B) -> B.
Proof.
by auto.
Qed.


Lemma snd_pair A B (a : A) (b : B) : snd (a, b) = b.
Proof.
by [].
Qed.


Ltac f_imply :=
match goal with
  | |- ?f ?a -> ?f' ?a' =>
    (replace f with f'; first replace a with a') => //
end.


Ltac trynow := (by []) || by auto || easy || by eauto.


Lemma ereplace_aux T (x y : T) : x = y -> x = y.
Proof.
by [].
Qed.
Arguments ereplace_aux {T} x y.

Tactic Notation "ereplace" uconstr(t) := erewrite (ereplace_aux t).

Tactic Notation "ereplace" uconstr(t) "in" hyp(H) :=
erewrite (ereplace_aux t) in H.




Tactic Notation "tacrw" tactic(tac) uconstr(t) :=
ereplace t; last by tac.

Tactic Notation "tacrw" tactic(tac) "in" hyp(H) :=
ereplace _ in H; last by tac.


Tactic Notation "uring_simplify" uconstr(t) := tacrw (ring_simplify) t.











Lemma eqb_iff (b1 b2 : bool) : (b1 <-> b2) -> b1 = b2.
Proof.
rewrite /iff.
 case: b1; case: b2 => //.
- move => [H _]. by have /H := isT.
- move => [_ H]. by have /H := isT.
Qed.


Lemma eqTb b : (true = b) <-> b.
Proof.
by [].
Qed.


Lemma eqFb b : (false = b) <-> ~~b.
Proof.
by case: b.
Qed.






(** From bool to Prop **)

Definition rweqP T x y := rwP (@eqP T x y).
Definition rwandP b1 b2 := rwP (@andP b1 b2).
Definition rworP b1 b2 := rwP (@orP b1 b2).
Definition rwnegP b := rwP (@negP b).

Lemma not_iff_eqF (b : bool) : (~ b) <-> (b = false).
Proof.
case: b => //.
by split.
Qed.

Hint Rewrite <- rweqP : b2PDb.
Hint Rewrite <- rwandP rworP rwnegP : b2PDb.
Hint Rewrite <- not_iff_eqF : b2PDb.










(** Tactics for structural simplification of a goal **)


(** adapted from http://adam.chlipala.net/cpdt/html/Match.html **)
Ltac notHyp P :=
  match goal with
    | [ _ : P |- _ ] => fail 1
    | _ => idtac
  end.


Ltac extend pf :=
  let t := type of pf in
    notHyp t; let id := fresh "Hextend" in have id := pf.


Ltac intro_new := match goal with [ |- ?P -> _] => notHyp P; intro end.





Ltac show_goal := match goal with [ |- ?T ] => idtac T end.


Tactic Notation "assert_goal_is" uconstr(t) :=
match goal with |- ?P =>
  unify P t
end.



Ltac clear_dup :=
  match goal with
    | [ H : ?X |- _ ] =>
      match goal with
        | [ H' : ?Y |- _ ] =>
          match H with
            | H' => fail 2
            | _ => unify X Y ; (clear H' || clear H)
          end
      end
  end.
Ltac clear_dups := repeat clear_dup.


Ltac clear_exact_dup :=
  match goal with
    | [ H : ?X |- _ ] =>
      match goal with
        | [ H' : X |- _ ] =>
          match H with
            | H' => fail 2
            | _ => (clear H' || clear H)
          end
      end
  end.
Ltac clear_exact_dups := repeat clear_exact_dup.




Tactic Notation "intro_new" := intro_new.
Tactic Notation "intro_new" ident(H) :=
match goal with [ |- ?P -> _] =>
  notHyp P; let H' := fresh H in intro H'
end.

Tactic Notation "intro2_new" :=
   (intro_new; (intro_new || intros _))
|| ((intro_new || intros _); intro_new).
Tactic Notation "intro2_new" ident(H1) ident(H2) :=
   (intro_new H1; (intro_new H2 || intros _))
|| ((intro_new H1 || intros _); intro_new H2).


Ltac simplP_step :=
match goal with
(*   | [ |- context [ negb (andb _ _) ] ] => rewrite negb_and *)
  | [ |- context [ negb (orb _ _) ] ] => rewrite negb_or

  | [ |- context [ is_true (@eq_op _ _ _) ] ] => rewrite -rweqP
  | H : context [ is_true (@eq_op _ _ _) ] |- _ => rewrite -rweqP in H
  | [ |- context [ is_true (andb _ _) ] ] => rewrite -rwandP
  | H : context [ is_true (andb _ _) ] |- _ => rewrite -rwandP in H
  | [ |- context [ is_true (orb _ _) ] ] => rewrite -rworP
  | H : context [ is_true (orb _ _) ] |- _ => rewrite -rworP in H
  | [ |- context [ is_true (negb _) ] ] => rewrite -rwnegP
  | H : context [ is_true (negb _) ] |- _ => rewrite -rwnegP in H
  | [ |- context [ @eq bool _ false ] ] => rewrite -not_iff_eqF
  | H : context [ @eq bool _ false ] |- _ => rewrite <- not_iff_eqF in H

  | [ |- forall i : ?T, _ ] => intro i
  | [ |- forall _ : ?T, _ ] => let H := fresh "Hsimpl0" in intro H

  | [ H : (_ /\ _) |- _ ] =>
    let H1 := fresh H "_proj1" in
    let H2 := fresh H "_proj2" in
    revert dependent H;
    try clear H;
    move => [H1 H2]
  | [ H : prod _ _ |- _ ] =>
    let H1 := fresh H "_fst" in
    let H2 := fresh H "_snd" in
    move: H => [H1 H2]

  (** rewrite -rwandP in H doesn't always work if H is a context
      hypothesis or is used in other hypotheses or in conclusion... **)
  | [ H : is_true (_ && _) |- _ ] =>
    revert dependent H; try clear H;
    (move=> /andP H ||
      (intro H; have := H; move=> /andP [];
       let H1 := fresh H "_copy_proj1" in
       let H2 := fresh H "_copy_proj2" in
       intro2_new H1 H2)
    )

  | [ |- _ /\ _ ] => constructor
  | [ |- _ <-> _ ] => constructor
end.


Ltac simplP :=
simpl;
clear_exact_dups;
repeat simplP_step;
clear_exact_dups.
Ltac simpl_structure := simplP.


Ltac autoP := simplP; auto.















(** About algebraic structures from Mathcomp: zmodType, ringType, etc. **)

Import GRing.
Local Open Scope ring_scope.


Lemma iternaddr_mulrn (V : zmodType) (n : nat) (r : V) :
  (ssrnat.iter n (+%R r) 0 = r *+ n)%RT.
Proof.
elim: n.
- by [].
- move => n Hind /=. rewrite Hind.
  symmetry. exact: GRing.mulrS.
Qed.


Lemma subrKl (V : zmodType) (x y : V) : x + (y - x) = y.
Proof.
rewrite addrC.
exact: subrK.
Qed.


Lemma addKrC (V : zmodType) (x y : V) : x + y - x = y.
Proof.
by rewrite GRing.addrC GRing.addKr.
Qed.


Lemma eq_subr0 (V : zmodType) (x y : V) : x = y -> x - y = 0.
Proof.
move => ->.
exact: subrr.
Qed.


Lemma scaler_mulr {R : ringType} (r s : regular_vectType R) :
  r *: s = r * s.
Proof.
by [].
Qed.


Ltac simplr_aux :=
rewrite
  ?add0r ?addr0 ?oppr0
  ?scaler0 ?scale1r ?scaler_mulr
  ?mul0r ?mulr0 ?mul1r ?mulr1.

Ltac simplr := repeat simplr_aux.


Lemma mulN1r {RT : ringType} (r : (regular_vectType RT)) :
  (-1) * r = -r.
Proof.
by rewrite mulNr mul1r.
Qed.


Lemma scalerC {RT : comRingType} {VT : vectType RT} c0 c1 (x : VT) :
  c0 *: (c1 *: x) = c1 *: (c0 *: x).
Proof.
by rewrite 2!scalerA mulrC.
Qed.


Lemma mulIf_iff (R : idomainType) (x : R) (x_neq0 : x <> 0%RT) x1 x2 :
  (x1 * x = x2 * x <-> x1 = x2)%RT.
Proof.
split.
- by apply: GRing.mulIf; exact/eqP.
- by move=> ->.
Qed.


Lemma ltr_sum (R : numDomainType) (I : eqType) (r : seq I)
              (P : ssrbool.pred I) (F G : I -> R) :
  has P r ->
  (forall i : I, P i -> (F i < G i)%RT) ->
  (\sum_(i <- r | P i) F i < \sum_(i <- r | P i) G i)%RT.
Proof.
move=> H all_lt.
have := H; move: H.
move/hasP => [x0 _ _].
move/(has_nthP x0) => [k k_lt Prk].
rewrite -(cat_take_drop k r).
rewrite !big_cat /=.
rewrite (drop_nth x0) //.
rewrite !big_cons Prk.
apply: Num.Theory.ler_lt_add; last first.
  apply: Num.Theory.ltr_le_add.
    by auto.
all: by apply: Num.Theory.ler_sum; auto.
Qed.

