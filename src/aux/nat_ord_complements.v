(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



(** Utilities about nat (natural integers from the Standard Library,
    with operations and functions from Mathcomp) and 'I_n (ordinals
    from Mathcomp):
    - Conversion between nat and ordinals
    - Tactics to solve goals about nat and/or ordinals
    - Ordinal shifts for indices of row/col/block matrices
    - Proof or construction by induction
**)

From mathcomp
Require Import ssreflect ssrbool ssrnat fintype ssralg.


Require Import lia_tactics utils.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Import GRing.Theory.


Local Open Scope nat.






(** Conversion between nat and ordinals **)


Definition ord_of_nat (n : nat) (n_gt0 : (n > 0)%nat) (k : nat) : 'I_n.
Proof.
move: n n_gt0 k.
case => // n _ k.
exact: (inord k).
Defined.
Arguments ord_of_nat [n].


Lemma ord_of_nat_of_ord {n : nat} (n_gt0 : (n > 0)%nat) (i : 'I_n) :
  ord_of_nat n_gt0 i = i.
Proof.
rewrite /ord_of_nat.
move: n n_gt0 i.
case => // n n_gt0 i.
by rewrite inord_val.
Qed.


Lemma nat_of_ord_of_nat {n : nat} (n_gt0 : (n > 0)%nat) (i : nat) :
  (i < n)%nat -> (ord_of_nat n_gt0 i : nat) = i.
Proof.
rewrite /ord_of_nat.
move: n n_gt0 i.
case => // n n_gt0 i i_lt.
by rewrite inordK.
Qed.


(** This is immediate from the definition of nat_of_ord,
    but "rewrite nat_of_Ordinal" is more precise than
    "rewrite /nat_of_ord" or "simpl (nat_of_ord _)"      **)
Lemma nat_of_Ordinal {n k} (Hlt : k < n) :
  (Ordinal Hlt : nat) = k.
Proof.
by [].
Qed.


Lemma Ordinal_of_ord {n} (i : 'I_n) (Hlt : i < n) :
  Ordinal Hlt = i.
Proof.
by apply: ord_inj.
Qed.


Lemma nat_pos_if_ord (n : nat) (i : 'I_n) : (n > 0)%nat.
Proof.
move: i.
case: n => //=.
all: (move => i; by have := ltn_ord i).
Qed.





Lemma odd_pred n : (0 < n)%nat -> odd n.-1 = ~~ odd n.
Proof.
case: n => //= n _.
by rewrite Bool.negb_involutive.
Qed.







(** Tactics for goals about nat and/or ordinals **)


Ltac simpl_ord := rewrite ?nat_of_ord_of_nat //.


Ltac hyps_ord :=
(** add hypotheses over nat for each ordinal in context **)
  repeat match goal with
    [ i : Finite.sort (ordinal_finType ?n) |- _ ] => simpl in i end;
  repeat match goal with
    [ i : 'I_?n |- _ ] => extend (ltn_ord i) end;
  repeat match goal with
    [ i : 'I_?n |- _ ] => extend (nat_pos_if_ord i) end.


Ltac simpln :=
(** fails if goal is not an inequality, or eq, or [not eq]
    over nat or a type convertible to nat **)
simpl_structure;
match goal with
  | [ |- context [ lt _ _ ] ] => rewrite /lt
  | [ |- context [ ge _ _ ] ] => rewrite /ge
  | [ |- context [ gt _ _ ] ] => rewrite /gt /lt
  | _ => idtac
end;
match goal with
  | [ |- is_true (leq _ _) ] => idtac
  | [ |- not (is_true (leq _ _)) ] => idtac
  | [ |- @eq ?T _ _ ] => change T with nat
  | [ |- not (@eq ?T _ _) ] => change T with nat
  | [ |- le _ _ ] => apply/leP
  | [ |- not (le _ _) ] => apply/leP/negP
  | [ |- not (and _ _) ] => idtac
  | _ => fail
end.


Ltac auton :=
try (
  try by [];
  simpln;
  try ssrnatlia;
  hyps_ord;
  simpl_ord;
  rewrite -?ltn_subRL ?subnKC //=;
  ssrnatlia
).











(** Ordinal shifts for indices of row/col/block matrices **)


Lemma mk_rshift_subproof m n p : (p < m + n -> m <= p -> p - m < n)%nat.
Proof.
by ssrnatlia.
Qed.


Lemma ord_lrshift m n (i : 'I_(m + n)) : i = match (ltnP i m) with
  | LtnNotGeq lt_i_m => lshift n (Ordinal (lt_i_m : (i<m)%nat))
  | GeqNotLtn le_m_i =>
    rshift m (Ordinal (mk_rshift_subproof (ltn_ord i) le_m_i))
end.
Proof.
case (ltnP i m) =>  [lt_i_m | le_m_i].
- exact: ord_inj.
- apply: ord_inj.
  by auton.
Qed.
(** "ord_lrshift" allows us to rewrite "i : 'I_(m + n)"
    as either an "lshift" or an "rshift".
    Then, "case: (ltnP i m)" produces expressions
    for which lemmas such as "col_mxEu" can be used.
    Example: "mx1_block" in mx_complements.v
**)


Lemma mk_rshift_subproof_inj m n (i j : 'I_(m+n)) Hi Hj :
  Ordinal (mk_rshift_subproof (ltn_ord i) Hi) =
  Ordinal (mk_rshift_subproof (ltn_ord j) Hj)
  -> i = j.
Proof.
move => H. apply: ord_inj.
have H': (  nat_of_ord (Ordinal (mk_rshift_subproof (ltn_ord i) Hi))
          = nat_of_ord (Ordinal (mk_rshift_subproof (ltn_ord j) Hj)) )
  by rewrite H //.
simpl in H'.
by rewrite -(subnKC Hi) -(subnKC Hj) H'.
Qed.








(** Proof or construction by induction **)


(** Usual Peano induction, except that initialization is up to
    a given "basis". **)
Lemma ind_basis (basis : nat) (P : nat -> Prop) :
  (forall n : nat, n <= basis -> P n) ->
  (forall n : nat, basis <= n -> P n -> P n.+1) ->
  forall n : nat, P n.
Proof.
move => BC IS; apply: nat_ind.
  by auto.
move => n IH.
by (have [?|?]: (n < basis \/ basis <= n) by ssrnatlia); auto.
Qed.
Arguments ind_basis : clear implicits.



(** Construction by strong induction over nat **)
Section Strongrec.

Context {T : Type} {nonempty : T}.


(** "strongrec f_rec n" is the function f : nat -> T such that

        forall n : nat, f n = f_rec n f

    f_rec is assumed to only use past values of its functionnal argument:
    "f_rec n g" may depend on "g 0", "g 1", ... , "g (n-1)", but not
    on "g n", "g (n+1)", etc.
    This is described by property "strongrec_safe" further below.
    In particular, "f_rec 0 g" does not depend on g, which allows us to
    construct "f 0" while we have no information yet on f.
    Type "T" is required to be nonempty. The witness "nonempty" is needed
    for the construction, but has no impact on the constructed function "f".
**)

Definition strongrec (f_rec : nat -> (nat -> T) -> T) (n : nat) : T :=
(** We use nat_rect : forall P : nat -> Type, P 0 ->
                      (forall n : nat, P n -> P n.+1) -> forall n : nat, P n
    to build a function "g : nat -> nat -> T" such that for all n, "g n" is
    a function that takes the same values as f on every m <= n.
    - "g 0" is the function that always takes the value "f 0", which is equal
      to "f_rec 0 h" for any function h, for example h = "fun _ => (nonempty:T)".
    - "g_succ (n : nat) (g_n : nat -> T) : nat -> T" returns the function "g (n+1)"
      if it was given the argument g_n = g n. To do so, it copies g_n for every m <= n,
      then sets the image of n+1 to "f_rec (n+1) g_n". (It also sets images for m > n+1
      to this same values, but this is not important.)
**)
  let g_0 (_ : nat ) := f_rec 0 (fun _ => nonempty) in
  let g_succ (n : nat) (g_n : nat -> T) (m : nat) : T :=
    if m <= n then g_n m else f_rec n.+1 g_n in
  (nat_rect (fun _ => nat -> T) g_0 g_succ n) n.


(** There should be no need to expand "strongrec" outside of this section.
    Instead, lemma "strongrec_n" serves as an interface. **)


(** For the recursive construction of "strongrec" to make sense,
    we need "f_rec" not to use the future, that is,
    "f_rec n g" may only depend on "g m" for m < n, not on
    any "g m" for m >= n. **)
Definition strongrec_safe (f_rec : nat -> (nat -> T) -> T) : Prop :=
  forall (n : nat) (g1 g2 : nat -> T),
  (forall m : nat, (m < n) -> g1 m = g2 m) ->
  f_rec n g1 = f_rec n g2.


Lemma strongrec0 f_rec :
  strongrec_safe f_rec ->
  forall g : nat -> T, strongrec f_rec 0 = f_rec 0 g.
Proof.
move => safe g; rewrite /strongrec /=.
exact: safe.
Qed.


Lemma strongrec_n (f_rec : nat -> (nat -> T) -> T) n :
  strongrec_safe f_rec ->
  strongrec f_rec n = f_rec n (strongrec f_rec).
Proof.
move => safe; case: n.
  exact: strongrec0.
move => n; rewrite [in LHS]/strongrec /=.
rewrite ifF; last by auton.
apply: safe => m Hm.
rewrite /strongrec.
move: n Hm; apply: (ind_basis m) => n.
  by move => *; replace n with m by ssrnatlia.
move => * /=.
by rewrite ifT //; auto.
Qed.


End Strongrec.

