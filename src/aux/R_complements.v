(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import all_ssreflect ssrnum ssralg matrix.

From Flocq
Require Import Core.

Require Import Lra Lia.

Require Import Rstruct utils nat_ord_complements Z_complements
               sum_complements.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope ring_scope.
Local Open Scope R.




Implicit Types
  (r x y : R)
  (k : Z).


Lemma RaddKrC x y : x + y - x = y.
Proof.
by ring.
Qed.


Lemma RsubrK : rev_right_loop Ropp Rplus.
Proof.
move => x y.
by ring.
Qed.


Lemma RsubrKA (x y z : R) :
  y - x + (x + z) = y + z.
Proof.
by ring.
Qed.


Lemma RaddrNKKNr r r1 r2 : (r1 - r) + (r - r2) = r1 - r2.
Proof.
by ring.
Qed.


Lemma RmulrK x y : x <> 0 -> y * x / x = y.
Proof.
intros; rewrite /Rdiv Rmult_assoc Rinv_r //.
by ring.
Qed.


Lemma RmulrVK x y : x <> 0 -> y / x * x = y.
Proof.
intro; rewrite Rmult_assoc Rinv_l //.
by ring.
Qed.
Definition RdivrK := RmulrVK.


Lemma RmulVKr (x y : R) : x <> 0 -> x * (/ x * y) = y.
Proof.
move=> Hx. rewrite -Rmult_assoc Rinv_r //. ring.
Qed.
Global Arguments RmulVKr : clear implicits.
Definition RdivKr := RmulVKr.


Lemma RdivrKA y x z :
  (y <> 0 -> (x * / y) * (y * z) = x * z)%R.
Proof.
move=> y_neq0.
by rewrite /Rdiv Rmult_assoc
           -(Rmult_assoc (/ _)) Rinv_l // Rmult_1_l //.
Qed.
Global Arguments RdivrKA : clear implicits.


Lemma RaddLR r r1 r2 : r1 + r = r2 <-> r1 = r2 - r.
Proof.
by lra.
Qed.


Lemma RsubLR r r1 r2 : r1 - r = r2 <-> r1 = r2 + r.
Proof.
by split => [<-|->]; ring.
Qed.


Lemma eqb_Rplus_eq_reg_r r r1 r2 :
  (r1 + r == r2 + r) = (r1 == r2).
Proof.
apply: eqb_iff.
simplP.
  apply: Rplus_eq_reg_r.
  by eassumption.
exact: Rplus_eq_compat_r.
Qed.


Lemma fold_Rdiv r1 r2 : r1 * / r2 = r1 / r2.
Proof.
by [].
Qed.


Lemma RdivLR r {r_neq : r <> 0} r1 r2 :
  r1 * / r = r2 <-> r1 = r2 * r.
Proof.
split.
- by move=> <-; rewrite RdivrK.
- by move=> ->; rewrite fold_Rdiv RmulrK.
Qed.


Lemma Rle_iff_sub x y : x <= y <-> 0 <= y - x.
Proof.
by lra.
Qed.


Lemma Rle_subLR r r1 r2 : r1 - r <= r2 <-> r1 <= r2 + r.
Proof.
by lra.
Qed.

Lemma Rlt_subLR r r1 r2 : r1 - r < r2 -> r1 < r2 + r.
Proof.
by lra.
Qed.

Lemma Rle_subRL r r1 r2 : r1 <= r2 - r <-> r1 + r <= r2.
Proof.
by lra.
Qed.

Lemma Rlt_subRL r r1 r2 : r1 < r2 - r <-> r1 + r < r2.
Proof.
by lra.
Qed.


Lemma Rler0_addl r r1 : r + r1 <= r1 <-> r <= 0.
Proof.
rewrite -{2}(GRing.add0r r1).
split.
- exact: Rplus_le_reg_r.
- exact: Rplus_le_compat_r.
Qed.


Lemma Rltr0_addl r r1 : r + r1 < r1 <-> r < 0.
Proof.
rewrite -{2}(GRing.add0r r1).
split.
- exact: Rplus_lt_reg_r.
- exact: Rplus_lt_compat_r.
Qed.


Lemma Rle_neq_lt (r1 r2 : R) :
  r1 <= r2 -> r1 <> r2 -> r1 < r2.
Proof.
by case.
Qed.


Lemma Rle_allltle r1 r2 :
  (forall r, r2 < r -> r1 <= r) -> r1 <= r2.
Proof.
move => H.
have := (H ((r1+r2)/2)).
by lra.
Qed.


Lemma Ropp_inj_iff r1 r2 : - r1 = - r2 <-> r1 = r2.
Proof.
by lra.
Qed.


Lemma eq0_from_Rdiv (a b : R) :
  b <> 0 -> a / b = 0 -> a = 0.
Proof.
rewrite /Rdiv. intros.
apply (Rmult_eq_reg_r (/ b)).
- by rewrite Rmult_0_l.
- exact: Rinv_neq_0_compat.
Qed.


Lemma rmul_if_ldiv (a b c : R) :
  b <> 0 -> a / b = c -> a = c * b.
Proof.
move => H <-. by rewrite Rmult_assoc Rinv_l // Rmult_1_r.
Qed.


Lemma ldiv_if_rmul (a b c : R) :
  b <> 0 -> a = c * b -> a / b = c.
Proof.
move => H ->.
rewrite /Rdiv Rmult_assoc Rinv_r // Rmult_1_r //.
Qed.


Lemma Rpmulr_llt0 x y :
  0 < x -> y * x < 0 <-> y < 0.
Proof.
rewrite -{2}(Rmult_0_l x).
split.
- exact: Rmult_lt_reg_r.
- exact: Rmult_lt_compat_r.
Qed.


Lemma Rpmulr_lle0 x y :
  0 < x -> y * x <= 0 <-> y <= 0.
Proof.
rewrite -{2}(Rmult_0_l x).
split.
- exact: Rmult_le_reg_r.
- by apply: Rmult_le_compat_r; exact: Rlt_le.
Qed.




Hint Resolve IZR_lt not_0_IZR : RDb.

Hint Extern 1 (_ < _)%Z => auto with zarith : RDb.
Hint Extern 1 (not (@eq Z _ _)) => auto with zarith : RDb.

Hint Rewrite Rplus_0_l Rplus_0_r Rminus_0_r : RDb.
Hint Rewrite Rplus_opp_l Rplus_opp_r Ropp_involutive : RDb.
Hint Rewrite Rmult_0_l Rmult_0_r Rmult_1_l Rmult_1_r : RDb.
Hint Rewrite Rinv_1 Rinv_l Rinv_r : RDb.

Hint Rewrite Rmult_plus_distr_r : RdvlpDb.

Hint Rewrite RaddKrC : RDb.

Ltac tryR := try easy || (try lra || (try auto with real RDb)).
Hint Rewrite RmulrK using tryR : RDb .





(** about Rabs **)

Lemma Rabs0_r0 (r : R) : Rabs r = 0 -> r = 0.
Proof.
move => H. case: (Req_dec r 0) => //.
intro; contradict H. exact: Rabs_no_R0.
Qed.

Lemma Rabs_sub_bounds eps r1 r2 :
  Rabs (r1 - r2) <= eps  <->  r2 - eps <= r1 <= r2 + eps.
Proof.
split.
- by move/Rabs_le_inv; lra.
- by move=> *; apply Rabs_le; lra.
Qed.


Lemma Rabs_id_iff x : Rabs x = x <-> 0 <= x.
Proof.
split.
- move=> <-.
  exact: Rabs_pos.
- exact: Rabs_pos_eq.
Qed.




(** about IZR and INR **)

Definition IZR_inj : injective IZR := eq_IZR.
Definition IZR_add := plus_IZR.

Lemma IZR_lt_iff k1 k2 : IZR k1 < IZR k2 <-> (k1 < k2)%Z.
Proof.
split.
- exact: lt_IZR.
- exact: IZR_lt.
Qed.

Lemma IZR_le_iff k1 k2 : IZR k1 <= IZR k2 <-> (k1 <= k2)%Z.
Proof.
split.
- exact: le_IZR.
- exact: IZR_le.
Qed.

Lemma mulr1n_INR (n : nat) : n%:R = INR n.
Proof.
elim: n => // n.
rewrite S_INR GRing.mulrSr.
by move=> ->.
Qed.

Lemma mulrn_RmultINR (r : R) (n : nat) : r *+ n = r * INR n.
Proof.
by rewrite -GRing.mulr_natr mulr1n_INR.
Qed.






(** Real numbers that are integers, i.e. of the form IZR _ **)


Definition is_integer (r : R) : bool :=
  IZR (up r) == r + 1.


Lemma up_IZR (z : Z) : up (IZR z) = (z + 1)%Z.
Proof.
symmetry; apply tech_up.
- apply: IZR_lt.
  by lia.
- rewrite plus_IZR.
  exact: Rle_refl.
Qed.


Lemma is_integer_IZR (z : Z) : is_integer (IZR z).
Proof.
by rewrite /is_integer up_IZR plus_IZR //.
Qed.


Lemma is_integer_IZR_eq (z : Z) (r : R) :
  r = IZR z -> is_integer r.
Proof.
move => ->.
exact: is_integer_IZR.
Qed.
Arguments is_integer_IZR_eq : clear implicits.


Lemma is_integer_witness (r : R) :
  is_integer r -> { z : Z | r = IZR z }.
Proof.
move => /eqP H.
exists (Zfloor r).
rewrite plus_IZR H /=.
by ring.
Qed.


Lemma is_integer_add (r1 r2 : R) :
  is_integer r1 -> is_integer r2 ->
  is_integer (r1 + r2).
Proof.
move => H1 H2.
have [z1 Hr1] := is_integer_witness H1.
have [z2 Hr2] := is_integer_witness H2.
rewrite Hr1 Hr2 -plus_IZR.
exact: is_integer_IZR.
Qed.


Lemma is_integer_mul (r1 r2 : R) :
  is_integer r1 -> is_integer r2 ->
  is_integer (r1 * r2).
Proof.
move => H1 H2.
have [z1 Hr1] := is_integer_witness H1.
have [z2 Hr2] := is_integer_witness H2.
rewrite Hr1 Hr2 -mult_IZR.
exact: is_integer_IZR.
Qed.


Lemma eq0_is_integer (r : R) :
  is_integer r -> Rabs r < 1 -> r = 0.
Proof.
move => /is_integer_witness [n Hn] /Rabs_lt_inv [Hlt1 Hlt2].
rewrite Hn. f_equal. apply: Z.le_antisymm.
all: [> apply: Zlt_succ_le | apply Z.lt_pred_le ].
all: apply: lt_IZR.
all: rewrite /= -Hn //.
Qed.


Lemma is_integer_opp (r : R) :
  is_integer r -> is_integer (- r).
Proof.
move => /is_integer_witness [z ->].
rewrite -opp_IZR. exact: is_integer_IZR.
Qed.


Lemma is_integer_opp_eqb (r : R) :
  is_integer (- r) = is_integer r.
Proof.
apply: eqb_iff; split.
rewrite -{2}(Ropp_involutive r).
all: exact: is_integer_opp.
Qed.





(** Real number d divides r, noted "d |d r", if r / d is an integer. **)


Definition Rdivides (r1 r2 : R) : bool := is_integer (r2 * / r1).
Infix " |d " := Rdivides (at level 70).


Lemma Rdivides_refl r {neq0 : r <> 0} : r |d r.
Proof.
rewrite /Rdivides Rinv_r //.
exact: is_integer_IZR.
Qed.


Lemma Rdivides_trans y {neq0 : y <> 0} x z :
  x |d y -> y |d z -> x |d z.
Proof.
move=> xy yz.
rewrite /Rdivides -(RdivrKA y) //.
exact: is_integer_mul.
Qed.


Lemma Rdivides_add r r1 r2 :
  r |d r1 -> r |d r2 -> r |d (r1 + r2).
Proof.
rewrite /Rdivides Rmult_plus_distr_r.
exact: is_integer_add.
Qed.


Lemma Rdivides_opp r r1 : r |d r1 -> r |d (- r1).
Proof.
rewrite /Rdivides -Ropp_mult_distr_l.
exact: is_integer_opp.
Qed.


Lemma Rdivides_opp_eqb r r1 : (r |d (- r1)) =  (r |d r1).
Proof.
apply: eqb_iff; split; last exact: Rdivides_opp.
move/Rdivides_opp.
by rewrite Ropp_involutive.
Qed.


Lemma Rdivides_sub r r1 r2 : r |d r1 -> r |d r2 -> r |d (r1 - r2).
Proof.
move=> *; apply: Rdivides_add => //.
exact: Rdivides_opp.
Qed.


Lemma Rdividesr0 r : r |d 0.
Proof.
rewrite /Rdivides Rmult_0_l.
exact: is_integer_IZR.
Qed.


Lemma Rdivides_mulIZR (r : R) (k : Z) :
  r <> 0 -> r |d (IZR k) * r.
Proof.
move => H.
rewrite /Rdivides Rmult_assoc Rinv_r // Rmult_1_r.
exact: is_integer_IZR.
Qed.


Lemma Rdivides_mulIZR_ex (r1 r2 : R) :
  r1 <> 0 -> (exists k : Z, r2 = (IZR k) * r1) -> r1 |d r2.
Proof.
move => H [k ->]. exact: Rdivides_mulIZR.
Qed.


Lemma Rdivides1_IZR (m : Z) : 1 |d IZR m.
Proof.
rewrite -[X in _ |d X]Rmult_1_r.
exact: Rdivides_mulIZR.
Qed.


Lemma Rdivides1_integer r : 1 |d r = is_integer r.
Proof.
by rewrite /Rdivides Rinv_1 Rmult_1_r.
Qed.






(** equality modulo a real number **)


Definition eqmodr (r r1 r2 : R) : bool := r |d r1 - r2.


Lemma eqmodr_refl r : reflexive (eqmodr r).
Proof.
move => r1; rewrite /eqmodr Rminus_diag_eq //.
exact: Rdividesr0.
Qed.


Lemma eqmodr_sym r : symmetric (eqmodr r).
Proof.
move => r1 r2.
rewrite /eqmodr -Rdivides_opp_eqb.
by rewrite Ropp_minus_distr.
Qed.


Lemma eqmodr_trans r : transitive (eqmodr r).
Proof.
move => r1 r2 r3 *.
rewrite /eqmodr -(RaddrNKKNr r1).
exact: Rdivides_add.
Qed.


Lemma eqmodr_add (r r1 r2 r3 r4 : R) :
  eqmodr r r1 r2 -> eqmodr r r3 r4 -> eqmodr r (r1 + r3) (r2 + r4).
Proof.
move/Rdivides_add => d12r; move/d12r.
rewrite /eqmodr; f_imply; f_equal.
by ring.
Qed.


Lemma eqmodr_opp (r r1 r2 : R) :
  eqmodr r r1 r2 -> eqmodr r (- r1) (- r2).
Proof.
move => eqmm; rewrite /eqmodr /Rminus -Ropp_plus_distr.
exact: Rdivides_opp.
Qed.


Lemma eqmodr_sub (r x y z t : R) :
  eqmodr r x y -> eqmodr r z t -> eqmodr r (x - z) (y - t).
Proof.
move=> *; apply: eqmodr_add => //.
exact: eqmodr_opp.
Qed.


Lemma eqmodr_add2l r r1 r2 r3 :
  eqmodr r (r1 + r2) (r1 + r3) = eqmodr r r2 r3.
Proof.
apply: eqb_iff; split => H.
  rewrite -(addKrC r1 r2) -(addKrC r1 r3).
all: apply: eqmodr_add => //.
all: exact: eqmodr_refl.
Qed.


Lemma eq_difflt_eqmodr (r x y : R) :
  Rabs (x - y) < r -> eqmodr r x y -> x = y.
Proof.
move => Hlt /is_integer_witness [n Hn].
have Hp_gt0 := (Rle_lt_trans _ _ _ (Rabs_pos (x - y)) Hlt).
have Hp_neq0 := not_eq_sym (Rlt_not_eq _ _ Hp_gt0).
apply: Rminus_diag_uniq.
apply: (eq0_from_Rdiv Hp_neq0).
apply: eq0_is_integer.
  rewrite /Rdiv Hn.
  exact: is_integer_IZR.
rewrite Rabs_mult (Rabs_right (/ r)); last first.
  by apply: Rgt_ge; exact: Rinv_0_lt_compat.
apply: (Rmult_lt_reg_r r) => //.
by rewrite Rmult_assoc Rinv_l // Rmult_1_l Rmult_1_r //.
Qed.


Lemma eqmodr_addmultiple r {r_neq0 : r <> 0} r1 k :
  eqmodr r (r1 + IZR k * r) r1.
Proof.
rewrite /eqmodr.
autorewrite with RDb.
exact: Rdivides_mulIZR.
Qed.


Lemma eqmodr_submultiple r {r_neq0 : r <> 0} r1 k :
  eqmodr r (r1 - IZR k * r) r1.
Proof.
rewrite -(Z.opp_involutive k) opp_IZR.
rewrite /Rminus Ropp_mult_distr_l Ropp_involutive.
exact: eqmodr_addmultiple.
Qed.


Lemma eqmodr_addmod r {r_neq0 : r <> 0} r1 : eqmodr r (r1 + r) r1.
Proof.
rewrite -{2}(Rmult_1_l r).
exact: eqmodr_addmultiple.
Qed.


Lemma eqmodr_addmulwitness r {r_neq0 : r <> 0} r1 r2 :
  eqmodr r r1 r2 -> { k : Z | r1 = r2 + IZR k * r }.
Proof.
move=> /is_integer_witness [k Hk].
exists k.
rewrite -Hk RmulrVK //.
by ring.
Qed.


Lemma eqmodr_mul_r r {r_neq0 : r <> 0} m {m_neq0 : m <> 0} r1 r2 :
  eqmodr m r1 r2 -> eqmodr (m * r) (r1 * r) (r2 * r).
Proof.
move=> eqmm.
rewrite /eqmodr /Rdivides -Rmult_minus_distr_r /Rdiv Rmult_assoc.
rewrite Rinv_mult_distr // (Rmult_comm r) Rmult_assoc.
rewrite Rinv_l // Rmult_1_r.
by assumption.
Qed.
Global Arguments eqmodr_mul_r r {r_neq0} m {m_neq0}.


Lemma eqmodr_mul_r_eq r {r_neq0 : r <> 0} m {m_neq0 : m <> 0} r1 r2 :
  eqmodr (m * r) (r1 * r) (r2 * r) = eqmodr m r1 r2.
Proof.
apply: eqb_iff; split; last first.
  exact: eqmodr_mul_r.
move=> /(eqmodr_mul_r (/ r)).
rewrite !Rmult_assoc Rinv_r // !Rmult_1_r //.
move=> -> //.
- exact: Rinv_neq_0_compat.
- exact: Rmult_integral_contrapositive_currified.
Qed.
Global Arguments eqmodr_mul_r_eq r {r_neq0} m {m_neq0}.


Lemma eqmod1_IZR (z z' : Z) : eqmodr 1 (IZR z) (IZR z').
Proof.
rewrite /eqmodr /Rdivides /Rdiv Rinv_1 Rmult_1_r -minus_IZR.
exact: is_integer_IZR.
Qed.


Lemma eqmodr_IZR (N n1 n2 : Z) :
  (N <> 0)%Z ->
  eqm N n1 n2 <-> eqmodr (IZR N) (IZR n1) (IZR n2).
Proof.
move => HN. have HIZRN := IZR_neq _ _ HN.
split.
- move => H. apply eqm_diff_0 in H.
  apply Z_div_exact_full_2 in H; last assumption.
  rewrite /eqmodr.
  apply (is_integer_IZR_eq ((n1 - n2) / N)%Z).
  apply: ldiv_if_rmul; first assumption.
  rewrite -minus_IZR -mult_IZR Zmult_comm.
  by f_equal.
- move => /is_integer_witness [m Hm].
  apply rmul_if_ldiv in Hm. 2: assumption.
  move: Hm. rewrite -minus_IZR -mult_IZR.
  move => /eq_IZR H.
  apply eqm_diff_0.
  rewrite H. exact: Z_mod_mult.
Qed.


Lemma eqmodr_Rdivides m {neq0 : m <> 0} m' r1 r2 :
  m' |d m -> eqmodr m r1 r2 -> eqmodr m' r1 r2.
Proof.
exact: Rdivides_trans.
Qed.
Global Arguments eqmodr_Rdivides : clear implicits.


Lemma eqmodr0 r r1 : eqmodr r r1 0 = (r |d r1).
Proof.
by rewrite /eqmodr Rminus_0_r.
Qed.


Lemma Rdivides_eqmodr m r r' :
  eqmodr m r r' -> m |d r -> m |d r'.
Proof.
rewrite -!eqmodr0 eqmodr_sym. exact: eqmodr_trans.
Qed.


Lemma Rdivides_eqmodr_all d m {m_neq0 : m <> 0} r r' :
  eqmodr m r r' -> d |d m -> d |d r -> d |d r'.
Proof.
move=> mrr' dm dr.
apply: Rdivides_eqmodr; last first.
  exact: dr.
apply: eqmodr_Rdivides; last first.
  exact: mrr'.
all: assumption.
Qed.


Lemma eqmodr_add_divided m r r1 r2 :
  m |d r -> eqmodr m r1 r2 -> eqmodr m (r1 + r) r2.
Proof.
move=> *.
rewrite -(GRing.addr0 r2).
apply: eqmodr_add => //.
by rewrite /eqmodr Rminus_0_r.
Qed.


Lemma eqmodr_add_divided_iff m r r1 r2 :
  m |d r -> eqmodr m r1 r2 <-> eqmodr m (r1 + r) r2.
Proof.
move=> *.
split.
  exact: eqmodr_add_divided.
move=> *; rewrite -(GRing.addrK r r1).
apply: eqmodr_add_divided => //.
exact: Rdivides_opp.
Qed.


Lemma Rle_addL_eqmodr r r1 r2 :
  eqmodr r r1 r2 -> r1 < r2 -> r1 + r <= r2.
Proof.
case_eq (Rltb 0 r) => /RltP; last by lra.
move=> r_gt0.
rewrite /eqmodr /Rdivides.
move/is_integer_witness => [k].
rewrite RdivLR; last by lra.
rewrite RsubLR.
move=> ->.
rewrite Rltr0_addl Rplus_assoc (Rplus_comm r2) -Rplus_assoc Rler0_addl.
rewrite -{3}(Rmult_1_l r) -Rmult_plus_distr_r -IZR_add.
rewrite Rpmulr_llt0 // Rpmulr_lle0 //.
rewrite IZR_lt_iff IZR_le_iff Z.add_1_r.
exact: Zlt_le_succ. 
Qed.


Lemma is_integer_RdividesIZR k {k_neq0 : (k <> 0)%Z} r :
  IZR k |d r -> is_integer r.
Proof.
rewrite -Rdivides1_integer.
apply: Rdivides_trans.
- exact: IZR_neq.
- exact: Rdivides1_IZR.
Qed.


Lemma eqmod1r0 r : eqmodr 1 r 0 = is_integer r.
Proof.
by rewrite eqmodr0 Rdivides1_integer.
Qed.


Lemma is_integer_eqmodrIZR_integer (k : Z) {k_neq0 : (k <> 0)%Z} (x r : R) :
  is_integer x -> eqmodr (IZR k) x r -> is_integer r.
Proof.
move=> x_ii kxr.
rewrite -eqmod1r0.
apply: eqmodr_trans; last first.
  by rewrite eqmod1r0; exact: x_ii.
rewrite eqmodr_sym.
apply: eqmodr_Rdivides; last exact kxr.
  exact: IZR_neq.
exact: Rdivides1_IZR.
Qed.
Arguments is_integer_eqmodrIZR_integer k {k_neq0}.


Lemma eqb_is_integer_eqmodrIZR (k : Z) {k_neq0 : (k <> 0)%Z} (r1 r2 : R) :
  eqmodr (IZR k) r1 r2 -> is_integer r1 = is_integer r2.
Proof.
move=> *; apply: eqb_iff.
split.
all: move=> r_ii; apply: (is_integer_eqmodrIZR_integer k _ _ r_ii) => //.
by rewrite eqmodr_sym.
Qed.
Arguments eqb_is_integer_eqmodrIZR k {k_neq0}.







(** Fractional part **)


Definition fracpart (x : R) := x - IZR (Zfloor x).


Lemma bounds_fracpart x : 0 <= fracpart x < 1.
Proof.
rewrite /fracpart.
have := (Zfloor_lb x).
have := (Zfloor_ub x).
by lra.
Qed.


Lemma eq_eqmodr_inbounds (p x y : R) :
  0 <= x < p -> 0 <= y < p -> eqmodr p x y -> x = y.
Proof.
move => Hx Hy Heqm; apply: eq_difflt_eqmodr; last first.
  exact Heqm.
by apply: Rabs_lt; lra.
Qed.


Lemma eq_fracpart_eqmodr1 x y :
  eqmodr 1 x y -> fracpart x = fracpart y.
Proof.
move => Heqm. apply: eq_eqmodr_inbounds.
  1,2: exact: bounds_fracpart.
apply: eqmodr_sub. assumption.
exact: eqmod1_IZR.
Qed.


Lemma sub_r_fracpart (x : R) :
  x - fracpart x = IZR (Zfloor x).
Proof.
rewrite /fracpart.
by ring.
Qed.






(** Zfloor, Zceil **)


Lemma iff_is_integer_Zfloor (r : R) :
  is_integer r <-> (IZR (Zfloor r) = r).
Proof.
split.
- rewrite minus_IZR. move => /eqP ->. ring.
- move => <-. exact: is_integer_IZR.
Qed.


Lemma IZR_Zfloor_is_integer (r : R) :
  is_integer r -> (IZR (Zfloor r) = r).
Proof.
apply iff_is_integer_Zfloor.
Qed.


Lemma iff_is_integer_Zceil (r : R) :
  is_integer r <-> (IZR (Zceil r) = r).
Proof.
apply: iff_trans.
  rewrite -is_integer_opp_eqb. exact: iff_is_integer_Zfloor.
rewrite opp_IZR. split.
- move => ->. ring.
- move => H. rewrite -[in RHS]H. ring.
Qed.


Lemma IZR_Zceil_is_integer (r : R) :
  is_integer r -> (IZR (Zceil r) = r).
Proof.
apply iff_is_integer_Zceil.
Qed.


Lemma iff_is_integer_Zfloor_Zceil (r : R) :
  is_integer r <-> (Zfloor r = Zceil r).
Proof.
split.
- intro. apply: eq_IZR.
  rewrite IZR_Zfloor_is_integer // IZR_Zceil_is_integer //.
- intro. apply iff_is_integer_Zfloor. apply: Rle_antisym.
  + exact: Zfloor_lb.
  + rewrite H. exact: Zceil_ub.
Qed.


Lemma Zfloor_Zceil_is_integer (r : R) :
  is_integer r -> (Zfloor r = Zceil r).
Proof.
apply iff_is_integer_Zfloor_Zceil.
Qed.


Lemma iff_neg_integer_Zfloor_Zceil (r : R) :
  ~~ is_integer r <-> (Zfloor r + 1 = Zceil r)%Z.
Proof.
split.
- intro; symmetry. apply: Zceil_floor_neq.
  rewrite -iff_is_integer_Zfloor. by apply/negP.
- move => H. apply/negP.
  rewrite iff_is_integer_Zfloor_Zceil -H. auto with zarith.
Qed.


Lemma Zfloor_Zceil_not_integer (r : R) :
  ~ is_integer r -> (Zfloor r + 1 = Zceil r)%Z.
Proof.
move => /negP. apply iff_neg_integer_Zfloor_Zceil.
Qed.


Lemma eqm_Zfloor (k : Z) {k_neq0 : (k <> 0)%Z} (r1 r2 : R) :
  eqmodr (IZR k) r1 r2 -> eqm k (Zfloor r1) (Zfloor r2).
Proof.
move=> Heqm; rewrite eqmodr_IZR //.
rewrite -!sub_r_fracpart.
apply: eqmodr_sub => //.
erewrite eq_fracpart_eqmodr1.
  exact: eqmodr_refl.
apply: eqmodr_Rdivides. 3: exact Heqm.
exact: not_0_IZR. exact: Rdivides1_IZR.
Qed.


Lemma eqm_Zceil (k : Z) {k_neq0 : (k <> 0)%Z} (r1 r2 : R) :
  eqmodr (IZR k) r1 r2 -> eqm k (Zceil r1) (Zceil r2).
Proof.
move=> *.
case_bool (is_integer r1).
- move=> *; rewrite -!Zfloor_Zceil_is_integer //; last first.
    exact: (is_integer_eqmodrIZR_integer k r1).
  exact: eqm_Zfloor.
- move => *; rewrite -!Zfloor_Zceil_not_integer //; last first.
    by rewrite -(eqb_is_integer_eqmodrIZR k r1).
  apply: Zplus_eqm.
  + exact: eqm_Zfloor.
  + exact: eqm_refl.
Qed.






(** Periodic function **)


Section Periodic.


Variable p : R.

Context { p_neq0 : p <> 0 }.


Context { T : Type }.

Variable f : R -> T.


Definition periodic := forall r, f (r + p) = f r.


Lemma eq_periodic_addmulkp r k :
  periodic -> f (r + IZR k * p) = f r.
Proof.
move=> Hper.
move: k; apply: Z.bi_induction.
  by autorewrite with RDb.
move=> k; rewrite plus_IZR.
autorewrite with RdvlpDb RDb.
by rewrite -Rplus_assoc Hper.
Qed.


Lemma eq_periodic_eqmodr r r' :
  periodic -> eqmodr p r r' -> f r = f r'.
Proof.
move=> Hper /(@eqmodr_addmulwitness _ p_neq0) [k ->].
exact: eq_periodic_addmulkp.
Qed.


End Periodic.






(** Quasi periodicity for addition of the period **)

Definition quasiperR p (f : R -> R) :=
  forall r, f (r + p) = f r + p.

Definition quasiperZ m (f : R -> Z) :=
  forall r, f (r + IZR m) = (f r + m)%Z.


Lemma eq_addmulkp_quasiperR p f k r :
  quasiperR p f -> f (r + IZR k * p) = f r + IZR k * p.
Proof.
move=> Hpreg.
move: k; apply: Z.bi_induction.
  by autorewrite with RDb.
move=> k; rewrite plus_IZR.
autorewrite with RdvlpDb RDb.
rewrite -Rplus_assoc Hpreg.
by split; lra.
Qed.


Lemma eqmodr_quasiperR p { p_neq0 : p <> 0 } f r r' :
  quasiperR p f -> eqmodr p r r' -> eqmodr p (f r) (f r').
Proof.
move=> Hpreg /(@eqmodr_addmulwitness p p_neq0) [k ->].
rewrite eq_addmulkp_quasiperR //.
exact: eqmodr_addmultiple.
Qed.




Hint Rewrite Zplus_0_r : Zdb.
Hint Rewrite Zmult_1_l : Zdb.

Hint Rewrite Zmult_plus_distr_l : ZdvlpDb.

Lemma quasiperZ_multiple m k f :
  quasiperZ m f -> quasiperZ (k * m) f.
Proof.
move=> Hpreg.
move: k; apply: Z.bi_induction.
  by move=> r; autorewrite with RDb Zdb.
move=> k.
rewrite /quasiperZ !mult_IZR plus_IZR /Z.succ.
autorewrite with RdvlpDb RDb ZdvlpDb Zdb.
split=> IH r; move: (IH r).
all: rewrite -Rplus_assoc Hpreg.
all: by move=> IHr; lia.
Qed.





Lemma up_S r : up (r + 1) = (up r + 1)%Z.
Proof.
have := (archimed r).
symmetry.
by apply: tech_up; rewrite plus_IZR; lra.
Qed.

Lemma quasiperZ_Zfloor z : quasiperZ z Zfloor.
Proof.
rewrite -(Z.mul_1_r z).
apply: quasiperZ_multiple.
move=> r.
rewrite /Zfloor up_S.
by ring.
Qed.


Lemma is_integer_addIZR r z :
  is_integer (r + IZR z) = is_integer r.
Proof.
apply: eqb_iff; split => Hint.
1: replace r with (r + IZR z - IZR z) by ring.
all: apply: is_integer_add => //.
1: apply: is_integer_opp.
all: exact: is_integer_IZR.
Qed.


Lemma quasiperZ_Zceil z : quasiperZ z Zceil.
Proof.
move=> r.
case_bool (is_integer r).
- move=> Hr.
  have:= Hr; rewrite -(is_integer_addIZR r z) => Hrz.
  rewrite -!Zfloor_Zceil_is_integer //.
  exact: quasiperZ_Zfloor.
- move=> Hr.
  have:= Hr; rewrite -(is_integer_addIZR r z) => Hrz.
  rewrite -!Zfloor_Zceil_not_integer //.
  rewrite quasiperZ_Zfloor.
  by ring.
Qed.









(** R as a numDomainType **)


Definition rwRleP r1 r2 := rwP (RleP r1 r2).
Definition rwRltP r1 r2 := rwP (RltP r1 r2).
Hint Rewrite <- rwRleP rwRltP : b2PDb.

Lemma eqr_eq_op : eqr = (@eq_op real_eqType).
Proof.
by [].
Qed.
Hint Rewrite eqr_eq_op : b2PDb.


Fact Rleb_norm_add x y :
  Rleb (Rabs (x + y)) (Rabs x + Rabs y).
Proof.
apply/RleP.
exact: Rabs_triang.
Qed.

Fact Rltb_add x y :
  Rltb 0 x -> Rltb 0 y -> Rltb 0 (x + y).
Proof.
rewrite -!rwRltP.
exact: Rplus_lt_0_compat.
Qed.

Definition Req0_norm := Rabs_eq_R0.

Fact Rleb_total x y : Rleb x y || Rleb y x.
Proof.
case Hcase: Rleb => //=.
move: Hcase => /RleP.
rewrite -rwRleP.
by lra.
Qed.

Definition RnormM := Rabs_mult.

Fact Rleb_def x y :
  Rleb x y = (Rabs (y - x)%RT == (y - x)%RT).
Proof.
apply: eqb_iff.
autorewrite with b2PDb.
by rewrite Rle_iff_sub Rabs_id_iff.
Qed.

Fact Rltb_def x y :
  Rltb x y = (y != x) && Rleb x y.
Proof.
apply: eqb_iff.
autorewrite with b2PDb.
by lra.
Qed.


Definition R_numMixin :=
  NumMixin Rleb_norm_add Rltb_add Req0_norm (in2W Rleb_total)
           RnormM Rleb_def Rltb_def.

Canonical Structure R_numDomainType :=
  Eval hnf in NumDomainType R R_numMixin.






(** sum over R **)


Lemma Rabs_sum_le (I : Type) (r : seq I) (P : ssrbool.pred I)
                   (F : I -> R) :
  Rabs (\sum_(i <- r | P i) F i) <= \sum_(i <- r | P i) Rabs (F i).
Proof.
elim: r.
  rewrite !big_nil Rabs_R0 -[GRing.zero _]/0.
  by lra.
move=> h t IH.
rewrite !big_cons.
case: ifP => // _.
apply: Rle_trans.
apply: Rabs_triang.
rewrite -{2}[@GRing.add _]/Rplus.
by lra.
Qed.


Lemma sum_Rle (I : Type) (r : seq I) (P : ssrbool.pred I) (F1 F2 : I -> R) :
  (forall i, P i -> F1 i <= F2 i) ->
  \sum_(i <- r | P i) F1 i <= \sum_(i <- r | P i) F2 i.
Proof.
move=> all_le.
apply/RleP.
apply: (@Num.Theory.ler_sum R_numDomainType).
move=> i Pi; apply/rwRleP.
by auto.
Qed.


Lemma sum_Rlt (I : eqType) (r : seq I)
              (P : ssrbool.pred I) (F1 F2 : I -> R) :
  has P r -> (forall i, P i -> F1 i < F2 i) ->
  \sum_(i <- r | P i) F1 i < \sum_(i <- r | P i) F2 i.
Proof.
move=> hasPr all_lt.
apply/RltP.
apply: (@ltr_sum R_numDomainType) => //.
intros; apply/rwRltP.
by auto.
Qed.


Lemma sum_ord_Rle {n : nat} (F1 : 'I_n -> R) (F2 : 'I_n -> R) :
  (forall i : 'I_n, (F1 i <= F2 i)%R) ->
  (\sum_(i < n) F1 i <= \sum_(i < n) F2 i)%R.
Proof.
move: n F1 F2; apply: nat_ind.
  move=> *; rewrite !big_ord0.
  exact: Rle_refl.
move=> n IH F1 F2 Fle; rewrite !big_ord_recl.
by apply: Rplus_le_compat; auto.
Qed.


Lemma sum_nat_Rle (low up : nat) (x1 x2 : nat -> R) :
  (forall i : nat, (low <= i < up)%nat -> (x1 i <= x2 i)%R) ->
  (\sum_(low <= i < up) x1 i <= \sum_(low <= i < up) x2 i)%R.
Proof.
move => xi_le; rewrite !big_ord_of_any_nat.
apply: sum_ord_Rle => i.
apply: xi_le.
by auton.
Qed.


Lemma sum_ord_Rabs {n : nat} (F : 'I_n -> R) :
  (Rabs (\sum_(i < n) F i) <= \sum_(i < n) Rabs (F i))%R.
Proof.
move: n F; apply: nat_ind.
  move=> F; rewrite !big_ord0 Rabs_R0.
  exact: Rle_refl.
move=> n IR F; rewrite !big_ord_recl.
apply: Rle_trans.
  exact: Rabs_triang.
apply: Rplus_le_compat => //.
exact: Rle_refl.
Qed.


Lemma sum_nat_Rabs (low up : nat) (x : nat -> R) :
  (Rabs (\sum_(low <= i < up) x i) <=
  \sum_(low <= i < up) Rabs (x i))%R.
Proof.
rewrite !big_ord_of_any_nat.
exact: sum_ord_Rabs.
Qed.




Definition absmx {h w : nat} (M : 'M[R]_(h, w)) :=
  \matrix_(i, j) Rabs (M i j).

