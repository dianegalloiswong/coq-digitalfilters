(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import all_ssreflect ssrint.

From Flocq
Require Import Core Operations.

Require Import ProofIrrelevance
               ZArith
               Reals Lra Lia.

Require Import utils Z_complements R_complements.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope R.






(** about Znearest **)


Lemma Znearest_ge choice r :
  r - / 2 <= IZR (Znearest choice r).
Proof.
have:= Znearest_half choice r.
move=> /Rabs_le_inv.
by lra.
Qed.


Lemma Znearest_le choice r :
  IZR (Znearest choice r) <= r + / 2.
Proof.
have:= Znearest_half choice r.
move=> /Rabs_le_inv.
by lra.
Qed.


Lemma Znearest_mid_up_true choice r :
  IZR (Znearest choice r) = r + /2 ->
  choice (Zfloor r).
Proof.
rewrite /Znearest.
case_eq (Rcompare (r - IZR (Zfloor r)) (/ 2)).
- case: choice => //.
  move=> /Rcompare_Eq_inv.
  by lra.
- have:= (Zfloor_lb r).
  by lra.
- move=> /Rcompare_Gt_inv diff_gt.
  have:= (Zceil_floor_neq r).
  apply: ask_proof; first lra.
  move=> ->. rewrite plus_IZR.
  by lra.
Qed.
Global Arguments Znearest_mid_up_true : clear implicits.


Lemma Znearest_lt choice r :
  ~~ choice (Zfloor r) ->
  IZR (Znearest choice r) < r + / 2.
Proof.
case: (Znearest_half choice r).
  move=> /Rabs_lt_inv.
  by lra.
rewrite /Rabs. case: Rcase_abs; last first.
  by lra.
move=> _ diff_eq /negP.
have:= (Znearest_mid_up_true choice r).
apply: ask_proof.
  by lra.
by [].
Qed.
Global Arguments Znearest_lt : clear implicits.


Lemma Znearest_mid choice r :
  Rabs (r - IZR (Znearest choice r)) = / 2 ->
  r - IZR (Zfloor r) = / 2.
Proof.
rewrite /Znearest.
case_eq (Rcompare (r - IZR (Zfloor r)) (/ 2)).
- by move=> /Rcompare_Eq_inv.
- move=> /Rcompare_Lt_inv (* diff_lt *) (* abs_diff *).
  rewrite /Rabs; case: Rcase_abs => //.
  have := Zfloor_lb r. lra.
- move=> /Rcompare_Gt_inv.
  rewrite /Rabs; case: Rcase_abs; last first.
    by have := Zceil_ub r; lra.
  move=> _ Hfloor Hceil. move: Hceil.
  have:= (Zceil_floor_neq r).
  apply: ask_proof; first lra.
  move=> ->. rewrite plus_IZR.
  by lra.
Qed.


Lemma Znearest_mid_choice choice r :
  Rabs (r - IZR (Znearest choice r)) = / 2 ->
  Znearest choice r = if choice (Zfloor r) then Zceil r else Zfloor r.
Proof.
move=> /Znearest_mid.
rewrite /Znearest.
case_eq (Rcompare (r - IZR (Zfloor r)) (/ 2)) => //.
- by move=> /Rcompare_Lt_inv; lra.
- by move=> /Rcompare_Gt_inv; lra.
Qed.


Lemma Znearest_gt (choice : Z -> bool) r :
  choice (Zfloor r) ->
  r - / 2 < IZR (Znearest choice r).
Proof.
case: (Znearest_half choice r).
  move=> /Rabs_lt_inv.
  by lra.
move=> /Znearest_mid_choice -> ->.
by have:= (Zceil_ub r); lra.
Qed.
Global Arguments Znearest_gt : clear implicits.









(** about bpow / twopow **)


Definition bpow_ge0 := bpow_ge_0.
Definition bpow_gt0 := bpow_gt_0.


Lemma bpow_neq0 (r : radix) (e : Z) : bpow r e <> 0.
Proof.
apply: not_eq_sym.
apply: Rlt_not_eq.
exact: bpow_gt0.
Qed.


Hint Resolve bpow_neq0 : RDb.
Hint Resolve bpow_gt0 : RDb.


Lemma bpow_IZR (r : radix) (e : Z) :
  (0 <= e)%Z -> bpow r e = IZR (Z.pow r e).
Proof.
by elim: e.
Qed.


Notation twopow := (bpow radix2).


Lemma twopow_gt0 e : 0 < twopow e.
Proof.
exact: bpow_gt0.
Qed.


Lemma twopowPe_half e : twopow (e - 1) = / 2 * twopow e.
Proof.
replace e with (e - 1 + 1)%Z at 2 by ring.
rewrite bpow_plus_1 -Rmult_assoc Rinv_l //. ring.
Qed.


Lemma twopowPe_half' e : twopow (e - 1) = twopow e / 2.
Proof.
by rewrite twopowPe_half Rmult_comm.
Qed.


Lemma bpow_mulbeta_pred r e :
  bpow r e = IZR r * bpow r (e-1).
Proof.
rewrite -[in LHS](ZsubzK 1 e).
exact: bpow_plus_1.
Qed.


Lemma twopow_double_pred e :
  twopow e = twopow (e-1) + twopow (e-1).
Proof.
rewrite bpow_mulbeta_pred. exact: double.
Qed.


Lemma Rdivides_bpow (beta : radix) (e1 e2 : Z) :
  (e1 <= e2)%Z -> bpow beta e1 |d bpow beta e2.
Proof.
move=> e12; rewrite -(Z.sub_add e1 e2).
rewrite bpow_plus [X in X*_]bpow_IZR; last by lia.
apply: Rdivides_mulIZR.
exact: bpow_neq0.
Qed.


Lemma eqmodr_bpowle (beta : radix) (e e' : Z) (r1 r2 : R) :
  (e' <= e)%Z -> eqmodr (bpow beta e) r1 r2 ->
  eqmodr (bpow beta e') r1 r2.
Proof.
move => Hle Heqm. apply: eqmodr_Rdivides.
- 3: exact: Heqm.
- exact: bpow_neq0.
- exact: Rdivides_bpow.
Qed.










(** about generic_format with FIX_exp **)


Lemma FIX_format_plus beta emin (x1 x2 : R) :
  FIX_format beta emin x1 ->
  FIX_format beta emin x2 ->
  FIX_format beta emin (x1 + x2).
Proof.
move => [f1 Hf1_val Hf1_exp] [f2 Hf2_val Hf2_exp].
apply FIX_spec with (f := (Fplus f1 f2)).
- by rewrite F2R_plus Hf1_val Hf2_val.
- by rewrite Fexp_Fplus Hf1_exp Hf2_exp Z.min_id.
Qed.


Lemma generic_format_FIX_plus beta emin (x1 x2 : R) :
  generic_format beta (FIX_exp emin) x1 ->
  generic_format beta (FIX_exp emin) x2 ->
  generic_format beta (FIX_exp emin) (x1 + x2).
Proof.
move => H1 H2.
apply: generic_format_FIX.
apply FIX_format_generic in H1.
apply FIX_format_generic in H2.
exact: FIX_format_plus.
Qed.


Lemma Fexp_Fmult (beta : radix) (f1 f2 : float beta) :
  Fexp (Fmult f1 f2) = (Fexp f1 + Fexp f2)%Z.
Proof.
by case: f1; case: f2.
Qed.


Lemma FIX_format_mult beta emin1 emin2 (x1 x2 : R) :
  FIX_format beta emin1 x1 ->
  FIX_format beta emin2 x2 ->
  FIX_format beta (emin1 + emin2) (x1 * x2).
Proof.
move => [f1 Hf1_val Hf1_exp] [f2 Hf2_val Hf2_exp].
apply FIX_spec with (f := (Fmult f1 f2)).
- by rewrite F2R_mult Hf1_val Hf2_val.
- by rewrite Fexp_Fmult Hf1_exp Hf2_exp.
Qed.


Lemma generic_format_FIX_mult beta emin1 emin2 (x1 x2 : R) :
  generic_format beta (FIX_exp emin1) x1 ->
  generic_format beta (FIX_exp emin2) x2 ->
  generic_format beta (FIX_exp (emin1 + emin2)) (x1 * x2).
Proof.
move => H1 H2.
apply: generic_format_FIX.
apply FIX_format_generic in H1.
apply FIX_format_generic in H2.
exact: FIX_format_mult.
Qed.


Lemma succFIX (beta : radix) (lsb : Z) (x : R) :
  succ beta (FIX_exp lsb) x = x + bpow beta lsb.
Proof.
rewrite /succ. case: Rle_bool.
- by rewrite ulp_FIX.
- rewrite /pred_pos. case: Req_bool.
  + rewrite /FIX_exp. ring.
  + rewrite ulp_FIX. ring.
Qed.


Lemma predFIX (beta : radix) (lsb : Z) (x : R) :
  pred beta (FIX_exp lsb) x = x - bpow beta lsb.
Proof.
rewrite /pred succFIX. ring.
Qed.


Lemma succFIX_gt (beta : radix) (lsb : Z) (x : R) :
  x < succ beta (FIX_exp lsb) x.
Proof.
rewrite succFIX. have := (bpow_gt0 beta lsb). lra.
Qed.









(** about log2 **)


Lemma ln_neq0 (x : R) : 0 < x -> x <> 1 -> ln x <> 0.
Proof.
move => Hgt0 /Rdichotomy [Hlt1|Hgt1].
apply: Rlt_not_eq.
  2: apply: Rgt_not_eq; apply: Rlt_gt.
all: rewrite -ln_1; apply: ln_increasing => //.
by auto with real.
Qed.


Lemma ln_gt0 (x : R) : 1 < x -> 0 < ln x.
Proof.
rewrite -ln_1.
apply: ln_increasing.
by lra.
Qed.


Fact ln2_gt0 : 0 < ln 2.
Proof.
by apply: ln_gt0; lra.
Qed.


Fact ln2_neq0 : ln 2 <> 0.
Proof.
by have := ln2_gt0; lra.
Qed.


Definition Rlog2 (r : R) := ln r / ln 2.


Lemma Rlog2_correct r : 0 < r -> Rpower 2 (Rlog2 r) = r.
Proof.
move => r_gt0; rewrite /Rpower Rmult_assoc Rinv_l; last first.
  exact: ln2_neq0.
by rewrite Rmult_1_r exp_ln.
Qed.
Global Arguments Rlog2_correct : clear implicits.


Lemma Rpower_bpow (radix : radix) (z : Z) :
  Rpower (IZR radix) (IZR z) = bpow radix z.
Proof.
rewrite bpow_powerRZ powerRZ_Rpower //.
exact: radix_pos.
Qed.


Lemma Rlog2_Rpower r : Rlog2 (Rpower 2 r) = r.
Proof.
rewrite /Rlog2 ln_exp.
apply: RmulrK.
exact: ln2_neq0.
Qed.


Lemma Rlog2_twopow z : Rlog2 (twopow z) = IZR z.
Proof.
by rewrite -Rpower_bpow Rlog2_Rpower.
Qed.


Lemma Rlog2_inj r1 r2 :
  0 < r1 -> 0 < r2 -> Rlog2 r1 = Rlog2 r2 -> r1 = r2.
Proof.
move=> r1_gt0 r2_gt0.
rewrite /Rlog2 /Rdiv.
move=> /Rmult_eq_reg_r eq_ln.
apply: ln_inv => //.
apply: eq_ln.
apply: Rinv_neq_0_compat.
exact: ln2_neq0.
Qed.


Lemma Rlog2_le_inv r1 r2 :
  0 < r1 -> 0 < r2 -> Rlog2 r1 <= Rlog2 r2 -> r1 <= r2.
Proof.
move=> r1_gt0 r2_gt0.
case: (Req_dec (Rlog2 r1) (Rlog2 r2)).
  move=> /(Rlog2_inj r1_gt0 r2_gt0).
  by lra.
rewrite /Rlog2.
move=> *.
apply: Rlt_le.
apply: ln_lt_inv => //.
apply: Rmult_lt_reg_r.
  apply: Rinv_0_lt_compat.
  exact: ln2_gt0.
by lra.
Qed.


Definition log2_floor (n : nat) : Z := Zfloor (Rlog2 (IZR n)).
Definition log2_ceil (n : nat) : Z := Zceil (Rlog2 (IZR n)).


Lemma twopow_log2_floor_gt (n : nat) :
  IZR n < twopow (log2_floor n + 1)%Z.
Proof.
case: (Req_dec (IZR n) 0).
  move => ->.
  by auto with RDb real.
move => n_Rneq_0.
have n_Rgt_0 : 0 < IZR n.
  apply: Rle_neq_lt.
  - apply: IZR_le.
    exact: Zle_0_nat.
  - exact: not_eq_sym.
rewrite -(Rlog2_correct (IZR n)) // -Rpower_bpow.
apply: Rpower_lt; auto with RDb.
rewrite plus_IZR.
exact: Zfloor_ub.
Qed.


Lemma twopow_log2_ceil_ge (n : nat) :
  IZR n <= twopow (log2_ceil n).
Proof.
case: (Req_dec (IZR n) 0).
  move => ->.
  by auto with real RDb.
move => n_Rneq_0.
have n_Rgt_0 : 0 < IZR n.
  apply: Rle_neq_lt.
  - apply: IZR_le.
    exact: Zle_0_nat.
  - exact: not_eq_sym.
rewrite -(Rlog2_correct (IZR n)) // -Rpower_bpow.
apply: Rle_Rpower.
  by auto with real.
rewrite /log2_ceil.
exact: Zceil_ub.
Qed.




(** misc **)

Lemma round_ext_rnd beta fexp rnd rnd' x :
  (forall r : R, rnd r = rnd' r) ->
  round beta fexp rnd x = round beta fexp rnd' x.
Proof.
move=> *.
rewrite /round.
by repeat f_equal.
Qed.

