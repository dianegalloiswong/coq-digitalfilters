(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import ssreflect ssrbool bigop fintype ssrnat ssralg seq.

Require Import lia_tactics utils nat_ord_complements.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Import GRing.Theory.


Local Open Scope nat.




Section AnyType.


Context (R : Type) (idx : R) (op : R -> R -> R).


Lemma eq_big_ord {n : nat} (F1 F2 : 'I_n -> R) :
  (forall i : 'I_n, F1 i = F2 i) ->
  \big[op/idx]_(i < n) F1 i = \big[op/idx]_(i < n) F2 i.
Proof.
move => H; exact: eq_big.
Qed.


Lemma big_ord_to_nat (n : nat) (n_gt0 : 0 < n) (F : 'I_n -> R) :
  \big[op/idx]_(i < n) F i =
  \big[op/idx]_(0 <= i < n) F (ord_of_nat n_gt0 i).
Proof.
rewrite big_mkord.
apply: eq_big => // i _.
by rewrite ord_of_nat_of_ord.
Qed.


Lemma eq_big_nat_eqbounds (low1 up1 low2 up2: nat) (F1 F2 : nat -> R) :
  low1 = low2 -> up1 = up2 ->
  (forall i : nat, (low1 <= i < up1)%nat -> F1 i = F2 i) ->
  \big[op/idx]_(low1 <= i < up1) F1 i = \big[op/idx]_(low2 <= i < up2) F2 i.
Proof.
by move => <- <-; exact: eq_big_nat.
Qed.


Lemma big_nat_congrF low up (P : nat -> bool) F1 F2 :
  (forall i : nat, low <= i < up -> P i -> F1 i = F2 i) ->
  \big[op/idx]_(low <= i < up | P i) F1 i =
  \big[op/idx]_(low <= i < up | P i) F2 i.
Proof.
move=> eqF.
apply: congr_big_nat => // i /andP [] Pi i_bnd.
exact: eqF.
Qed.


Lemma big_nat_congrP low up P1 P2 F :
  (forall i : nat, low <= i < up -> P1 i = P2 i) ->
  \big[op/idx]_(low <= i < up | P1 i) F i =
  \big[op/idx]_(low <= i < up | P2 i) F i.
Proof.
move=> eqPi.
exact: congr_big_nat.
Qed.


Lemma big_nat_shiftUp n low up P (F : nat -> R) :
  \big[op/idx]_(low <= i < up | P i) F i =
  \big[op/idx]_(low + n <= i < up + n | P (i - n)) F (i - n).
Proof.
rewrite big_addn.
apply: congr_big_nat => //.
  by ssrnatlia.
all: by intros; f_equal; ssrnatlia.
Qed.


Lemma big_nat_shiftDown n low up P (F : nat -> R) :
  n <= low ->
  \big[op/idx]_(low <= i < up | P i) F i =
  \big[op/idx]_(low - n <= i < up - n | P (n + i)) F (n + i).
Proof.
move => n_le.
case_bool (low < up) => Hcase; last first.
  by rewrite !big_geq //; ssrnatlia.
rewrite [in RHS](big_nat_shiftUp n).
apply: congr_big_nat.
  1,2: by ssrnatlia.
all: by intros; f_equal; auton.
Qed.
Arguments big_nat_shiftDown : clear implicits.


Lemma big_ord_of_any_nat (low up : nat) (F : nat -> R) :
  \big[op/idx]_(low <= i < up) F i = \big[op/idx]_(i < up-low) F (low+i).
Proof.
by rewrite (big_nat_shiftDown low) // subnn big_mkord.
Qed.


Lemma big_nat0 (n : nat) (P : nat -> bool) (F : nat -> R) :
  \big[op/idx]_(n <= i < 0 | P i) F i = idx.
Proof.
exact: big_geq.
Qed.


Lemma big_nat_allfalse low up (P : nat -> bool) F :
  (forall i, low <= i < up -> ~~ P i) ->
  \big[op/idx]_(low <= i < up | P i) F i = idx.
Proof.
move=> NP.
erewrite <- (big_pred0_eq idx) at 2.
apply: congr_big_nat => //.
simplP.
apply/negP.
apply: NP.
by simplP.
Qed.


Lemma big_nat_widen_low (low1 low2 up : nat)
                        (P : pred nat) (F : nat -> R) :
  low2 <= low1 ->
  \big[op/idx]_(low1 <= i < up | P i) F i =
  \big[op/idx]_(low2 <= i < up | P i && (low1 <= i)) F i.
Proof.
case_bool (low1 < up); last first.
  intros; rewrite big_geq; last by ssrnatlia.
  rewrite big_nat_allfalse //.
  by auton.
move=> up_gt le_low.
rewrite -(subKn le_low).
elim: (low1 - low2).
  apply: congr_big_nat => //.
    by ssrnatlia.
  move=> i /andP [-> _].
  by rewrite andbT.
move=> n ->.
rewrite subnS.
case_eq (low1 - n) => //=.
move=> m m_eq.
rewrite [RHS]big_ltn_cond; last by ssrnatlia.
rewrite ifF //.
by auton.
Qed.


End AnyType.


Arguments big_nat_widen_low [R idx op low1].




Section Monoid.


Context (R : Type) (idx : R) (op : Monoid.law idx).


Lemma big_ord_allid (n : nat) (F : 'I_n -> R) :
  (forall i : 'I_n, F i = idx) ->
  \big[op/idx]_(i < n) F i = idx.
Proof.
move: F; induction n.
- intros; exact: big_ord0.
- move => F Hidx. rewrite big_ord_recr Hidx Monoid.mulm1. exact: IHn.
Qed.


Lemma big_nat_allid (low up : nat) (F : nat -> R) :
  (forall i, low <= i < up -> F i = idx) ->
  \big[op/idx]_(low <= i < up) F i = idx.
Proof.
move => Hidx; rewrite big_ord_of_any_nat.
apply: big_ord_allid => i.
apply: Hidx.
by auton.
Qed.


Lemma big_nat_singlenonid (n low up : nat) (F : nat -> R) :
  (low <= n < up)%nat ->
  (forall i : nat, (low <= i < up)%nat -> (i <> n)%nat -> F i = idx) ->
  \big[op/idx]_(low <= i < up) F i = F n.
Proof.
move => _Hn Hidx. rewrite (@big_cat_nat _ _ _ n) //. 2,3: auton.
rewrite big_nat_allid.
rewrite (@big_cat_nat _ _ _ n.+1) //. 2: auton.
rewrite big_nat1 big_nat_allid.
by rewrite Monoid.mul1m Monoid.mulm1.
all: intros; apply: Hidx; auton.
Qed.
Arguments big_nat_singlenonid : clear implicits.


Lemma big_ord_singlenonid {n : nat} (i : 'I_n) (F : 'I_n -> R) :
  (forall i' : 'I_n, i' <> i -> F i' = idx) ->
  \big[op/idx]_(i' < n) F i' = F i.
Proof.
move => Hidx. rewrite big_ord_to_nat. auton. move => _Hn.
rewrite (big_nat_singlenonid i).
by rewrite ord_of_nat_of_ord. auton.
move => i' _Hi' Hneq. apply: Hidx.
move => Hneq2. contradict Hneq. rewrite -Hneq2. auton.
Qed.


End Monoid.




Section ComMonoid.


Context (R : Type) (idx : R) (op : Monoid.com_law idx).


Lemma exchange_big_dep_nat_le low up (P : pred nat) (Q : rel nat)
                          (xQ : pred nat) F :
  (forall i j : nat,
        (low <= i <= j) && (j < up) -> P i -> Q i j -> xQ j) ->
  \big[op/idx]_(low <= i < up | P i)
    \big[op/idx]_(i <= j < up | Q i j) F i j =
  \big[op/idx]_(low <= j < up | xQ j)
    \big[op/idx]_(low <= i < j.+1 | P i && Q i j) F i j.
Proof.
move=> HxQ.
erewrite big_nat_congrF; last first.
  move=> i i_bnd Pi.
  rewrite (big_nat_widen_low low) //.
  by auton.
simpl.
erewrite exchange_big_dep_nat; last first.
  simplP.
  apply: HxQ; try eassumption.
  by auton.
apply: big_nat_congrF => j j_bnd xQj.
rewrite [RHS](big_nat_widen _ _ up); last by auton.
apply: congr_big_nat => // i i_bnd.
apply: eqb_iff.
by simplP.
Qed.


Lemma exchange_big_nat_le low up (P Q : pred nat) F :
  \big[op/idx]_(low <= i < up | P i)
    \big[op/idx]_(i <= j < up | Q j) F i j =
  \big[op/idx]_(low <= j < up | Q j)
    \big[op/idx]_(low <= i < j.+1 | P i) F i j.
Proof.
erewrite exchange_big_dep_nat_le; last first.
  by intros; eassumption.
apply: big_nat_congrF => // j _ Qj.
apply: big_nat_congrP => i _.
rewrite Qj.
by case: (P i).
Qed.


End ComMonoid.


Arguments big_nat_shiftUp [R idx op].
Arguments big_nat_shiftDown [R idx op].
Arguments big_nat_singlenonid [R idx op].
Arguments big_ord_singlenonid [R idx op n].
Arguments big_nat_widen_low [R idx op low1].




Section Sum_nat.


Local Open Scope ring_scope.


Context (R : zmodType).


(** same as big_cat_nat, but more readable when used in rewrite **)
Lemma sum_nat_cat (k m n : nat) (F : nat -> R) :
  (m <= k)%nat -> (k <= n)%nat ->
  \sum_(m <= i < n) F i = \sum_(m <= i < k) F i + \sum_(k <= i < n) F i.
Proof. exact: big_cat_nat. Qed.
Arguments sum_nat_cat : clear implicits.



Lemma eq_sum_nat_eqbounds (low1 up1 low2 up2: nat) (F1 F2 : nat -> R) :
  low1 = low2 -> up1 = up2 ->
  (forall i : nat, (low1 <= i < up1)%nat -> F1 i = F2 i) ->
  \sum_(low1 <= i < up1) F1 i = \sum_(low2 <= i < up2) F2 i.
Proof. move => <- <-. exact: eq_big_nat. Qed.



Lemma sum_nat_shiftUp (n low up : nat) (F : nat -> R) :
  \sum_(low <= i < up) F i =
  \sum_((low+n)%nat <= i < (up+n)%nat) F (i-n)%nat.
Proof.
rewrite big_addn. apply: eq_sum_nat_eqbounds; auton.
move => i _Hi. f_equal. auton.
Qed.



Lemma sum_nat_shiftDown (n low up : nat) (F : nat -> R) :
  (n <= low)%nat ->
  \sum_(low <= i < up) F i =
  \sum_((low-n)%nat <= i < (up-n)%nat) F (n+i)%nat.
Proof.
move => _H.
have[Hcase|Hcase]: (low < up \/ up <= low)%nat by ssrnatlia.
- rewrite [in RHS](sum_nat_shiftUp n).
  apply: eq_sum_nat_eqbounds; auton.
  move => i _Hi. f_equal. auton.
- rewrite !big_geq //. auton.
Qed.
Arguments sum_nat_shiftDown : clear implicits.



Lemma sum_nat_all0 (m n : nat) (F : nat -> R) :
  (forall i, (m <= i < n)%nat -> F i = 0) -> \sum_(m <= i < n) F i = 0.
Proof.
move => H. rewrite (sum_nat_shiftDown m) // subnn big_mkord.
apply: big1 => i _. apply: H.
simpl_structure. auton.
simpl in i. hyps_ord. by rewrite -ltn_subRL.
Qed.



Lemma sum_nat_singlenon0 (n low up : nat) (F : nat -> R) :
  (low <= n < up)%nat ->
  (forall i : nat, (low <= i < up)%nat -> (i <> n)%nat -> F i = 0) ->
  \sum_(low <= i < up) F i = F n.
Proof.
move => _Hn H.
rewrite (sum_nat_cat n); auton. rewrite sum_nat_all0.
rewrite (sum_nat_cat n.+1); auton. rewrite big_nat1 sum_nat_all0.
by simplr.
all: intros; apply: H; auton.
Qed.
Arguments sum_nat_singlenon0 : clear implicits.



Lemma sum_ord_singlenon0 {n : nat} (i : 'I_n) (F : 'I_n -> R) :
  (forall i' : 'I_n, i' <> i -> F i' = 0) ->
  \sum_(i' < n) F i' = F i.
Proof.
move => H. rewrite big_ord_to_nat. exact: nat_pos_if_ord.
intro. rewrite (sum_nat_singlenon0 i). 2: auton.
by rewrite ord_of_nat_of_ord.
move => m _Hm Hneq. apply: H => Heq. move: Hneq.
by rewrite -Heq nat_of_ord_of_nat.
Qed.
Arguments sum_ord_singlenon0 [n].



Lemma sum_nat_rev (low up : nat) (F : nat -> R) :
  \sum_(low <= i < up) F i =
  \sum_(low <= i < up) F (low + up - i - 1)%nat.
Proof.
rewrite big_nat_rev. apply: eq_big_nat => i _.
f_equal. ssrnatlia.
Qed.



Lemma sum_nat_add (low up : nat) (F1 F2 : nat -> R) :
  \sum_(low <= i < up) (F1 i + F2 i) =
  \sum_(low <= i < up) F1 i + \sum_(low <= i < up) F2 i.
Proof. exact: big_split. Qed.


Lemma sum_nat_opp (low up : nat) (F : nat -> R) :
  \sum_(low <= i < up) (- F i) = - \sum_(low <= i < up) F i.
Proof.
exact: GRing.sumrN.
Qed.


Lemma sum_nat_sub (low up : nat) (F1 F2 : nat -> R) :
  \sum_(low <= i < up) (F1 i - F2 i) =
  \sum_(low <= i < up) F1 i - \sum_(low <= i < up) F2 i.
Proof. by rewrite sum_nat_add sum_nat_opp. Qed.



Lemma term_diff_sum_nat (x : nat -> R) (n : nat) :
  x n = \sum_(0 <= i < n+1) x i - \sum_(0 <= i < n) x i.
Proof.
by rewrite addn1 big_nat_recr //=
           [X in X-_]addrC -addrA subrr addr0.
Qed.


End Sum_nat.



Arguments sum_nat_cat [R].
Arguments sum_nat_shiftDown [R].
Arguments sum_nat_singlenon0 [R].
Arguments sum_ord_singlenon0 [R n].




Lemma sum_const_seq (R : zmodType) (I : Type)
                    (r : seq I) (P : ssrbool.pred I) (x : R) :
  (\sum_(i <- r | P i) x = x *+ count P r)%R.
Proof.
by rewrite big_const_seq iternaddr_mulrn.
Qed.

