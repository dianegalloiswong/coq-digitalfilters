(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



(** Utilities about type int (relative integers from Mathcomp):
    - Conversion between int and nat
    - Tactics to solve equalities and inequalities involving int/nat/ord,
      often at the same time.
    - Inductions over int
**)

From mathcomp
Require Import ssreflect ssrbool ssrfun eqtype
               fintype ssrnat ssrint ssrnum ssralg.

Require Import lia_tactics utils nat_ord_complements.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Import GRing.Theory.


Local Open Scope int.






Implicit Types (k : int) (n : nat).




(** Conversion between int and nat
    (Mathcomp already provides coercion Posz : nat -> int)
**)


Definition nat_of_int k := match k with
  | Posz n => n
  | Negz _ => 0%nat
end.

Coercion nat_of_int : int >-> nat.


Lemma nat_of_int_eq0 (k : int) : k <= 0 -> nat_of_int k = O.
Proof. by case: k; case. Qed.


Lemma nat_of_int_le (k1 k2 : int) : k1 <= k2 -> (k1 <= k2)%nat.
Proof. by case: k1; case: k2. Qed.


Lemma nat_of_int_gt_0 (k : int) : k > 0 -> (k > 0)%nat.
Proof. by case: k. Qed.


Lemma nat_of_int_lt (k1 k2 : int) : k1 < k2 -> 0 < k2 -> (k1 < k2)%nat.
Proof. by case: k2; case: k1. Qed.


Lemma S_nat_of_int (k : int) : k >= 0 -> (k+1)%nat = k+1.
Proof. by case: k. Qed.


Lemma Posz_nat_of_int k : k >= 0 -> Posz (nat_of_int k) = k.
Proof. by case: k. Qed.
Arguments Posz_nat_of_int : clear implicits.


Lemma nat_of_int_Posz (n : nat) : nat_of_int (Posz n) = n.
Proof. by []. Qed.


Lemma Posz0 : Posz 0 = 0.
Proof. by []. Qed.


Lemma PoszS n : Posz n.+1 = Posz n + 1.
Proof.
by rewrite intS addrC.
Qed.


Lemma Posz_add (n m : nat) : Posz (n + m)%nat = Posz n + Posz m.
Proof. by []. Qed.


Lemma Posz_nat_of_int_Hnat (k : int) :
(0 < nat_of_int k)%nat -> Posz (nat_of_int k) = k.
Proof. by case: k. Qed.


Lemma Posz_inj (m n : nat) : Posz m = Posz n -> m = n.
Proof. move => H. by inversion H. Qed.


Lemma Posz_neq (m n : nat) : (m <> n)%nat -> (Posz m <> Posz n).
Proof. move => H H1. contradict H. exact: Posz_inj. Qed.







(** Tactics to solve equalities and inequalities over nat and int,
    using hypotheses over ord, nat and int. **)



(** ringz: tactic ring over int **)


Notation intT := (GRing.Zmodule.sort int_ZmodType).


Definition intT_to_int : intT -> int := id.


Lemma id_intT_to_int z : z = intT_to_int z.
Proof. by []. Qed.


Definition ringz_theory :=
  @mk_rt int 0 1
    (@GRing.add _) (@GRing.mul _) (fun x y => x - y) (@GRing.opp _) eq
    (@add0r _) (@addrC _) (@addrA _)
    (@mul1r _) (@mulrC _) (@mulrA _) (@mulrDl _)
    (fun x y => erefl (x - y)) (@addrN _).

Add Ring ringz : ringz_theory (preprocess [rewrite /intT_to_int]).


Ltac rw_pre_ringz :=
rewrite -?[GRing.zero _]/(GRing.zero int_ZmodType)
          -?[Posz O]/(GRing.zero int_ZmodType)
        -?[GRing.one _]/(GRing.one int_Ring)
          -?[Posz 1]/(GRing.one int_Ring)
        -?[@GRing.add _]/(@GRing.add int_ZmodType)
        -?[@GRing.mul _]/(@GRing.mul int_Ring)
        -?[@GRing.opp _]/(@GRing.opp int_ZmodType).

Tactic Notation "rw_pre_ringz" "in" hyp(H) :=
rewrite -?[GRing.zero _]/(GRing.zero int_ZmodType)
          -?[Posz O]/(GRing.zero int_ZmodType)
        -?[GRing.one _]/(GRing.one int_Ring)
          -?[Posz 1]/(GRing.one int_Ring)
        -?[@GRing.add _]/(@GRing.add int_ZmodType)
        -?[@GRing.mul _]/(@GRing.mul int_Ring)
        -?[@GRing.opp _]/(@GRing.opp int_ZmodType) in H.


Ltac pre_ringz_eq :=
intros;
rw_pre_ringz;
match goal with |- @eq ?T ?lhs _ =>
  change T with int;
  match type of lhs with
    | int => idtac
    | _ => rewrite [lhs]id_intT_to_int
  end
end.


Ltac ringz := pre_ringz_eq; ring.






(** autoz for varied goals over int,
    potentially with variables and hypotheses about nat or ord **)


Ltac autoz_weak := fail "autoz_weak will be redefined later".


Ltac simplz_weak_step :=
rewrite Posz_add || rewrite PoszS || rewrite Posz0
|| (rewrite -subzn; [| by auton])
|| (rewrite predn_int; [| by auton])
|| (rewrite Posz_nat_of_int_Hnat; [| by auton])
|| (rewrite Posz_nat_of_int; [| by autoz_weak])
|| (rewrite @nat_of_ord_of_nat; [| by auton]).
(*add [rewrite NegzE]? -> not needed so far*)


Ltac simplz_weak := repeat simplz_weak_step.


Tactic Notation "simplz_weak" "in" hyp(H) := tacrw simplz_weak in H.


Ltac trytacz := try ringz; try intlia.


Ltac autoz_weak ::=
simplP;
trytacz;
simplz_weak;
trytacz.


Ltac addhyps_lez :=
repeat
  match goal with [ H : is_true (leq _ _) |- _ ] =>
    let id := fresh H "_lez" in
    have id := H; rewrite -lez_nat in id;
    simplz_weak in id; move: id;
    intro_new; move: H
  end;
intros.


Ltac addhyps_neqz :=
repeat
  match goal with [ H : not (@eq nat _ _) |- _ ] =>
    let id := fresh H "_neqz" in
    have id := H; apply Posz_neq in id;
    simplz_weak in id; move: id;
    intro_new;
    move: H
  end;
intros.


Ltac addhyps_z :=
clear_exact_dups;
hyps_ord;
addhyps_lez;
addhyps_neqz.


Ltac autoz :=
autoz_weak;
addhyps_z;
trytacz;
simplz_weak;
trytacz.





(** simplz and ringz_simplify **)


Tactic Notation "tacrwz" tactic(tac) uconstr(t) :=
ereplace t;
last (
  match goal with
    | [ |- @eq ?T ?lhs _ ] => change T with nat;
                              rewrite -[lhs]nat_of_int_Posz;
                              apply: f_equal
    | [ |- @eq ?T _ _    ] => change T with int
    | [ |- @eq ?T _ _    ] => fail "[tacrwz " tac " " t "]: neither int nor nat"
    | _                    => fail "[tacrwz " tac " " t "]: not an @eq"
  end;
  tac;
  by []
);
rewrite ?nat_of_int_Posz.


Tactic Notation "simplz" := simplP; addhyps_z; simplz_weak.

Tactic Notation "simplz" uconstr(t) := tacrwz simplz t.


Tactic Notation "ringz_simplify" := pre_ringz_eq; ring_simplify.

Tactic Notation "ringz_simplify" uconstr(t) :=
tacrwz (simplz; ringz_simplify) t.

Tactic Notation "ringz_simplify" "LHS" :=
match goal with |- @eq _ ?lhs _ => ringz_simplify lhs end.






(** autonz: for goals which are over nat
    but involve elements of type int **)

Ltac autonz :=
auton;
simplz;
try (
  apply: nat_of_int_le || apply: nat_of_int_lt || apply: nat_of_int_gt_0
  || apply: Posz_inj
);
autoz.







Tactic Notation "ringz_simplify" := pre_ringz_eq; ring_simplify.

Tactic Notation "ringz_simplify" uconstr(t) :=
tacrwz (simplz; ringz_simplify) t.

Tactic Notation "ringz_simplify" "LHS" :=
match goal with |- @eq _ ?lhs _ => ringz_simplify lhs end.

Tactic Notation "tacrwz" tactic(tac) uconstr(t) :=
ereplace t;
last (
  match goal with
    | [ |- @eq ?T ?lhs _ ] => change T with nat;
                              rewrite -[lhs]nat_of_int_Posz;
                              apply: f_equal
    | [ |- @eq ?T _ _    ] => change T with int
    | [ |- @eq ?T _ _    ] => fail "[tacrwz " tac " " t "]: neither int nor nat"
    | _                    => fail "[tacrwz " tac " " t "]: not an @eq"
  end;
  tac;
  by []
);
rewrite ?nat_of_int_Posz.












(** Proofs by induction and recursive constructions over int.
    Notations :
    - BC: base case
    - IS: induction step **)

Implicit Types (i : int). (** also (k : int) (n : nat) from above **)



(** Proofs by induction over int:
    - peanoindz: initial case for all k <= 0,
      then usual Peano induction step for k >= 0
    - peanoindz_basis: initial case for all k <= basis,
      then usual Peano induction step for k >= basis
    - strongindz: initial case for all k < 0,
      then strong induction step for k >= 0 **)


Lemma peanoindz (P : int -> Prop) :
  (forall k, k <= 0 -> P k) -> 
  (forall k, 0 <= k -> P k -> P (k + 1)) ->
  forall k, P k.
Proof.
move=> BC IS.
case.
- elim.
    by eauto.
  intros; simplz.
  by eauto.
- by eauto.
Qed.


Lemma peanoindz_basis (basis : int) (P : int -> Prop) :
  (forall k, k <= basis -> P k) -> 
  (forall k, basis <= k -> P k -> P (k + 1)) ->
  forall k, P k.
Proof.
move=> BC IS k.
rewrite -(subrK basis k).
pattern (k - basis).
apply: peanoindz; clear k.
  move=> k k_le0; apply: BC.
  by intlia.
move=> k k_ge0 IH.
rewrite -addrA (addrC 1) addrA.
apply: IS => //.
by intlia.
Qed.

Arguments peanoindz_basis : clear implicits.


Lemma strongindz (P : int -> Prop) :
  (forall k, k < 0 -> P k) -> 
  (forall k, k >= 0 -> (forall i, i < k -> P i) -> P k) ->
  forall k, P k.
Proof.
move=> BC IS.
cut (forall k i, i < k -> P i).
  move=> Q k.
  apply: (Q (k + 1)).
  by intlia.
apply: peanoindz.
  move=> k k_le0 i i_lt; apply: BC.
  by intlia.
move=> k k_ge0 IH i i_lt.
case_bool (i < 0) => i_case.
  by eauto.
apply: IS.
  by intlia.
move=> j j_lt.
apply: IH.
by intlia.
Qed.




(** Recursive construction of f : int -> T.
    f_BC : int -> T fixes values for all k <= 0:
      forall k, k <= 0 -> f k = f_BC k.
    f_IS : int -> T -> T describes Peano construction for k >= 0:
      forall k, k >= 0 -> f (k+1) = f_IS k (f k).                 **)
Section Rec_peano.


Context (T : Type).
Implicit Types (f_BC : int -> T)
               (f_IS : int -> T -> T).


Definition peanorecz f_BC f_IS k : T :=
let f_nat := nat_rect (fun _ => T) (f_BC 0) (fun m => f_IS (Posz m)) in
match k with
  | Negz _ => f_BC k
  | Posz 0 => f_BC 0
  | Posz n => f_nat n
end.


Lemma peanoreczBC f_BC f_IS k :
  (k <= 0) -> peanorecz f_BC f_IS k = f_BC k.
Proof.
case: k => // n.
by case: n.
Qed.


Lemma peanoreczIS f_BC f_IS k :
  0 <= k ->
  peanorecz f_BC f_IS (k + 1) = f_IS k (peanorecz f_BC f_IS k).
Proof.
case: k => // n.
case: n => //= n _.
rewrite -addnE addn1.
by reflexivity.
Qed.


Lemma peanoreczIS_k f_BC f_IS k :
  0 < k ->
  peanorecz f_BC f_IS k = f_IS (k - 1) (peanorecz f_BC f_IS (k - 1)).
Proof.
move=> k_gt0.
rewrite -(subrK 1 k).
rewrite peanoreczIS; last by intlia.
by ringz_simplify (_ + _ - _).
Qed.


End Rec_peano.




(** Strong recursive construction of f : int -> T.
    f_BC : int -> T fixes values for all k < 0:
      forall k, k < 0 -> f k = f_BC k.
    f_IS : int -> (int -> T) -> T describes construction for k >= 0:
      forall k, k >= 0 -> f k = f_IS k f'
        where f' is the restriction of f to (-infinity, k).
    Since working with partial fonctions is cumbersome, f' is any
    function that is identical to f on (-infinity, k). However,
    its values over [k, +infinity) should never be used,
    as expressed by the strongrecz_safe property.
**)
Section Rec_strong.


Context (T : Type)
        (f_BC : int -> T)
        (f_IS : int -> (int -> T) -> T).


(** We want to build f : int -> T as described above.
    We use peanorecz to build an auxiliary function g
    such that for all k, (g k) is a function int -> T
    that takes the same values as f on (-infinity, k]
    (its values over (k, +infinity) don't matter).

    To contruct such a function g : int -> int -> T,
    that is int -> T' where T' := int -> T, we need
    g_BC : int -> T' and g_IS : int -> T' -> T'.

    - g_BC provides the values of g for k <= 0.
      It is sufficient that for any k, g_BC k
      is the same as f over (-infinity, 0].
      We know that:
      f i = f_BC i for i < 0
      f 0 = f_IS 0 f' for any f' that is the same
      as f over (-infinity, 0), for instance f' = f_BC.
      So we set:
      g_BC k = fun i =>   f_BC i          if i < 0
                          f_IS 0 f_BC     if i = 0
                          *not important* if i > 0

    - g_IS must ensure that g (k+1) = g_IS k (g k)
      for k >= 0. Since g k is already the same as f
      for i <= k, then g (k+1) must be the same as g k
      for these values. We also need
      g (k+1) (k+1) = f (k+1) = f_IS (k+1) f'
      where f' is f over (-infinity, k+1),
      for instance f' = g k. So what we want is:
      g (k+1) i =   g k i             if i <= k
                    f_IS (k+1) (g k)  if i = k+1
                    *not important*   if i > k+1
      Therefore:
      g_IS k gk = fun i =>   gk i              if i <= k
                             f_IS (k+1) gk     if i = k+1
                             *not important*   if i > k+1
      (We need this for k >= 0, and g_IS k doesn't
      matter for k < 0 so it might as well be the same.)
**)


Let T' := int -> T.

Let g_BC k : T' :=
  fun i => if i < 0 then f_BC i else f_IS 0 f_BC.

Let g_IS k (gk : T') : T' :=
  fun i => if i <= k then gk i else f_IS (k + 1) gk.

Let g := peanorecz g_BC g_IS.


Definition strongrecz k : T := g k k.

Notation f := strongrecz.


Lemma strongreczBC k :
  k < 0 -> strongrecz k = f_BC k.
Proof.
move=> k_lt0; rewrite /strongrecz /g peanoreczBC //; last first.
  by auto.
by rewrite /g_BC ifT.
Qed.


Lemma strongrecz_aux k i : i <= k -> g k i = f i.
Proof.
move: k; apply: (peanoindz_basis (i - 1)).
  by autoz.
move=> k k_ge IH _.
case_bool (k < 0) => k_case.
  rewrite /f /g !peanoreczBC; try by intlia.
  rewrite /g_BC.
  by case: ifP.
rewrite /g peanoreczIS; last by intlia.
fold g.
rewrite /g_IS; case: ifP => ik_case.
  exact: IH.
replace i with (k + 1) by autoz.
rewrite /f [in RHS]/g peanoreczIS; last by intlia.
fold g.
rewrite /g_IS ifF; last by autoz.
by reflexivity.
Qed.


(** For the recursive construction to be meaningful,
    we need f_IS (the function providing the step
    construction) not to use the future, that is,
    [f_IS k f'] can only depend of [f i] for i < k,
    not on any [f i] with i >= k. **)
Definition strongrecz_safe : Prop :=
  forall k (g1 g2 : int -> T),
  0 <= k ->
  (forall i, (i < k) -> g1 i = g2 i) -> 
  f_IS k g1 = f_IS k g2.


Hypothesis safe: strongrecz_safe.


Lemma strongreczIS k :
  0 <= k -> strongrecz k = f_IS k strongrecz.
Proof.
move=> k_ge0.
rewrite /strongrecz.
case_bool (k == 0).
  move=> /eqP ->.
  rewrite /g peanoreczBC //=.
  apply: safe => // i i_lt0.
  rewrite peanoreczBC; last by auto.
  by rewrite /g_BC ifT.
move=> k_neq0.
rewrite [in LHS]/g peanoreczIS_k; last by intlia.
fold g.
rewrite /g_IS ifF; last by autoz.
ringz_simplify (_ + _).
apply: safe => // i i_lt.
rewrite strongrecz_aux //.
by intlia.
Qed.


End Rec_strong.

