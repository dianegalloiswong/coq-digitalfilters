(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



(** Tests for tactics from files lia_tactics, utils,
    nat_ord_complements and int_complements.
**)

From mathcomp
Require Import all_ssreflect ssralg ssrint ssrnum.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.





(** Tactics ssrnatlia and intlia from lia_tactics.v
    found at https://github.com/amahboubi/lia4mathcomp
    Most of these tests are adapted from test_lia_tactics.v
    from the same repository.
**)
Require Import lia_tactics.

Section Lia_tactics.

Local Open Scope nat.

Lemma test_nat1 (x y : nat) :  x < y -> x < y + 1.
Proof.
ssrnatlia.
Qed.

Lemma test_nat2 (x y : nat) :  x < y -> 0 < y.
Proof.
ssrnatlia.
Qed.

Lemma test_nat3 (x y : nat) :  x == y -> 1 < y -> 0 < x.
Proof.
ssrnatlia.
Qed.

Import GRing.Theory.
Import Num.Theory.

Local Open Scope ring_scope.

Lemma test_int1 (x y : int) :  x < y -> x < y + 1.
Proof.
intlia.
Qed.

Lemma test_int2 (x : int) : 1 / 2%:R < x -> 0 < x.
intlia.
Qed.

Lemma test_int3 (x : int) : x > 0 /\ x < 2 -> x = 1.
intlia.
Qed.

Goal forall k m : int, k + 0 = k.
intros; intlia.
Qed.

Goal forall k m : int, k <= 0 -> k + m <= m.
intros; intlia.
Qed.

End Lia_tactics.











(** auton from nat_ord_complements.v **)

Require Import utils nat_ord_complements.

Local Open Scope nat.


(** goal comes from sum_complements.big_nat_widen_low **)
Goal forall (i n m : nat) (b : bool),
i - n = m.+1 -> b && (i <= m) = false.
(* time auton. .035s *)
auton.
Qed.


Goal forall (n m : nat), m < n -> ~ (n <= m)%coq_nat.
auton.
Qed.


Goal forall (n m : nat), m < n -> ~ (m >= n)%coq_nat.
auton.
Qed.


Require Import sum_complements.

(** lemma comes from sum_complements.v **)
Lemma exchange_big_dep_nat_le
  (R : Type) (idx : R) (op : Monoid.com_law idx)
  low up (P : pred nat) (Q : rel nat)
  (xQ : pred nat) F :
(forall i j : nat,
        (low <= i <= j) && (j < up) -> P i -> Q i j -> xQ j) ->
  \big[op/idx]_(low <= i < up | P i)
    \big[op/idx]_(i <= j < up | Q i j) F i j
= \big[op/idx]_(low <= j < up | xQ j)
    \big[op/idx]_(low <= i < j.+1 | P i && Q i j) F i j.
Proof.
move=> HxQ.
erewrite sum_complements.big_nat_congrF; last first.
  move=> i i_bnd Pi.
  rewrite (big_nat_widen_low low) //.
(***)
(* time simplP. .004*)
(***)
  by auton.
simpl.
erewrite exchange_big_dep_nat; last first.
(* time simplP. .037*)
  simplP.
  apply: HxQ; try eassumption.
  by auton.
apply: big_nat_congrF => j j_bnd xQj.
rewrite [RHS](big_nat_widen _ _ up); last by auton.
apply: congr_big_nat => // i i_bnd.
apply: eqb_iff.
(*** test <-> ***)
(* time simplP. .031*)
by simplP.
Qed.






(** simplP from utils.v **)

Goal forall (i n m : nat) (b : bool),
i - n = m.+1 -> b && (i <= m) = false.
simplP.
ssrnatlia.
Qed.


Section SimplP.

Context (ctxtn : nat).
Hypothesis (ctxtH : 0 < ctxtn).
Hypothesis (ctxtand : 0 < ctxtn /\ ctxtn <= 3).
Hypothesis (ctxtandb : 1 < ctxtn <= 5).

Goal forall P Q R, True /\ True -> P ctxtand -> Q ctxtandb -> R ctxtandb.
(* time simplP. .04 *)
simplP.
Abort.

End SimplP.








(** tactics about int **)

Require Import int_complements.

Local Open Scope ring_scope.




(** ringz **)

Goal (0=0 :> int).
ringz.
Qed.

Goal (0:int)=0.
ringz.
Qed.

Goal forall k : int, (k - k = 0)%int.
ringz.
Qed.

Goal forall k : int, (k - k = Posz O)%int.
ringz.
Qed.

Goal forall k, @eq (GRing.Zmodule.sort ssrint.int_ZmodType)
  (@GRing.add ssrint.int_ZmodType k
     (@GRing.opp (ssrnum.Num.NumDomain.zmodType ssrint.int_numDomainType) k))
  (GRing.zero ssrint.int_ZmodType).
ringz.
Qed.

Goal forall (k : int) (i : nat), (k - (i:int) + 1)%R + i - 1 -k = 0.
ringz.
Qed.





(** ringz_simplify **)

Goal forall k : int, (k - k = 0)%int.
assert_fails by [].
ringz_simplify.
assert_goal_is (0 = 0 :> int).
by [].
Qed.

Goal forall k : int, True -> (k - k + Posz O = Posz O)%int.
ringz_simplify.
assert_goal_is (Posz O = Posz O).
by [].
Qed.

Goal forall k : int, 0 = k * 0 -> (k - k + Posz O = Posz O)%int.
intro.
ringz_simplify (_ + _).
assert_goal_is (0 = k * 0 -> 0 = 0 :> int).
ringz_simplify (_ * _).
assert_goal_is (0 = Posz 0 -> 0 = 0 :> int).
by [].
Qed.

Goal forall k : int, 0 = k + 0 -> (k - k + Posz O = Posz O)%int.
intro.
ringz_simplify (_ + _).
assert_goal_is (0 = k -> k - k + Posz 0 = Posz 0).
ringz_simplify (_ + _).
assert_goal_is (0 = k -> 0 = 0 :> int).
by [].
Qed.

Goal forall (i : nat) (k : int), 0 + (i:int) <= (k - (i:int) + 1)%int + i.
intros.
ringz_simplify (_ + _).
assert_goal_is (is_true (Posz i <= k - Posz i + 1 + Posz i)).
ringz_simplify (_ + _).
assert_goal_is (is_true (Posz i <= k + 1)).
Abort.


Goal forall (i : nat) (k : int),
(0 + i <= nat_of_int (k - Posz i + 1) + i)%nat.
intros.
assert_fails progress ringz_simplify (_ + _).
ringz_simplify (_ + _)%nat.
assert_goal_is (is_true (i <= nat_of_int (k - Posz i + 1) + i)%nat).
assert_fails progress ringz_simplify (_ + _)%nat.
Abort.


Goal forall (i : nat) (k : int),
0 <= k -> (0 <= i < k+1)%nat ->
  (0 + i <= nat_of_int (k - Posz i + 1) + i)%nat.
intros.
ringz_simplify (_ + _)%nat.
assert_goal_is (i <= (k - (i:int) + 1)%int + i : Prop)%nat.
ringz_simplify (_ + _)%nat.
assert_goal_is (i <= (k + 1)%int : Prop)%nat.
Abort.

Goal forall (k : int) (i : nat), (k - (i:int) + 1)%R + i = k + 1.
intros.
ringz_simplify LHS.
by [].
Qed.

Goal forall (k : int) (i : nat), k + 1 = (k - (i:int) + 1) + i.
intros.
ringz_simplify (_ + _).
ringz_simplify (_ + _ + _).
by [].
Qed.

Goal forall (k : int) (n : nat),
k >= 0 -> (0 <= n < (k + 1)%int)%nat ->
  ((k - (n:int) + 1)%int + n)%nat = (k + 1)%nat.
simplz.
ringz_simplify LHS.
ringz_simplify (_ + _)%nat.
by [].
Qed.





(** uring_simplify **)

Goal forall n : nat, (n * 0 = n -> n + 0 * n = 0)%nat.
intro.
assert_fails ring_simplify.
assert_fails ring_simplify (_ + _)%nat.
(* intro. ring_simplify. assert_fails by []. *)
uring_simplify (_ + _)%nat.
uring_simplify (_ * _)%nat.
by [].
Qed.

Goal forall n : nat, (n - n = 0)%nat.
intro.
assert_fails (ring_simplify; ssrnatlia).
(* ring_simplify. auton. *)
assert_fails progress uring_simplify (_ - _)%nat.
ssrnatlia.
Qed.



(** simplz **)

Goal forall (i : nat) (k : int),
0 <= k -> (0 <= i < k+1)%nat ->
  (0 + i <= nat_of_int (k - Posz i + 1) + i)%nat.
intros.
simplz (nat_of_int _ + _)%nat.
assert_goal_is (0 + i <= (k - (i:int) + 1 + i)%int : Prop)%nat.
Abort.

Lemma test_int4 (x : int) : (x > 0) && (x < 2) -> x = 1.
simplz.
intlia.
Qed.




(** autoz **)

Lemma test_int4' (x : int) : (x > 0) && (x < 2) -> x = 1.
autoz.
Qed.


Goal forall (k : int) (n : nat),
  0 <= k -> (0 <= n < k + 1)%nat ->
  (k - (n : int) + 1)%int + n = k + 1.
autoz.
Qed.

Goal forall (k : int) (n : nat),
  0 <= k -> (0 <= n < k + 1)%nat ->
  (k - (n : int) + 1 + n)%int = k + 1.
autoz.
Qed.

Goal forall
(T : Type)
(f_BC : int -> T)
(f_IS : int -> (int -> T) -> T),
let T' := int -> T : Type in
let g_BC := fun=> (fun i : int => if i < 0 then f_BC i else f_IS 0 f_BC)
in
let g_IS := fun (k : int) (gk : T') (i : int) =>
        if i <= k then gk i else f_IS (k + 1) gk in
let g := peanorecz g_BC g_IS : int -> T' in
let f k := g k k in
forall
(i k : int)
(k_ge : i - 1 <= k)
(IH : i <= k -> g k i = f i)
(k_case : ~ k < 0)
(ik_case : (i <= k) = false),
k + 1 = i.
(* time autoz. .053 *)
autoz.
Qed.




(** autonz **)

Goal forall k m : int,
0 <= m <= k ->
  (0 <= m < (k + 1)%int)%nat.
assert_fails abstract auton.
assert_fails abstract autoz.
autonz.
Qed.

Goal forall k m : int,
0 <= m <= k ->
  (0 <= m < k.+1)%nat.
assert_fails abstract auton.
assert_fails abstract autoz.
autonz.
Qed.

Goal forall k m : int,
0 <= m <= k ->
  (0 <= m < (k + 1))%nat.
autonz.
assert_goal_is (is_true (m < k + 1)%nat).
Abort.
Goal forall k m : int,
0 <= m <= k ->
  (0 <= m < (k + 1))%nat.
intros.
rewrite addn1.
assert_fails abstract auton.
assert_fails abstract autoz.
autonz.
Qed.




(** goals from SIF.v **)
Section from_SIF.

Context
(R : comUnitRingType)
(n : nat)
(a b : nat -> R)
(n_gt0 : (n > 0)%nat)
(u : int -> R)
(i : nat)
(k : int)
(y_SIF : int -> R)
.


Goal forall (Hi : (i.+1 < n)%nat) (Hyp0 : (n - 1 - i.+1 < n)%nat)
            (Hyp1 : (n - 1 - i < n)%nat),
b (n - 1 - i.+1 + 1) * u (k - 1) - a (n - 1 - i.+1 + 1) * y_SIF (k - 1) =
b (n - i.+1 + @nat_of_ord i.+2 ord0) * u (k - 1 - Posz (@nat_of_ord i.+2 ord0)) -
a (n - i.+1 + @nat_of_ord i.+2 ord0) * y_SIF (k - 1 - Posz (@nat_of_ord i.+2 ord0)).

intros.
f_equal; last f_equal; f_equal; f_equal.
all: (* time *) autonz. (*0.7s*)
Qed.


Goal forall (i_lt : (i.+1 < n)%nat) (Hyp0 : (n - 1 - i.+1 < n)%nat)
            (Hyp1 : (n - 1 - i < n)%nat),
\sum_(j < i.+1)
   (b (n - i + j) * u (k - 1 - 1 - Posz j) -
    a (n - i + j) * y_SIF (k - 1 - 1 - Posz j)) =
\sum_(i0 < i.+1)
   (b (n - i.+1 + (lift ord0 i0)) *
    u (k - 1 - Posz  (lift ord0 i0)) -
    a (n - i.+1 + (lift ord0 i0)) *
    y_SIF (k - 1 - Posz ((lift ord0 i0)))).

intros; apply: eq_big_ord => j.
rewrite lift0.
f_equal; last f_equal; f_equal; f_equal.
(* simplz. Undo. *)
(* auton. autoz. Undo 2. *)
all: (* time *) autonz. (* 1.1s *)
Qed.


End from_SIF.

