(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import all_ssreflect ssralg matrix.

Require Import lia_tactics
               utils nat_ord_complements int_complements sum_complements.

Require Import ProofIrrelevance.



Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Import GRing.Theory.


Local Open Scope ring_scope.





Notation "v _[ i ]ord" := (@fun_of_matrix _ _ 1 v i ord0) (at level 10).



Ltac simplmx_aux :=
  simplr_aux;
  rewrite ?mulmx0 ?mul0mx ?mulmx1 ?mul1mx
          ?invmx1 ?mul_scalar_mx.
Ltac simplmx :=
repeat simplmx_aux.

Ltac simplmxN_aux :=
  simplmx_aux;
  rewrite ?mulNmx ?mulmxN ?opprK.
Ltac simplmxN := repeat simplmxN_aux.

Ltac simplmx_block_aux :=
  simplmxN_aux;
  rewrite ?mul_col_mx ?mul_mx_row ?mul_row_col
          ?mul_row_block ?mul_block_col
          ?add_col_mx.
Ltac simplmx_block := repeat simplmx_block_aux.



Infix " ▐ " := row_mx (at level 51, left associativity).
Infix " ▬ " := col_mx (at level 51, left associativity).



Implicit Types n h w : nat.



Lemma veP (R : Type) n (v1 v2 : 'cV[R]_n) :
  (forall i : 'I_n, v1 i ord0 = v2 i ord0) <-> v1 = v2.
Proof.
split; last first.
  by move=> ->.
move => eqi; apply/matrixP => i j.
by rewrite zmodp.ord1.
Qed.


Lemma trmxD (R : ringType) h w (A B : 'M[R]_(h,w)) :
  trmx (A + B) = trmx A + trmx B.
Proof.
apply/matrixP => i j.
by rewrite !mxE.
Qed.




Section block.


Context (R : ringType).


Lemma usubmx0 (h1 h2 w : nat) :
  (@usubmx R h1 h2 w) 0 = 0.
Proof.
apply/matrixP => i j.
by rewrite !mxE.
Qed.


Lemma dsubmx0 (h1 h2 w : nat) :
  (@dsubmx R h1 h2 w) 0 = 0.
Proof.
apply/matrixP => i j.
by rewrite !mxE.
Qed.


Lemma usubmxD (h1 h2 w : nat) (A B : 'M[R]_(h1+h2, w)) :
  usubmx (A + B) = usubmx A + usubmx B.
Proof.
apply/matrixP => i j.
by rewrite !mxE.
Qed.


Lemma dsubmxD (h1 h2 w : nat) (A B : 'M[R]_(h1+h2, w)) :
  dsubmx (A + B) = dsubmx A + dsubmx B.
Proof.
apply/matrixP => i j.
by rewrite !mxE.
Qed.


Lemma mx1_block n1 n2 :
  block_mx 1%:M 0 0 1%:M = (1%:M : 'M[R]_(n1+n2)).
Proof.
apply/matrixP => i j.
rewrite [in LHS](ord_lrshift i) [in LHS](ord_lrshift j).
case (ltnP i n1) => i_case; case (ltnP j n1) => j_case.
- by rewrite col_mxEu row_mxEl !mxE.
- rewrite col_mxEu row_mxEr !mxE /nat_of_bool ifF //.
  apply/eqP=> eqij; move: j_case; rewrite -eqij.
  by ssrnatlia.
- rewrite col_mxEd row_mxEl !mxE /nat_of_bool ifF //.
  apply/eqP=> eqij; move: j_case; rewrite -eqij.
  by ssrnatlia.
- rewrite col_mxEd row_mxEr !mxE.
  f_equal.
  rewrite /nat_of_bool.
  case: ifP; case: ifP => //.
  + by move=> /eqP neqij /eqP /mk_rshift_subproof_inj.
  + move=> /eqP eqij /eqP neqOrd; contradict neqOrd.
    apply: ord_inj; simpl.
    by rewrite eqij.
Qed.


End block.




Section unitmx.


Context (R : comUnitRingType) (n : nat).
Implicit Types (A B : 'M[R]_n).


Lemma unitmx_from_mulXmx A B : A *m B = 1%:M -> A \in unitmx.
Proof.
move=> mulAB.
apply: proj1.
apply: mulmx1_unit.
by eassumption.
Qed.


Lemma unitmx_from_mulmxX A B : A *m B = 1%:M -> B \in unitmx.
Proof.
move=> mulAB.
apply: proj2.
apply: mulmx1_unit.
by eassumption.
Qed.


Lemma invmx_from_mul A B : A *m B = 1%:M -> invmx A = B.
Proof.
move => mulAB.
rewrite -[LHS]mulmx1 -mulAB mulmxA mulVmx ?mul1mx //.
apply: unitmx_from_mulXmx.
by eassumption.
Qed.


Lemma invmx_mul A B :
  A \in unitmx -> B \in unitmx ->
  invmx (A *m B) = invmx B *m invmx A.
Proof.
move => uA uB.
apply: invmx_from_mul.
rewrite -mulmxA (mulmxA B) mulmxV // mul1mx mulmxV //.
Qed.


Lemma unitmx_from_unitmx_mulXmx A B :
  A *m B \in unitmx -> A \in unitmx.
Proof.
rewrite unitmx_mul.
by simplP.
Qed.


Lemma unitmx_from_unitmx_mulmxX A B :
  A *m B \in unitmx -> B \in unitmx.
Proof.
rewrite unitmx_mul.
by simplP.
Qed.


End unitmx.




(** GRing.exp works for matrices of the form 'M_(n'.+1)
    but even when we know the dimension to be positive,
    using castmx is cumbersome. **)
Section Expmx.


Context (R : ringType) (n : nat).


Fixpoint expmx (A : 'M[R]_n) (m : nat) :=
match m with
  | O => 1%:M
  | S m' => expmx A m' *m A
end.


Lemma expmx0 A : expmx A O = 1%:M.
Proof.
by [].
Qed.


Lemma expmxSl A m : expmx A (S m) = A *m expmx A m.
Proof.
induction m.
  by simpl; simplmx.
by simpl; rewrite mulmxA -IHm.
Qed.


End Expmx.







Section Col.


Context (R : ringType).


Lemma fun_of_matrix_col h w (A : 'M[R]_(h,w)) (i : 'I_h) (j : 'I_w) :
  col j A i ord0 = A i j.
Proof.
by rewrite mxE.
Qed.


Lemma mx_ext_col h w (A B : 'M[R]_(h,w)) :
  (forall j : 'I_w, col j A = col j B) -> A = B.
Proof.
move => H. apply/matrixP => i j.
by rewrite -fun_of_matrix_col H mxE.
Qed.


Lemma col_mulmx h m w (A : 'M[R]_(h,m)) (B : 'M_(m,w)) (j : 'I_w) :
  col j (A *m B) = A *m col j B.
Proof.
apply/veP => i.
rewrite !mxE.
apply: eq_big_ord => k.
by rewrite mxE.
Qed.


End Col.






(** Matrix of size 1 x 1 **)


Notation "a %:M1" := (const_mx a : 'M_1) (at level 8, format "a %:M1").
Notation "a %:M1[ R ]" := (const_mx a : 'M[R]_1) (at level 8, only parsing).

Notation "A %:sc" :=
(A (GRing.zero (zmodp.Zp_zmodType O)) (GRing.zero (zmodp.Zp_zmodType O)))
  (at level 8, format "A %:sc").



Section mx11_anyType.


Context {R : Type}.


Lemma mx11P (A B : 'M[R]_1) : A%:sc = B%:sc <-> A = B.
Proof.
split; last first.
  by move=> ->.
move => H; apply/matrixP => i j.
by rewrite !zmodp.ord1.
Qed.


Lemma mx11K (A : 'M[R]_1) : (A%:sc)%:M1 = A.
Proof.
apply/matrixP => i j.
by rewrite mxE !zmodp.ord1.
Qed.


Lemma trmx11 (A : 'M[R]_1) : A^T = A.
Proof.
by rewrite -(mx11K A) trmx_const.
Qed.


End mx11_anyType.



Lemma mx11_1 {R : ringType} : 1%:M1[R] = 1%:M.
Proof.
apply/mx11P.
by rewrite !mxE.
Qed.





(** integer indices instead of ordinal **)


Section fmxn.


Local Open Scope nat.


Context (R : ringType) (h w : nat).


Definition fmxn (A : 'M[R]_(h, w)) (i j : nat) : R :=
  match ltnP i h, ltnP j w with
    | LtnNotGeq Hi, LtnNotGeq Hj => A (Ordinal Hi) (Ordinal Hj)
    | _, _ => 0
  end.


Notation "A _[ i , j ]" := (fmxn A i j) (at level 10).


Lemma fmxnE A i j (Hi : i < h) (Hj : j < w) :
  A _[i, j] = A (Ordinal Hi) (Ordinal Hj).
Proof.
rewrite /fmxn.
case (ltnP i h); case (ltnP j w) => Hj' Hi'.
  f_equal; f_equal; exact: proof_irrelevance.
all: exfalso; ssrnatlia.
Qed.


Lemma fmxn_ord  (A : 'M[R]_(h, w)) (i : 'I_h) (j : 'I_w) :
  A _[ i , j ] = A i j.
Proof.
by rewrite fmxnE !Ordinal_of_ord.
Qed.


Lemma fmxn_0 i j (Hi : i < h) (Hj : j < w) : 0 _[i, j] = 0%R.
Proof.
by rewrite fmxnE mxE.
Qed.


Lemma fmxn_add A B (i j : nat) (Hi : i < h) (Hj : j < w) :
  (A + B) _[i, j] = (A _[i, j] + B  _[i, j])%R.
Proof.
by rewrite !fmxnE mxE.
Qed.


Lemma fmxn_scale c A (i j : nat) (Hi : (i < h)%nat) (Hj : (j < w)%nat) :
  (c *: A) _[i, j] = (c * (A _[i, j]))%R.
Proof.
by rewrite !fmxnE mxE.
Qed.


Lemma fmxn_const (c : R) (i j : nat)
                 (Hi : (i < h)%nat) (Hj : (j < w)%nat) :
  (const_mx c) _[i, j] = c.
Proof.
by rewrite fmxnE mxE.
Qed.


Lemma fmxn_matrix F i j (Hi : (i < h)%nat) (Hj : (j < w)%nat) :
  (\matrix_(i<h,j<w) F i j) _[i, j] = F (Ordinal Hi) (Ordinal Hj).
Proof.
by rewrite fmxnE mxE.
Qed.


Lemma fmxn_sum (low up : nat) (As : nat -> _) (i j : nat)
  (Hi : (i < h)%nat) (Hj : (j < w)%nat) :
(\sum_(low <= k < up) As k) _[i, j] = (\sum_(low <= k < up) (As k _[i, j]))%R.
Proof.
rewrite fmxnE summxE.
apply: eq_big_nat => k _Hk.
by rewrite fmxnE.
Qed.


End fmxn.



Notation "A _[ i , j ]" := (fmxn A i j) (at level 10).
Notation "v _[ i ]" := (@fmxn _ _ 1 v i 0) (at level 10).



Lemma fmxn_mul (R : ringType) (h m w : nat)
               (A : 'M[R]_(h, m)) (B : 'M[R]_(m, w))
               (i j : nat) (Hi : (i < h)%nat) (Hj : (j < w)%nat) :
  (A *m B) _[i, j] = \sum_(0 <= k < m) A _[i,k] * B _[k,j].
Proof.
rewrite fmxnE mxE.
move: A B. case: m => [|m] A B.
  by rewrite big_ord0 big_geq.
rewrite big_ord_to_nat.
apply: eq_big_nat => k k_bnd.
rewrite -!fmxn_ord.
by rewrite ?nat_of_ord_of_nat.
Qed.


Lemma fmxn_scalar_eq (R : ringType) (n : nat) (a : R)
                     (i : nat) (Hi : (i < n)%nat) :
  (a%:M : 'M_n) _[i, i] = a.
Proof.
rewrite fmxnE mxE eq_refl.
exact: mulr1n.
Qed.


Lemma mx11Pnat (R : ringType) (A B : 'M[R]_1) :
  A _[0, 0] = B _[0, 0] <-> A = B.
Proof.
split; last first.
  by move=> ->.
move => H; apply/mx11P.
by rewrite -!fmxn_ord.
Qed.


Lemma scalar_of_mx11_fmxn {R : ringType} (A : 'M[R]_1) :
  A%:sc = A _[0,0].
Proof.
by rewrite -fmxn_ord.
Qed.




Ltac dvlp_fmxn_step :=
   (rewrite fmxn_0; [| auton ..])
|| (rewrite fmxn_add; [| auton ..])
|| (rewrite fmxn_mul ?big_nat1; [| auton ..])
|| (rewrite fmxn_scale; [| auton ..])
|| (rewrite fmxn_const; [| auton ..])
|| (rewrite fmxn_scalar_eq; [| auton ..])
|| (rewrite fmxn_matrix; [ auton .. |]; intros;
     rewrite ?nat_of_Ordinal)
|| (rewrite fmxn_sum ?big_nat1; [| auton ..])
.


Ltac dvlp_fmxn :=
repeat ((* intros; *) dvlp_fmxn_step).

