(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import all_ssreflect ssralg vector ssrint ssrnum matrix.

Require Import lia_tactics utils int_complements mx_complements
               signal filter State_Space.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Import GRing.Theory.


Local Open Scope R.




Section ErrorFiltersStSp.


Context {RT : comRingType} {nu ny nx : nat}.


Variable stsp : @StateSpace RT nu ny nx.


Notation A := (StSp_A stsp).
Notation B := (StSp_B stsp).
Notation C := (StSp_C stsp).
Notation D := (StSp_D stsp).


Variable u : @vsignal RT nu.


(** H is the model filter corresponding to stsp assuming infinite precision.
    We note y the image of u by this filter, and x the intermediate signal
    used to construct y.
 **)
Definition H : filter := filter_from_StSp stsp.




(** Consider a finite precision arithmetic. The model filter usually cannot
    be computed exactly. We note x' and y' the computed versions of x and y.
    The coefficients A, B, C, D of the filter may not be representable in
    the chosen arithmetic: we note A', B', C', D' the representable numbers
    that we have to use instead (for example, obtained using round-to-nearest).
    Then, noting +. and *. the finite precision operations, we have:
      x'(k+1) = A' *. x'(k) +. B' *. u(k)
      y'(k)   = C' *. x'(k) +. D' *. u(k)
    We use signals eps_x and eps_y to record computation errors. More precisely,
    eps_x(k) represents the total error made during the computation of x'(k+1),
    and eps_y(k) during the computation of y'(k). Then, we have:
      x'(k+1) = A' x'(k) + B' u(k) + eps_x(k)
      y'(k)   = C' x'(k) + D' u(k) + eps_y(k)
 **)


Variable A' : 'M[RT]_nx.
Variable B' : 'M[RT]_(nx, nu).
Variable C' : 'M[RT]_(ny, nx).
Variable D' : 'M[RT]_(ny, nu).


Variable eps_x : @vsignal RT nx.
Variable eps_y : @vsignal RT ny.
Definition eps := col_signal eps_x eps_y.




Definition x'_fIS k (x'k : 'cV_nx) := A' *m x'k + B' *m u k + eps_x k.
Definition x' := signal_peanorec x'_fIS.


Fact y'_causal : causal (fun k => C' *m x' k + D' *m u k + eps_y k).
Proof.
move => k _Hk; rewrite !signal_causal //.
by simplmx.
Qed.
Definition y' := Build_signal y'_causal.


Definition B_Dop : 'M[RT]_(nx, nx + ny) := row_mx 1%:M 0.
Definition D_Dop : 'M[RT]_(ny, nx + ny) := row_mx 0 1%:M.
Definition stsp_Dop := Build_StateSpace A' B_Dop C' D_Dop.
Definition H_Dop : filter := filter_from_StSp stsp_Dop.


Definition A_Dc : 'M_(nx + nx) := col_mx (row_mx A 0) (row_mx (A'-A) A').
Definition B_Dc : 'M_(nx + nx, nu) := col_mx B (B'-B).
Definition C_Dc : 'M_(ny, nx + nx) := row_mx (C'-C) C'.
Definition D_Dc : 'M_(ny, nu) := D'-D.
Definition stsp_Dc := Build_StateSpace A_Dc B_Dc C_Dc D_Dc.
Definition H_Dc : filter := filter_from_StSp stsp_Dc.


Lemma usub_x_c : usubsignal (x stsp_Dc u) = x stsp u.
Proof.
apply/signalP.
apply: peanoindz => k _Hk.
  rewrite /= !peanoreczBC //. exact: usubmx0.
move => Hind. rewrite signalE !x_Sk usubmxD. f_equal.
all: rewrite mul_col_mx col_mxKu //.
rewrite -(vsubmxK (x stsp_Dc u k)) mul_row_col.
simplmx.
by rewrite -Hind.
Qed.


Lemma x'_prop :
  x' = x stsp u + x stsp_Dop eps + dsubsignal (x stsp_Dc u).
Proof.
apply/signalP.
apply: peanoindz => k _Hk.
  rewrite /= !peanoreczBC // dsubmx0.
  by simplmx.
move => Hind.
rewrite signal_peanorecIS //.
fold x'.
rewrite /x'_fIS Hind 2!signalE 2![in RHS]signalE.
rewrite !x_Sk !mulmxDr.
simpl (StSp_A _).
rewrite -{1}(subrKl A A').
rewrite mulmxDl -!addrA.
f_equal.
rewrite -(subrKl B B').
rewrite (mulmxDl B) -(addrA (B *m _)) (addrC (B *m _)) ![in LHS]addrA addrC.
f_equal.
rewrite (addrC ((A'-A) *m _)) -!addrA.
f_equal.
simpl (StSp_B _).
rewrite ![in LHS]addrA addrC.
f_equal.
  by rewrite mul_row_col; simplmx.
rewrite [in RHS]signalE x_Sk dsubmxD.
rewrite !mul_col_mx !col_mxKd.
f_equal.
rewrite -(vsubmxK (x stsp_Dc u k)) mul_row_col.
f_equal.
by rewrite -usub_x_c.
Qed.


Theorem error_filters_StSp : y' - H u = H_Dop eps + H_Dc u.
Proof.
rewrite -(addrK (H u) (H_Dop eps + H_Dc u)); f_equal.
rewrite [RHS]addrC (addrC (H_Dop _)) addrA.
apply/signalP => k.
rewrite signalE 2![in RHS]signalE.
rewrite !y_k mul_row_col.
simplmx.
rewrite addrA; f_equal.
rewrite -(subrKl D D') mulmxDl addrC (addrC _ (D *m _)) -!addrA.
f_equal.
rewrite !(addrC (_ *m u k)) !addrA.
f_equal.
rewrite x'_prop !mulmxDr -{1}(subrKl C C') mulmxDl -!addrA.
f_equal.
rewrite -(vsubmxK (x stsp_Dc u k)) mul_row_col.
by rewrite -!addrA [X in _+X]addrC -usub_x_c.
Qed.


End ErrorFiltersStSp.

