(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import all_ssreflect.

From Flocq
Require Import Core Round_odd.

Require Import Lia.

Require Import utils Flocq_complements FIX.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Local Open Scope Z.




Notation "# lsb" := (roundFIX lsb Zrnd_odd) (at level 0).
Notation "add# lsb" := (addFIX lsb Zrnd_odd) (at level 0).




Section Odd_rounding_properties.


Global Instance exists_NE_FIX : forall (beta : radix) (e : Z),
      Exists_NE beta (FIX_exp e).
Proof.
intros beta e; unfold Exists_NE, FIX_exp; simpl.
right; split; auto.
Qed.


Lemma Double_round_DN_or_UP:
  forall (beta : radix) (fexp fexpe : Z -> Z) (rnd1 rnd2 : R -> Z),
  Valid_rnd rnd1 -> Valid_rnd rnd2 -> Valid_exp fexp -> Valid_exp fexpe ->
  (forall e, fexpe e <= fexp e) ->
  forall x : R,
  round beta fexp rnd1 (round beta fexpe rnd2 x) = round beta fexp Zfloor x \/
  round beta fexp rnd1 (round beta fexpe rnd2 x) = round beta fexp Zceil x.
Proof with auto with typeclass_instances.
intros beta fexp fexpe rnd1 rnd2 Hr1 Hr2 Hf1 Hf2 He x.
pose (y:=round beta fexpe rnd2 x); fold y.
apply Only_DN_or_UP with (generic_format beta fexp) x...
apply round_DN_pt...
apply round_UP_pt...
apply generic_format_round...
split.
apply round_ge_generic...
apply generic_format_round...
apply round_ge_generic...
apply generic_inclusion_mag with fexp.
intros _; apply He.
apply generic_format_round...
apply round_DN_pt...
apply round_le_generic...
apply generic_format_round...
apply round_le_generic...
apply generic_inclusion_mag with fexp.
intros _; apply He.
apply generic_format_round...
apply round_UP_pt...
Qed.


Lemma round_N_odd_FIX: forall choice x le l,
   le <= l-2 ->
   roundFIX l (Znearest choice) (# le x) =
   roundFIX l (Znearest choice) x.
Proof with auto with typeclass_instances.
intros choice x le l H.
apply round_N_odd...
Qed.


Lemma round_odd_odd_FIX : forall x le l,
  le <= l -> # l (# le x) = # l x.
Proof with auto with typeclass_instances.
intros x le l H; unfold roundFIX.
(* l=le *)
case (Zle_lt_or_eq le l); try easy.
2: intros K; rewrite K; apply round_generic...
2: apply generic_format_round...
intros H'.
(* x in F_le *)
case (generic_format_EM radix2 (FIX_exp le) x); intros L.
rewrite (round_generic _ _ _ x); try easy...
(* *)
apply Rnd_odd_pt_unique with radix2 (FIX_exp l) x...
2: apply round_odd_pt...
split.
apply generic_format_round...
right; split.
case Double_round_DN_or_UP with radix2 (FIX_exp l) (FIX_exp le) 
   Zrnd_odd Zrnd_odd x...
intros H1; left; rewrite H1; apply round_DN_pt...
intros H1; right; rewrite H1; apply round_UP_pt...
generalize (round_odd_pt radix2 (FIX_exp l)); intros T.
specialize (T (round radix2 (FIX_exp le) Zrnd_odd x)).
destruct T as (_,Y); case Y; clear Y.
2: intros (_,Y); easy.
intros H1.
generalize (round_odd_pt radix2 (FIX_exp le)); intros T.
specialize (T x).
destruct T as (_,Y); case Y; clear Y.
intros M; contradict L.
rewrite <- M.
apply generic_format_round...
intros (_,Y).
destruct Y as (g,(Hg1,(Hg2,Hg3))).
absurd (Z.even (Fnum g) = true).
rewrite Hg3; easy.
cut (generic_format radix2 (FIX_exp l)
    (round radix2 (FIX_exp le) Zrnd_odd x)).
2: rewrite <- H1.
2: apply generic_format_round...
rewrite Hg1.
unfold canonical, cexp, FIX_exp in Hg2.
unfold F2R; rewrite Hg2.
unfold generic_format, sclmant, cexp, FIX_exp.
unfold F2R; simpl.
intros K.
assert (H0: exists n:Z, Fnum g = n*radix2^(l-le)).
eexists.
apply eq_IZR; apply Rmult_eq_reg_r with (twopow le).
2: apply Rgt_not_eq, bpow_gt0.
rewrite K.
rewrite mult_IZR 2!Rmult_assoc.
f_equal.
rewrite <- bpow_IZR.
rewrite <- bpow_plus; f_equal; ring.
lia.
destruct H0 as (n,Hn).
rewrite Hn.
rewrite Z.even_mul.
rewrite Z.even_pow; simpl.
apply orbT.
lia.
Qed.


Lemma add_even_rodd: forall x y l,
isFIX (l + 1) x -> ( x + (# l y) = # l (x + y) )%R.
Proof with auto with typeclass_instances.
intros x y l Fx; unfold roundFIX.
(* y in F_l *)
case (generic_format_EM radix2 (FIX_exp l) y); intros L.
rewrite (round_generic _ _ _ y); try easy...
apply sym_eq, round_generic...
apply/isFIX_P; apply isFIX_Rplus.
apply isFIX_le with (l+1); try easy; lia.
by apply/isFIX_P.
(* *)
assert (Ft: generic_format radix2 (FIX_exp l) (x + round radix2 (FIX_exp l) Zrnd_odd y)).
apply/isFIX_P; apply isFIX_Rplus.
apply isFIX_le with (l+1); try easy; lia.
apply/isFIX_P; apply generic_format_round...
apply (Rnd_odd_pt_unique radix2 (FIX_exp l) (x+y)).
2: apply round_odd_pt...
split; try assumption.
right; split.
pose (fd:= round radix2 (FIX_exp l) Zfloor (x+y)).
pose (fu:= round radix2 (FIX_exp l) Zceil (x+y)).
assert (K: (x + round radix2 (FIX_exp l) Zrnd_odd y = fd)%R
            \/ (x + round radix2 (FIX_exp l) Zrnd_odd y = fu)%R).
apply (Only_DN_or_UP (generic_format radix2 (FIX_exp l)) (x+y))...
apply round_DN_pt...
apply round_UP_pt...
(* . *)
case (round_DN_or_UP radix2 (FIX_exp l) Zrnd_odd y); intros J; rewrite J; split.
apply Rplus_le_reg_r with (-x)%R.
apply Rle_trans with (round radix2 (FIX_exp l) Zfloor y);[idtac|right; ring].
apply round_DN_pt...
apply/isFIX_P; apply isFIX_Rplus.
apply/isFIX_P; apply generic_format_round...
apply/isFIX_P; apply generic_format_opp; apply/isFIX_P.
apply isFIX_le with (l+1); try easy; lia.
apply Rplus_le_reg_l with x; ring_simplify (x+(fd+-x))%R.
apply round_DN_pt...
apply Rle_trans with (x+y)%R.
apply Rplus_le_compat_l.
apply round_DN_pt...
apply round_UP_pt...
apply Rle_trans with (x+y)%R.
apply round_DN_pt...
apply Rplus_le_compat_l.
apply round_UP_pt...
apply Rplus_le_reg_r with (-x)%R.
apply Rle_trans with (round radix2 (FIX_exp l) Zceil y);[right; ring|idtac].
apply round_UP_pt...
apply/isFIX_P; apply isFIX_Rplus.
apply/isFIX_P; apply generic_format_round...
apply/isFIX_P; apply generic_format_opp; apply/isFIX_P.
apply isFIX_le with (l+1); try easy; lia.
apply Rplus_le_reg_l with x; ring_simplify (x+(fu+-x))%R.
apply round_UP_pt...
(* . *)
case K; intros K'; rewrite K'.
left; apply round_DN_pt...
right; apply round_UP_pt...
(* *)
generalize (round_odd_pt radix2 (FIX_exp l)); intros T.
specialize (T y).
destruct T as (_,Y); case Y; clear Y.
intros H1; contradict L.
rewrite <- H1; apply generic_format_round...
intros (_,Y).
destruct Y as (g,(Hg1,(Hg2,Hg3))).
exists (Float radix2 ((Ztrunc ((sclmant (l + 1)) x)*2)+Fnum g) l); split.
move: Fx => /isFIX_P {1}->.
rewrite Hg1; unfold F2R; simpl.
rewrite plus_IZR mult_IZR Rmult_plus_distr_r; f_equal.
unfold cexp, FIX_exp; simpl.
rewrite bpow_plus bpow_1; simpl; ring.
rewrite Hg2.
unfold cexp, FIX_exp; easy.
(* *)
split.
unfold canonical, FIX_exp, cexp; simpl; easy.
simpl.
rewrite Zplus_comm Zmult_comm.
rewrite Z.even_add_mul_even; try easy.
exists 1%Z; easy.
Qed.


Lemma add_rodd_even: forall x y l,
  isFIX (l + 1) y -> ( (# l x) + y = # l (x + y) )%R.
Proof.
intros; rewrite Rplus_comm [in RHS]Rplus_comm.
exact: add_even_rodd.
Qed.


Lemma add_odd_odd_simple : forall x y le l,
  le < l -> isFIX l x ->
  add# l (# le y) x = add# l x y.
Proof with auto with typeclass_instances.
intros x y le l H Fx.
unfold addFIX, binopFIX.
rewrite Rplus_comm add_even_rodd.
apply round_odd_odd_FIX; lia.
apply isFIX_le with l; try easy; lia.
Qed.


Lemma add_odd_odd : forall x y l le lx,
  le < lx -> le < l -> isFIX lx x ->
  add# l (# le y) x = add# l x y.
Proof with auto with typeclass_instances.
intros x y l le lx H1 H2 Fx.
unfold addFIX, binopFIX.
case (Zle_or_lt lx l); intros H3.
2: apply add_odd_odd_simple; try easy.
2: apply isFIX_le with (2:=Fx); try easy.
2: lia.
(* *)
rewrite <- round_odd_odd_FIX with (x:=(Rplus (# le y) x)) (le:=lx).
2: lia.
rewrite <- round_odd_odd_FIX with (x:=(Rplus x y)) (le:=lx).
2: lia.
f_equal.
apply add_odd_odd_simple; try easy.
Qed.


End Odd_rounding_properties.

