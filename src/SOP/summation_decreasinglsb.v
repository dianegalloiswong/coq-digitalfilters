(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import ssreflect ssrbool ssrfun ssrnat seq bigop path eqtype ssralg.

From Flocq
Require Import Core.

Require Import lia_tactics Rstruct
               utils Z_complements R_complements seq_complements
               FIX SOP_model.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Local Open Scope ring_scope.
Local Open Scope Z.





Section Large_lsb.


Variable ls : seq Z.
Variable ps : seq R.

Hypothesis are_lsb_ls_ps : are_lsb ls ps.

Hypothesis ls_srt : sorted Z.leb ls.

Notation n := (size ps).



Fixpoint vi (i : nat) :=
match i with
  | O => ps ` n.-1
  | S p => addFIX (ls ` (n.-1 - p.+1)%nat) Zfloor
              (vi p) (ps ` (n.-1 - p.+1)%nat)
end.


Definition summation_decreasinglsb : R := vi n.-1.


Lemma isFIX_vi i :
  (i < n)%nat -> isFIX (ls ` (n.-1 - i)) (vi i).
Proof.
case: i.
  move=> n_gt0. rewrite subn0 /vi.
  apply: are_lsb_nth => //.
  by ssrnatlia.
move=> i Si_lt.
exact: isFIX_roundFIX.
Qed.


Lemma isFIX_vi_lPre i :
  (i.+1 < n)%nat -> isFIX (ls ` (n.-1 - i.+1)) (vi i).
Proof.
move=> Si_lt.
apply: isFIX_le; last first.
  apply: isFIX_vi.
  by ssrnatlia.
apply: Zle_nth_sorted => //.
rewrite (all2_same_size are_lsb_ls_ps).
by apply/andP; ssrnatlia.
Qed.


Lemma vi_correct i :
  (i < n)%nat -> vi i = \sum_(x <- lastn i.+1 ps) x.
Proof.
move: i.
 apply: nat_ind.
  move=> n_gt0.
  rewrite /vi (lastn1s 0%R); last first.
    exact: nnil_sizegt0.
  rewrite big_seq1.
  exact: nth_last.
move=> i IH Si_lt /=.
rewrite addFIX_isFIX_exact; last first.
- apply: are_lsb_nth => //.
  by ssrnatlia.
- exact: isFIX_vi_lPre.
rewrite (lastS 0%R) // big_cons GRing.addrC.
replace (@GRing.add _) with Rplus by trivial.
f_equal.
  by apply: IH; ssrnatlia.
f_equal.
by ssrnatlia.
Qed.


Fact summation_decreasinglsb_n0 :
  n = O -> summation_decreasinglsb = 0%R.
Proof.
move=> n0.
rewrite /summation_decreasinglsb.
rewrite n0 Nat.pred_0 /= n0 Nat.pred_0 /=.
by rewrite [ps]size0nil.
Qed.


Theorem summation_decreasinglsb_exact :
  summation_decreasinglsb = \sum_(x <- ps) x.
Proof.
case_eq (n == O) => /eqP.
  move=> n0.
  rewrite summation_decreasinglsb_n0 //.
  by rewrite [ps]size0nil // big_nil.
move=> /= n_neq0.
rewrite /summation_decreasinglsb vi_correct; last by ssrnatlia.
f_equal.
rewrite prednK; last by ssrnatlia.
by rewrite lastn_size.
Qed.


Lemma isFIX_summation_decreasinglsb :
  isFIX (ls ` 0) summation_decreasinglsb.
Proof.
case_eq (n == O) => /eqP.
  move=> n0.
  rewrite summation_decreasinglsb_n0 //.
  rewrite [ls]size0nil.
    exact: isFIX_0.
  by rewrite (all2_same_size are_lsb_ls_ps).
move=> n_neq0; simpl in n_neq0.
ereplace O.
  apply: isFIX_vi.
all: by ssrnatlia.
Qed.


End Large_lsb.

