(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import ssreflect ssrbool seq bigop eqtype.

From Flocq
Require Import Core Operations.

Require Import Lia Lra.

Require Import Rstruct utils Z_complements R_complements Flocq_complements.



Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.




Local Open Scope R.




Section FIX.


Variable lsb : Z.


Definition isFIX (r : R) : bool := (twopow lsb |d r).


Lemma isFIX_P (r : R) :
  reflect (generic_format radix2 (FIX_exp lsb) r) (isFIX r).
Proof.
apply: Bool.iff_reflect.
rewrite -/(is_true (isFIX r)).
split.
- move => ->.
  apply: Rdivides_mulIZR.
  exact: bpow_neq0.
- move => /is_integer_witness [z Hz].
  apply: generic_format_FIX.
  apply FIX_spec with (f := Float _ z lsb) => //.
  rewrite /F2R /= -Hz Rmult_assoc Rinv_l.
    by ring.
  exact: bpow_neq0.
Qed.


Lemma isFIX_multwopow (m : Z) :
  isFIX (IZR m * twopow lsb).
Proof.
apply/isFIX_P.
apply: generic_format_FIX.
by apply FIX_spec with (f := Float _ m lsb).
Qed.



Section Var_rnd.


Variable rnd : R -> Z.

Hypothesis rnd_valid : Valid_rnd rnd.


Definition roundFIX := (round radix2 (FIX_exp lsb) rnd).

Definition binopFIX op (r1 r2 : R) :=
  roundFIX (op r1 r2).


Definition addFIX := binopFIX Rplus.

Definition mulFIX := binopFIX Rmult.


Lemma isFIX_0 : (isFIX 0).
Proof.
apply/isFIX_P. exact: generic_format_0.
Qed.


Lemma isFIX_roundFIX (r : R) : isFIX (roundFIX r).
Proof.
apply/isFIX_P. exact: generic_format_round.
Qed.


Lemma roundFIX_id (r : R) : isFIX r -> roundFIX r = r.
Proof.
move=> /isFIX_P.
exact: round_generic.
Qed.

Lemma isFIX_roundFIX_id (r : R) :
  roundFIX r = r -> isFIX r.
Proof.
move => <-.
apply/isFIX_P.
exact: generic_format_round.
Qed.


Lemma isFIX_big_binopFIX op (s : seq R) :
  isFIX (\big[binopFIX op / 0]_(r <- s) r).
Proof.
case: s.
- rewrite big_nil. exact: isFIX_0.
- move => r s. rewrite big_cons. exact: isFIX_roundFIX.
Qed.


Lemma isFIX_Rplus (f1 f2 : R) :
  isFIX f1 -> isFIX f2 -> isFIX (f1 + f2).
Proof.
rewrite -!(rwP (isFIX_P _)).
exact: generic_format_FIX_plus.
Qed.


Lemma addFIX_isFIX_exact (x1 x2 : R) :
  isFIX x1 -> isFIX x2 -> addFIX x1 x2 = x1 + x2.
Proof.
intros; apply: roundFIX_id.
exact: isFIX_Rplus.
Qed.


Lemma roundFIX_error (r : R) :
  Rabs (roundFIX r - r) < twopow lsb.
Proof.
case_eq (r == 0).
  move => /eqP ->. rewrite roundFIX_id; last exact isFIX_0.
  rewrite Rminus_0_r Rabs_R0. exact: bpow_gt0.
move => /eqP Hr.
eapply Rlt_le_trans; first exact: error_lt_ulp.
rewrite ulp_FIX; by right.
Qed.


End Var_rnd.



Lemma roundFIX_rN_error (choice : Z -> bool) (r : R) :
  Rabs ( roundFIX (Znearest choice) r - r ) <= twopow (lsb - 1).
Proof.
eapply Rle_trans; first apply: error_le_half_ulp.
right. rewrite ulp_FIX. symmetry.
exact: twopowPe_half.
Qed.



Definition faithful (r x : R) : Prop :=
  x = roundFIX Zfloor r \/ x = roundFIX Zceil r.



Lemma diff_lt_faithful (r x : R) :
  isFIX x -> Rabs (x - r) < twopow lsb ->
  faithful r x.
Proof.
move => /isFIX_P xFIX /Rabs_lt_inv diff_lt.
case: (Rle_lt_dec x r) => Hcase.
- left.
  symmetry.
  apply: round_DN_eq => //.
  split => //.
  rewrite succFIX.
  by lra.
- right.
  symmetry.
  apply: round_UP_eq => //.
  by split; [ rewrite predFIX |]; lra.
Qed.



End FIX.

Arguments roundFIX_id {lsb rnd rnd_valid} r.




Lemma isFIX_le lsb lsb' x :
  (lsb <= lsb')%Z -> isFIX lsb' x -> isFIX lsb x.
Proof.
move=> lsb_le isFIX_lsb'.
apply: Rdivides_trans; last first.
- exact: isFIX_lsb'.
- exact: Rdivides_bpow.
- auto with RDb.
Qed.


Lemma isFIX_Rmult (l1 l2 : Z) (x1 x2 : R) :
  isFIX l1 x1 -> isFIX l2 x2 -> isFIX (l1 + l2)%Z (x1 * x2).
Proof.
rewrite -!(rwP (isFIX_P _ _)).
exact: generic_format_FIX_mult.
Qed.


Lemma mulFIX_isFIX_exact l1 l2 (x1 x2 : R) rnd :
  Valid_rnd rnd ->
  isFIX l1 x1 -> isFIX l2 x2 ->
  mulFIX (l1 + l2) rnd x1 x2 = x1 * x2.
Proof.
move=> *; apply: roundFIX_id.
exact: isFIX_Rmult.
Qed.


Lemma mulFIX_lsble_exact l1 l2 l (x1 x2 : R) rnd :
  Valid_rnd rnd ->
  isFIX l1 x1 -> isFIX l2 x2 ->
  (l <= l1 + l2)%Z ->
  mulFIX l rnd x1 x2 = x1 * x2.
Proof.
move=> *; apply: roundFIX_id.
apply: isFIX_le.
  by eassumption.
exact: isFIX_Rmult.
Qed.



Global Notation sclmant lsb := (scaled_mantissa radix2 (FIX_exp lsb)).

Fact roundFIX_sclmant lsb rnd r :
  roundFIX lsb rnd r = IZR (rnd (sclmant lsb r)) * twopow lsb.
Proof.
by [].
Qed.

Fact sclmant_simpl lsb r : sclmant lsb r = r * twopow (- lsb).
Proof.
by [].
Qed.



Definition ZlocalDN (rnd : R -> Z) (x : R) := ((rnd x) == Zfloor x).

Lemma Zceil_not_ZlocalDN (rnd : R -> Z) :
  Valid_rnd rnd -> forall x : R,
  ~ ZlocalDN rnd x -> rnd x = Zceil x.
Proof.
move => Hrnd x. case (Zrnd_DN_or_UP rnd x) => //.
move => H /negP/eqP. by [].
Qed.

Lemma rnd_if_ZlocalDN (rnd : R -> Z) :
  Valid_rnd rnd -> forall x : R,
  rnd x = (if ZlocalDN rnd x then Zfloor else Zceil) x.
Proof.
intros. case: ifP.
- by move/eqP => ->.
- move => /eqP Hcase. apply: Zceil_not_ZlocalDN. by apply/negP/eqP.
Qed.


Lemma eqm_ZlocalDN (rnd rnd' : R -> Z) (m : Z) (x x' : R) :
  Valid_rnd rnd -> Valid_rnd rnd' ->
  ZlocalDN rnd x = ZlocalDN rnd' x' ->
  (m <> 0)%Z -> eqmodr (IZR m) x x' ->
  eqm m (rnd x) (rnd' x').
Proof.
move => Hrnd Hrnd' HZlocalDN Hm Heqm.
rewrite 2!rnd_if_ZlocalDN.
rewrite HZlocalDN. case: ifP.
- intros; exact: eqm_Zfloor.
- intros; exact: eqm_Zceil.
Qed.




Definition RlocalDN lsb (rnd : R -> Z) (r : R) :=
  roundFIX lsb rnd r == roundFIX lsb Zfloor r.



Lemma RlocalDN_ZlocalDN lsb rnd r :
  RlocalDN lsb rnd r =
  ZlocalDN rnd (scaled_mantissa radix2 (FIX_exp lsb) r).
Proof.
apply: eqb_iff.
rewrite /RlocalDN !roundFIX_sclmant.
split; last first.
  by move => /eqP ->.
move => /eqP H; apply /eqP.
apply: eq_IZR.
apply: Rmult_eq_reg_r.
  exact: H.
by auto with RDb.
Qed.


Lemma eqmodr_scaled_mantissa q lsb r r' :
  q <> 0 ->
  eqmodr q (scaled_mantissa radix2 (FIX_exp lsb) r)
           (scaled_mantissa radix2 (FIX_exp lsb) r') =
  eqmodr (q * twopow lsb) r r'.
Proof with auto with RDb.
intro; rewrite -(eqmodr_mul_r_eq (twopow lsb)) //...
rewrite !Rmult_assoc bpow_opp Rinv_l...
by autorewrite with RDb.
Qed.



Section misc.

Variable lsb : Z.


Variable choice : Z -> bool.
Notation rN := (Znearest choice).


Ltac translate_Rcompare_in H :=
try apply Rcompare_Lt_inv in H;
try apply Rcompare_not_Lt_inv in H;
try apply Rcompare_Eq_inv in H;
try apply Rcompare_Gt_inv in H;
try apply Rcompare_not_Gt_inv in H.

Ltac translate_Rcompare_hyps :=
repeat match goal with
  | [ H : Rcompare _ _ = _ |- _ ] => translate_Rcompare_in H
  | [ H : not (Rcompare _ _ = _) |- _ ] => translate_Rcompare_in H
end.

Ltac lra_with_Rcompare := simpl; translate_Rcompare_hyps; lra.


Lemma ZlocalDN_rN (x : R) :
  ZlocalDN rN x = match Rcompare (x - IZR (Zfloor x)) (/ 2) with
                    | Lt => true
                    | Eq => ~~ choice (Zfloor x)
                    | Gt => false
                  end.
Proof.
rewrite /ZlocalDN /rN. case Hcase: Rcompare.
- apply/eqP. case: choice => //=.
  rewrite -Zfloor_Zceil_not_integer. lia.
  rewrite iff_is_integer_Zfloor. lra_with_Rcompare.
- by apply/eqP.
- apply/eqP. rewrite -Zfloor_Zceil_not_integer. lia.
  rewrite iff_is_integer_Zfloor. lra_with_Rcompare.
Qed.


Definition Rcompare_mid lsb r :=
  Rcompare (r - roundFIX lsb Zfloor r) (twopow lsb / 2).

Lemma Rcompare_mid_sclmant r :
  Rcompare_mid lsb r =
  Rcompare (sclmant lsb r - IZR (Zfloor (sclmant lsb r))) (/ 2).
Proof.
rewrite /Rcompare_mid -[X in X - _](scaled_mantissa_mult_bpow radix2 (FIX_exp lsb) r).
rewrite roundFIX_sclmant -Rmult_minus_distr_r /Rdiv (Rmult_comm (twopow lsb)).
apply: Rcompare_mult_r. auto with RDb.
Qed.


Lemma RlocalDN_rN (r : R) :
  RlocalDN lsb rN r = match Rcompare_mid lsb r with
                        | Lt => true
                        | Eq => ~~ choice (Zfloor (scaled_mantissa radix2 (FIX_exp lsb) r))
                        | Gt => false
                      end.
Proof.
rewrite Rcompare_mid_sclmant.
rewrite RlocalDN_ZlocalDN ZlocalDN_rN //.
Qed.


Lemma Rcompare_mid_eqm_ulp r r' :
eqmodr (twopow lsb) r r' ->
  Rcompare_mid lsb r = Rcompare_mid lsb r'.
Proof.
intro.
rewrite !Rcompare_mid_sclmant. f_equal.
apply: eq_fracpart_eqmodr1.
rewrite eqmodr_scaled_mantissa // Rmult_1_l //.
Qed.
Global Arguments Rcompare_mid_eqm_ulp [r].


End misc.





(** Usual rounding mods and inequalities
    (for summation_nearest.v) **)


Lemma round_DN_UP_le beta fexp {valid_exp_ : Valid_exp fexp} r :
  round beta fexp Zfloor r <= r <= round beta fexp Zceil r.
Proof.
case_eq (r == F2R (Float beta (Ztrunc (scaled_mantissa beta fexp r))
                              (cexp beta fexp r)))
       => /eqP.
  move=> r_generic_format.
  rewrite !round_generic //.
  by split; exact: Req_le.
move=> r_not_generic_format.
have:= (round_DN_UP_lt beta fexp r).
move=> [] //.
by split; apply: Rlt_le.
Qed.


Lemma round_DN_le beta fexp {valid_exp_ : Valid_exp fexp} r :
  round beta fexp Zfloor r <= r.
Proof.
have:= (round_DN_UP_le beta r).
by move=> [].
Qed.


Lemma round_UP_ge beta fexp {valid_exp_ : Valid_exp fexp} r :
  r <= round beta fexp Zceil r.
Proof.
have:= (round_DN_UP_le beta r).
by move=> [].
Qed.


Notation "▼_( l ) r" := (roundFIX l Zfloor r) (at level 35).
Notation "r1 ▼+_( l ) r2" := (addFIX l Zfloor r1 r2) (at level 50).

Notation "▲_( l ) r" := (roundFIX l Zceil r) (at level 35).


Lemma rd_le lsb r : ▼_(lsb) r <= r.
Proof.
exact: round_DN_le.
Qed.


Lemma ru_ge lsb r : r <= ▲_(lsb) r.
Proof.
exact: round_UP_ge.
Qed.


Lemma roundFIX_succ_gt lsb rnd (Vrnd : Valid_rnd rnd) r :
  r < roundFIX lsb rnd r + twopow lsb.
Proof.
rewrite Rplus_comm.
apply: Rlt_subLR.
apply: Rle_lt_trans.
  apply: Rle_abs.
rewrite Rabs_minus_sym.
exact: roundFIX_error.
Qed.


Lemma rd_succ_gt lsb r : (r < ▼_(lsb) r + twopow lsb)%R.
Proof.
exact: roundFIX_succ_gt.
Qed.


Lemma eq_rd lsb x r :
  isFIX lsb x ->
  (x <= r < x + twopow lsb)%R ->
  x = ▼_(lsb) r.
Proof.
move=> /isFIX_P x_gf r_bnd.
symmetry.
apply: round_DN_eq => //.
by rewrite succFIX.
Qed.




(** Round-to-nearest, inequalities and midpoints
    (for summation_nearest.v) **)
Section Nearest.


Variable lsb : Z.


Notation "▼ r" := (roundFIX lsb Zfloor r) (at level 35).
Notation "▲ r" := (roundFIX lsb Zceil r) (at level 35).


Definition ismidpoint r := r - ▼ r == twopow (lsb - 1).


Lemma exact_notmidpoint x :
  isFIX lsb x -> ~ ismidpoint x.
Proof.
move=> *.
rewrite /ismidpoint roundFIX_id //.
rewrite /Rminus Rplus_opp_r.
simplP.
apply: not_eq_sym.
exact: bpow_neq0.
Qed.


Lemma ru_succ_rd r :
  ~ isFIX lsb r -> ▲ r = ▼ r + twopow lsb.
Proof.
move/negP/isFIX_P => *.
rewrite /roundFIX -succ_DN_eq_UP //.
by rewrite succFIX.
Qed.


Lemma ismidpoint_sub_ru r :
  ismidpoint r <-> ▲ r - r == twopow (lsb - 1).
Proof.
case_bool (isFIX lsb r).
  by move=> *; rewrite /ismidpoint !roundFIX_id.
move=> r_notFIX.
rewrite ru_succ_rd //.
rewrite /Rminus Rplus_comm -Rplus_assoc.
rewrite -rweqP RaddLR.
rewrite (twopow_double_pred lsb).
uring_simplify (_ - _).
rewrite -Ropp_inj_iff Ropp_plus_distr !Ropp_involutive.
by rewrite rweqP.
Qed.


Lemma ismidpoint_eqmodr r :
  ismidpoint r <-> eqmodr (twopow lsb) r (twopow (lsb - 1)).
Proof.
rewrite -{2}(RsubrK (▼ r) r).
rewrite -eqmodr_add_divided_iff; last first.
  exact: isFIX_roundFIX.
split.
  move/eqP => <-.
  exact: eqmodr_refl.
move=> eqmlsb.
apply/eqP.
apply: eq_difflt_eqmodr; last first.
  exact: eqmlsb.
apply: Rabs_lt; split.
- have := rd_le lsb r.
  have : twopow (lsb - 1) < twopow lsb.
    by apply: bpow_lt; lia.
  by lra.
- have := rd_succ_gt lsb r.
  have := twopow_gt0 (lsb - 1).
  by lra.
Qed.


Variable choice : Z -> bool.


Lemma Znearest_if x :
  Znearest choice x =
  let d := x - IZR (Zfloor x) in
  if (Rltb (/ 2) d) || ((d == / 2) && choice (Zfloor x))
  then Zceil x else Zfloor x.
Proof.
rewrite /Znearest.
case_eq (Rcompare (x - IZR (Zfloor x)) (/ 2))
         => [/Rcompare_Eq_inv|/Rcompare_Lt_inv|/Rcompare_Gt_inv].
- case: ifP => _ *.
  + rewrite andbT ifT //.
    by simplP; auto.
  + rewrite andbF orbF ifF //.
    by apply/RltP; lra.
- move=> *; rewrite ifF //.
  simplP.
  by move=> [/RltP|]; lra.
- move=> *; rewrite ifT //.
  simplP.
  by left; exact/RltP.
Qed.



Notation rn := (roundFIX lsb (Znearest choice)).


Lemma ismidpoint_abs_sub_rn r :
  ismidpoint r <-> (Rabs (rn r - r) == twopow (lsb - 1)).
Proof.
rewrite roundFIX_sclmant.
rewrite Znearest_if /=.
case: ifP => _; rewrite -roundFIX_sclmant.
- rewrite Rabs_pos_eq.
    exact: ismidpoint_sub_ru.
  by have := (ru_ge lsb r); lra.
- rewrite Rabs_minus_sym Rabs_pos_eq //.
  by have := (rd_le lsb r); lra.
Qed.


Lemma ismidpoint_sclmant r :
  ismidpoint r <-> (sclmant lsb r - IZR (Zfloor (sclmant lsb r)) == / 2).
Proof.
rewrite -!rweqP.
rewrite -[X in _ <-> X](mulIf_iff (@bpow_neq0 radix2 lsb)).
rewrite -[@ssralg.GRing.mul _]/Rmult.
rewrite -twopowPe_half.
rewrite Rmult_plus_distr_r -Ropp_mult_distr_l.
rewrite Rmult_assoc.
rewrite -bpow_plus Zplus_opp_l Rmult_1_r.
by [].
Qed.


Lemma midpoint_isFIX_pred r :
  ismidpoint r -> isFIX (lsb - 1) r.
Proof.
move=> /ismidpoint_eqmodr ismp.
apply: Rdivides_eqmodr_all; cycle 1.
- rewrite eqmodr_sym.
  exact: ismp.
- by apply: Rdivides_bpow; lia.
- apply: Rdivides_refl.
all: exact: bpow_neq0.
Qed.


Lemma rn_bounds r :
  rn r - twopow (lsb - 1) <= r <= rn r + twopow (lsb - 1).
Proof.
apply Rabs_sub_bounds.
rewrite Rabs_minus_sym.
exact: roundFIX_rN_error.
Qed.


Lemma ge_rnFIX r : rn r - twopow (lsb - 1) <= r.
Proof.
have := (rn_bounds r).
by move=> [].
Qed.


Lemma le_rnFIX r : r <= rn r + twopow (lsb - 1).
Proof.
have := (rn_bounds r).
by move=> [].
Qed.


Lemma lt_rnFIX_notmidpoint r :
  ~ ismidpoint r -> r < rn r + twopow (lsb - 1).
Proof.
move=> r_not_midpoint.
apply: Rle_neq_lt.
  exact: le_rnFIX.
move=> r_eq.
move: r_not_midpoint.
rewrite ismidpoint_abs_sub_rn.
rewrite {2}r_eq.
uring_simplify (_ - _).
rewrite Rabs_Ropp Rabs_pos_eq; last exact: bpow_ge0.
by [].
Qed.


Definition choiceR r := choice (Zfloor (sclmant lsb r)).


Lemma rn_midpoint r :
  ismidpoint r ->
  rn r = roundFIX lsb (if choiceR r then Zceil else Zfloor) r.
Proof.
rewrite ismidpoint_sclmant => /eqP r_mp.
rewrite /roundFIX /round.
repeat f_equal.
rewrite /Znearest.
rewrite r_mp Rcompare_Eq // /choiceR.
by case: choice.
Qed.


Lemma lt_rnFIX_tiesup r :
  choiceR r ->
  r < roundFIX lsb (Znearest choice) r + twopow (lsb - 1).
Proof.
move=> r_choice.
case_bool (ismidpoint r); last first.
  exact: lt_rnFIX_notmidpoint.
move=> r_midpoint.
rewrite rn_midpoint //.
rewrite r_choice.
rewrite -{1}[r]ssralg.GRing.addr0.
apply: Rplus_le_lt_compat.
  exact: ru_ge.
exact: twopow_gt0.
Qed.


Lemma eq_rnFIX_notmidpoint x r :
  isFIX lsb x ->
  ~ ismidpoint r ->
  x - twopow (lsb - 1) <= r <= x + twopow (lsb - 1) ->
  x = rn r.
Proof.
move=> x_FIX notmp r_bnd.
apply: eq_difflt_eqmodr.
  rewrite Rabs_minus_sym.
  erewrite <- RaddrNKKNr.
  apply: Rle_lt_trans.
    exact: Rabs_triang.
  apply: Rplus_lt_le_compat; last first.
    rewrite Rabs_sub_bounds.
    exact: r_bnd.
  apply: Rle_neq_lt.
    exact: roundFIX_rN_error.
  move/eqP.
  by rewrite -ismidpoint_abs_sub_rn.
rewrite -twopow_double_pred.
apply: Rdivides_sub => //.
exact: isFIX_roundFIX.
Qed.


End Nearest.




(** Auxiliary lemmas for summation_nearest.v **)


Lemma Rplus_rd_isFIX l x y :
  isFIX l y -> (▼_(l) x) + y = ▼_(l) (x + y).
Proof.
move=> y_FIX.
apply: eq_rd.
  apply: isFIX_Rplus => //.
  exact: isFIX_roundFIX.
split.
  apply: Rplus_le_compat_r.
  exact: rd_le.
rewrite Rplus_assoc (Rplus_comm y) -Rplus_assoc.
apply: Rplus_lt_compat_r.
exact: rd_succ_gt.
Qed.


Lemma rd_rd_lsble l l' x :
  (l <= l')%Z -> ▼_(l') (▼_(l) x) = ▼_(l') x.
Proof.
move=> l_le.
apply: eq_rd.
  exact: isFIX_roundFIX.
split.
  by apply: Rle_trans; apply: rd_le.
apply: Rlt_le_trans.
  exact: (rd_succ_gt l).
apply: Rle_addL_eqmodr; last first.
  exact: rd_succ_gt.
apply: Rdivides_sub.
  exact: isFIX_roundFIX.
apply: Rdivides_add.
  apply: Rdivides_trans; last exact: isFIX_roundFIX.
    exact: bpow_neq0.
  all: exact: Rdivides_bpow.
Qed.


Lemma rn_rdpred_sticky l tau x :
  let sticky := (▼_(l-1) x != x) in
  roundFIX l (Znearest (fun z => sticky || tau z)) (▼_(l-1) x) =
  roundFIX l (Znearest tau) x.
Proof.
move=> sticky.
case_eq sticky => /eqP /=; last first.
  by move=> ->.
move=> x_neq.
apply: eq_rnFIX_notmidpoint.
- exact: isFIX_roundFIX.
- move=> x_mp; contradict x_neq.
  apply: roundFIX_id.
  exact: midpoint_isFIX_pred.
split.
  apply: Rle_trans.
    exact: ge_rnFIX.
  exact: rd_le.
apply: Rle_trans.
  apply: Rlt_le.
  apply: (rd_succ_gt (l - 1)).
apply: Rle_addL_eqmodr; last first.
  exact: lt_rnFIX_tiesup.
apply: Rdivides_sub; last apply: Rdivides_add.
- exact: isFIX_roundFIX.
- apply: isFIX_le; last first.
    exact: isFIX_roundFIX.
  by lia.
- apply: Rdivides_refl.
  exact: bpow_neq0.
Qed.


Notation roundFIXnearest_tiesup l :=
  (roundFIX l (Znearest (fun _ => true))).


Lemma rn_rdpred l x :
  roundFIXnearest_tiesup l (▼_(l-1) x) =
  roundFIXnearest_tiesup l x.
Proof.
rewrite -[RHS]rn_rdpred_sticky.
apply: round_ext_rnd.
by rewrite orbT.
Qed.

