(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



(** Faithful Sum-Of-Products algorithm with guard bits **)

From mathcomp
Require Import all_ssreflect ssralg.

From Flocq
Require Import Core Operations.

Require Import Lia Lra.

Require Import Rstruct lia_tactics
               utils nat_ord_complements
               Flocq_complements R_complements Z_complements
               seq_complements sum_complements
               FIX overf FIXoverf
               SOP_model.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



Local Open Scope ring_scope.
Local Open Scope R.




Section SOP_guard.


Variable lf : Z.
Variables s t : seq R.

Notation n := (minn (size s) (size t)).

Notation SOP_model := (SOP_model s t).

Variable lc : Z.



Section Any_rounds.


Variable rndmul : R -> Z.


Definition prods_guard : seq R :=
  map2 (mulFIX lc rndmul) s t.


Variable rndadd : R -> Z.


Definition SOP_guard_aux : R :=
  \big[addFIX lc rndadd / 0]_(x <- prods_guard) x.


Variable rndf : R -> Z.


Definition SOP_guard : R := roundFIX lf rndf SOP_guard_aux.


Hypothesis rndmul_valid : Valid_rnd rndmul.


Lemma isFIX_prods_guard : all (isFIX lc) prods_guard.
Proof.
apply/all_mapP.
case => *.
exact: isFIX_roundFIX.
Qed.


Hypothesis rndadd_valid : Valid_rnd rndadd.


Lemma SOP_guard_aux_eq_sum_prods : SOP_guard_aux = \sum_(x <- prods_guard) x.
Proof.
have /all_filterP:= isFIX_prods_guard => H.
rewrite /SOP_guard_aux -H.
rewrite !big_filter.
apply: eq_big_op.
- 3: apply: addFIX_isFIX_exact.
- apply: isFIX_0.
- intros; apply: isFIX_roundFIX.
- by [].
Qed.


Lemma diff_SOP_guard_aux_lt_mulneps epsilon :
  (0 < n)%nat ->
  (forall r, Rabs (roundFIX lc rndmul r - r) < epsilon) ->
  Rabs (SOP_guard_aux - SOP_model)%RT < INR n * epsilon.
Proof.
move => n_gt0 Heps.
rewrite SOP_guard_aux_eq_sum_prods /SOP_model.
rewrite !big_map -GRing.sumrB.
eapply Rle_lt_trans.
  exact: Rabs_sum_le.
eapply Rlt_le_trans.
- apply: sum_Rlt.
    by rewrite has_predT size_zip.
  move => i _. destruct i as (x1, x2).
  eapply Rlt_le_trans.
  + exact: Heps.
  + apply: Req_le.
    instantiate (1 := (fun _ => _)).
    by auto.
- apply: Req_le.
  rewrite sum_const_seq count_predT size_zip.
  rewrite mulrn_RmultINR INR_IZR_INZ Rmult_comm.
  by reflexivity.
Qed.


Hint Resolve INR_lt not_0_INR : RDb.
Hint Extern 1 (_ < _)%nat => auton : RDb.
Hint Extern 1 (not (@eq nat _ _)) => auton : RDb.


Lemma diff_SOP_guard_aux_le_muln epsilon :
  (0 < n)%nat ->
  (forall r, Rabs (roundFIX lc rndmul r - r) <= epsilon) ->
  Rabs (SOP_guard_aux - SOP_model)%RT <= INR n * epsilon.
Proof with auto with RDb.
move => n_gt0 Heps.
apply: Rle_allltle => r Hr.
apply: Rlt_le.
rewrite -(RmulVKr (INR n) r)...
apply: diff_SOP_guard_aux_lt_mulneps => // r'.
apply: Rle_lt_trans.
  exact: Heps.
apply: (Rmult_lt_reg_l (INR n))...
  apply: lt_0_INR.
  by apply/ltP.
rewrite RmulVKr...
Qed.


Lemma diff_SOP_guard_aux_lt_mulnulp :
  (0 < n)%nat ->
  Rabs ( SOP_guard_aux - SOP_model )%RT < (INR n * twopow lc).
Proof.
intro; apply: diff_SOP_guard_aux_lt_mulneps => //.
exact: roundFIX_error.
Qed.


Lemma SOP_guard_aux_n0 : n = O -> SOP_guard_aux = 0.
Proof.
move=> n0.
rewrite /SOP_guard_aux [prods_guard]size0nil; last first.
  by rewrite size_map size_zip.
by rewrite big_nil.
Qed.


End Any_rounds.





Section Final_rN_others_any.


Variables rndmul rndadd : R -> Z.

(** any tie-breaking rule for round-to-nearest works **)
Variable choicef : Z -> bool.
Notation rNf := (Znearest choicef).

Hypothesis rndmul_valid : Valid_rnd rndmul.
Hypothesis rndadd_valid : Valid_rnd rndadd.

Hypothesis lc_le : (lc <= lf - 1 - log2_ceil n)%Z.


Lemma n_le :
  INR n <= twopow (lf - 1 - lc).
Proof.
rewrite INR_IZR_INZ.
apply: (Rle_trans _ _ _ (twopow_log2_ceil_ge n)).
apply: bpow_le.
by lia.
Qed.


Lemma diff_SOP_guard_aux_lt :
  Rabs ( SOP_guard_aux rndmul rndadd - SOP_model ) < twopow (lf - 1).
Proof.
case_eq (n == O) => /eqP.
  move=> n0.
  rewrite SOP_guard_aux_n0 // SOP_model_n0 //.
  rewrite Rminus_0_r Rabs_R0.
  exact: twopow_gt0.
move=> n_neq0.
apply: Rlt_le_trans.
  apply: diff_SOP_guard_aux_lt_mulnulp.
  by auton.
eapply Rle_trans.
  apply: Rmult_le_compat_r; first auto with RDb real.
  exact: n_le.
rewrite -bpow_plus. apply: bpow_le.
by lia.
Qed.


Lemma diff_SOP_guard_lt :
  Rabs (SOP_guard rndmul rndadd rNf - SOP_model) < twopow lf.
Proof.
eapply Rle_lt_trans.
  rewrite /Rminus -(RsubrKA (SOP_guard_aux rndmul rndadd)).
  exact: Rabs_triang.
rewrite twopow_double_pred.
apply: Rplus_le_lt_compat.
- exact: roundFIX_rN_error.
- exact: diff_SOP_guard_aux_lt.
Qed.


Theorem SOP_guard_faithful :
  faithful lf SOP_model (SOP_guard rndmul rndadd rNf).
Proof.
apply: diff_lt_faithful.
- exact: isFIX_roundFIX.
- exact: diff_SOP_guard_lt.
Qed.


End Final_rN_others_any.



Corollary SOP_guard_rD_faithful :
  (lc <= lf - 1 - log2_ceil n)%Z ->
  forall choice,
  faithful lf SOP_model (SOP_guard Zfloor Zfloor (Znearest choice)).
Proof.
intros.
exact: SOP_guard_faithful.
Qed.





Section Final_rN_mul_rN.


Variable choicemul : Z -> bool.
Notation rNmul := (Znearest choicemul).

Variable rndadd : R -> Z.

Variable choicef : Z -> bool.
Notation rNf := (Znearest choicef).

Hypothesis rndadd_valid : Valid_rnd rndadd.


Lemma diff_SOP_guard_rN_le :
  Rabs ( SOP_guard_aux rNmul rndadd - SOP_model )%RT <= (INR n * twopow (lc - 1)).
Proof.
case_eq (n == O) => /eqP.
  intro.
  rewrite SOP_guard_aux_n0 // SOP_model_n0 // GRing.subr0 Rabs_R0.
  apply: Rmult_le_pos.
    exact: pos_INR.
  exact: bpow_ge0.
intro.
apply: diff_SOP_guard_aux_le_muln.
  by auton.
exact: roundFIX_rN_error.
Qed.


Hypothesis lc_le : (lc <= lf - 1 - log2_floor n)%Z.


Lemma n_lt : INR n < twopow (lf - lc).
Proof.
rewrite INR_IZR_INZ.
apply: (Rlt_le_trans _ _ _ (twopow_log2_floor_gt _)).
apply: bpow_le.
lia.
Qed.


Lemma twopow_calc_final_lt : INR n * twopow (lc - 1) < twopow (lf - 1).
Proof.
eapply Rlt_le_trans.
  apply Rmult_lt_compat_r.
    by auto with RDb.
  exact: n_lt.
right.
rewrite -bpow_plus.
by f_equal; ring.
Qed.


Lemma diff_SOP_guard_aux_rN_lt :
  Rabs ( SOP_guard_aux rNmul rndadd - SOP_model ) < twopow (lf - 1).
Proof.
eapply Rle_lt_trans.
- exact: diff_SOP_guard_rN_le.
- exact: twopow_calc_final_lt.
Qed.


Lemma diff_SOP_guard_rN_lt :
  Rabs (SOP_guard rNmul rndadd rNf - SOP_model) < twopow lf.
Proof.
eapply Rle_lt_trans.
  replace (SOP_guard rNmul rndadd rNf - SOP_model)
    with ((SOP_guard rNmul rndadd rNf - SOP_guard_aux rNmul rndadd) +
          (SOP_guard_aux rNmul rndadd - SOP_model))
    by ring.
  exact: Rabs_triang.
rewrite twopow_double_pred.
apply: Rplus_le_lt_compat.
  2: exact: diff_SOP_guard_aux_rN_lt.
eapply Rle_trans.
    apply: error_le_half_ulp.
apply: Req_le.
rewrite ulp_FIX.
replace lf with (lf - 1 + 1)%Z at 1 by ring.
rewrite bpow_plus_1 -Rmult_assoc Rinv_l //. ring.
Qed.


Theorem SOP_guard_rN_faithful :
  faithful lf SOP_model (SOP_guard rNmul rndadd rNf).
Proof.
apply: diff_lt_faithful.
- exact: isFIX_roundFIX.
- exact: diff_SOP_guard_rN_lt.
Qed.


End Final_rN_mul_rN.






(** Taking overflows into account **)



Variables wf wc : width.

Notation final_mb := (wf + lf)%Z.
Notation final_p := (twopow final_mb).
Notation calc_p := (twopow (wc + lc)%Z).

Notation final_msb := (wf + lf - 1)%Z.


Hypothesis wc_ge : (wf + lf - lc <= wc)%Z.


Lemma final_p_le : final_p <= calc_p.
Proof.
apply: bpow_le. apply Z.le_sub_le_add_r.
eapply Z.le_trans. exact: Zeq_le. exact: wc_ge.
Qed.



Section Any_rounds_OW.


Variable rndmul : R -> Z.
Hypothesis rndmul_valid : Valid_rnd rndmul.

Variable rndadd : R -> Z.
Hypothesis rndadd_valid : Valid_rnd rndadd.

Variable rndf : R -> Z.


Definition prods_guard_OW : seq R := 
  map2 (mulFOW lc wc rndmul) s t.


Definition SOP_guard_aux_OW : R :=
  \big[addFOW lc wc rndadd / 0]_(x <- prods_guard_OW) x.


Definition SOP_guard_OW : R :=
  roundFOW lf wf rndf SOP_guard_aux_OW.


Lemma isFIX_prods_guard_OW : all (isFIX lc) prods_guard_OW.
Proof.
apply/all_mapP.
case => *.
exact: isFIX_roundFOW.
Qed.


Notation prd := (2 ^ wc)%Z.
Hypothesis rndmul_preg : quasiperZ prd rndmul.
Hypothesis rndadd_preg : quasiperZ prd rndadd.


Lemma eqmodr_prods_guard :
  all2 (eqmodr (twopow (wc + lc))) prods_guard_OW (prods_guard rndmul).
Proof.
rewrite /prods_guard_OW /prods_guard. move: s t.
apply: seq_ind2'.
- by case.
- move => x. by case.
- move => r1 r2 t1 t2 Hind.
  apply/andP; split. 2: exact: Hind.
  rewrite eqmodr_sym. exact: eqmodr_roundFIXFOW.
Qed.


Lemma eqmodr_SOP_guard_aux : eqmodr calc_p SOP_guard_aux_OW (SOP_guard_aux rndmul rndadd).
Proof.
eapply eqmodr_trans; last first.
  exact: eqmodr_big_addFOW_addFIX.
rewrite /SOP_guard_aux_OW (eq_big_addFOW_eqmodr (prods_guard rndmul)) //.
- exact: eqmodr_refl.
- exact: eqmodr_prods_guard.
Qed.


Lemma eqmodr_final_p_SOP_guard_aux :
  eqmodr final_p SOP_guard_aux_OW (SOP_guard_aux rndmul rndadd).
Proof.
eapply eqmodr_bpowle.
  apply Z.le_sub_le_add_r. eapply Z.le_trans.
  exact: Zeq_le. exact: wc_ge.
exact: eqmodr_SOP_guard_aux.
Qed.


End Any_rounds_OW.


Definition rN_like_SOP_guard_aux rndmul rndadd fchoice :=
  Znearest (fun _ =>
    fchoice (Zfloor (sclmant lf (SOP_guard_aux_OW rndmul rndadd)))).





Section Final_rN_others_any_OW.


Variables rndmul rndadd : R -> Z.

Variable choicef : Z -> bool.
Notation rNf := (Znearest choicef).


Hypothesis rndmul_valid : Valid_rnd rndmul.
Hypothesis rndadd_valid : Valid_rnd rndadd.

Hypothesis rndmul_preg : quasiperZ (2 ^ wc) rndmul.
Hypothesis rndadd_preg : quasiperZ (2 ^ wc) rndadd.
Hypothesis rNf_preg : quasiperZ (2 ^ wf) rNf.


Notation rN' := (rN_like_SOP_guard_aux rndmul rndadd choicef).


Lemma RlocalDN_rN' :
  RlocalDN lf rN' (SOP_guard_aux rndmul rndadd) =
  RlocalDN lf rNf (SOP_guard_aux_OW rndmul rndadd).
Proof.
rewrite !RlocalDN_rN
        (Rcompare_mid_eqm_ulp (SOP_guard_aux_OW rndmul rndadd))
        // eqmodr_sym.
eapply eqmodr_bpowle; last first.
  exact: eqmodr_final_p_SOP_guard_aux.
by generalize (width_pos wf); auto with zarith.
Qed.


Lemma eqmodr_SOP_guard_rN' :
  eqmodr final_p (SOP_guard_OW rndmul rndadd rNf) (SOP_guard rndmul rndadd rN').
Proof.
apply: eqmodr_trans.
  rewrite eqmodr_sym.
  exact: eqmodr_roundFIXFOW.
apply: eqmodr_roundFIX_same_RlocalDN.
- symmetry. apply: RlocalDN_rN'.
- exact: eqmodr_final_p_SOP_guard_aux.
Qed.

Hypothesis lc_le : (lc <= lf - 1 - log2_ceil n)%Z.

Theorem SOP_guard_OW_faithfulFOW :
  faithfulFOW lf wf SOP_model (SOP_guard_OW rndmul rndadd rNf).
Proof.
apply diff_lt_faithfulFOW.
- exact: isFIXo_roundFOW.
- exists (SOP_guard rndmul rndadd rN').
  split.
  + exact: eqmodr_SOP_guard_rN'.
  + exact: diff_SOP_guard_lt.
Qed.


Hypothesis wf_ge :
  (Zceil (Rlog2 (Rabs SOP_model + twopow lf)) - lf + 1 <= wf)%Z.


Fact SOP_model_le : Rabs SOP_model <= twopow (wf + lf - 1)%Z - twopow lf.
Proof.
apply: (Rplus_le_reg_r (twopow lf)).
ring_simplify.
apply: Rlog2_le_inv.
- apply: Rplus_le_lt_0_compat.
    exact: Rabs_pos.
  exact: twopow_gt0.
- exact: twopow_gt0.
rewrite Rlog2_twopow.
apply: Rle_trans.
  exact: Zceil_ub.
apply: IZR_le.
by lia.
Qed.


Lemma In_cppe_SOP_guard_from_diff_SOP_guard_lt :
  Rabs (SOP_guard rndmul rndadd rN' - SOP_model) < twopow lf ->
  In_cppe final_mb (SOP_guard rndmul rndadd rN').
Proof.
move=> Hdiff_lt.
apply In_cpp_Rabs_lt.
rewrite -twopowPe_half' -[X in Rabs X](RsubrK SOP_model).
apply: Rle_lt_trans.
  exact: Rabs_triang.
apply: Rlt_le_trans.
  apply: Rplus_lt_le_compat.
    exact: Hdiff_lt.
  exact: SOP_model_le.
ring_simplify.
exact: Rle_refl.
Qed.


Lemma In_cppe_SOP_guard :
  In_cppe final_mb (SOP_guard rndmul rndadd rN').
Proof.
apply: In_cppe_SOP_guard_from_diff_SOP_guard_lt.
exact: diff_SOP_guard_lt.
Qed.



Theorem SOP_guard_OW_faithful :
  faithful lf SOP_model (SOP_guard_OW rndmul rndadd rNf).
Proof.
apply diff_lt_faithful.
  exact: isFIX_roundFOW.
replace (SOP_guard_OW rndmul rndadd rNf) with (SOP_guard rndmul rndadd rN').
  exact: diff_SOP_guard_lt.
apply: eq_In_cpp_eqmodr.
- exact: In_cppe_SOP_guard.
- have [H H'] := (isFIXo_roundFOW lf wf rNf (SOP_guard_aux_OW rndmul rndadd)).
  by assumption.
- rewrite eqmodr_sym.
  exact: eqmodr_SOP_guard_rN'.
Qed.


End Final_rN_others_any_OW.


Section Final_rN_mul_rN_OW.

Variable choicemul : Z -> bool.
Notation rNmul := (Znearest choicemul).

Variable rndadd : R -> Z.

Variable choicef : Z -> bool.
Notation rNf := (Znearest choicef).

Hypothesis rndadd_valid : Valid_rnd rndadd.

Hypothesis rNmul_preg : quasiperZ (2 ^ wc) rNmul.
Hypothesis rndadd_preg : quasiperZ (2 ^ wc) rndadd.
Hypothesis rNf_preg : quasiperZ (2 ^ wf) rNf.

Hypothesis lc_le : (lc <= lf - 1 - log2_floor n)%Z.


Notation rN' := (rN_like_SOP_guard_aux rNmul rndadd choicef).


Theorem SOP_guard_OW_rN_faithfulFOW :
  faithfulFOW lf wf SOP_model (SOP_guard_OW rNmul rndadd rNf).
Proof.
apply diff_lt_faithfulFOW.
- exact: isFIXo_roundFOW.
- exists (SOP_guard rNmul rndadd rN').
  split.
  + exact: eqmodr_SOP_guard_rN'.
  + exact: diff_SOP_guard_rN_lt.
Qed.


Hypothesis wf_ge :
  (Zceil (Rlog2 (Rabs SOP_model + twopow lf)) - lf + 1 <= wf)%Z.


Lemma In_cppe_SOP_guard_rN : In_cppe final_mb (SOP_guard rNmul rndadd rN').
Proof.
apply: In_cppe_SOP_guard_from_diff_SOP_guard_lt => //.
exact: diff_SOP_guard_rN_lt.
Qed.


Theorem SOP_guard_OW_rN_faithful : faithful lf SOP_model (SOP_guard_OW rNmul rndadd rNf).
Proof.
apply diff_lt_faithful.
  exact: isFIX_roundFOW.
replace (SOP_guard_OW rNmul rndadd rNf) with (SOP_guard rNmul rndadd rN').
  exact: diff_SOP_guard_rN_lt.
apply: eq_In_cpp_eqmodr.
- exact: In_cppe_SOP_guard_rN.
- have [H H'] := (isFIXo_roundFOW lf wf rNf (SOP_guard_aux_OW rNmul rndadd)).
  by assumption.
- rewrite eqmodr_sym.
  exact: eqmodr_SOP_guard_rN'.
Qed.


End Final_rN_mul_rN_OW.



End SOP_guard.

