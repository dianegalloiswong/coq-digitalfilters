(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import ssreflect ssrbool ssrfun ssrnat seq bigop path eqtype ssralg.

From Flocq
Require Import Core.

Require Import Lia Lra.

Require Import lia_tactics Rstruct
               utils nat_ord_complements
               Z_complements R_complements seq_complements Flocq_complements
               FIX SOP_model summation_decreasinglsb.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Local Open Scope Z.
Local Open Scope ring_scope.
Local Open Scope R.




(** For inputs whose LSB are < lf.
    LSBs need to be sorted.        **)
Module Small_lsb.
Section Small_lsb.

Variable lf : Z.

Variable ls : seq Z.
Variable ps : seq R.

Hypothesis are_lsb_ls_ps : are_lsb ls ps.
Hypothesis ls_srt : sorted Z.leb ls.
Hypothesis all_small : all (fun l => Z.ltb l lf) ls.

Notation n := (size ps).



(** computation **)


(** by convention l_n = lf - 1 **)
Definition getl := nth (lf - 1)%Z ls.


(** "vi i" is the partial sum up to p_i for 0 <= i < n *)
Fixpoint vi (i : nat) :=
match i with
  | O => ▼_(getl 1) (ps ` 0)
  | S j => vi j ▼+_(getl j.+2) (ps ` j.+1)
end.


(** Intermediary result after all inputs have been
    added but before final rounding to F_lf, known
    as q in the thesis **)
Definition q := (vi n.-1).


Definition summation_smalllsb : R := roundFIXnearest_tiesup lf q.





(** proof of correctness **)


Lemma isFIX_li_pi (i : nat) :
  isFIX (getl i) (ps ` i).
Proof.
rewrite /getl.
exact: are_lsb_nth_default0.
Qed.


Lemma size_ls : size ls = n.
Proof.
apply: all2_same_size.
exact: are_lsb_ls_ps.
Qed.


Lemma getl_last_lt : (getl n.-1 < lf)%Z.
Proof.
rewrite -size_ls.
case_bool ((size ls).-1 < size ls)%nat.
  move => *.
  move: all_small => /all_nthP nth_ltb.
  apply/ZltP.
  exact: nth_ltb.
move=> *.
rewrite /getl nth_default; first by lia.
by auton.
Qed.


Lemma getl_le i : (getl i <= getl i.+1)%Z.
Proof.
apply: Zle_nth_sorted_lastle => //.
rewrite -nth_last size_ls.
fold getl.
have:= getl_last_lt.
by lia.
Qed.


Lemma isFIX_lSi_vi i : isFIX (getl i.+1) (vi i).
Proof.
by case: i => [ | p ]; exact: isFIX_roundFIX.
Qed.


Lemma isFIX_li_vi i : isFIX (getl i) (vi i).
Proof.
apply: isFIX_le; last first.
  exact: isFIX_lSi_vi.
exact: getl_le.
Qed.


Lemma vi_succ i : vi i.+1 = vi i ▼+_(getl i.+2) (ps ` i.+1).
Proof.
by [].
Qed.


Lemma vi_rd_sum (i :nat) :
  (i < n)%nat ->
  vi i = ▼_(getl i.+1) (\sum_(x <- take i.+1 ps) x).
Proof.
move: i.
apply: nat_ind.
  move=> predn_gt0.
  erewrite take1; last by ssrnatlia.
  by rewrite big_seq1.
move=> i IH Si_lt.
apply apply_l in IH; last by ssrnatlia.
rewrite vi_succ IH.
rewrite /addFIX /binopFIX Rplus_rd_isFIX; last first.
  exact: isFIX_li_pi.
rewrite rd_rd_lsble; last first.
  exact: getl_le.
rewrite [in RHS](take_nth 0%R); last by ssrnatlia.
by rewrite big_rcons.
Qed.


Notation mres := (\sum_(i <- ps) i).

Fact q_rd_mres_n0 :
  (n = 0)%nat -> q = ▼_(lf - 1) mres.
Proof.
move=> *.
rewrite /q [ps]size0nil // big_nil.
rewrite /= [ps]size0nil //.
by repeat (rewrite (roundFIX_id 0); last exact: isFIX_0).
Qed.


Lemma q_rd_mres : q = ▼_(lf - 1) mres.
Proof.
case_bool (0 < n)%nat; last first.
  move=> *.
  apply: q_rd_mres_n0.
  by ssrnatlia.
move=> *.
rewrite /q vi_rd_sum; last by ssrnatlia.
rewrite prednK // take_size.
by rewrite /getl nth_default // size_ls //.
Qed.


Theorem summation_smalllsb_correct :
  summation_smalllsb = roundFIXnearest_tiesup lf mres.
Proof.
rewrite /summation_smalllsb q_rd_mres.
exact: rn_rdpred.
Qed.





(** Enforcing a given tie-breaking rule thanks to sticky bits **)


(** "stickyi i" is the sticky bit after the computation of "vi i" *)
Fixpoint stickyi (i : nat) :=
match i with
  | O => vi O != (ps ` O)
  | S j => stickyi j || ( vi j.+1 != (vi j + (ps ` j.+1))%R )
end.

Definition stickyq := (stickyi n.-1).


(** Desired tie-breaking rule **)
Variable tau : Z -> bool.


Definition summation_smalllsb_sticky : R :=
  roundFIX lf (Znearest (fun z => stickyq || tau z)) q.


Lemma stickyi_vi i :
(i < n)%nat ->
  stickyi i = (vi i != \sum_(x <- take i.+1 ps) x).
Proof.
move: i; apply: nat_ind.
  move=> predn_gt0.
  erewrite take1; last by ssrnatlia.
  by rewrite big_seq1.
move=> i IH Si_lt.
apply apply_l in IH; last by ssrnatlia.
rewrite (take_nth 0%R); last by ssrnatlia.
rewrite big_rcons.
rewrite /stickyi -/stickyi.
rewrite IH.
case_eq (vi i != \sum_(x <- take i.+1 ps) x) => /eqP; last first.
  by move=> /= ->.
move=> *.
rewrite orTb eqTb.
apply/eqP.
apply: Rlt_not_eq.
rewrite /vi; fold vi.
apply: Rle_lt_trans.
  exact: rd_le.
apply: Rplus_lt_compat_r.
apply: Rle_neq_lt => //.
rewrite vi_rd_sum; last by ssrnatlia.
exact: rd_le.
Qed.


Fact stickyq_mres_n0 :
  (n = 0)%nat -> stickyq = (▼_(lf-1) mres != mres).
Proof.
move=> n0.
rewrite /stickyq n0 Nat.pred_0 /=.
rewrite /q [ps]size0nil // big_nil.
by repeat (rewrite (roundFIX_id 0); last exact: isFIX_0).
Qed.


Lemma stickyq_mres :
  stickyq = (▼_(lf-1) mres != mres).
Proof.
case_bool (0 < n)%nat; last first.
  move=> *.
  apply: stickyq_mres_n0.
  by ssrnatlia.
move=> *.
rewrite /stickyq stickyi_vi; last by ssrnatlia.
rewrite -/q q_rd_mres // prednK //.
by rewrite take_size.
Qed.


Theorem summation_smalllsb_sticky_correct :
  summation_smalllsb_sticky = roundFIX lf (Znearest tau) mres.
Proof.
rewrite /summation_smalllsb_sticky.
rewrite stickyq_mres // q_rd_mres //.
exact: rn_rdpred_sticky.
Qed.




(** with last addition directly rounded to nearest **)


Definition summation_smalllsb_sticky_lastaddrn : R :=
  if (n <= 1)%nat then
    roundFIX lf (Znearest tau) (head 0%R ps)
  else
    addFIX lf (Znearest (fun z => stickyi n.-2 || tau z))
      (vi n.-2) (ps ` n.-1).


Lemma lastaddrn_midpoint_exact :
  (0 < n.-1)%nat ->
  ismidpoint lf (\sum_(j <- take n.-1 ps) j + nth 0%R ps n.-1)%R ->
  vi n.-2 = \sum_(x <- take n.-1 ps) x.
Proof.
move=> n_gt ismp.
rewrite vi_rd_sum; last by ssrnatlia.
rewrite prednK //.
rewrite roundFIX_id // /isFIX.
rewrite -[X in _ |d X](GRing.addrK (ps ` n.-1)).
apply: Rdivides_sub; last first.
  exact: isFIX_li_pi.
apply: Rdivides_trans; cycle 2.
- apply: midpoint_isFIX_pred.
  exact: ismp.
- exact: bpow_neq0.
- apply: Rdivides_bpow.
  by have * := getl_last_lt; lia.
Qed.


Theorem summation_smalllsb_sticky_lastaddrn_correct :
  summation_smalllsb_sticky_lastaddrn =
  roundFIX lf (Znearest tau) mres.
Proof.
rewrite /summation_smalllsb_sticky_lastaddrn.
case: ifP.
  move=> *; f_equal.
  by erewrite head_01_sum.
move=> /eqP n_pred_neq.
have n_gt: (0 < n.-1)%nat by auton.
rewrite [ps in RHS](rconsI 0%R); last first.
  apply: notnil.
  by ssrnatlia.
rewrite big_rcons /=.
rewrite stickyi_vi; last by ssrnatlia.
rewrite prednK //.
case_eq (vi n.-2 != \sum_(x <- take n.-1 ps) x)
         => /eqP /=; last first.
  by move=> ->.
move=> vi_neq.
apply: eq_rnFIX_notmidpoint.
- exact: isFIX_roundFIX.
- by move=> /(lastaddrn_midpoint_exact n_gt).
split.
  apply: Rle_trans.
    exact: ge_rnFIX.
  apply: Rplus_le_compat_r.
  rewrite vi_rd_sum; last by ssrnatlia.
  rewrite prednK //.
  exact: rd_le.
apply: Rle_trans.
  apply: Rplus_le_compat_r.
  apply: Rlt_le.
  exact: (rd_succ_gt (getl n.-1)).
rewrite -{1 2}[n.-1]prednK //.
rewrite -vi_rd_sum; last by ssrnatlia.
rewrite Rplus_comm -Rplus_assoc [X in (X+_)%R]Rplus_comm.
apply: Rle_addL_eqmodr; last first.
  exact: lt_rnFIX_tiesup.
have * := getl_last_lt.
apply: Rdivides_sub; apply: Rdivides_add.
- rewrite -{1}[n.-1]prednK //.
  exact: isFIX_lSi_vi.
- exact: isFIX_li_pi.
- apply: Rdivides_trans; cycle 2.
  + exact: isFIX_roundFIX.
  + exact: bpow_neq0.
  + by apply: Rdivides_bpow; lia.
- by apply: Rdivides_bpow; lia.
Qed.


End Small_lsb.
End Small_lsb.







Section Any_lsb.



Variable lf : Z.

Variable ls : seq Z.
Variable ps : seq R.

Hypothesis are_lsb_ls_ps : are_lsb ls ps.
Hypothesis ls_srt : sorted Z.leb ls.

Notation n := (size ps).




(** computation **)


Notation m := (find (Z.leb lf) ls).


Notation ps_small := (take m ps).
Notation ls_small := (take m ls).
Notation q_small := (Small_lsb.q lf ls_small ps_small).


Notation ps_large := (drop m ps).
Notation ls_large := (drop m ls).
Notation q_large := (summation_decreasinglsb ls_large ps_large).


Notation Zrnup := (Znearest (fun=> true)).

Definition summation_nearest := addFIX lf Zrnup q_small q_large.




(** proof of correctness **)


Lemma size_ls_small : size ls_small = m.
Proof.
rewrite size_take.
case: ifP => //.
have := find_size (Z.leb lf) ls.
by auton.
Qed.


Lemma all_small : all (Z.ltb^~ lf) ls_small.
Proof.
apply/(all_nthP 0%Z) => i.
rewrite size_ls_small => i_lt.
rewrite nth_take //.
rewrite Z.ltb_antisym.
apply: negbT.
exact: before_find.
Qed.


Lemma isFIX_q_large : isFIX (lf - 1) q_large.
Proof.
case_bool (has (Z.leb lf) ls); last first.
  move=> /negP /find_nhas m_eq.
  rewrite summation_decreasinglsb_n0.
    exact: isFIX_0.
  by rewrite m_eq (all2_same_size are_lsb_ls_ps) drop_size.
move=> *.
apply: isFIX_le; last first.
  apply: isFIX_summation_decreasinglsb.
  exact: all2_drop.
rewrite nth_drop addn0.
apply: (Z.le_trans _ lf).
  by lia.
apply/ZleP.
by rewrite nth_find.
Qed.


Notation mres := (\sum_(i <- ps) i).

Lemma add_q_mres : q_small + q_large = ▼_(lf - 1) mres.
Proof.
have * := Zleb_trans.
rewrite Small_lsb.q_rd_mres; cycle 1.
- exact: all2_take.
- exact: sorted_take.
- exact: all_small.
rewrite Rplus_rd_isFIX; last first.
  exact: isFIX_q_large.
rewrite summation_decreasinglsb_exact; cycle 1.
- exact: all2_drop.
- exact: sorted_drop.
by rewrite -big_cat cat_take_drop.
Qed.


Theorem summation_nearest_correct :
  summation_nearest = roundFIX lf Zrnup mres.
Proof.
rewrite /summation_nearest /addFIX /binopFIX add_q_mres.
exact: rn_rdpred.
Qed.





(** Enforcing a given tie-breaking rule thanks to sticky bits **)

Variable tau : Z -> bool.

Notation stickyq_small := (Small_lsb.stickyq lf ls_small ps_small).

Definition summation_nearest_sticky : R :=
  addFIX lf (Znearest (fun z => stickyq_small || tau z)) q_small q_large.


Lemma stickyq_small_mres :
  stickyq_small = (▼_(lf-1) mres != mres).
Proof.
have * := Zleb_trans.
rewrite Small_lsb.stickyq_mres => //; cycle 1.
- exact: all2_take.
- exact: sorted_take.
- exact: all_small.
rewrite -{3 4}[ps](cat_take_drop m) big_cat.
rewrite -Rplus_rd_isFIX; last first.
  rewrite -(summation_decreasinglsb_exact (all2_drop m are_lsb_ls_ps)).
    exact: isFIX_q_large.
  exact: sorted_drop.
by rewrite eqb_Rplus_eq_reg_r.
Qed.


Theorem summation_nearest_sticky_correct :
  summation_nearest_sticky = roundFIX lf (Znearest tau) mres.
Proof.
rewrite /summation_nearest_sticky /addFIX /binopFIX add_q_mres.
rewrite stickyq_small_mres.
exact: rn_rdpred_sticky.
Qed.


End Any_lsb.

