(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import all_ssreflect ssralg.

From Flocq
Require Import Core Operations.

Require Import Lia Lra.

Require Import Rstruct utils nat_ord_complements
               Flocq_complements R_complements Z_complements seq_complements
               FIX.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Local Open Scope ring_scope.
Local Open Scope R.





Section SOP_model.


Variables s t : seq R.


Definition prods_model : seq R := map2 Rmult s t.


Definition SOP_model : R := \sum_(r <- prods_model) r.


Notation n := (minn (size s) (size t)).


Fact n_ge0 : (0 <= n)%Z.
Proof.
exact: Zle_0_nat.
Qed.


Fact n_Rge0 : 0 <= (IZR n).
Proof.
apply: IZR_le.
exact: n_ge0.
Qed.


Fact size_prods_model : size prods_model = n.
Proof.
by rewrite size_map size_zip.
Qed.


Lemma SOP_model_n0 : n = O -> SOP_model = 0.
Proof.
move=> n0.
rewrite /SOP_model [prods_model]size0nil; last first.
  by rewrite size_prods_model.
by rewrite big_nil.
Qed.


End SOP_model.






Definition are_lsb (ls : seq Z) (s : seq R) :bool :=
  all2 isFIX ls s.


Lemma size_are_lsb (ls : seq Z) (s : seq R) :
  are_lsb ls s -> size ls = size s.
Proof.
exact: all2_same_size.
Qed.


Lemma are_lsb_nth z0 r0 (ls : seq Z) (s : seq R) :
  are_lsb ls s ->
  forall i, (i < size s)%nat -> isFIX (nth z0 ls i) (nth r0 s i).
Proof.
move=> are_lsb i i_lt.
apply: nth_all2 => //.
by rewrite (all2_same_size are_lsb).
Qed.


Lemma are_lsb_nth_default0 z0 (ls : seq Z) (s : seq R) :
  are_lsb ls s ->
  forall i, isFIX (nth z0 ls i) (nth 0 s i).
Proof.
move=> are_lsb i.
case_bool (i < size ls)%nat.
  by move=> *; apply: nth_all2 => //.
move=> *.
rewrite !nth_default; cycle 1.
  rewrite -(all2_same_size are_lsb).
  1,2: auton.
exact: isFIX_0.
Qed.


Lemma are_lsb_consP l ls x s :
  are_lsb (l :: ls) (x :: s) <-> (isFIX l x /\ are_lsb ls s).
Proof.
by rewrite -rwandP.
Qed.

