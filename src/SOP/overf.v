(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import ssreflect ssrbool eqtype.

From Flocq
Require Import Core.

Require Import ZArith Reals Lra.

Require Import ProofIrrelevance.

Require Import Rstruct utils Flocq_complements R_complements.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope R_scope.



Section In_cpp.

(** period **)
Variable p : R.
Hypothesis p_gt0 : 0 < p.


(** centric principal period **)
Definition In_cpp r := - (p / 2) <= r < p / 2.


Lemma In_cpp_Rabs_lt r :
  Rabs r < p / 2 -> In_cpp r.
Proof.
move=> /Rabs_lt_inv.
by split; lra.
Qed.


Lemma Rabs_le_In_cpp (r : R) :
  In_cpp r -> Rabs r <= p / 2.
Proof.
move => [r_ge r_lt].
apply: Rabs_le.
by split; lra.
Qed.


Lemma In_cpp_eq_min_or (r : R) :
  In_cpp r -> r = - (p / 2) \/ Rabs r < p / 2.
Proof.
move => [Hle Hlt].
case Hcase: (- (p / 2) == r).
- left.
  by move/eqP: Hcase.
- right.
  move/eqP: Hcase => Hneq.
  apply: Rabs_lt.
  split.
  + exact: Rle_neq_lt.
  + assumption.
Qed.


Lemma diff_lt_In_cpp (r1 r2 : R) :
  In_cpp r1 -> In_cpp r2 ->
  Rabs (r1 - r2) < p.
Proof.
rewrite /In_cpp=> Hr1 Hr2.
case: (In_cpp_eq_min_or Hr2).
- move => ->.
  rewrite Rabs_right.
  + rewrite /Rminus Ropp_involutive.
    by lra.
  + apply: Rle_ge.
    apply: Rle_0_minus.
    exact: (proj1 Hr1).
- move=> Hcase.
  eapply Rle_lt_trans.
    exact: Rabs_triang.
  replace p with (p / 2 + p / 2) by lra.
  apply: Rplus_le_lt_compat.
  + exact: Rabs_le_In_cpp.
  + rewrite Rabs_Ropp.
    exact: Hcase.
Qed.


Lemma eq_In_cpp_eqmodr (r1 r2 : R) :
  In_cpp r1 -> In_cpp r2 -> eqmodr p r1 r2 -> r1 = r2.
Proof.
move => Hr1 Hr2 Heqmodr.
apply: eq_difflt_eqmodr; last first.
  exact: Heqmodr.
exact: diff_lt_In_cpp.
Qed.


End In_cpp.



(** centric real modulo:
    cmodr p r = r' such that r and r' are congruent modulo p and
                             -p/2 <= r' < p/2
**)
Section cmodr.


Variable p : R.
Hypothesis p_gt0 : 0 < p.


Definition cmodr r :=
  let k := Znearest (fun _ => true) (r/p) in
  r - IZR k * p.


Lemma In_cpp_cmodr r : In_cpp p (cmodr r).
Proof with auto with real.
rewrite /cmodr; split.
- have := (Znearest_le xpredT (r/p)).
  move=> /(Rmult_le_compat_r p).
  apply: ask_proof...
  rewrite Rmult_plus_distr_r Rmult_assoc Rinv_l...
  by lra.
- have := (Znearest_gt xpredT (r/p)).
  apply: ask_proof...
  move=> /(Rmult_lt_compat_r p).
  apply: ask_proof...
  rewrite Rmult_plus_distr_r Rmult_assoc Rinv_l...
  by lra.
Qed.


Lemma eqmodr_cmodr r : eqmodr p (cmodr r) r.
Proof.
apply: eqmodr_submultiple.
by auto with real.
Qed.


Lemma cmodr_id_In_cpp r : In_cpp p r -> cmodr r = r.
Proof.
intro; apply: eq_In_cpp_eqmodr.
- exact: In_cpp_cmodr.
- by assumption.
- exact: eqmodr_cmodr.
Qed.


Lemma periodic_cmodr : periodic p cmodr.
Proof.
move=> r.
apply: eq_In_cpp_eqmodr.
  1,2: exact: In_cpp_cmodr.
apply: eqmodr_trans.
  exact: eqmodr_cmodr.
apply: eqmodr_trans; last first.
  rewrite eqmodr_sym.
  exact: eqmodr_cmodr.
apply: eqmodr_addmod.
by auto with real.
Qed.


End cmodr.

Arguments eqmodr_cmodr p {p_gt0} r.




Section Overf.


(** exposant of period, such that WRAP overflow is modulo 2^ep
    ep = msb + 1 = w + lsb
**)
Variable ep : Z.


(** centric principal period from an exposant **)
Definition In_cppe := In_cpp (twopow ep).


Lemma In_cppe_0 : In_cppe 0.
Proof.
have := (twopow_gt0 ep).
by split; lra.
Qed.


(** characterization of an overflow **)
Definition Is_overf (f : R -> R) :=
(forall r, In_cppe (f r)) /\ forall r, In_cppe r -> f r = r.


(** saturated overflow **)
Definition overfSAT (lsb : Z) r :=
  if Rlt_le_dec r (- twopow (ep - 1))
  then - twopow (ep - 1)
  else
    if Rlt_le_dec r (twopow (ep - 1))
    then r
    else twopow (ep - 1) - twopow lsb.


Lemma overfSAT_Is_overf lsb :
  (lsb <= ep - 1)%Z -> Is_overf (overfSAT lsb).
Proof.
move=> lsb_le.
split; rewrite /overfSAT /In_cppe /In_cpp -twopowPe_half'.
- move=> r.
  have := (twopow_gt0 (ep - 1)).
  case: ifP; case: Rlt_le_dec => //.
    move=> _ _.
    by split => //; lra.
  case: ifP; case: Rlt_le_dec => //.
  split.
    by have := (bpow_le radix2 _ _ lsb_le); lra.
  by have := (twopow_gt0 lsb); lra.
- move=> r [] *.
  case: ifP; case: Rlt_le_dec => //.
    by intros; lra.
  case: ifP; case: Rlt_le_dec => //.
  by intros; lra.
Qed.


(** modular overflow **)
Definition overfWRAP : R -> R := cmodr (twopow ep).


Lemma overfWRAP_In_cppe r : In_cppe (overfWRAP r).
Proof.
apply: In_cpp_cmodr.
by auto with RDb.
Qed.


Lemma overfWRAP_id_In_cppe r : In_cppe r -> overfWRAP r = r.
Proof.
apply: cmodr_id_In_cpp.
by auto with RDb.
Qed.


Lemma overfWRAP_Is_overf : Is_overf overfWRAP.
Proof.
split.
- exact: overfWRAP_In_cppe.
- exact: overfWRAP_id_In_cppe.
Qed.


Lemma overfWRAP_periodic :
periodic (twopow ep) overfWRAP.
Proof.
apply: periodic_cmodr.
by auto with RDb.
Qed.


End Overf.

