(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)




From mathcomp
Require Import ssreflect ssrbool seq bigop.

From Flocq
Require Import Core Operations.

Require Import Lia Lra.

Require Import Rstruct utils
               R_complements Flocq_complements seq_complements
               FIX overf.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Local Open Scope Z.
Local Open Scope R.




Section Width.

Local Open Scope Z.

Record width :=  { width_val :> Z ; width_prop : 2 <= width_val }.
Global Arguments width_prop : clear implicits.

Variable w: width.

Hint Resolve width_prop : zarith.


Lemma width_pos : 0 <= w.
Proof.
(* info_eauto with zarith. *)
simple eapply Z.le_trans; first simple apply Z.le_refl; simple eapply Z.le_trans;
  first simple apply Z.le_refl; first simple eapply Z.le_trans;
  first simple apply Z.le_refl; simple eapply Z.le_trans;
  first simple apply Pos2Z.is_nonneg; simple apply width_prop.
Qed.
Hint Resolve width_pos : zarith.


Lemma width_ge_1 : 0 <= w - 1.
Proof.
generalize (width_prop w); auto with zarith.
Qed.

Hint Resolve width_ge_1 : zarith.


Lemma pow2w_pos : 0 < 2^w.
Proof.
apply Z.pow_pos_nonneg; auto with zarith.
Qed.


Lemma Zpow_2_plus: 2^(w-1) + 2^(w-1) = 2^w.
Proof.
replace (width_val w) with (1+(w-1)) at 3 by ring.
rewrite Zpower_exp; auto with zarith.
apply Z.le_ge; auto with zarith.
Qed.


Lemma pow2w_neq0 : (2 ^ w <> 0)%Z.
Proof.
apply: Z.pow_nonzero.
  by lia.
exact: width_pos.
Qed.


End Width.

Global Arguments width_pos : clear implicits.
Hint Resolve width_pos width_ge_1 pow2w_pos pow2w_neq0 : zarith.



Lemma isFIX_overfWRAP (lsb : Z) (w : width) (x : R) :
  isFIX lsb x -> isFIX lsb (overfWRAP (w + lsb)%Z x).
Proof.
rewrite /isFIX -!eqmodr0.
apply: eqmodr_trans.
rewrite /overfWRAP.
apply: eqmodr_Rdivides; last first.
  apply: eqmodr_cmodr.
  1,3: by auto with RDb.
apply: Rdivides_bpow.
have w_pos:= (width_pos w).
by auto with zarith.
Qed.


Lemma IZR_Zpow2w (w : width) : IZR (2^w)%Z = (twopow w).
Proof.
rewrite bpow_IZR //.
exact: width_pos.
Qed.


Lemma roundFIX_quasiperR (w : width) (lsb : Z) rnd :
  quasiperZ (2^w)%Z rnd ->
  quasiperR (twopow (w + lsb)%Z) (roundFIX lsb rnd).
Proof.
rewrite /quasiperZ /quasiperR.
move=> Hrnd r.
rewrite /roundFIX /round /cexp /FIX_exp /F2R /= /scaled_mantissa /cexp.
rewrite Rmult_plus_distr_r -bpow_plus.
rewrite -Zplus_assoc; autorewrite with ZDb.
rewrite -IZR_Zpow2w Hrnd.
rewrite plus_IZR IZR_Zpow2w.
by rewrite Rmult_plus_distr_r -bpow_plus.
Qed.




Section FOW.


Variables (lsb : Z) (w : width).


Notation ep := (w + lsb)%Z. (* = msb + 1 *)
Notation p := (twopow ep).


Definition isFIXo (x : R) := (isFIX lsb x /\ In_cppe ep x).


Notation oW := (overfWRAP ep).


Section Var_rnd.


Variable rnd : R -> Z.


Definition roundFOW r := oW (roundFIX lsb rnd (oW r)).


Definition binopFOW op (r1 r2 : R) := roundFOW (op r1 r2).


Definition addFOW := binopFOW Rplus.
Definition mulFOW := binopFOW Rmult.


Hypothesis rnd_valid : Valid_rnd rnd.


Lemma isFIX_roundFOW (r : R) :
  isFIX lsb (roundFOW r).
Proof.
rewrite /roundFOW /oW.
apply: isFIX_overfWRAP.
exact: isFIX_roundFIX.
Qed.


Lemma In_cpp_roundFOW (r : R) :
  In_cppe ep (roundFOW r).
Proof.
exact: overfWRAP_In_cppe.
Qed.


Lemma isFIXo_roundFOW (r : R) :
  isFIXo (roundFOW r).
Proof.
split; [ exact: isFIX_roundFOW | exact: In_cpp_roundFOW ].
Qed.


Lemma isFIX_big_binopFOW op (s : seq R) :
  isFIX lsb (\big[binopFOW op / 0]_(r <- s) r).
Proof.
case: s.
- rewrite big_nil. exact: isFIX_0.
- move => r s. rewrite big_cons. exact: isFIX_roundFOW.
Qed.


Lemma In_cpp_big_binopFOW op (s : seq R) :
  In_cppe ep (\big[binopFOW op / 0]_(x <- s) x).
Proof.
case: s.
- rewrite big_nil. exact: In_cppe_0.
- move => r s. rewrite big_cons. exact: In_cpp_roundFOW.
Qed.



Section Periodic_rnd.


Let pZ := (2^w)%Z.

Context { preg_rnd : quasiperZ pZ rnd }.



Lemma periodic_roundFOW : periodic p roundFOW.
Proof.
rewrite /periodic => r.
rewrite /roundFOW.
by rewrite overfWRAP_periodic.
Qed.



Lemma eqmodr_roundFIXFOW (r : R) :
  eqmodr p (roundFIX lsb rnd r) (roundFOW r).
Proof.
apply: eqmodr_trans; last first.
  rewrite /roundFOW /oW /overfWRAP eqmodr_sym.
  apply: eqmodr_cmodr.
  by auto with RDb.
apply: eqmodr_quasiperR.
- by auto with RDb.
- exact: roundFIX_quasiperR.
rewrite eqmodr_sym.
apply: eqmodr_cmodr.
by auto with RDb.
Qed.


Lemma eqmodr_roundFOW_isFIX (f : R) :
  isFIX lsb f -> eqmodr p (roundFOW f) f.
Proof.
move => H. rewrite -{2}(@roundFIX_id lsb rnd _ f) //.
rewrite eqmodr_sym. exact: eqmodr_roundFIXFOW.
Qed.


Lemma eqmodr_addFOW_isFIX (f1 f2 : R) :
  isFIX lsb f1 -> isFIX lsb f2 ->
  eqmodr p (addFOW f1 f2) (f1 + f2).
Proof.
move => H1 H2.
apply: eqmodr_roundFOW_isFIX.
exact: isFIX_Rplus.
Qed.


Lemma eq_roundFOW_eqmodr (r r' : R) :
  eqmodr p r r' -> roundFOW r = roundFOW r'.
Proof.
apply: eq_periodic_eqmodr.
  by auto with RDb.
exact: periodic_roundFOW.
Qed.
Global Arguments eq_roundFOW_eqmodr [r] r'.

Lemma eq_addFOW_eqmodr (r1 r2 r1' r2' : R) :
  eqmodr p r1 r1' -> eqmodr p r2 r2' ->
  addFOW r1 r2 = addFOW r1' r2'.
Proof.
move => Heqm1 Heqm2.
apply: eq_roundFOW_eqmodr.
exact: eqmodr_add.
Qed.


Lemma eqmodr_roundFIXFOW_eqmodr (r r' : R) :
  eqmodr p r r' ->
  eqmodr p (roundFIX lsb rnd r) (roundFOW r').
Proof.
move=> Heqmodr.
rewrite (eq_roundFOW_eqmodr r); last by rewrite eqmodr_sym.
exact: eqmodr_roundFIXFOW.
Qed.


Lemma eqmodr_big_addFOW_addFIX (s : seq R) :
  eqmodr p (\big[addFOW / 0]_(x <- s) x)
           (\big[addFIX lsb rnd / 0]_(x <- s) x).
Proof.
elim: s.
- rewrite !big_nil.
  exact: eqmodr_refl.
- move => r s Hind.
  rewrite !big_cons eqmodr_sym.
  apply: eqmodr_roundFIXFOW_eqmodr.
  by rewrite eqmodr_add2l eqmodr_sym.
Qed.


Lemma Jackson_rule_seqR (s : seq R) :
  In_cppe ep (\big[addFIX lsb rnd / 0]_(x <- s) x) ->
  \big[addFOW / 0]_(x <- s) x =
  \big[addFIX lsb rnd / 0]_(x <- s) x.
Proof.
intro.
apply: eq_In_cpp_eqmodr.
- apply: In_cpp_big_binopFOW.
- assumption.
- exact: eqmodr_big_addFOW_addFIX.
Qed.


Lemma eq_big_addFOW_eqmodr (s t : seq R) :
  all2 (eqmodr p) s t ->
  \big[addFOW / 0]_(x <- s) x = \big[addFOW / 0]_(x <- t) x.
Proof.
move => Hall2.
have Hsize := (all2_same_size Hall2).
move: s t Hsize Hall2.
apply: seq_ind2.
  by intros; rewrite !big_nil //.
move => x y s t Hsize IH.
rewrite /all2 => /andP [Hxy Hst].
rewrite !big_cons.
apply: eq_addFOW_eqmodr => //.
rewrite IH //.
exact: eqmodr_refl.
Qed.


Lemma roundFOW_In_cppe_eqmodr (r x : R) :
  In_cppe ep x ->
  eqmodr p (roundFIX lsb rnd r) x ->
  roundFOW r = x.
Proof.
move => Hx Heqm.
apply: eq_In_cpp_eqmodr.
- exact: In_cpp_roundFOW.
- exact Hx.
- apply: eqmodr_trans; last first.
    exact Heqm.
  rewrite eqmodr_sym.
  exact: eqmodr_roundFIXFOW.
Qed.


End Periodic_rnd.


End Var_rnd.



Lemma isFIX_eqmodr (f f' : R) :
  eqmodr p f f' -> isFIX lsb f -> isFIX lsb f'.
Proof.
move => Heqm Hf.
apply: Rdivides_eqmodr; last first.
  exact Hf.
apply: eqmodr_Rdivides; last exact Heqm.
  exact: bpow_neq0.
rewrite bpow_plus [X in X*_]bpow_IZR. 2: exact: width_pos.
apply: Rdivides_mulIZR.
exact: bpow_neq0.
Qed.


Definition faithfulFOW (r x : R) :=
  x = roundFOW Zfloor r \/ x = roundFOW Zceil r.


Lemma diff_lt_faithfulFOW (r x : R) :
  isFIXo x ->
  (exists x', eqmodr p x x' /\ Rabs (x' - r) < twopow lsb) ->
  faithfulFOW r x.
Proof.
move => [HFIX HcR] [x' [Hmod Hlt]].
have Hx'FIX:= isFIX_eqmodr Hmod HFIX.
have Hx' := diff_lt_faithful Hx'FIX Hlt.
rewrite eqmodr_sym in Hmod.
case: Hx' => Hcase; [ left | right].
all: symmetry; apply: roundFOW_In_cppe_eqmodr => //.
- 2,4: by rewrite -Hcase.
- exact: quasiperZ_Zfloor.
- exact: quasiperZ_Zceil.
Qed.


End FOW.

Global Arguments eq_big_addFOW_eqmodr [lsb w rnd s] t.



Lemma mulFIX_isFIX_exact l1 l2 (x1 x2 : R) rnd :
  Valid_rnd rnd ->
  isFIX l1 x1 -> isFIX l2 x2 ->
  mulFIX (l1 + l2) rnd x1 x2 = x1 * x2.
Proof.
move=> *; apply: roundFIX_id.
exact: isFIX_Rmult.
Qed.




Lemma eqmodr_roundFIX_same_RlocalDN (w:width) lsb rnd rnd' r r' :
  Valid_rnd rnd -> Valid_rnd rnd' ->
  RlocalDN lsb rnd r = RlocalDN lsb rnd' r' ->
  eqmodr (twopow (w+lsb)) r r' ->
  eqmodr (twopow (w+lsb)) (roundFIX lsb rnd r) (roundFIX lsb rnd' r').
Proof with auto with zarith RDb.
move => Hrnd Hrnd' H_RlocalDN Heqm.
rewrite /roundFIX. rewrite /round /F2R /=.
rewrite bpow_plus. rewrite eqmodr_mul_r...
rewrite bpow_IZR...
rewrite -eqmodr_IZR...
apply: eqm_ZlocalDN... rewrite -!RlocalDN_ZlocalDN //.
rewrite eqmodr_scaled_mantissa -bpow_IZR...
rewrite -bpow_plus. exact: Heqm.
Qed.


Arguments isFIXo_roundFOW lsb w rnd {rnd_valid} r.

