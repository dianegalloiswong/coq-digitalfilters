(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import all_ssreflect ssralg.

From Flocq
Require Import Core Operations.

Require Import Lia Lra.

Require Import Rstruct lia_tactics utils
               Flocq_complements R_complements Z_complements seq_complements
               FIX SOP_model.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Local Open Scope R.




Section Kulisch.


Variables (ls lt : seq Z) (lf : Z).
Variables s t : seq R.
Notation n := (minn (size s) (size t)).


Hypothesis are_lsb_s : are_lsb ls s.
Hypothesis are_lsb_t : are_lsb lt t.


(** desired rounding mode for result **)
Variable rndf : R -> Z.


(** rounding mode for intermediary computations **)
Variable rndc : R -> Z.
Hypothesis rndc_valid : Valid_rnd rndc.


(** lprods_i := (ls_i + lt_i) is the LSB of product (s_i * t_i) **)
Definition lprods : seq Z := (map2 Zplus ls lt).


(** LSB used for intermediary computations:
    has to be smaller than all the LSB of the products
    (usually we simply choose their minimum            **)
Variable lmin : Z.
Hypothesis lmin_le : all (Z.leb lmin) lprods.


Definition prods_Kulisch : seq R :=
  map3 (fun l => mulFIX l rndc) lprods s t.

Definition SOP_Kulisch : R :=
  roundFIX lf rndf (\big[addFIX lmin rndc / 0]_(x <- prods_Kulisch) x).




Fact size_lprods : size lprods = n.
Proof.
by rewrite size_map2 (size_are_lsb are_lsb_s) (size_are_lsb are_lsb_t).
Qed.


Fact size_prods_Kulisch : size prods_Kulisch = n.
Proof.
by rewrite size_map3 size_lprods -minnA minnn.
Qed.


Lemma isFIX_prods_Kulisch : all (isFIX lmin) prods_Kulisch.
Proof.
apply/(all_nthP 0) => i.
rewrite size_map3 !leq_min.
simplP.
apply: isFIX_le; last first.
  rewrite (nth_map3 0%Z 0 0) => //.
  exact: isFIX_roundFIX.
by apply/ZleP/all_nthP => //.
Qed.


Lemma prods_exact : prods_Kulisch = prods_model s t.
Proof.
apply: (@eq_from_nth _ 0).
  by rewrite size_prods_Kulisch size_prods_model.
rewrite size_prods_Kulisch.
move=> i i_lt.
move: (i_lt); rewrite leq_min; simplP.
rewrite /prods_Kulisch /prods_model.
rewrite (nth_map2 0 0) //.
rewrite (nth_map3 0%Z 0 0) //; last first.
  by rewrite size_lprods.
rewrite (nth_map2 0%Z 0%Z); cycle 1.
- by rewrite (size_are_lsb are_lsb_s).
- by rewrite (size_are_lsb are_lsb_t).
apply: mulFIX_isFIX_exact.
- exact: are_lsb_nth.
- exact: are_lsb_nth.
Qed.


Theorem SOP_Kulisch_correct :
  SOP_Kulisch = roundFIX lf rndf (SOP_model s t).
Proof.
rewrite /SOP_Kulisch; f_equal.
rewrite /SOP_model -prods_exact.
have:= isFIX_prods_Kulisch => /all_filterP <-.
rewrite !big_filter.
apply: eq_big_op; cycle 2.
- exact: addFIX_isFIX_exact.
- by [].
- exact: isFIX_0.
- by move=> *; exact: isFIX_roundFIX.
Qed.

End Kulisch.

