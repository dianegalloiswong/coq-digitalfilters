(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import ssreflect ssrbool ssrfun ssrnat seq bigop path eqtype ssralg.

From Flocq
Require Import Core.

Require Import lia_tactics Rstruct
               utils nat_ord_complements
               Z_complements R_complements seq_complements
               FIX odd_rounding
               SOP_model summation_decreasinglsb.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Local Open Scope ring_scope.
Local Open Scope Z.





Section Any_lsb.


Variable ps : seq R.
Variable ls : seq Z.
Variable lf : Z.


Hypothesis ps_nonempty : ps <> [::].
Hypothesis are_lsb_ls_ps : are_lsb ls ps.


Notation n := (size ps).


Fixpoint ki (i : nat) :=
match i with
  | O => lf - 2
  | S p => Z.min (ki p) (ls ` (n.-1 - p)%nat) - 1
end.


Fixpoint ti (i : nat) :=
match i with
  | O => # (ki n.-1) (ps ` 0)
  | S p => add# (ki (n.-2 - p)%nat) (ti p) (ps ` p.+1)
end.


Definition summation_odd_any_lsb : R := ti n.-1.




Fact ki_S i : ki i.+1 = Z.min (ki i) (ls ` (n.-1 - i)%nat) - 1.
Proof.
by [].
Qed.


Lemma t_correct (i :nat) :
  (i < n)%nat ->
  ti i = # (ki (n.-1 - i)%nat) (\sum_(x <- take i.+1 ps) x).
Proof.
move: i. apply: nat_ind.
  move=> n_gt0.
  rewrite /ti subn0.
  f_equal.
  erewrite take_nth.
    by rewrite take0 rconsnil big_seq1.
  by assumption.
move=> i IH i_lt /=.
rewrite (take_nth 0%R) // big_rcons.
replace (@Monoid.operator _ _ _) with Rplus by reflexivity.
rewrite IH; last ssrnatlia.
rewrite (@add_odd_odd _ _ _ _ (ls ` i.+1)); last first.
- exact: are_lsb_nth.
1,2: replace (_ - i)%nat with ((n.-2 - i).+1) by ssrnatlia.
1,2: rewrite ki_S.
1,2: replace (n.-1 - _)%nat with i.+1 by ssrnatlia.
1,2: set (p := (ki _)); set q := (ls ` _).
have := Z.le_min_r p q.
2: have := Z.le_min_l p q.
1,2: by ssrnatlia.
rewrite /addFIX /binopFIX. f_equal.
  by f_equal; ssrnatlia.
by rewrite Rplus_comm.
Qed.


Theorem summation_odd_any_lsb_correct :
  summation_odd_any_lsb = # (lf - 2) (\sum_(x <- ps) x).
Proof.
have n_gt0 : (0 < n)%nat by exact: size_gt0.
rewrite /summation_odd_any_lsb t_correct; last ssrnatlia.
rewrite subnn. f_equal.
by rewrite prednK // take_size //.
Qed.


End Any_lsb.










(** For inputs whose lsb are < lf-2.
    Lsbs need to be sorted and without repetition **)
Section Small_lsb.


Variable ps : seq R.
Variable ls : seq Z.
Variable lf : Z.

Notation n := (size ps).

Hypothesis are_lsb_ls_ps : are_lsb ls ps.

Hypothesis ls_sorted_lt :
  forall i j, (i < j < size ls)%nat -> ls ` i < ls ` j.

Hypothesis lastl_le : ls ` n.-1 <= lf - 2.

Hypothesis two_elems : (1 < n)%nat.


(** si i is the intermediate result for 0 <= i <= n-2 *)
Fixpoint si (i : nat) :=
match i with
  | O => # (ls ` 1 - 1) (ps ` 0)
  | S p => add# (ls ` p.+2 - 1) (si p) (ps ` p.+1)
end.


Definition summation_odd_small_lsb : R := add# (lf - 2) (si n.-2) (ps ` n.-1).



Fact si_S i : si i.+1 = add# (ls ` i.+2 - 1) (si i) (ps ` i.+1).
Proof.
by [].
Qed.


Lemma s_correct (i :nat) :
  (i < n.-1)%nat ->
  si i = # (ls ` i.+1 - 1) (\sum_(x <- take i.+1 ps) x).
Proof.
move: i. apply: nat_ind.
  move=> Pn_gt0.
  rewrite /si. f_equal.
  have n_gt0 : (0 < n)%nat by ssrnatlia.
  by rewrite (take_nth 0%R) // take0 rconsnil big_seq1.
move=> i IH Si_ltPn.
have Si_ltn : (i.+1 < n)%nat by ssrnatlia.
rewrite  (take_nth 0%R) // big_rcons.
replace (@Monoid.operator _ _ _) with Rplus by easy.
rewrite si_S IH; last ssrnatlia.
rewrite (@add_odd_odd _ _ _ _ (ls ` i.+1)); last first.
- exact: are_lsb_nth.
- rewrite -Z.sub_lt_mono_r.
  apply: ls_sorted_lt.
  rewrite (all2_same_size are_lsb_ls_ps).
  by apply/andP; ssrnatlia.
- by intlia.
rewrite /addFIX /binopFIX. f_equal.
by rewrite Rplus_comm.
Qed.


Theorem summation_odd_small_lsb_correct :
  summation_odd_small_lsb = # (lf - 2) (\sum_(x <- ps) x).
Proof.
rewrite /summation_odd_small_lsb s_correct; last ssrnatlia.
erewrite (@add_odd_odd _ _ _ _ (ls ` n.-1)); last first.
- by apply: are_lsb_nth => //; ssrnatlia.
all: rewrite prednK; last ssrnatlia.
- by have := lastl_le; intlia.
- by intlia.
rewrite /addFIX /binopFIX. f_equal.
rewrite -[ps in RHS]take_size.
rewrite -[n in RHS]prednK; last ssrnatlia.
rewrite (take_nth 0%R); last ssrnatlia.
by rewrite big_rcons Rplus_comm.
Qed.


End Small_lsb.




Section Full_algorithm.


Variable choice : Z -> bool.
Let rN := Znearest choice.


Variable ps_input : seq R.
Variable ls_input : seq Z.
Variable lf : Z.

Hypothesis are_lsb_ls_ps_input : are_lsb ls_input ps_input.
Hypothesis ls_input_srt : sorted Z.leb ls_input.



(** preprocessing **)


Definition merge_heads_x ps l :=
match ps with
  | x0 :: x1 :: tx => add# l x0 x1 :: tx
  | _ => ps
end.


Fixpoint merge_same_lsb (ps : seq R) (ls : seq Z) :=
match ps, ls with
  | x0 :: tx , l0 :: ((l1 :: _) as tl) =>
    if l0 == l1 then
      merge_same_lsb (merge_heads_x ps l0) tl
    else
      (x0 :: (merge_same_lsb tx tl).1 , l0 :: (merge_same_lsb tx tl).2)
  | _ , _ => (ps, ls)
end.


Let ps_pp := (merge_same_lsb ps_input ls_input).1.
Let ls_pp := (merge_same_lsb ps_input ls_input).2.



(** split into small lsbs and large lsbs **)


Let m := find (fun l => Z.ltb (lf - 2) l) ls_pp.
Let ps_small := take m ps_pp.
Let ls_small := take m ls_pp.
Let ps_large := drop m ps_pp.
Let ls_large := drop m ls_pp.



(** computation **)


Definition summation_odd :=
roundFIX lf rN (addFIX (lf - 2) Zfloor
                 (summation_odd_small_lsb ps_small ls_small lf)
                 (summation_decreasinglsb ls_large ps_large)).




Hypothesis m_gt : (1 < m)%nat.
Hypothesis m_lt : (m < size ps_pp)%nat.
Hypothesis ps_small_several_elt : (1 < size ps_small)%nat.
Hypothesis ps_large_nonempty : ps_large <> [::].
Hypothesis also_large_nonempty : has (Z.ltb (lf - 2)) ls_pp.

Hypothesis m_gt0 : (0 < m)%nat.
Hypothesis ps_size_gt0 : (0 < size ls_large)%nat.





(** proofs about preprocessing **)


Definition invar_xl Sum ps ls :=
  are_lsb ls ps /\ sorted Z.leb ls /\ \sum_(x <- ps) x = Sum.


Lemma invar_xl_cons Sum Sum' x0 tx l0 tl :
  (Sum' = x0 + Sum)%R -> isFIX l0 x0 -> (tl <> [::] -> l0 <= tl `0) ->
  invar_xl Sum tx tl ->
  invar_xl Sum' (x0 :: tx) (l0 :: tl).
Proof.
rewrite /invar_xl.
move=> -> H0 H01 [Hlsb [Hsrt Hsum]].
repeat split.
- by rewrite all2_consP.
- clear Hlsb Hsum. move: tl H01 Hsrt.
  case => // l1 ttl.
  simpl nth.
  rewrite sorted_Zleb_cons2P.
  move=> H01 Hsrt.
  split => //.
  exact: H01.
- by rewrite big_cons Hsum.
Qed.


Lemma merge_heads_invar_xl ps l1 ttl Sum :
  invar_xl Sum ps (l1 :: l1 :: ttl) ->
  invar_xl Sum (merge_heads_x ps l1) (l1 :: ttl).
Proof.
case: ps; [ | move=> x0; case; [ | move=> x1 tx ] ] => //.
  1,2: by move=> /proj1 /all2_same_size.
rewrite /invar_xl !are_lsb_consP sorted_Zleb_cons2P.
move=> [[H0] [H1 Hlsb]] [[_ Hsrt] Hsum].
repeat split => //.
  exact: isFIX_roundFIX.
move: Hsum.
rewrite !big_cons => <-.
rewrite addFIX_isFIX_exact //.
rewrite -![@GRing.add _]/Rplus //=.
by ring.
Qed.


Lemma merge_same_lsb_unfold1 x0 tx l0 l1 ttl :
  merge_same_lsb (x0 :: tx) (l0 :: l1 :: ttl)
= if l0 == l1 then
    merge_same_lsb (merge_heads_x (x0 :: tx) l0) (l1 :: ttl)
  else
    (x0 :: (merge_same_lsb tx (l1 :: ttl)).1 , l0 :: (merge_same_lsb tx (l1 :: ttl)).2).
Proof.
by [].
Qed.


Lemma merge_same_lsb_snd0 ps l0 tl :
  (merge_same_lsb ps (l0 :: tl)).2 ` 0 = l0.
Proof.
move: ps l0.
elim: tl => //.
  by case.
move=> l1 ttl IH ps l0.
case: ps => // x0 tx.
rewrite merge_same_lsb_unfold1.
case_eq (l0 == l1) => // /eqP <-.
exact: IH.
Qed.


Lemma merge_same_lsb_sndNnil ps l0 tl :
  (merge_same_lsb ps (l0 :: tl)).2 <> [::].
Proof.
move: ps l0.
elim: tl => //.
  by case.
move=> l1 ttl IH ps l0.
case: ps => // x0 tx.
rewrite merge_same_lsb_unfold1.
by case: (l0 == l1).
Qed.


Lemma merge_same_lsb_invar_xl_srtZlt ps ls Sum :
  invar_xl Sum ps ls ->
  invar_xl Sum (merge_same_lsb ps ls).1 (merge_same_lsb ps ls).2
    /\ sorted Z.ltb (merge_same_lsb ps ls).2.
Proof.
move: ps Sum.
elim: ls => //.
  by case.
move=> l0 tl IH ps Sum.
case: ps.
  by rewrite /invar_xl => [[/all2_same_size]].
move=> x0 tx.
case: tl IH => // l1 ttl IH.
rewrite merge_same_lsb_unfold1.
case_eq (l0 == l1) => [/eqP -> | /eqP Hneq ].
  intro; apply: IH.
  exact: merge_heads_invar_xl.
rewrite {1}/invar_xl !are_lsb_consP sorted_Zleb_cons2P.
move=> [[HF0 HF1] [[Hle Hsrt] Hsum]].
split.
  apply: invar_xl_cons => //; cycle 1.
  - by rewrite merge_same_lsb_snd0.
  - exact: (proj1 (IH _ _ _)).
  - by move: Hsum; rewrite big_cons.
rewrite snd_pair.
have not_nil := merge_same_lsb_sndNnil (* tx l1 ttl *).
rewrite -[(merge_same_lsb _ _).2](cons_nth0_behead 0) //.
apply: (@sorted_cons _ 0).
rewrite cons_nth0_behead //.
split.
  apply/ZltP.
  rewrite merge_same_lsb_snd0.
  by auto with zarith.
exact: (proj2 (IH _ _ _)).
Qed.



(** what it means for ps_pp and ls_pp **)


Lemma invar_xl_pp :
  invar_xl (\sum_(x <- ps_input) x) ps_pp ls_pp.
Proof.
exact: proj1 (merge_same_lsb_invar_xl_srtZlt _).
Qed.


Lemma are_lsb_pp : are_lsb ls_pp ps_pp.
Proof.
have [//] := invar_xl_pp.
Qed.


Lemma sum_pp :
  \sum_(x <- ps_pp) x = \sum_(x <- ps_input) x.
Proof.
have [_ [//]] := invar_xl_pp.
Qed.


Lemma eq_size_pp :
size ps_pp = size ls_pp.
Proof.
symmetry.
apply: all2_same_size.
exact: are_lsb_pp.
Qed.


Fact ls_pp_sorted_lt : sorted Z.ltb ls_pp.
Proof.
exact: proj2 (merge_same_lsb_invar_xl_srtZlt _).
Qed.



(** proofs about small / large lsbs **)


Lemma are_lsb_small : are_lsb ls_small ps_small.
Proof.
rewrite /ls_small /ps_small !take_mask eq_size_pp.
apply: all2_mask.
exact: are_lsb_pp.
Qed.


Lemma are_lsb_large : are_lsb ls_large ps_large.
Proof.
rewrite /ls_large /ps_large !drop_mask eq_size_pp.
apply: all2_mask.
exact: are_lsb_pp.
Qed.


Lemma size_ps_small : size ps_small = m.
Proof.
by rewrite size_take ifT.
Qed.


Lemma size_ls_small : size ls_small = m.
Proof.
by rewrite size_take ifT // -eq_size_pp.
Qed.


Lemma size_ls_pp : size ls_pp = (m + size ls_large)%nat.
Proof.
by rewrite -(cat_take_drop m ls_pp) size_cat size_ls_small.
Qed.


Lemma ls_small_sorted_lt i j :
  (i < j < size ls_small)%nat -> ls_small ` i < ls_small ` j.
Proof.
rewrite size_ls_small.
move=> /andP Hij.
rewrite !nth_take; try ssrnatlia.
apply: Zlt_sorted.
apply: ls_pp_sorted_lt.
apply/andP.
rewrite size_ls_pp.
by ssrnatlia.
Qed.



Lemma drop_sorted (T : eqType) (leT : rel T) (s : seq T) (i : nat) :
  transitive leT -> sorted leT s -> sorted leT (drop i s).
Proof.
move=> ?.
apply: subseq_sorted => //.
exact: drop_subseq.
Qed.



Lemma Zle_sorted_le (s : seq Z) (i j : nat) :
  sorted Z.leb s -> (i <= j < size s)%nat -> s ` i <= s ` j.
Proof.
case_eq (i == j) => /eqP.
  by move=> -> *; auto with zarith.
move=> *.
exact: Zle_nth_sorted.
Qed.


Lemma ls_large_sorted : sorted Z.leb ls_large.
Proof.
apply: sorted_Zltle.
rewrite /ls_large.
apply: drop_sorted.
  exact: Zltb_trans.
exact: ls_pp_sorted_lt.
Qed.


Lemma ls_small_le i :
(i < size ps_small)%nat -> ls_small ` i <= lf - 2.
Proof.
rewrite size_ps_small => i_lt.
rewrite nth_take //.
apply Z.ltb_ge.
exact: before_find.
Qed.


Lemma ls_large0_gt : lf - 2 < ls_large ` 0.
Proof.
rewrite nth_drop addn0.
apply/ZltP.
apply: nth_find.
exact: also_large_nonempty.
Qed.


Lemma ls_large_gt i :
(i < size ls_large)%nat -> lf - 2 < ls_large ` i.
Proof.
move=> i_lt.
apply: Z.lt_le_trans.
  exact: ls_large0_gt.
apply: Zle_sorted_le.
  exact: ls_large_sorted.
by apply/andP.
Qed.



(** correctness **)


Theorem summation_odd_correct :
  summation_odd = roundFIX lf rN (\sum_(x <- ps_input) x).
Proof.
rewrite /summation_odd.
rewrite /addFIX. rewrite /binopFIX.
rewrite summation_odd_small_lsb_correct //; first 1 last.
- exact: are_lsb_small.
- exact: ls_small_sorted_lt.
- apply: ls_small_le.
  by ssrnatlia.
rewrite add_rodd_even; last first.
  apply: isFIX_le; last first.
    apply: isFIX_summation_decreasinglsb => //.
    exact: are_lsb_large.
  have:= (ls_large_gt ps_size_gt0).
  by intlia.
rewrite summation_decreasinglsb_exact //; first 1 last.
- exact: are_lsb_large.
- exact: ls_large_sorted.
rewrite (@roundFIX_id (lf - 2)); last exact: isFIX_roundFIX.
rewrite round_N_odd_FIX; last by intlia.
by rewrite -big_cat cat_take_drop sum_pp.
Qed.


End Full_algorithm.

