(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



(** Definition of a discrete signal, operations on signals,
    set of signals as a monoid, Z-module and module on a ring,
    recursive construction of a signal,
    scalar/vector/matrix signal,
    Dirac impulse signal, convolution, signals as a commutative ring,
    decomposition of vector signal as scalar signal
**)

From mathcomp
Require Import ssreflect ssrbool ssrfun eqtype choice ssrnat bigop fintype
               ssralg vector ssrint ssrnum matrix.

(** For signal extensionality **)
Require Import FunctionalExtensionality ProofIrrelevance.

(** For LPO for decidability of signal equality **)
Require Import Coquelicot.Markov.

(** For signal as a choiceType.
    As shown in Epsilon_instances.v, this can be proven
    using axiom Epsilon, but we rather declare it as its
    own axiom since it is weaker. **)
Require Import choiceType_from_Epsilon Epsilon_instances.

Require Import lia_tactics
               utils nat_ord_complements int_complements
               sum_complements mx_complements.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Import GRing.Theory.


Local Open Scope ring_scope.


Implicit Types (k K : int) (n : nat).




(** Definition of a discrete signal, operations on signals,
    set of signals as a monoid, Z-module and RT-module,
    recursive construction of a signal  **)
Section Context_VT.


Context {RT : comRingType} {VT : vectType RT}.


Definition causal (x : int -> VT) :=
  forall k : int, k < 0 -> x k = 0.


Record signal := { signal_fun :> int -> VT ;
                   signal_causal : causal signal_fun }.


Implicit Types x y : signal.


Lemma signalP (x1 x2 : signal) : x1 =1 x2 <-> x1 = x2.
Proof.
split; last first.
  by move=> ->.
move: x1 x2 => [f1 H1] [f2 H2] /=.
move=> /functional_extensionality eqf.
move: H1.
rewrite eqf.
intro; f_equal.
exact: proof_irrelevance.
Qed.


Lemma signalP_ge0 (x1 x2 : signal) :
  (forall k : int, k >= 0 -> x1 k = x2 k) <-> x1 = x2.
Proof.
split; last first.
  by move=> ->.
move => eq_ge0.
apply/signalP => k.
case: (Num.Theory.lerP 0 k).
- exact: eq_ge0.
- by intro; rewrite !signal_causal.
Qed.


Lemma signalE (f : int -> VT) (cf : causal f) :
  signal_fun (Build_signal cf) =1 f.
Proof.
by [].
Qed.


Definition signal0 := Build_signal (fun _ _ => erefl 0).


Lemma neqsignal (x1 x2 : signal) :
  (exists k, x1 k <> x2 k) -> x1 <> x2.
Proof.
move=> [k neqk] eqx.
contradict neqk.
by rewrite eqx.
Qed.


(** Most general way to build a signal from a function int -> VT:
    set image of all k < 0 to 0 **)
Fact signal_of_fun_causal (f : int -> VT) :
  causal (fun k => if k < 0 then 0 else f k).
Proof.
by move => k ->.
Qed.
Definition signal_of_fun (f : int -> VT) :=
  Build_signal (signal_of_fun_causal f).






(** signal as eqType **)


(** forall x y : signal, {x = y} + {x <> y} **)
Lemma signal_comparable : comparable signal.
Proof.
move=> x y.
pose P (n:nat) := x (Posz n) <> y (Posz n).
have := LPO P.
move=> [n|Hex|Hnot].
- subst P => /=.
  by case_eq (x n == y n) => /eqP; [right|left] => //.
- move: Hex => [n Hn].
  right.
  apply/signalP => H.
  by have:= H n.
- left.
  apply/signalP_ge0 => k.
  case: k => // n _.
  have := Hnot n.
  by case_eq (x n == y n) => /eqP //.
Qed.


Definition signal_eqMixin := comparableMixin signal_comparable.

Canonical signal_eqType := EqType signal signal_eqMixin.





(** signal as choiceType **)


Let fnv := (nat -> VT).

Definition to_fnv : signal -> fnv :=
  fun x n => x n.

Definition of_fnv : fnv -> signal := signal_of_fun.

Fact to_fnv_K : cancel to_fnv of_fnv.
Proof.
move=> x.
apply/signalP_ge0 => k k_ge0 /=.
rewrite ifF; last by autoz.
by rewrite /to_fnv Posz_nat_of_int.
Qed.

(** (unused) **)
Fact of_fnv_K : cancel of_fnv to_fnv.
Proof.
move=> f.
exact: functional_extensionality => n.
Qed.


Lemma signal_epsilon_statement : 
  forall P : signal -> Prop,
  {x : signal | (exists x0, P x0) -> P x}.
Proof.
move=> P.
have [f f_prop] := (fnv_epsilon_statement (fun f => P (of_fnv f))).
apply: exist.
move=> [x Px].
apply: f_prop.
exists (to_fnv x).
by rewrite to_fnv_K.
Qed.


Definition signal_choiceMixin :=
  EpsilonChoiceMixin signal_epsilon_statement.

Canonical signal_choiceType := ChoiceType signal signal_choiceMixin.








(** Three elementary operations for signal processing:
    addition, scaling (multiplication by a scalar), and delay **)


(** addition of signals **)
Fact adds_causal (x1 x2 : signal) :
  causal (fun k => x1 k + x2 k).
Proof.
move => k Hk /=.
by rewrite !signal_causal // add0r.
Qed.
Definition adds (x1 x2 : signal) :=
  Build_signal (adds_causal x1 x2).


(** scaling of a signal (multiplication by a scalar) **)
Fact scales_causal (c : RT) (x : signal) :
  causal (fun k => c *: x k).
Proof.
move => k Hk.
rewrite !signal_causal // GRing.scaler0.
by [].
Qed.
Definition scales (c : RT) (x : signal) :=
  Build_signal (scales_causal c x).


(** Delay of signal x by K >= 0 : signal k => x(k-K).
    For negative K, arbitrarily returns the null signal. **)
Fact delay_causal (K : int) (x : signal) :
  causal (fun k => if K >= 0 then x (k-K) else 0).
Proof.
move => k Hk.
case: ifP => // HK.
apply: signal_causal.
by intlia.
Qed.
Definition delay (K : int) (x : signal) :=
  Build_signal (delay_causal K x).


Lemma delayE (K : int) (K_ge0 : K >= 0) (x : signal) :
  delay K x =1 fun k => x (k - K).
Proof.
move => k /=.
case: Num.Theory.lerP => // ?.
by intlia.
Qed.


(** For k <= 0, an antidelay of k corresponds to a delay of (-k).
    For k > 0, our definition of delay should not be applied to -k < 0,
    however it would intuitively mean advancing the signal instead of
    delaying it.
    Here, there is no requirement on the sign of k.
    However, we do require that the signal argument x is null before k.
    Note that this condition is true of any signal x when k <= 0.
**)
Fact antidelay_causal K (x : signal) :
  (forall k, (k < K) -> x k = 0) ->
  causal (fun k => x (k + K)).
Proof.
move => x_Kcausal k k_lt0.
apply: x_Kcausal.
by intlia.
Qed.
Definition antidelay
  K (x : signal) (x_Kcausal : forall k, k < K -> x k = 0) :=
  Build_signal (antidelay_causal x_Kcausal).


Lemma delay_antidelay K (x : signal) (K_ge0 : K >= 0)
                        (x_Kcausal : forall k, k < K -> x k = 0) :
  delay K (antidelay x_Kcausal) = x.
Proof.
apply/signalP => k.
rewrite delayE //=.
f_equal.
by ringz.
Qed.




(** signal as zmodType **)


Fact addsA : associative adds.
Proof.
move => *; apply/signalP => k /=.
by rewrite addrA.
Qed.

Fact addsC : commutative adds.
Proof.
move => *; apply/signalP => k /=.
by rewrite addrC.
Qed.

Fact add0s : left_id signal0 adds.
Proof.
move => *; apply/signalP => k /=.
by rewrite add0r.
Qed.

Fact opps_causal (x : signal) :
  causal (fun k => - x k).
Proof.
move => k k_lt0.
by rewrite !signal_causal // GRing.oppr0.
Qed.
Definition opps (x : signal) := Build_signal (opps_causal x).

Fact addNs : left_inverse signal0 opps adds.
Proof.
move => *; apply/signalP => k /=.
exact: addNr.
Qed.


Definition signal_zmodMixin := ZmodMixin addsA addsC add0s addNs.

Canonical signal_zmodType := ZmodType signal signal_zmodMixin.


Lemma sumsE n (xs : 'I_n -> signal) :
  \sum_(i < n) xs i =1 fun k => \sum_(i < n) xs i k.
Proof.
move=> k.
move: n xs; apply: nat_ind.
  by intros; rewrite !big_ord0.
move=> n IH xs.
by rewrite !big_ord_recr /= IH.
Qed.




(** signal as lmodType **)


Fact scalesA a b x : scales a (scales b x) = scales (a * b) x.
Proof.
apply/signalP => k.
exact: scalerA.
Qed.

Fact scale1s : left_id 1 scales.
Proof.
move => *; apply/signalP => k /=.
exact: scale1r.
Qed.

Fact scalesDr : right_distributive scales adds.
Proof.
move => *; apply/signalP => k /=.
exact: scalerDr.
Qed.

Fact scalesDl x : {morph (scales^~ x) : a b / (a + b)}.
Proof.
move => *; apply/signalP => k /=.
exact: scalerDl.
Qed.


Definition signal_lmodMixin := LmodMixin scalesA scale1s scalesDr scalesDl.

Canonical signal_lmodType := LmodType RT signal signal_lmodMixin.









(** Recursive construction of a signal using... **)


(** ...usual Peano recursion:
      x(k)   = 0              for k <= 0
      x(k+1) = f_IS (k) (x(k))   for k >= 0
    given f_IS : int -> VT -> VT.
    x(0) could be initialized to a given value rather than 0,
    but in practice every signal we will need to build this way
    verifies x(0) = 0 so we decide to make the definition simpler. **)


Fact signal_peanorec_causal (f_IS : int -> VT -> VT) :
  causal (peanorecz (fun _ => (0:VT)) f_IS).
Proof.
move => k k_lt0.
rewrite peanoreczBC //.
by intlia.
Qed.
Definition signal_peanorec (f_IS : int -> VT -> VT) :=
  Build_signal (signal_peanorec_causal f_IS).


Lemma signal_peanorecBC (f_IS : int -> VT -> VT) (k : int) :
  k <= 0 -> signal_peanorec f_IS k = 0.
Proof.
move=> k_le0 /=.
by rewrite peanoreczBC.
Qed.


Lemma signal_peanorecIS (f_IS : int -> VT -> VT) (k : int) :
  0 <= k ->
  signal_peanorec f_IS (k+1) = f_IS k (signal_peanorec f_IS k).
Proof.
by intro; rewrite signalE peanoreczIS.
Qed.




(** ...strong recursion:
      x(k) = 0         for k < 0
      x(k) = f_IS k f'   for k >= 0 where f' verifies:
                                     forall i < k, f'(i) = x(i)
    given f_IS : int -> (int -> VT) -> VT                        **)


Fact signal_strongrec_causal (f_IS : int -> (int -> VT) -> VT) :
  causal (strongrecz (fun _ => 0) f_IS).
Proof.
move=> k k_lt0.
by rewrite strongreczBC.
Qed.
Definition signal_strongrec (f_IS : int -> (int -> VT) -> VT) : signal :=
  Build_signal (signal_strongrec_causal f_IS).


Lemma signal_strongrecIS f_IS :
  (strongrecz_safe f_IS) ->
  forall k, k >= 0 ->
  signal_strongrec f_IS k = f_IS k (signal_strongrec f_IS).
Proof.
exact: strongreczIS.
Qed.


(** If f_IS is both safe for strong recursion and causal (provided
    the relevant values of f' are already 0),
    we can extend the lemma signal_strongrecIS to negative k. **)


Definition signal_strongrec_safe_and_causal
  (f_IS : int -> (int -> VT) -> VT) := 
   strongrecz_safe f_IS /\
   forall k (f' : int -> VT), k < 0 ->
   (forall (i : int), i < k -> f' i = 0) ->
     f_IS k f' = 0.


Lemma signal_strongrec_k f_IS k :
  signal_strongrec_safe_and_causal f_IS ->
  signal_strongrec f_IS k = f_IS k (signal_strongrec f_IS).
Proof.
move => [Hwf H_lt0].
have[Hk|Hk]: (k < 0 \/ 0 <= k) by intlia.
- rewrite H_lt0 //.
    exact: signal_causal.
  intros; apply: signal_causal.
  by intlia.
- exact: signal_strongrecIS.
Qed.


Lemma signal_strongrec_x_k (x : signal) f_IS k :
  (signal_strongrec_safe_and_causal f_IS) ->
  (forall m, x m = f_IS m x) ->
  x k = signal_strongrec f_IS k.
Proof.
move=> Hwf Hx_f_IS.
move: k; apply strongindz => k Hk.
  by rewrite !signal_causal.
move=> IH; rewrite Hx_f_IS signal_strongrec_k //.
by apply Hwf.
Qed.




End Context_VT.






(** about scalar and vector signals **)
Section Context_RT.


Context {RT : comRingType}.


Definition scsignal := @signal RT (regular_vectType RT).

Definition vsignal (n : nat) := @signal RT (matrix_vectType RT n 1).

Definition msignal (h w : nat) := @signal RT (matrix_vectType RT h w).





(** about scalar signals **)



(** about Dirac impulse **)


Fact dirac_causal :
  causal (fun k => if k is 0 then (1 : regular_vectType RT) else 0).
Proof.
by case.
Qed.
Definition dirac : scsignal := Build_signal dirac_causal.


Lemma dirac0 : dirac 0 = 1.
Proof.
by [].
Qed.


Lemma diracNot0 k : k <> 0 -> dirac k = 0.
Proof.
case: k => //.
by case.
Qed.


(** unused **)
Lemma dirac_even k : dirac (-k) = dirac k.
Proof.
case: k => //=.
by case.
Qed.


(** unused **)
Lemma dirac_delay_exchange k l : k >= 0 -> l >= 0 ->
  delay k dirac l = delay l dirac k.
Proof.
intros; rewrite !delayE // -dirac_even.
f_equal.
by ringz.
Qed.


Lemma dirac_delay_arg_eq k : k >= 0 -> delay k dirac k = 1.
Proof.
intros; rewrite delayE // -dirac0.
f_equal.
by ringz.
Qed.


Lemma dirac_delay_arg_neq k l : k >= 0 -> l >= 0 ->
  k <> l -> delay k dirac l = 0.
Proof.
intros; rewrite !delayE //.
apply: diracNot0.
by intlia.
Qed.


Lemma signal_sum_dirac (x : scsignal) (k m : int) :
  0 <= m <= k ->
  x m = (\sum_(i < (k + 1)%int) (x i : RT) *: delay i dirac) m.
Proof.
move=> m_bnd.
rewrite sumsE big_ord_to_nat.
  by autonz.
move => k_gt.
rewrite (sum_nat_singlenon0 m); swap 1 2.
- by autonz.
- rewrite nat_of_ord_of_nat; last first.
    by autonz.
  rewrite /= Posz_nat_of_int; last by autoz.
  rewrite subrr /=.
  by simplr.
move => i /andP _Hi _Hneq.
rewrite signalE delayE // diracNot0; last first.
  by autoz.
exact: scaler0.
Qed.





(** convolution product of scalar signals **)
Fact convols_causal (x y : scsignal) :
  causal (fun k : int => \sum_(0 <= i < (k + 1)%int) x i * y (k-(i:int))).
Proof.
move=> k k_lt0.
apply: big_geq.
rewrite nat_of_int_eq0 //.
by intlia.
Qed.
Definition convols (x y : scsignal) : scsignal :=
Build_signal (convols_causal x y).



(** scsignal as a commutative ring **)


Fact convolsA : associative convols.
Proof.
move=> x1 x2 x3.
apply/signalP_ge0 => k k_ge0 /=.
erewrite eq_big_nat; last first.
  move=> i i_bnd.
  rewrite mulr_sumr (big_nat_shiftUp i).
  uring_simplify (_ + _)%nat.
  ringz_simplify (_ + _)%nat.
  by [].
simpl.
rewrite exchange_big_nat_le /=.
apply: big_nat_congrF => j j_bnd _.
rewrite mulr_suml.
apply: congr_big_nat => //; first by auton.
move=> i i_bnd.
simplz.
ringz_simplify (_ - _ - _).
exact: mulrA.
Qed.


Fact convolsC : commutative convols.
Proof.
move=> x y.
apply/signalP => k /=.
rewrite sum_nat_rev.
apply: eq_big_nat => i /andP[Hi1 Hi2].
rewrite mulrC.
f_equal; f_equal.
all: by autonz.
Qed.


Fact convol1s s : convols dirac s = s.
Proof.
apply/signalP_ge0 => k k_ge0.
rewrite signalE.
rewrite (sum_nat_singlenon0 0).
- rewrite dirac0.
  by simplr.
- by autonz.
move=> i i_bnd i_neq0.
rewrite diracNot0; last by autonz.
by simplr.
Qed.


Fact convolsDl : left_distributive convols adds.
Proof.
move=> x1 x2 x3.
apply/signalP => k /=.
rewrite -sum_nat_add.
apply: big_nat_congrF => i _ _.
exact: mulrDl.
Qed.


Fact nonzero1s : dirac != 0.
Proof.
simplP.
apply: neqsignal.
exists 0; simpl.
apply/eqP.
exact: oner_neq0.
Qed.


Definition signal_ringMixin :=
  ComRingMixin convolsA convolsC convol1s convolsDl nonzero1s.

Canonical signal_ringType := RingType signal signal_ringMixin.

Canonical signal_comRingType := ComRingType signal convolsC.







(** about vector signals **)


(** column block concatenation of two vector signals **)
Section Col_signal.


Context { n1 n2 : nat }.


Lemma col_signal_causal (x1 : vsignal n1) (x2 : vsignal n2) :
  causal (fun k => col_mx (x1 k) (x2 k)).
Proof.
intros k _Hk; rewrite !signal_causal //. exact: col_mx0.
Qed.


Definition col_signal x1 x2 : vsignal (n1+n2)%nat :=
  Build_signal (col_signal_causal x1 x2).


End Col_signal.




(** signal of subvectors **)
Section Submx.


Context { n1 n2 : nat }.


Fact usubsignal_causal (x : vsignal (n1+n2)) :
  causal (fun k => usubmx (x k)).
Proof.
move => k _Hk.
rewrite signal_causal //.
exact: usubmx0.
Qed.
Definition usubsignal (x : vsignal (n1+n2)) :=
  Build_signal (usubsignal_causal x).


Fact dsubsignal_causal (x : vsignal (n1+n2)) :
  causal (fun k => dsubmx (x k)).
Proof.
move => k _Hk.
rewrite signal_causal //.
exact: dsubmx0.
Qed.
Definition dsubsignal (x : vsignal (n1+n2)) :=
  Build_signal (dsubsignal_causal x).


End Submx.






(** from scalar signal to vector signal of size 1 and back **)


Fact vsignal_of_sc_causal (x : scsignal) :
  causal (fun k => (x k)%:M1[RT]).
Proof.
intros k _Hk.
by rewrite signal_causal //.
Qed.
Definition vsignal_of_sc x : vsignal 1 :=
  Build_signal (vsignal_of_sc_causal x).


Fact scsignal_of_v_causal (x : vsignal 1) :
  causal (fun k => (x k)%:sc : (regular_vectType RT)).
Proof.
move => k _Hk.
rewrite signal_causal //.
by rewrite mxE.
Qed.
Definition scsignal_of_v x : scsignal :=
  Build_signal (scsignal_of_v_causal x).




(** Decomposition of a vector signal as scalar signals **)
Section Decomp_vsignal.


Context {n : nat}.


Fact ith_scsignal_causal (x : vsignal n) (i : 'I_n) :
  causal (fun k => (x k) _[i]ord : regular_vectType RT).
Proof.
move => k _Hk.
rewrite signal_causal //.
exact: mxE.
Qed.
Definition ith_scsignal (x : vsignal n) (i : 'I_n) : scsignal :=
  Build_signal (ith_scsignal_causal x i).


Lemma ith_scsignal_applied (x : vsignal n) i (k : int) :
  ith_scsignal x i k = x k _[i]ord.
Proof.
by [].
Qed.


Lemma vsignal_ext_ith_scsignal (x1 x2 : vsignal n) :
  (forall i : 'I_n, ith_scsignal x1 i = ith_scsignal x2 i) ->
  x1 = x2.
Proof.
move => H.
apply/signalP => k.
apply/veP => i.
by rewrite -!ith_scsignal_applied // H.
Qed.


Fact vsignal_of_n_sc_causal (xs : 'I_n -> scsignal) :
  causal (fun k => \matrix_(i, _) xs i k : 'cV[RT]_n).
Proof.
move => k _Hk.
apply/matrixP => i j.
by rewrite !mxE signal_causal.
Qed.
Definition vsignal_of_n_sc (xs : 'I_n -> scsignal) : vsignal n :=
  Build_signal (vsignal_of_n_sc_causal xs).


Lemma vsignal_of_n_sc_compat_add (xs1 xs2 : 'I_n -> scsignal) :
  vsignal_of_n_sc (fun m => xs1 m + xs2 m) =
  vsignal_of_n_sc xs1 + vsignal_of_n_sc xs2.
Proof.
apply/signalP => k.
apply/veP => i'.
by rewrite !mxE.
Qed.


Definition vsignal_of_sc_at_i
             (x : scsignal) (i : 'I_n) : vsignal n :=
  vsignal_of_n_sc (fun j => if j == i then x else signal0).


Lemma vsignal_of_sc_at_i_compat_add
        (x1 x2 : scsignal) (i : 'I_n) :
  vsignal_of_sc_at_i (x1 + x2) i =
  vsignal_of_sc_at_i x1 i + vsignal_of_sc_at_i x2 i.
Proof.
apply/signalP => k.
apply/veP => i'.
rewrite !mxE.
case: (i' == i).
  by [].
by simplr.
Qed.


Lemma vsignal_of_sc_at_i_compat_scale
        (c : RT) (x : scsignal) (i : 'I_n) :
  vsignal_of_sc_at_i (c *: x) i = c *: vsignal_of_sc_at_i x i.
Proof.
apply/signalP => k.
apply/veP => i'.
rewrite !mxE.
case: (i' == i).
  by [].
by simplr.
Qed.


Lemma vsignal_of_sc_at_i_compat_delay
        (K : int) (x : scsignal) (i : 'I_n) (K_ge0 : K >= 0) :
  vsignal_of_sc_at_i (delay K x) i = delay K (vsignal_of_sc_at_i x i).
Proof.
apply/signalP => k.
rewrite !delayE //.
apply/veP => i'.
rewrite !mxE.
case: (i' == i).
  by rewrite delayE.
by [].
Qed.


Lemma vsignal_of_sc_at_i_applied_index_i
        (x : scsignal) (i : 'I_n) (k : int) :
  (vsignal_of_sc_at_i x i k) _[i]ord = x k.
Proof.
by rewrite mxE ifT.
Qed.


Lemma vsignal_of_sc_at_i_applied_index_neq
        (x : scsignal) (i : 'I_n) (k : int) (i' : 'I_n) :
  i' <> i -> (vsignal_of_sc_at_i x i k) _[i']ord = 0.
Proof.
move => neq.
rewrite mxE ifF //.
by apply/eqP.
Qed.


Lemma decomp_vsignal (x : vsignal n) :
  x = \sum_(i < n) @vsignal_of_sc_at_i (ith_scsignal x i) i.
Proof.
apply/signalP => k; apply/veP => i.
rewrite sumsE summxE (big_ord_singlenonid i).
  by rewrite mxE ifT.
move => i' _Hneq.
rewrite mxE ifF //.
apply/eqP.
by auto.
Qed.


End Decomp_vsignal.




(** convolution of matrix signal and vector signal **)


Fact convol_mx_ve_causal {m n : nat}
       (As : msignal m n) (x : vsignal n) :
  causal (fun k => \sum_(i < (k+1)%int) As i *m x (k-(i:int))).
Proof.
move => k _Hk.
rewrite nat_of_int_eq0.
apply: big_ord0.
by intlia.
Qed.
Definition convol_mx_ve {m n : nat}
             (As : msignal m n) (x : vsignal n) : vsignal m :=
  Build_signal (convol_mx_ve_causal As x).


Lemma convol_mx_ve_sum {h w n : nat}
        (As : msignal h w) (xs : 'I_n -> vsignal w) :
  convol_mx_ve As (\sum_(i < n) xs i) =
  \sum_(i < n) convol_mx_ve As (xs i).
Proof.
apply/signalP => k.
rewrite sumsE exchange_big /=.
apply: eq_big_ord => i.
rewrite sumsE.
exact: mulmx_sumr.
Qed.


Lemma sum_msignalE {n h w} (xs : 'I_n -> msignal h w) (k : int) i j :
  (\sum_(m < n) xs m) k i j = \sum_(m < n) (xs m k i j).
Proof.
induction n.
  by rewrite !big_ord0 mxE.
rewrite !big_ord_recl mxE.
by f_equal.
Qed.


End Context_RT.

