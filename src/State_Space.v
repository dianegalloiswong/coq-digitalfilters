(**
This file is part of the DigitalFilters library
Copyright (C) 2017-2021 Gallois-Wong, Boldo, Hilaire

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
**)



From mathcomp
Require Import all_ssreflect ssralg vector ssrint ssrnum matrix.

Require Import lia_tactics
               utils nat_ord_complements int_complements sum_complements
               mx_complements
               signal filter TF_coefficients.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Import GRing.Theory.


Local Open Scope R.


(** The MIMO filter corresponding to a State Space (A, B, C, D)
    is defined by the input-output relationship:
      x(k+1) = Ax(k) + Bu(k)
        y(k) = Cx(k) + Du(k)
    where x is the signal of state vectors.
    If x has size nx >= 0 then A has size nx*nx, B has size nx*nu,
                               C has size ny*nx, D has size ny*nu.
    An initial state vector x0 is also provided for x(0), although
    is will simply be the null vector most of the time.            **)
Section StateSpace.


Context {RT : comRingType} {nu ny : nat}.

Context {nx : nat}.

Record StateSpace := { StSp_A : 'M[RT]_(nx)     ;
                       StSp_B : 'M[RT]_(nx, nu) ;
                       StSp_C : 'M[RT]_(ny, nx) ;
                       StSp_D : 'M[RT]_(ny, nu) ;
                     }.



(** Given a State-Space stsp, we want to build
    the corresponding filter. **)
Variable stsp : StateSpace.

Notation A := (StSp_A stsp).
Notation B := (StSp_B stsp).
Notation C := (StSp_C stsp).
Notation D := (StSp_D stsp).




Section Build_output.



(** Input signal u of size nu.
    We want to build the corresponding output y
    through the filter of State-Space stsp. **)
Variable u : @vsignal RT nu.


(** Signal of state vectors x of size nx,
    which appears in the construction of y
    by a MIMO filter of StateSpace stsp.                 **)
Definition x : vsignal nx := signal_peanorec
  (fun k (xk : 'cV_nx) => (A *m xk) + (B *m u k)).


(** Output signal y, of size ny, corresponding to the input u **)
Fact y_causal : causal (fun k => C *m x k + D *m u k).
Proof.
intros k _Hk.
rewrite !signal_causal => //.
by simplmx.
Qed.
Definition y := Build_signal y_causal.




(** additional properties of x and y **)


Lemma x_kle0 k : k <= 0 -> x k = 0.
Proof.
exact: peanoreczBC.
Qed.


(** We do not need the assumption k >= 0 here, as everything is null when k < 0. **)
Lemma x_Sk k : x (k + 1) = (A *m x k) + (B *m u k).
Proof.
case: k => n.
- exact: peanoreczIS.
- rewrite !x_kle0 //; last first.
    by rewrite NegzE; intlia.
  rewrite signal_causal //.
  by simplmx.
Qed.


(** We often want exactly this depth of unfolding. **)
Lemma y_k k : y k = C *m x k + D *m u k.
Proof.
by [].
Qed.


End Build_output.


(** Filter corresponding to a State-Space **)
Definition filter_from_StSp : filter := y.





(** We want to prove that the filter corresponding
    to a State-Space is Linear Time-Invariant. **)
Section filter_from_StSp_is_LTI.



Lemma x_compat_add u1 u2 k : x (u1 + u2) k = x u1 k + x u2 k.
Proof.
move: k; apply: peanoindz => k _Hk.
- rewrite !x_kle0 //. by simplmx.
- intro Hind; rewrite !x_Sk Hind.
  rewrite !mulmxDr -!addrA; f_equal.
  rewrite !addrA; f_equal.
  exact: addrC.
Qed.



Lemma filter_from_StSp_compat_add :
  filter_compat_add filter_from_StSp.
Proof.
move => u1 u2. apply/signalP => k.
rewrite [in RHS]signalE 3!y_k x_compat_add.
rewrite !mulmxDr -!addrA; f_equal.
rewrite !addrA; f_equal.
exact: addrC.
Qed.


Lemma x_compat_scale c u k : x (c *: u) k = c *: x u k.
Proof.
move: k; apply: peanoindz => k _Hk.
- rewrite !x_kle0 //.
  by rewrite scaler0.
- intro Hind; rewrite !x_Sk Hind.
  by rewrite scalerDr -2!scalemxAr.
Qed.


Lemma filter_from_StSp_compat_scale :
  filter_compat_scale filter_from_StSp.
Proof.
move => c u. apply/signalP => k.
rewrite [in RHS]signalE 2!y_k x_compat_scale.
by rewrite scalerDr -2!scalemxAr.
Qed.


Lemma x_compat_delay K u k :
  (K >= 0) -> x (delay K u) k = x u (k - K).
Proof.
move => _HK.
move: k; apply: peanoindz => k _Hk.
- rewrite !x_kle0 //.
  by intlia.
- intro Hind; rewrite !x_Sk Hind delayE //.
  rewrite -x_Sk.
  f_equal.
  by ringz.
Qed.


Lemma filter_from_StSp_compat_delay :
  filter_compat_delay filter_from_StSp.
Proof.
move => K u _HK. apply/signalP => k.
rewrite delayE // 2!y_k delayE //.
by rewrite x_compat_delay.
Qed.


Theorem filter_from_StSp_is_LTI :
  LTI_filter filter_from_StSp.
Proof.
repeat split.
- exact: filter_from_StSp_compat_add.
- exact: filter_from_StSp_compat_scale.
- exact: filter_from_StSp_compat_delay.
Qed.


End filter_from_StSp_is_LTI.



End StateSpace.






Notation SISO_StateSpace := (fun RT nx => @StateSpace RT 1 1 nx).

Section SISO_ImpulseResponse.


Context {RT : comRingType} {nx : nat}.


Variable stsp : @SISO_StateSpace RT nx.

Notation A := (StSp_A stsp).
Notation B := (StSp_B stsp).
Notation C := (StSp_C stsp).
Notation D := (StSp_D stsp).



(** On the input u = dirac, the signal x
    from the State Space equations verifies:
      x k = A^(k-1) B for k >= 1
**)
Lemma x_of_dirac : forall k : int, 0 < k ->
  x stsp (vsignal_of_sc dirac) k = (expmx A (k - 1)%Z) *m B.
Proof.
apply: (peanoindz_basis 1).
  move => k k_le1 k_gt0.
  replace k with (1 : int) by intlia.
  rewrite /= mx11_1.
  by simplmx.
move => k k_ge1 IH _.
rewrite x_Sk IH; last by intlia.
rewrite mulmxA -expmxSl.
rewrite signalE diracNot0; last by intlia.
simplmx.
repeat f_equal.
by autonz.
Qed.


(** The impulse response of the SISO filter corresponding to
    the SISO State-Space (A, B, C, D) is:
              D           if k = 0 (coercing the 1x1 matrix into a scalar)
    fun k =>  C A^(k-1) B if k > 0 (same coercion)
              0           if k < 0 (since it is a signal).
**)
Theorem SISO_StateSpace_impulse_response k :
  impulse_response (to_SISO (filter_from_StSp stsp)) k =
  (match k with
    | Posz 0     => D
    | Posz (S _) => C *m expmx A (k-1) *m B
    | Negz _     => 0
  end)%:sc.
Proof.
case: k; first case.
- rewrite /= mx11_1.
  by simplmx.
- move=> n.
  rewrite 2!signalE.
  rewrite x_of_dirac //.
  rewrite signalE diracNot0 //.
  simplmx.
  by rewrite mulmxA.
- by intro; rewrite signal_causal // mxE.
Qed.


End SISO_ImpulseResponse.





Section MIMO_ImpulseResponse.


Context {RT : comRingType} {nu ny nx : nat}.


Variable stsp : @StateSpace RT nu ny nx.


Notation A := (StSp_A stsp).
Notation B := (StSp_B stsp).
Notation C := (StSp_C stsp).
Notation D := (StSp_D stsp).



(** On an input signal u such that forall k > 0, u(k) = 0,
    the signal x from the State Space equations verifies:
      x k = A^(k-1) B u(0) for k > 0
    Note that signals which correspond to a dirac signal
    for one index and are null for other indexes are such inputs.
**)
Lemma x_of_diraclike (u : vsignal _) (Hu : forall k : int, k > 0 -> u k = 0) :
  forall k : int, 0 < k -> x stsp u k = (expmx A (k - 1)%Z) *m B *m u 0.
Proof.
apply: (peanoindz_basis 1).
{ (** k <= 1 **)
  move => k _Hk _k_gt0. (have/eqP: (k==1) by intlia) => ->.
  replace (GRing.one (Num.NumDomain.ringType _))
    with (0+(1:int)) at 1 by apply: intZmod.add0z.
  rewrite x_Sk x_kle0 // mulmx0 add0r.
  f_equal. by rewrite subrr /= mul1mx. }
{ (** k -> k + 1 for k >= 1 **)
  move => k _Hk Hind _. rewrite x_Sk Hu // mulmx0 addr0.
  rewrite Hind // !mulmxA -expmxSl.
  f_equal; f_equal; f_equal. rewrite -addn1 S_nat_of_int.
  f_equal; ringz. intlia. }
Qed.


(** On an input signal u such that forall k > 0, u(k) = 0,
    the output signal y in k is equal to:
      D u(0)             if k = 0
      C A^(k-1) B u(0)   if k > 0
      0                  if k < 0 (since it is a signal).
**)
Lemma y_of_diraclike (u : vsignal _)
                     (Hu : forall k : int, k > 0 -> u k = 0) (k : int) :
  filter_from_StSp stsp u k = match k with
                                | Posz 0     => D *m u 0
                                | Posz (S _) =>
                                    C *m expmx A (k-1) *m B *m u 0
                                | Negz _     => 0
                              end.
Proof.
case: k; first case.
- by rewrite /= mulmx0 add0r.
- move => k. rewrite signalE Hu // mulmx0 addr0.
  by rewrite x_of_diraclike // !mulmxA.
- intro; by rewrite signal_causal.
Qed.



(** The matrix of impulse responses of a filter given by State Space (A, B, C, D) in k is
      D           if k = 0
      C A^(k-1) B if k > 0
      0           if k < 0 (since it is a signal).
**)
Theorem StateSpace_impulse_response k :
  MIMO_impulse_response (filter_from_StSp stsp) k =
  match k with
    | Posz 0     => D
    | Posz (S _) => C *m expmx A (k-1) *m B
    | Negz _     => 0
  end.
Proof.
apply/matrixP => i j.
rewrite mxE ith_scsignal_applied y_of_diraclike.
2:{ intros. apply/veP => i'. rewrite !mxE.
    case: ifP => //. rewrite diracNot0 //; intlia. }
case: k; first case. 2: move => k.
1,2: rewrite mxE (sum_ord_singlenon0 j);
     [ rewrite vsignal_of_sc_at_i_applied_index_i dirac0 mulr1 //
     | intros; by rewrite vsignal_of_sc_at_i_applied_index_neq // mulr0].
intros; by rewrite !mxE.
Qed.



End MIMO_ImpulseResponse.






Section From_TFC.


Context {RT : comRingType}.


Variable tfc : @TFCoeffs RT.

Notation n := (TFC_n tfc).
Notation a := (TFC_a tfc).
Notation b := (TFC_b tfc).



(** Build a state space from transfer function coefficients,
    so that the filter corresponding to this state space is
    the same as the filter obtained directly from the coefficients.

Let a1, a2, ... , an, b0, b1, ..., bn transfer function coefficients of order n.
Using DFII, the corresponding filter is defined by the equations:

    e(k) = u(k) - sum_{1<=i<=n} ai e(k-i)
    y(k) = sum_{0<=i<=n} bi e(k-i)

where e is an intermediary signal.

We use the state vector x(k) to store previous values of e:

    x(k) = ( e(k-n)         of (StSp_n stsp) := n
             e(k-n+1)
             ...
             e(k-1) )

i.e. x(k)_i     = e(k-n+i)    for 0 <= i < n

From
    x(k+1) = A x(k) + B u(k)
we get
    forall i, 0 <= i < n ->
         e(k+1-n+i) = ( sum_{0<=j<n} A_(i,j) e(k-n+j) ) + B_(i,0) u(k)

For i < n-1, the only nonzero term should be A(i,j) for j such that k+1-n+i = k-n+j, so j = i+1
For i = n-1, we get
    B_(n-1,0) u(k) + ( sum_{0<=j<n} A_(n-1,j) e(k-n+j) )
      = e(k)
      = u(k) - sum_{1<=i<=n} ai e(k-i)
      = u(k) - sum_{0<=i<n} a(n-i) e(k-n+i)

So:

A = (0 1 0 ... 0             of size n x n
     0 0 1 ... 0
         ...
     0 ....... 1
    -an ..... -a1)

B = (0                       of size n x 1
    ...
     0
     1)


From
    y(k) = C x(k) + D u(k)
we get
    sum_{0<=i<=n} bi e(k-i) = ( sum_{0<=i<n} C_(0,i) e(k-n+i) ) + D_(0,0) u(k)
so
    D_(0,0) u(k) + sum_{0<=i<n} C_(0,i) e(k-n+i)
     = sum_{0<=i<=n} b(n-i) e(k-n+i)
     = b0 e(k) + sum_{0<=i<n} b(n-i) e(k-n+i)
     = b0 ( u(k) - sum_{1<=i<=n} ai e(k-i) ) + sum_{0<=i<n} b(n-i) e(k-n+i)
     = b0 u(k) - sum_{0<=i<n} b0*a(n-i) e(k-n+i) + sum_{0<=i<n} b(n-i) e(k-n+i)

so:

C = (bn-an*b0 ... b1-a1*b0)      of size 1 x n

D = (b0)                         of size 1 x 1

**)



Definition StSp_from_TFC : StateSpace := @Build_StateSpace RT 1 1 n
(* StSp_A *) (\matrix_(i,j) (if (i:nat) == (n-1)%nat then - a (n-j)
                               else if (i+1)%nat == j then 1 else 0))
(* StSp_B *) (\col_i (if (i:nat) == (n-1)%nat then 1 else 0))
(* StSp_C *) (\row_j (b (n-j) - a (n-j) * b 0))
(* StSp_D *) (b 0)%:M1.



Section Var_u.


Variable u : @scsignal RT.
Notation e := (DFII_u2e tfc u).
Notation x := (x StSp_from_TFC (vsignal_of_sc u)).


Lemma x_stores_n_previous_e (k : int) (i : nat) :
  (i < n)%nat -> x k _[i] = e (k-(n:int)+i).
Proof.
move: k i; apply: strongindz => k _Hk.
  move => i _Hi; rewrite !signal_causal //.
    by dvlp_fmxn.
  by autoz.
move => Hind i _Hi.
replace k with (k-1+1)%Z at 1 by ringz.
rewrite x_Sk.
dvlp_fmxn.
case Hcase: (i == n-1)%nat; move: Hcase => /eqP Hcase.
- (** case i = n-1 **)
  rewrite mul1r Hcase e_val addrC.
  f_equal.
    by f_equal; autoz.
  rewrite -[in RHS]mulN1r -scaler_mulr scaler_sumr
    [in RHS]sum_nat_rev [in RHS](sum_nat_shiftDown 1) //.
  apply: eq_big_nat_eqbounds; try auton => j _Hj.
  rewrite !scaler_mulr mulrA.
  f_equal.
  + dvlp_fmxn.
    rewrite ifT // mulN1r.
    by f_equal; f_equal; auton.
  + rewrite Hind //; last intlia.
    by f_equal; autoz.
- (** case i < n-1 **)
  simplmx.
  rewrite (sum_nat_singlenon0 (i+1)); first 1 last.
  + by auton.
  + move => j _Hj _Hneq.
    dvlp_fmxn.
    rewrite ifF ?ifF; try by auton.
    exact: mul0r.
  + dvlp_fmxn.
    rewrite ifF; last by auton.
    rewrite ifT // mul1r Hind //; last by intlia.
    f_equal.
    by autoz.
Qed.


Theorem StSp_from_TFC_correct_MIMO k :
  filter_from_StSp StSp_from_TFC (vsignal_of_sc u) k =
  (filter_from_TFC tfc u k)%:M1.
Proof.
rewrite DFI_DFII_same_filter signalE /filter_by_DFII /DFII_e2y signalE.
apply/mx11Pnat.
dvlp_fmxn.
rewrite big_nat_recl //.
erewrite eq_big_nat; last first.
  by intros; rewrite fmxn_matrix // !nat_of_Ordinal mulrBl.
rewrite sum_nat_sub -addrA [in RHS]addrC.
f_equal.
- rewrite [in RHS]sum_nat_rev.
  apply: eq_big_nat => i _Hi.
  rewrite scaler_mulr; f_equal.
    by f_equal; auton.
  rewrite x_stores_n_previous_e //.
  by f_equal; autoz.
- rewrite e_val addrC scalerBr scaler_mulr scaler_sumr.
  f_equal.
    by rewrite subr0.
  f_equal.
  rewrite [in RHS]sum_nat_rev [in RHS](sum_nat_shiftDown 1) //.
  apply: eq_sum_nat_eqbounds.
  - by [].
  - by ssrnatlia.
  move => i _Hi; rewrite scaler_mulr mulrA.
  f_equal.
  + rewrite mulrC.
    by f_equal; f_equal; auton.
  + rewrite x_stores_n_previous_e //.
    by f_equal; autoz.
Qed.


End Var_u.



Theorem StSp_from_TFC_correct :
  to_SISO (filter_from_StSp StSp_from_TFC) = filter_from_TFC tfc.
Proof.
apply: filter_ext => u k.
rewrite /to_SISO /scsignal_of_v signalE.
by rewrite StSp_from_TFC_correct_MIMO // mxE.
Qed.


End From_TFC.

















(** For any State-Space (A, B, C, D) and invertible matrix T,
    the State-Space (T^(-1) A T, T^(-1) B, C T, D) describes
    the same filter as (A, B, C, D).
**)
Section StSp_change_base.


Context {RT : comUnitRingType} {nu ny nx : nat}.


Variable stsp : @StateSpace RT nu ny nx.


Notation A := (StSp_A stsp).
Notation B := (StSp_B stsp).
Notation C := (StSp_C stsp).
Notation D := (StSp_D stsp).


Variable T : 'M[RT]_nx.
Hypothesis unitmx_T : T \in unitmx.


Definition StSp_change_base :=
  Build_StateSpace (invmx T *m A *m T) (invmx T *m B) (C *m T) D.


Lemma StSp_change_base_x_k (u : signal) k :
  x StSp_change_base u k = invmx T *m x stsp u k.
Proof.
revert k; apply peanoindz.
  intros; by rewrite /= 2?peanoreczBC // mulmx0.
intros k _Hk Hind; rewrite 2!x_Sk Hind.
simpl (StSp_A _); simpl (StSp_B _).
by rewrite -!mulmxA (mulmxA T) mulmxV ?unitmx_T // mul1mx mulmxDr.
Qed.


Theorem StSp_change_base_same_filter :
  filter_from_StSp StSp_change_base = filter_from_StSp stsp.
Proof.
apply: filter_ext => u k.
rewrite /filter_from_StSp !y_k.
simpl (StSp_C _); simpl (StSp_D _).
by rewrite StSp_change_base_x_k -!mulmxA (mulmxA T)
           mulmxV ?unitmx_T // mul1mx.
Qed.


End StSp_change_base.





(** We note M^T the transpose of the matrix M.
    A SISO State-Space (A, B, C, D) characterizes the same
    filter as the State-Space (A^T, C^T, B^T, D^T).
    (Note that D^T = D since D has size 1x1 for a SISO State-Space.)
**)
Section StSp_transpose.


Context {RT : comRingType} {nx : nat}.


Variable stsp : @StateSpace RT 1 1 nx.


Notation A := (StSp_A stsp).
Notation B := (StSp_B stsp).
Notation C := (StSp_C stsp).
Notation D := (StSp_D stsp).


Definition StSp_transpose :=
  Build_StateSpace A^T C^T B^T D^T.


Lemma StSp_transpose_x_prop (u : signal) k M :
  A *m M = M *m A ->
  B^T *m M^T *m x StSp_transpose u k = C *m M *m x stsp u k.
Proof.
move: M; apply peanoindz with (k := k); clear k => k.
  intros; by rewrite /= 2?peanoreczBC // 2!mulmx0.
intros _Hk Hind M H_comm; rewrite 2!x_Sk.
simpl (StSp_A _); simpl (StSp_B _).
rewrite 2!mulmxDr; f_equal.
- rewrite [in LHS]mulmxA -[X in X *m _]mulmxA -trmx_mul.
  rewrite Hind.
  by rewrite H_comm !mulmxA.
  by rewrite [in LHS]H_comm mulmxA.
- rewrite !mulmxA; f_equal.
  rewrite -!trmx_mul mulmxA.
  exact: trmx11.
Qed.


Theorem StSp_transpose_same_filter :
  filter_from_StSp StSp_transpose = filter_from_StSp stsp.
Proof.
apply: filter_ext => u k.
rewrite /filter_from_StSp !y_k.
simpl (StSp_C _); simpl (StSp_D _).
rewrite trmx11 -(mulmx1 B^T) -trmx1.
by rewrite StSp_transpose_x_prop mulmx1 ?mul1mx.
Qed.


End StSp_transpose.

