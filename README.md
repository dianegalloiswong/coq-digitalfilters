# DigitalFilters Coq library

## Introduction

The DigitalFilters library formalizes linear time-invariant (LTI) digital filters, with a focus on rounding error analysis in finite-precision implementations. This library was developped during Diane Gallois-Wong's PhD thesis *Formalisation en Coq des algorithmes de filtres numériques calculés en précision finie*, under the supervision of Sylvie Boldo and Thibault Hilaire, at Université Paris-Saclay -- LRI/LMF -- Inria Saclay. This work was supported by a grant from the "Fondation CFM pour la Recherche". For more information, see the [manuscript (in French)](https://tel.archives-ouvertes.fr/tel-03202580).

## Compilation

Install the dependencies and run:

    coq_makefile -f _CoqProject -o Makefile
    make

## Dependencies

| Opam package           | Version |
|------------------------|-------- |
| coq                    | 8.12.2  |
| coq-coquelicot         | 3.2.0   |
| coq-flocq              | 3.4.0   |
| coq-mathcomp-algebra   | 1.12.0  |
| coq-mathcomp-fingroup  | 1.12.0  |
| coq-mathcomp-ssreflect | 1.12.0  |

## Organisation

A graph describing the organization of this library is given in file_dependencies.png. It is color-coded to match the chapters of the thesis manuscript.

Directories:
- `src`: main Coq files related to:
  + definition and common properties of signals and filters (chapter 2)
  + State-Space and SIF realizations (chapter 3)
  + rounding error analysis (chapter 4)
- `src/SOP`: main Coq files related to Sum-of-Products (chapter 5)
- `src/aux`: auxiliary and external Coq files
